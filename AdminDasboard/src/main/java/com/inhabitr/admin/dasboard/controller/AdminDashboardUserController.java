package com.inhabitr.admin.dasboard.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;


import javax.persistence.Transient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.inhabitr.admin.dasboard.model.AdminDashboardUserEntity;
import com.inhabitr.admin.dasboard.services.ItemService;

@RestController
@CrossOrigin
@RequestMapping("/apps/api/user/")
public class AdminDashboardUserController {
	@Autowired
	private ItemService service;
	
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	public static final String imageURL="https://d12kqwzvfrkt5o.cloudfront.net/products/";
	public Logger getLogger() {
		return logger;
	}

/*	@PostMapping("signup/")
	public List<AdminDashboardUserEntity> getItemBytype(@RequestParam() String productType,
			@RequestParam(defaultValue = "") Long category_sgid, @RequestParam(defaultValue = "") Boolean isAvailable,
			@RequestParam(defaultValue = "") String searchStr, @RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(defaultValue = "asc") String sortDir,
			@RequestParam(defaultValue = "display_name") String sortBy) {  try {
		    	PropertyUserData addUser = this.propertyService.signup(propertyUser);
		        if (addUser != null)
		            return Response.status(Status.CREATED).entity(addUser).build();
		    } catch (PreRegisteredUserException pe) {
		        getLogger().error(pe.getMessage());//
		        String message = "{\"message\":\"Email address is already registered. Please try logging in \"}";
		    	return Response.ok(message).build();
		        //return Response.status(Status.EXPECTATION_FAILED).entity(pe.getOriginalExceptionMessage()).build();
		    } catch (InhabitrException e) {
		    	String message = "{\"message\":\"Unable to register Restaurent with this User \"}";
		    	return Response.ok(message).build();
		    }
		    return Response.status(Status.EXPECTATION_FAILED).entity(FAILED).build();

	}*/

}
