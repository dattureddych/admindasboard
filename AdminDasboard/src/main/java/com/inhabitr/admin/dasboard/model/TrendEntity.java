package com.inhabitr.admin.dasboard.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



/**
 * @author Abhijit Patil
 * @param <T>
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type")
@Table(name = "trend")
public abstract class TrendEntity<T extends TrendableEntity>  {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;

    private @Column(name = "score") Double score = 50.00;
    private @Column(name = "type", insertable = false, updatable = false) String type;
    private @Column(name = "sub_type") String subType;
    private @Column(name = "last_activity_type") String lastActivityType;
    private @Column(name = "state_sgid")Long stateSgid;
    private @Column(name = "city_sgid")Long citySgid;
    private @Column(name = "last_activity_time")
    @Temporal(TemporalType.TIMESTAMP) Date lastActivityTime;
    private @Column(name = "accessed_datetime")
    @Temporal(TemporalType.TIMESTAMP) Date accessedTime;


    @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "brew_owner_sgid")
    private BrewerEntity owner;
    

    public Long getCitySgid() {
		return citySgid;
	}

	public void setCitySgid(Long citySgid) {
		this.citySgid = citySgid;
	}

	public Long getStateSgid() {
		return stateSgid;
	}

	public void setStateSgid(Long stateSgid) {
		this.stateSgid = stateSgid;
	}

	public void setType(String type) {
		this.type = type;
	}


    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getType() {
        return type;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    

    public String getLastActivityType() {
        return lastActivityType;
    }

    public void setLastActivityType(String lastActivityType) {
        this.lastActivityType = lastActivityType;
    }

    public Date getLastActivityTime() {
        return lastActivityTime;
    }

    public void setLastActivityTime(Date lastActivityTime) {
        this.lastActivityTime = lastActivityTime;
    }

   
    @Override
    public String toString() {
        return "Trend-" + String.valueOf(this.sgid);
    }

	
}
