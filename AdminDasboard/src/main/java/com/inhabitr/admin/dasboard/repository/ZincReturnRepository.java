package com.inhabitr.admin.dasboard.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.ZincReturnEntity;

@Repository
public interface ZincReturnRepository extends PagingAndSortingRepository<ZincReturnEntity, Long> {
	Page<ZincReturnEntity> findAll(Pageable paging);	
	@Query(value ="select * from zinc_return z "
			  + " inner join product p on p.saffron_item_sgid=z.item_sgid "
			  + " inner join customer_service_requests sr on z.reference=sr.order_id and z.item_sgid=sr.item_sgid "
			  + " inner join category c on c.sgid=p.category_id "
			  + " where UPPER(z.status) like %:status% and (UPPER(p.name) like %:searchStr% "
			  + " or UPPER(z.reference) like %:searchStr% "
			  + " or UPPER(c.name) like %:searchStr% or (sr.service_reference_id) like %:searchStr% )",
			  nativeQuery = true) 
	Page<ZincReturnEntity> findByFilters(String status,String searchStr, Pageable paging);

	@Query(value ="select * from zinc_return z "
			  + " inner join product p on p.saffron_item_sgid=z.item_sgid "
			  + " inner join customer_service_requests sr on z.reference=sr.order_id and z.item_sgid=sr.item_sgid "
			  + " inner join category c on c.sgid=p.category_id "
			  + " where z.label_link!=null and UPPER(z.status) like %PENDING% and (UPPER(p.name) like %:searchStr% "
			  + " or UPPER(z.reference) like %:searchStr% "
			  + " or UPPER(c.name) like %:searchStr% or (sr.service_reference_id) like %:searchStr% )",
			  nativeQuery = true) 
	Page<ZincReturnEntity> findByFilters1(String searchStr, Pageable paging);

	
	 @Query(value ="select * from zinc_return z "
			  + " inner join product p on p.saffron_item_sgid=z.item_sgid "
			  + " inner join customer_service_requests sr on z.reference=sr.order_id and z.item_sgid=sr.item_sgid "
			  + " inner join category c on c.sgid=p.category_id "
			  + " where (UPPER(p.name) like %:searchStr% "
			  + " or UPPER(z.reference) like %:searchStr% "
			  + " or UPPER(c.name) like %:searchStr% or (sr.service_reference_id) like %:searchStr% ) ",
			  nativeQuery = true) 
	Page<ZincReturnEntity> findByFilters(String searchStr, Pageable paging);

	 @Query(value ="select * from zinc_return z "
			  + " inner join product p on p.saffron_item_sgid=z.item_sgid "
			  + " inner join customer_service_requests sr on z.reference=sr.order_id and z.item_sgid=sr.item_sgid "
			  + " inner join category c on c.sgid=p.category_id ",
			  nativeQuery = true) 
	Page<ZincReturnEntity> findByFilters(Pageable paging);
	 
	@Query(value ="select * from zinc_return z "
			  + " inner join product p on p.saffron_item_sgid=z.item_sgid "
			  + " inner join customer_service_requests sr on z.reference=sr.order_id and z.item_sgid=sr.item_sgid "
			  + " inner join category c on c.sgid=p.category_id where UPPER(z.status) like %:status%",
			  nativeQuery = true)  
	Page<ZincReturnEntity> findByStatus(String status,Pageable paging);
	
	@Query(value ="select * from zinc_return z "
			  + " inner join product p on p.saffron_item_sgid=z.item_sgid "
			  + " inner join customer_service_requests sr on z.reference=sr.order_id and z.item_sgid=sr.item_sgid "
			  + " inner join category c on c.sgid=p.category_id where z.label_link!=null and UPPER(z.status) like %PENDING%",
			  nativeQuery = true)  
	Page<ZincReturnEntity> findByStatus1(Pageable paging);
	 
	List<ZincReturnEntity> findBySgid(Long sgid);
	
}
