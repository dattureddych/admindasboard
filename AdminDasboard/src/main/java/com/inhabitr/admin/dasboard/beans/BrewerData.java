package com.inhabitr.admin.dasboard.beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Dattu Reddy
 */
public class BrewerData  {
    private String firstName;
    private String lastName;
    private String description;
    private String trendBrewId;
    private String promotionCode;
    private Date dateOfBirth;
    private Date createdDate;
    private String loginEmail;
    private String password;
    private String authKey;
    
    private String socialNetworkUserId;
    private String socialNetworkAccessToken;
    private String guestToken;
    private String rewardAuthToken;
    private String rewardStatus; 
    private String address;
    private String city;
    private String state; 
    private String country;
    private String zipcode;

    private Long pointBalance;
    private Long cloverPointBalance;
    private Long trendStreetId;
   
    private Map<String, Long> brewStats;
    private Map<String, Long> networkStats;
    private Integer numWishes;
    private Integer numCloverWishes;
    private Integer numOffers;
    private Integer numCloverOffers;

    private String emailAdderss;
    private String mobileNumber;
    private String homePhoneNumber;
    private String businessPhoneNumber;

   
    private Long notificationCount;
    private Boolean isGuestUser = Boolean.FALSE;
    private Boolean loyaltyFlag = Boolean.FALSE;
    private Long loyaltySgid;
    private BigDecimal rewardTotalBalance;
    
    public String getRewardStatus() {
		return rewardStatus;
	}

	public void setRewardStatus(String rewardStatus) {
		this.rewardStatus = rewardStatus;
	}

	public String getRewardAuthToken() {
		return rewardAuthToken;
	}

	public void setRewardAuthToken(String rewardAuthToken) {
		this.rewardAuthToken = rewardAuthToken;
	}

	public Boolean getLoyaltyFlag() {
		return loyaltyFlag;
	}

	public void setLoyaltyFlag(Boolean loyaltyFlag) {
		this.loyaltyFlag = loyaltyFlag;
	}

	public Long getLoyaltySgid() {
		return loyaltySgid;
	}

	public void setLoyaltySgid(Long loyaltySgid) {
		this.loyaltySgid = loyaltySgid;
	}
	public Boolean getIsGuestUser() {
		return isGuestUser;
	}

	public void setIsGuestUser(Boolean isGuestUser) {
		this.isGuestUser = isGuestUser;
	}

	public String getGuestToken() {
		return guestToken;
	}

	public void setGuestToken(String guestToken) {
		this.guestToken = guestToken;
	}

	public void setNetworkStats(Map<String, Long> networkStats) {
		this.networkStats = networkStats;
	}

	public Integer getNumCloverWishes() {
		return numCloverWishes;
	}

	public void setNumCloverWishes(Integer numCloverWishes) {
		this.numCloverWishes = numCloverWishes;
	}

	public Integer getNumCloverOffers() {
		return numCloverOffers;
	}

	public void setNumCloverOffers(Integer numCloverOffers) {
		this.numCloverOffers = numCloverOffers;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getRewardTotalBalance() {
		return rewardTotalBalance;
	}

	public void setRewardTotalBalance(BigDecimal rewardTotalBalance) {
		this.rewardTotalBalance = rewardTotalBalance;
	}

	//    public String getFacebookURL() {
//        return facebookURL;
//    }
//
//    public void setFacebookURL(String facebookURL) {
//        this.facebookURL = facebookURL;
//    }
//
//    public String getTwitterURL() {
//        return twitterURL;
//    }
//
//    public void setTwitterURL(String twitterURL) {
//        this.twitterURL = twitterURL;
//    }
    public Long getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(Long notificationCount) {
        this.notificationCount = notificationCount;
    }

    public Long getPointBalance() {
        return pointBalance;
    }

    public void setPointBalance(Long pointBalance) {
        this.pointBalance = pointBalance;
    }

    public Long getCloverPointBalance() {
		return cloverPointBalance;
	}

	public void setCloverPointBalance(Long cloverPointBalance) {
		this.cloverPointBalance = cloverPointBalance;
	}

	public String getTrendBrewId() {
        return trendBrewId;
    }

    public void setTrendBrewId(String trendBrewId) {
        this.trendBrewId = trendBrewId;
    }

    public String getPromotionCode() {
		return promotionCode;
	}

	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}

	public Long getTrendStreetId() {
        return trendStreetId;
    }

    public void setTrendStreetId(Long trendStreetId) {
        this.trendStreetId = trendStreetId;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getLoginEmail() {
        return loginEmail;
    }

    public void setLoginEmail(String loginEmail) {
        this.loginEmail = loginEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSocialNetworkUserId() {
        return socialNetworkUserId;
    }

    public void setSocialNetworkUserId(String socialNetworkUserId) {
        this.socialNetworkUserId = socialNetworkUserId;
    }

    public String getSocialNetworkAccessToken() {
        return socialNetworkAccessToken;
    }

    public void setSocialNetworkAccessToken(String socialNetworkAccessToken) {
        this.socialNetworkAccessToken = socialNetworkAccessToken;
    }

    public String getEmailAdderss() {
        return emailAdderss;
    }

    public void setEmailAdderss(String emailAdderss) {
        this.emailAdderss = emailAdderss;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getHomePhoneNumber() {
        return homePhoneNumber;
    }

    public void setHomePhoneNumber(String homePhoneNumber) {
        this.homePhoneNumber = homePhoneNumber;
    }

    public String getBusinessPhoneNumber() {
        return businessPhoneNumber;
    }

    public void setBusinessPhoneNumber(String businessPhoneNumber) {
        this.businessPhoneNumber = businessPhoneNumber;
    }
    public Map<String, Long> getBrewStats() {
        return brewStats;
    }

    public void setBrewStats(Map<String, Long> brewStats) {
        this.brewStats = brewStats;
    }

    public Map<String, Long> getNetworkStats() {
        return networkStats;
    }

    public void setNetworktats(Map<String, Long> networkStats) {
        this.networkStats = networkStats;
    }

    public Integer getNumWishes() {
        return numWishes;
    }

    public void setNumWishes(Integer numWishes) {
        this.numWishes = numWishes;
    }

    public Integer getNumOffers() {
        return numOffers;
    }

    public void setNumOffers(Integer numOffers) {
        this.numOffers = numOffers;
    } 

    public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public void addBrewStat(String key, Long value) {
        if (this.brewStats == null)
            this.brewStats = new HashMap<String, Long>();
        this.brewStats.put(key, value);
    }

    public void addNetworkStat(String key, Long value) {
        if (this.networkStats == null)
            this.networkStats = new HashMap<String, Long>();
        this.networkStats.put(key, value);
    }
 }

