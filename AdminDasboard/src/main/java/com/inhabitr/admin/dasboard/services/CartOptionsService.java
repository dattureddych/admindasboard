package com.inhabitr.admin.dasboard.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


import com.inhabitr.admin.dasboard.model.CartOptionsEntity;
import com.inhabitr.admin.dasboard.repository.CartOptionsRepository;
import com.inhabitr.admin.dasboard.repository.OrderRepository;

@Service
public class CartOptionsService {
	@Autowired
	private CartOptionsRepository cartOptionsRepository;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
	
	public List<CartOptionsEntity> findCartOptionDetailById(Long sgid,String reference) {
		getLogger().info(".................Inside getAllOrder..........");
    	List<CartOptionsEntity> orderList = cartOptionsRepository.findCartOptionDetailById(sgid,reference);
      //  getLogger().info("pagedResult list Size=="+pagedResult.getContent().size());
      return orderList;
	}
	
	public List<CartOptionsEntity> findCartOptionDetailById(Long sgid,Long cartTokenSgid,Boolean hasReference) {
		getLogger().info(".................Inside getAllOrder..........");
		List<CartOptionsEntity> orderList =new 	ArrayList<CartOptionsEntity>();
		if(hasReference==true) {
        orderList = cartOptionsRepository.findCartOptionDetailById(sgid,cartTokenSgid);
		}else {
		orderList = cartOptionsRepository.findCartOptionDetailBySgid(sgid,cartTokenSgid);
		}
      //  getLogger().info("pagedResult list Size=="+pagedResult.getContent().size());
      return orderList;
	}
	
	public List<CartOptionsEntity> findCartOptionPaymentySgid(Long sgid,Long cartTokenSgid) {
		getLogger().info(".................Inside getAllOrder..........");
		List<CartOptionsEntity> orderList =new 	ArrayList<CartOptionsEntity>();
		orderList = cartOptionsRepository.findCartOptionPaymentySgid(sgid,cartTokenSgid);
		
       return orderList;
	}
}
