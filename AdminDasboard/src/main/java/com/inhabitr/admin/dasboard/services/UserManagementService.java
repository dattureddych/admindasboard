package com.inhabitr.admin.dasboard.services;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.inhabitr.admin.dasboard.beans.User;
import com.inhabitr.admin.dasboard.model.ConfigurationEntity;
import com.inhabitr.admin.dasboard.model.NotificationTemplateEntity;
import com.inhabitr.admin.dasboard.model.UserEntity;
import com.inhabitr.admin.dasboard.repository.ConfigurationRepository;
import com.inhabitr.admin.dasboard.repository.NotificationTemplateRepository;
import com.inhabitr.admin.dasboard.repository.UserRepository;
import com.inhabitr.admin.dashboard.commons.exception.InhabitrException;
import com.inhabitr.admin.dashboard.commons.exception.KoinPlusCommonExceptionCode;
import com.inhabitr.admin.dashboard.commons.utils.KoinPlusStringUtils;
import com.koinplus.common.authentication.exception.InvalidLoginCredentialsException;
import com.koinplus.common.authentication.exception.InvalidResetPasswordException;
import com.koinplus.common.authentication.exception.PreRegisteredUserException;
import com.koinplus.common.authentication.exception.UnregisteredUserException;
import com.koinplus.common.util.KoinPlusEncryptionUtil;

@Service
public class UserManagementService {
	
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ConfigurationRepository configurationRepository;
	
	@Autowired private NotificationTemplateRepository notificationTemplateRepository;
	
	@Autowired private EmailService emailNotificationSvc;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
	
	@Autowired
	private KoinPlusEncryptionUtil encryptionUtil;
	
	public User signup(User profile) throws PreRegisteredUserException, InvalidLoginCredentialsException, UnregisteredUserException{
		User userProfileResponse = null;
		try {
			User userProfile = findUser(profile.getEmail(), profile.getPassword());
			if(userProfile != null) {
				throw new PreRegisteredUserException();
			}
		} catch (UnregisteredUserException ue) {
			userProfileResponse = addUser(profile);
		} catch(InvalidLoginCredentialsException e) {
			throw new PreRegisteredUserException();
		}
		
	return userProfileResponse;	
	}
	
   public User findUser(String email, String password) 
		   throws InvalidLoginCredentialsException, UnregisteredUserException{
	   User profile = new User();
	   try {
		   List<UserEntity> userList = userRepository.getUser(email);
		   if (!CollectionUtils.isEmpty(userList)) {
			   if (userList.size() == 1) {
				   UserEntity userEntity = userList.get(0);
				   if (userEntity.getPassword() == null) {
					   if (userEntity.getPasswordPlain().equals(password)) {
						   byte[] salt = encryptionUtil.generateSalt();
							byte[] pass = encryptionUtil.getEncryptedPassword(profile.getPassword(), salt);
							userEntity.setSalt(salt);
							userEntity.setPassword(pass);
							userEntity.setStatus(1);
													
							profile.setPassword(new String(userEntity.getPassword()));
							profile.setSalt(new String(userEntity.getSalt()));
							profile.setStatus(userEntity.getStatus());
							//update		
							userRepository.update(userEntity.getSalt(), userEntity.getPassword(), userEntity.getEmail());
							return profile;
					   }else {
							throw new InvalidLoginCredentialsException();
						}
				   }else {
						getLogger().info("Authenticating Ca_Admin");
						boolean authenticated = encryptionUtil.authenticate(password, userEntity.getPassword(),
								userEntity.getSalt());
						getLogger().info("Authenticated Ca_Admin {}", authenticated);
						if (authenticated) {
							profile.setSgid(userEntity.getSgId());
							profile.setCa_admin_id(userEntity.getCaAdminId());
							profile.setFirstName(userEntity.getFirstName());
							profile.setLastName(userEntity.getLastName());
							profile.setMiddleName(userEntity.getMiddleName());
							profile.setEmail(userEntity.getEmail());
							profile.setPassword(new String(userEntity.getPassword()));
							profile.setPasswordPlain(userEntity.getPasswordPlain());
							profile.setTempPassword(userEntity.getTempPassword());
							profile.setSalt(new String(userEntity.getSalt()));
							profile.setUpdateDateTime(userEntity.getUpdateDateTime());
							profile.setStatus(userEntity.getStatus());
							profile.setDisabled(userEntity.getDisabled());
							return profile;
						} else {
							throw new InvalidLoginCredentialsException();
						}
					}
			   } else {
					getLogger().error("There should not be more than one record");
					throw new InvalidLoginCredentialsException();
				}
		   } else {
				throw new UnregisteredUserException();
			}
		   
	   } catch (NoSuchAlgorithmException e) {
			throw new InvalidLoginCredentialsException();
		} catch (InvalidKeySpecException e) {
			throw new InvalidLoginCredentialsException();
		}
	}
   
   public User addUser(User user) throws PreRegisteredUserException{
	   User response = new User();
	   if(user != null) {
		  UserEntity userEntity = new UserEntity();
		  String ca_admin_id = getUserCaAdminId(user);
		  userEntity.setEmail(user.getEmail());
		  userEntity.setPasswordPlain(user.getPasswordPlain());
		  userEntity.setCaAdminId(ca_admin_id);
		  userEntity.setFirstName(user.getFirstName());
		  userEntity.setLastName(user.getLastName());
		  userEntity.setMiddleName(user.getMiddleName());
		  
		   try {
			   byte[] salt = encryptionUtil.generateSalt();
			   byte[] password = encryptionUtil.getEncryptedPassword(user.getPassword(), salt);
			   userEntity.setSalt(salt);
			   userEntity.setPassword(password);
			   userEntity.setStatus(1);
			   userEntity.setDisabled(0);
			   
		   } catch (NoSuchAlgorithmException ne) {
			   getLogger().error("Inserting Ca_Admin password failed");
		   } catch (InvalidKeySpecException e) {
			   getLogger().error("Inserting Ca_Admin password failed");
		   }
		   
		   // insert
		   		  userRepository.createUser(userEntity.getCaAdminId(),
				  userEntity.getFirstName(), userEntity.getLastName(), userEntity.getMiddleName(), 
				  userEntity.getEmail(), userEntity.getPassword(), userEntity.getSalt(), 
				  userEntity.getTempPassword(), userEntity.getPasswordPlain(), 
				  userEntity.getStatus(), userEntity.getDisabled());
		   		  
		   		  List<UserEntity> adminUser = userRepository.getUser(userEntity.getEmail());
		   		  
		   		 if (!CollectionUtils.isEmpty(adminUser)) {
		   			if (adminUser.size() == 1) {
					   UserEntity users = adminUser.get(0);

		   			  response.setSgid(users.getSgId());
					  response.setCa_admin_id(users.getCaAdminId());
					  response.setFirstName(users.getFirstName());
					  response.setLastName(users.getLastName());
					  response.setMiddleName(users.getMiddleName());
					  response.setEmail(users.getEmail());
					  response.setPassword(new String(users.getPassword()));
					  response.setPasswordPlain(users.getPasswordPlain());
					  response.setTempPassword(users.getTempPassword());
					  response.setSalt(new String(users.getSalt()));
					  response.setUpdateDateTime(users.getUpdateDateTime());
					  response.setStatus(users.getStatus());
					  response.setDisabled(users.getDisabled());
		   			}
		   		 }
	   }
	   getLogger().info("User is inserted");
	   return response;
	}
   
   

    private String getUserCaAdminId(User user) {
	// TODO Auto-generated method stub
	getLogger().info("Name is {} {}", user.getFirstName(), user.getLastName());
	String firstNamePart = user.getFirstName().toLowerCase().replaceAll(" ", ".").replaceAll("'", ".");
	String lastNamePart = "tb";
	if (user.getLastName() != null) {
		lastNamePart = user.getLastName().toLowerCase().replaceAll(" ", ".").replaceAll("'", ".");
	}
	String ca_admin_id = firstNamePart + '.' + lastNamePart;
	UserEntity userEntity = find(ca_admin_id);
	
	while (userEntity != null) {
			ca_admin_id = firstNamePart + '.' + lastNamePart + '.' + (Math.round(Math.random() * 89999) + 10000);
			userEntity = find(ca_admin_id);
	}
	getLogger().info("AdminUSer Id is {}", ca_admin_id);
	return ca_admin_id;
    }

    public UserEntity find(String ca_admin_id) {
	List<UserEntity> userList = userRepository.find(ca_admin_id);
	UserEntity userEntity = null;
	if(!CollectionUtils.isEmpty(userList)) {
		return userList.get(0);
	}else {
	return userEntity;
	}
    }

    public User login(String email, String password) throws InvalidLoginCredentialsException, UnregisteredUserException {
	// TODO Auto-generated method stub
    	try {
	     User userProfile = findUser(email, password);
	     return userProfile;
    	} catch (InvalidLoginCredentialsException ex) {
    		throw new InvalidLoginCredentialsException();
    	}catch (UnregisteredUserException ex) {
    		throw new UnregisteredUserException();
    	}      
    	
	}

    public User resetPassword(User user) {
	// TODO Auto-generated method stub
	return null;
    }
    
    public String forgotPassword(String email) throws UnregisteredUserException, InvalidLoginCredentialsException,InhabitrException {
    	try {
    		User adminUser = this.checkIfRegisteredEmail(email);
    		if (adminUser != null) {
    			String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    			String tempPass = RandomStringUtils.random(8, characters);
    			
    			this.setTemporaryPassword(adminUser.getSgid(), tempPass);
    			NotificationTemplateEntity notificationTypeEntity = this.notificationTemplateRepository.findByName("RESET_ADMIN_USER_PASSWORD");
    			
    			String subjectTemplate = notificationTypeEntity.getEmailSubjectTemplate();
    			String subject = subjectTemplate;
    			
    			String messageTemplate = notificationTypeEntity.getEmailMessageTemplate();
    			Map<String, Object> params = new HashMap<String, Object>();
    			params.put("USER_NAME", adminUser.getFirstName());
    			params.put("TEMPORARY_PASSWORD", tempPass);
    			params.put("EMAIL", email);
    			params.put("USER_EMAIL", email);
    			params.put("DATE", new java.util.Date());
    			
    			ConfigurationEntity applicationUrl = this.configurationRepository.findByName("ADMIN_DASHBOARD_APPLICATION_URL");
    			params.put("APPLICATION_URL", applicationUrl.getParamValue());
    			ConfigurationEntity serverUrl = this.configurationRepository.findByName("ADMIN_SERVER_URL");
    			params.put("SERVER_URL", serverUrl.getParamValue());
    			
    			String message = KoinPlusStringUtils.getStringFromTemplate(messageTemplate, params);
    			
    			this.emailNotificationSvc.sendEmailNotification(email, subject, message);
    			return tempPass;
    		}
    	} catch (InvalidLoginCredentialsException ex) {
    		throw new InvalidLoginCredentialsException();
    	}
    	 throw new InhabitrException(KoinPlusCommonExceptionCode.BAD_REQUEST);
    }

	public User checkIfRegisteredEmail(String email)
			throws InvalidLoginCredentialsException, UnregisteredUserException {
		User profile = new User();
		List<UserEntity> userList = userRepository.getUser(email);
		if (!CollectionUtils.isEmpty(userList)) {
			if (userList.size() > 1) {
				getLogger().error("There should not be more than one record");
				throw new InvalidLoginCredentialsException();
			}
			UserEntity userEntity = userList.get(0);
			if (userEntity == null) {
				throw new UnregisteredUserException();

			} else {
				profile.setSgid(userEntity.getSgId());
				profile.setCa_admin_id(userEntity.getCaAdminId());
				profile.setFirstName(userEntity.getFirstName());
				profile.setLastName(userEntity.getLastName());
				profile.setMiddleName(userEntity.getMiddleName());
				profile.setEmail(userEntity.getEmail());
				profile.setPassword(new String(userEntity.getPassword()));
				profile.setPasswordPlain(userEntity.getPasswordPlain());
				profile.setTempPassword(userEntity.getTempPassword());
				profile.setSalt(new String(userEntity.getSalt()));
				profile.setUpdateDateTime(userEntity.getUpdateDateTime());
				profile.setStatus(userEntity.getStatus());
				profile.setDisabled(userEntity.getDisabled());
				return profile;

			}

		}
     return null;

	}
	
	public void setTemporaryPassword(Integer sgid, String temporaryPassword) {
        UserEntity users = this.userRepository.getUserBySgid(sgid);
        users.setTempPassword(temporaryPassword);
        this.userRepository.updateUserDetails(sgid,temporaryPassword);
    }
	
	 public UserEntity resetUserPassword(String email, String tempPassword, String newPassword) 
			 throws InvalidLoginCredentialsException, InvalidResetPasswordException, 
			 PreRegisteredUserException {
		try {
		 UserEntity propertyUser = this.resetPassword(email, tempPassword, newPassword);
	        if (propertyUser != null) {
	            return propertyUser;
	        }
		}catch (InvalidLoginCredentialsException e) {
            throw new InvalidLoginCredentialsException();
        }
		catch (InvalidResetPasswordException e) {
            throw new InvalidResetPasswordException();
        }
		catch (PreRegisteredUserException e) {
            throw new PreRegisteredUserException();
        }
		return null;
	  }
	 public UserEntity resetPassword(String email, String tempPassword, String newPassword) throws InvalidLoginCredentialsException, InvalidResetPasswordException, PreRegisteredUserException {
	        try {
	        	List<UserEntity> propertyUsers = this.userRepository.getUser(email);
	            if (!CollectionUtils.isEmpty(propertyUsers) && propertyUsers.size() == 1) {
	            	UserEntity propertyUserEntity = propertyUsers.get(0);
	                if (propertyUserEntity.getTempPassword() != null) {
	                    if (propertyUserEntity.getTempPassword().equals(tempPassword)) {
	                    	if(propertyUserEntity.getPassword()!=null && propertyUserEntity.getSalt()!=null){
	                    	getLogger().info("Authenticating Admin User");
	                        boolean authenticated = this.encryptionUtil.authenticate(newPassword, propertyUserEntity.getPassword(), propertyUserEntity.getSalt());
	                        getLogger().info("Authenticated Admin User {}", authenticated);
	                        if (authenticated) {
	                    		String message = "{\"message\":\"Make sure the new password is different from the password used before.\"}";
	                           	getLogger().info(message);
	                           	throw new PreRegisteredUserException(message);
	                    	}
	                    	}
	                        byte[] salt = this.encryptionUtil.generateSalt();
	                        byte[] password = this.encryptionUtil.getEncryptedPassword(newPassword, salt);
	                        propertyUserEntity.setSalt(salt);
	                        propertyUserEntity.setPassword(password);
	                        propertyUserEntity.setTempPassword(null);
	                        this.userRepository.updateUserInfo(email,propertyUserEntity.getTempPassword(),password,salt);
	                        return propertyUserEntity;
	                    } else {
	                        throw new InvalidResetPasswordException();
	                    }
	                }
	            }
	        } catch (NoSuchAlgorithmException e) {
	            throw new InvalidLoginCredentialsException();
	        } catch (InvalidKeySpecException e) {
	            throw new InvalidLoginCredentialsException();
	        }
	        throw new InvalidLoginCredentialsException();
	    }
	
}
