package com.inhabitr.admin.dasboard.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.ItemEntity;
import com.inhabitr.admin.dasboard.model.PackageEntity;

@Repository
public interface SearchRepository extends JpaRepository<ItemEntity, Long> {
    //For Item 
	@Query(value= "select i.sgid, i.name as itemName, i.image_url, c.name as categoryName  from item i inner join category c on i.name like CONCAT ('%', c.name, '%') WHERE i.name like %:name% and c.name like %:name%", nativeQuery = true)
	public List<Object> findByDisplayName(@Param("name") String name);
	
	//For Customer 
	@Query(value= "select b.sgid, b.first_name , b.last_name, b.middle_name, bc.email_address , bc.mobile_number, bc.home_phone_number from brewer b inner join brewer_contact bc on b.sgid=bc.brewer_sgid WHERE b.first_name like %:queryParam% or b.last_name like %:queryParam% or b.middle_name like %:queryParam% or bc.email_address like %:queryParam% or bc.mobile_number like %:queryParam% or bc.home_phone_number like %:queryParam%", nativeQuery = true)
	public List<Object> findByFirstName(@Param("queryParam") String queryParam);
	
	//For Package
	//@Query(value= "select b.sgid,b.name, p.name as product_name, b.slug, bc.sku, c.name as category_name,b.saffron_item_sgid from package b inner join package_product_relation bc on b.sgid=bc.package_id inner join product p on p.sgid=bc.product_id inner join category c on c.sgid=p.category_id WHERE b.name like %:queryParam% or p.name like %:queryParam% or c.name like %:queryParam% or b.sku like %:queryParam% or b.slug like %:queryParam%", nativeQuery = true)
	//public List<Object> findByName(@Param("queryParam") String queryParam);
	
	//For Service Request
	@Query(value= "select c.sgid as serviceRequestNum,b.first_name ,b.last_name,b.middle_name,bc.email_address,bc.mobile_number,bc.home_phone_number from customer_service_requests c inner join brewer b on b.sgid=c.brewer_id inner join brewer_contact bc on bc.brewer_sgid=b.sgid WHERE b.name like %:queryParam% or p.name like %:queryParam% or c.name like %:queryParam% or b.sku like %:queryParam% or b.slug like %:queryParam%", nativeQuery = true)
	public List<Object> findByMiddleName(@Param("queryParam") String queryParam);
	
	//For Pricing
	@Query(value= "select b.sgid, b.first_name , b.last_name, b.middle_name, bc.email_address , bc.mobile_number, bc.home_phone_number from brewer b inner join brewer_contact bc on b.sgid=bc.brewer_sgid WHERE b.first_name like %:queryParam% or b.last_name like %:queryParam% or b.middle_name like %:queryParam% or bc.email_address like %:queryParam% or bc.mobile_number like %:queryParam% or bc.home_phone_number like %:queryParam%", nativeQuery = true)
	public List<Object> findBySalutation(@Param("queryParam") String queryParam);
	
	//For Cart Configuration
	@Query(value= "select b.sgid, b.first_name , b.last_name, b.middle_name, bc.email_address , bc.mobile_number, bc.home_phone_number from brewer b inner join brewer_contact bc on b.sgid=bc.brewer_sgid WHERE b.first_name like %:queryParam% or b.last_name like %:queryParam% or b.middle_name like %:queryParam% or bc.email_address like %:queryParam% or bc.mobile_number like %:queryParam% or bc.home_phone_number like %:queryParam%", nativeQuery = true)
	public List<Object> findByHomePhoneNumber(@Param("queryParam") String queryParam);
	
	//For Order
	@Query(value= "select o.sgid as orderNum,o.user_id ,o.order_type,o.first_name,o.last_name,o.email,o.phone,o.address from orders o WHERE o.first_name like %:queryParam% or o.last_name like %:queryParam% or o.email like %:queryParam% or o.phone like %:queryParam% or o.address like %:queryParam%", nativeQuery = true)
	public List<Object> findByLastName(@Param("queryParam") String queryParam);

}
     