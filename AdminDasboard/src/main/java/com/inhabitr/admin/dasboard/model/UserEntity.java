package com.inhabitr.admin.dasboard.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ca_admin_user")
public class UserEntity {
//	@Id
//	private @Column(name = "sgid") Integer sgid;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Integer sgid;
	public Integer getSgId() {
		return sgid;
	}

	public void setSgId(Integer sgid) {
		this.sgid = sgid;
	}

	private @Column(name = "ca_admin_id") String caAdminId;
	private @Column(name = "first_name") String firstName;
    private @Column(name = "last_name") String lastName;
    private @Column(name = "middle_name") String middleName;
    private @Column(name = "email") String email;
    private @Column(name = "password") byte[] password;
    private @Column(name = "salt") byte[] salt;
    private @Column(name = "temp_password") String tempPassword;
    private @Column(name = "password_plain") String passwordPlain;
    private @Column(name = "updated_datetime") Date updateDateTime;
    private @Column(name = "status") Integer status;
    private @Column(name = "disabled") Integer disabled;
    
    
    
	public String getCaAdminId() {
        return caAdminId;
    }

    public void setCaAdminId(String caAdminId) {
        this.caAdminId = caAdminId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    
    public byte[] getPassword() {
        return password;
    }

    public void setPassword(byte[] password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getTempPassword() {
        return tempPassword;
    }

    public void setTempPassword(String tempPassword) {
        this.tempPassword = tempPassword;
    }

    public String getPasswordPlain() {
        return passwordPlain;
    }

    public void setPasswordPlain(String passwordPlain) {
        this.passwordPlain = passwordPlain;
    }
    
    public byte[] getSalt() {
        return salt;
    }

    public Date getUpdateDateTime() {
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	public void setSalt(byte[] salt) {
        this.salt = salt;
    }
    
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    
    public Integer getDisabled() {
        return disabled;
    }

    public void setDisabled(Integer disabled) {
        this.disabled = disabled;
    }
}
