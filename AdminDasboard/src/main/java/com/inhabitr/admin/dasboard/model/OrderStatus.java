package com.inhabitr.admin.dasboard.model;

public enum OrderStatus  implements KoinPlusEnum<String> {

	PENDING("PENDING"),
	SUBMITTED("SUBMITTED"),
	ERROR("ERROR"),
	FAILED("FAILED"),
	SUCCESS("SUCCESS"),
    DELIVERED("DELIVERED"),
    CANCELLED("CANCELLED");
   
    
    private String value;

    private static final KoinPlusEnumEnhancer<OrderStatus> enhancer = new KoinPlusEnumEnhancer<OrderStatus>(values());

    private OrderStatus(String name) {
        this.value = name;
    }

    // This is delegation.
    @Override
    public String toString() {
        return enhancer.toString(this);
    }

    // This too is delegation.
    public static OrderStatus lookup(String name) {
        return enhancer.lookup(name);
    }

    @Override
    public String getValue() {
        return value;
    }
}
