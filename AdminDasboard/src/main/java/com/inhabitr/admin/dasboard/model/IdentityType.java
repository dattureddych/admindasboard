package com.inhabitr.admin.dasboard.model;

public enum IdentityType implements KoinPlusEnum {
	SKU("SKU"),
	ASIN("ASIN"),
	MPN("MPN"),
	UPC("UPC"),
	ISBN("ISBN"),
        TWO_TAP("TWO_TAP"),
        SHOPIFY("SHOPIFY"),
	CLOVER_ID("CLOVER_ID"),
	CLOVER_TOCKEN_ID("CLOVER_TOCKEN_ID"),
	INDIX_ID("INDIX_ID"),
	INDIX_MPID("INDIX_MPID"),
	PROSPERENT_ID("PROSPERENT_ID"),
	VIGLINKS_ID("VIGLINKS_ID"),
        SEMANTICS_THREE_ID("SEMANTICS_THREE_ID"),
        TICKET_REWARDS_ID("TICKET_REWARDS_ID"),
	PIN("PIN");

	//private static final KoinPlusEnumEnhancer<IdentityType> enhancer = new KoinPlusEnumEnhancer<IdentityType>(values());

	private String value;

	private IdentityType(String name) {
		this.value = name;
	}

	public String getValue() {
		return value;
	}

	
}
