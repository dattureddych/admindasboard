package com.inhabitr.admin.dasboard.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inhabitr.admin.dasboard.response.SearchResponse;
import com.inhabitr.admin.dasboard.services.SearchService;

@RestController
@CrossOrigin
@RequestMapping("/apps/api/search")
public class SearchController {
	
	@Autowired
	private SearchService searchService;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
	
	@GetMapping("/search-item/{name}")
	public ResponseEntity<List<SearchResponse>> search(@PathVariable("name") String name) {
		getLogger().info("Inside /apps/api/search/search-item/"+name);
		return new ResponseEntity<List<SearchResponse>>(searchService.search(name), HttpStatus.OK);
	}
}
