package com.inhabitr.admin.dasboard.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.ProductCombinationsEntity;

@Repository
public interface ProductCombinationRepository extends PagingAndSortingRepository<ProductCombinationsEntity, Long> {

	 @Query(value ="select *  from product_sku_variation sku "
			   + "WHERE sku.product_id =:product_id order by sku.asset_value asc limit 1",
			  nativeQuery = true) 
	ProductCombinationsEntity findVariantDetail(Integer product_id);
}
