package com.inhabitr.admin.dasboard.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.CartEntity;
import com.inhabitr.admin.dasboard.model.ItemEntity;

@Repository
public interface AbandonmentCartRepository  extends PagingAndSortingRepository<CartEntity, Long> {
	
	 @Query(value ="select distinct c.sgid from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			    + " inner join cart_item ci on ci.cart_sgid=c.sgid "
			 	+" where ct.ops_dashboard_order_id is null and ct.status ='IS_AVAILABLE' and "
			 	+ " ( UPPER(b.first_name) like %:searchStr% or UPPER(b.last_name) like %:searchStr% "
			 	+ " or UPPER(bb.email_address) like %:searchStr% "
			 	+ " or bb.mobile_number like %:searchStr% or bb.home_phone_number like %:searchStr%  )  "
			    + "  and (select distinct sum(cii.item_quantity) from cart_item cii where cii.cart_sgid=c.sgid and (cii.status='IS_AVAILABLE' or cii.status='INITIATED') )<:cartSize"
			    + " group by c.sgid ",
			  nativeQuery = true) 
	Page<Long> findUnRegisteredCartByFilters(String searchStr,Integer cartSize,Pageable paging); 
	 	  
	 @Query(value ="select * from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			   	+" where ct.ops_dashboard_order_id is null  and ct.status ='IS_AVAILABLE' and "
			 	+ " ( UPPER(b.first_name) like %:searchStr% or UPPER(b.last_name) like %:searchStr% "
			 	+ " or UPPER(bb.email_address) like %:searchStr% "
			 	+ " or bb.mobile_number like %:searchStr% or bb.home_phone_number like %:searchStr%  )  "
			 	//+ "  and (select distinct count(*) from cart_item cii where cii.cart_sgid=c.sgid)>:cartSize"
			   + " and c.sgid in (:sgids) ",
			  nativeQuery = true) 
	Page<CartEntity> findUnRegisteredCartByFilters(String searchStr,List<Long> sgids,Pageable paging); 
	 
	 @Query(value ="select distinct c.sgid from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			    + " inner join cart_item ci on ci.cart_sgid=c.sgid "
			 	+" where ct.ops_dashboard_order_id is not null and ct.status ='IS_AVAILABLE'  and "
			 	+ " ( UPPER(b.first_name) like %:searchStr% or UPPER(b.last_name) like %:searchStr% "
			 	+ " or UPPER(bb.email_address) like %:searchStr% "
			 	+ " or bb.mobile_number like %:searchStr% or bb.home_phone_number like %:searchStr%  )  "
			    + "  and (select distinct sum(cii.item_quantity) from cart_item cii where cii.cart_sgid=c.sgid and (cii.status='IS_AVAILABLE' or cii.status='INITIATED'))<:cartSize"
			    + " group by c.sgid ",
			  nativeQuery = true) 
	Page<Long> findRegisteredCartByFilters(String searchStr,Integer cartSize,Pageable paging); 
	 
	 @Query(value ="select * from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			   	+" where ct.ops_dashboard_order_id is not null  and ct.status ='IS_AVAILABLE' and "
			 	+ " ( UPPER(b.first_name) like %:searchStr% or UPPER(b.last_name) like %:searchStr% "
			 	+ " or UPPER(bb.email_address) like %:searchStr% "
			 	+ " or bb.mobile_number like %:searchStr% or bb.home_phone_number like %:searchStr%  )  "
			    + " and c.sgid in (:sgids) ",
			  nativeQuery = true) 
	Page<CartEntity> findRegisteredCartByFilters(String searchStr,List<Long> sgids,Pageable paging); 
	 
	 @Query(value ="select distinct c.sgid from cart c " 
			    + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			    + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			    + " inner join cart_item ci on ci.cart_sgid=c.sgid "
			 	+" where ct.ops_dashboard_order_id is null and ct.status ='IS_AVAILABLE' "
			 	+ "  and (select distinct sum(cii.item_quantity) from cart_item cii where cii.cart_sgid=c.sgid and (cii.status='IS_AVAILABLE' or cii.status='INITIATED'))<:cartSize"
			    + " group by c.sgid ",
			  nativeQuery = true) 
	Page<Long> findUnRegisteredCartByFilters(Integer cartSize,Pageable paging); 
	 
	 @Query(value =" select * from cart c " 
			     + " inner join cart_token ct on ct.cart_sgid=c.sgid  "
			     + " inner join brewer b on b.sgid=ct.brewer_sgid "
			     + " inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			   	 + " where ct.ops_dashboard_order_id is null  and ct.status ='IS_AVAILABLE' "
			     + " and c.sgid in (:sgids) ",
			  nativeQuery = true) 
	Page<CartEntity> findUnRegisteredCartByFilters(List<Long> sgids,Pageable paging); 
	 
	 @Query(value ="select distinct c.sgid from cart c " 
			    + " inner join cart_token ct on ct.cart_sgid=c.sgid "
			    + " inner join brewer b on b.sgid=ct.brewer_sgid "
			    + " inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			    + " inner join cart_item ci on ci.cart_sgid=c.sgid "
			 	+ " where ct.ops_dashboard_order_id is not null and ct.status ='IS_AVAILABLE' "
			    + " and (select distinct sum(cii.item_quantity) from cart_item cii where cii.cart_sgid=c.sgid and (cii.status='IS_AVAILABLE' or cii.status='INITIATED'))<:cartSize "
			    + " group by c.sgid ",
			  nativeQuery = true) 
	Page<Long> findRegisteredCartByFilters(Integer cartSize,Pageable paging); 
	 
	 @Query(value ="select * from cart c " 
			    + " inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + " inner join brewer b on b.sgid=ct.brewer_sgid "
			    + " inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			   	+ " where ct.ops_dashboard_order_id is not null  and ct.status ='IS_AVAILABLE' "
			 	+ " and c.sgid in (:sgids) ",
			  nativeQuery = true) 
	Page<CartEntity> findRegisteredCartByFilters(List<Long> sgids,Pageable paging);
	 
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	 
	 @Query(value ="select distinct c.sgid from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			    + " inner join cart_item ci on ci.cart_sgid=c.sgid "
			 	+" where ct.status ='IS_AVAILABLE' and "
			 	+ " ( UPPER(b.first_name) like %:searchStr% or UPPER(b.last_name) like %:searchStr% "
			 	+ " or UPPER(bb.email_address) like %:searchStr% "
			 	+ " or bb.mobile_number like %:searchStr% or bb.home_phone_number like %:searchStr%  )  "
			    + "  and (select distinct sum(cii.item_quantity) from cart_item cii where cii.cart_sgid=c.sgid and (cii.status='IS_AVAILABLE' or cii.status='INITIATED'))<:cartSize"
			    + " group by c.sgid ",
			  nativeQuery = true) 
	Page<Long> findAllCartByFilters(String searchStr,Integer cartSize,Pageable paging); 
	 
	 @Query(value ="select * from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			   	+" where ct.status ='IS_AVAILABLE' and "
			 	+ " ( UPPER(b.first_name) like %:searchStr% or UPPER(b.last_name) like %:searchStr% "
			 	+ " or UPPER(bb.email_address) like %:searchStr% "
			 	+ " or bb.mobile_number like %:searchStr% or bb.home_phone_number like %:searchStr%  )  "
			 	+ " and c.sgid in (:sgids) ",
			  nativeQuery = true)  
	Page<CartEntity> findAllCartByFilters(String searchStr,List<Long> sgids,Pageable paging); 
	 

	 @Query(value ="select distinct c.sgid from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			    + " inner join cart_item ci on ci.cart_sgid=c.sgid "
			 	+" where ct.ops_dashboard_order_id is null and ct.status ='IS_AVAILABLE' and "
			 	+ " ( UPPER(b.first_name) like %:searchStr% or UPPER(b.last_name) like %:searchStr% "
			 	+ " or UPPER(bb.email_address) like %:searchStr% "
			 	+ " or bb.mobile_number like %:searchStr% or bb.home_phone_number like %:searchStr%  )  "
			    + "  and (select distinct sum(cii.item_quantity) from cart_item cii where cii.cart_sgid=c.sgid and (cii.status='IS_AVAILABLE' or cii.status='INITIATED'))>:cartSize"
			    + " group by c.sgid ",
			  nativeQuery = true) 
	Page<Long> searchUnRegisteredCart(String searchStr,Pageable paging); 
	 
	 @Query(value ="select * from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			   	+" where ct.ops_dashboard_order_id is null  and ct.status ='IS_AVAILABLE' and "
			 	+ " ( UPPER(b.first_name) like %:searchStr% or UPPER(b.last_name) like %:searchStr% "
			 	+ " or UPPER(bb.email_address) like %:searchStr% "
			 	+ " or bb.mobile_number like %:searchStr% or bb.home_phone_number like %:searchStr%  )  "
			 	+ " and c.sgid in (:sgids) ",
			  nativeQuery = true) 
	Page<CartEntity> searchUnRegisteredCart(String searchStr,List<Long> sgids,Pageable paging); 
	 
	 @Query(value ="select distinct c.sgid from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			    + " inner join cart_item ci on ci.cart_sgid=c.sgid "
			 	+" where ct.ops_dashboard_order_id is not null and ct.status ='IS_AVAILABLE' and "
			 	+ " ( UPPER(b.first_name) like %:searchStr% or UPPER(b.last_name) like %:searchStr% "
			 	+ " or UPPER(bb.email_address) like %:searchStr% "
			 	+ " or bb.mobile_number like %:searchStr% or bb.home_phone_number like %:searchStr%  )  "
			    + "  and (select distinct sum(cii.item_quantity) from cart_item cii where cii.cart_sgid=c.sgid and (cii.status='IS_AVAILABLE' or cii.status='INITIATED') )>:cartSize"
			    + " group by c.sgid ",
			  nativeQuery = true) 
	Page<Long> searchRegisteredCart(String searchStr,Pageable paging); 
	 
	 @Query(value ="select * from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			   	+" where ct.ops_dashboard_order_id is not null  and ct.status ='IS_AVAILABLE' and "
			 	+ " ( UPPER(b.first_name) like %:searchStr% or UPPER(b.last_name) like %:searchStr% "
			 	+ " or UPPER(bb.email_address) like %:searchStr% "
			 	+ " or bb.mobile_number like %:searchStr% or bb.home_phone_number like %:searchStr%  )  "
			    + " and c.sgid in (:sgids) ",
			  nativeQuery = true) 
	Page<CartEntity> searchRegisteredCart(String searchStr,List<Long> sgids,Pageable paging); 
		 //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	 
	 @Query(value ="select distinct c.sgid from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			    + " inner join cart_item ci on ci.cart_sgid=c.sgid "
			 	+" where ct.status ='IS_AVAILABLE' and ci.status ='IS_AVAILABLE' "
			 	+ "  and (select distinct sum(cii.item_quantity) from cart_item cii where cii.cart_sgid=c.sgid and (cii.status='IS_AVAILABLE' or cii.status='INITIATED') )<:cartSize"
			    + " group by c.sgid ",
			  nativeQuery = true) 
	Page<Long> findCartByCartSize(Integer cartSize,Pageable paging); 
	 
	 @Query(value ="select * from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			   	+" where ct.status ='IS_AVAILABLE' "
			 	+ " and c.sgid in (:sgids) ",
			  nativeQuery = true) 
	Page<CartEntity> findCartByCartSize(List<Long> sgids,Pageable paging); 

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
	 
	 @Query(value ="select distinct c.sgid from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			    + " inner join cart_item ci on ci.cart_sgid=c.sgid "
			 	+" where ct.ops_dashboard_order_id is null and ct.status ='IS_AVAILABLE'  "
			    + " group by c.sgid ",
			  nativeQuery = true) 
	Page<Long> findUnRegisteredCart(Pageable paging); 
	 
	 @Query(value ="select * from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			   	+" where ct.ops_dashboard_order_id is null  and ct.status ='IS_AVAILABLE' "
			 	+ " and c.sgid in (:sgids) ",
			  nativeQuery = true) 
	Page<CartEntity> findUnRegisteredCart(List<Long> sgids,Pageable paging); 
	 
	 @Query(value ="select distinct c.sgid from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			    + " inner join cart_item ci on ci.cart_sgid=c.sgid "
			 	+" where ct.ops_dashboard_order_id is not null and ct.status ='IS_AVAILABLE'  "
			 	+ " group by c.sgid ",
			  nativeQuery = true) 
	Page<Long> findRegisteredCart(Pageable paging); 
	 
	 @Query(value ="select * from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			   	+" where ct.ops_dashboard_order_id is not null  and ct.status ='IS_AVAILABLE' "
			    + " and c.sgid in (:sgids) ",
			  nativeQuery = true) 
	Page<CartEntity> findRegisteredCart(List<Long> sgids,Pageable paging); 
	 
	 ////////////////////////////////////////////////
	 @Query(value ="select distinct c.sgid from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			    + " inner join cart_item ci on ci.cart_sgid=c.sgid "
			 	+" where ct.status ='IS_AVAILABLE'  and "
			 	+ " ( UPPER(b.first_name) like %:searchStr% or UPPER(b.last_name) like %:searchStr% "
			 	+ " or UPPER(bb.email_address) like %:searchStr% "
			 	+ " or bb.mobile_number like %:searchStr% or bb.home_phone_number like %:searchStr%  )  "
			    + " group by c.sgid ",
			  nativeQuery = true) 
	Page<Long> searchCart(String searchStr,Pageable paging); 
	 
	 @Query(value ="select * from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			   	+" where ct.status ='IS_AVAILABLE' and "
			 	+ " ( UPPER(b.first_name) like %:searchStr% or UPPER(b.last_name) like %:searchStr% "
			 	+ " or UPPER(bb.email_address) like %:searchStr% "
			 	+ " or bb.mobile_number like %:searchStr% or bb.home_phone_number like %:searchStr%  )  "
			    + " and c.sgid in (:sgids) ",
			  nativeQuery = true) 
	Page<CartEntity> searchCart(String searchStr,List<Long> sgids,Pageable paging); 
	 
	 
	 @Query(value ="select distinct c.sgid from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			    + " inner join cart_item ci on ci.cart_sgid=c.sgid "
			 	+" where ct.status ='IS_AVAILABLE'  "
			    + " group by c.sgid ",
			  nativeQuery = true) 
	Page<Long> findAllCart(Pageable paging); 
	 
	 @Query(value ="select * from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			   	+" where ct.status ='IS_AVAILABLE' "
			 	+ " and c.sgid in (:sgids) ",
			  nativeQuery = true) 
	Page<CartEntity> findAllCart(List<Long> sgids,Pageable paging); 
	  
	 @Query(value ="select distinct c.sgid from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			    + " inner join cart_item ci on ci.cart_sgid=c.sgid "
			 	+" where ct.ops_dashboard_order_id is null and ct.status ='IS_AVAILABLE'  and "
			 	+ " ( UPPER(b.first_name) like %:searchStr% or UPPER(b.last_name) like %:searchStr% "
			 	+ " or UPPER(bb.email_address) like %:searchStr% "
			 	+ " or bb.mobile_number like %:searchStr% or bb.home_phone_number like %:searchStr%  )  "
			    + "  and (select distinct sum(cii.item_quantity) from cart_item cii where cii.cart_sgid=c.sgid and (cii.status='IS_AVAILABLE' or cii.status='INITIATED'))>:cartSize"
			    + " group by c.sgid ",
			  nativeQuery = true) 
	Page<Long> findUnRegisteredCartByFiltersNew(String searchStr,Integer cartSize,Pageable paging); 
	 
	 @Query(value ="select distinct c.sgid from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			    + " inner join cart_item ci on ci.cart_sgid=c.sgid "
			 	+" where ct.ops_dashboard_order_id is not null and ct.status ='IS_AVAILABLE' and "
			 	+ " ( UPPER(b.first_name) like %:searchStr% or UPPER(b.last_name) like %:searchStr% "
			 	+ " or UPPER(bb.email_address) like %:searchStr% "
			 	+ " or bb.mobile_number like %:searchStr% or bb.home_phone_number like %:searchStr%  )  "
			    + "  and (select distinct sum(cii.item_quantity) from cart_item cii where cii.cart_sgid=c.sgid and (cii.status='IS_AVAILABLE' or cii.status='INITIATED'))>:cartSize"
			    + " group by c.sgid ",
			  nativeQuery = true) 
	Page<Long> findRegisteredCartByFiltersNew(String searchStr,Integer cartSize,Pageable paging); 
	 
	 @Query(value ="select distinct c.sgid from cart c " 
			    + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			    + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			    + " inner join cart_item ci on ci.cart_sgid=c.sgid "
			 	+" where ct.ops_dashboard_order_id is null and ct.status ='IS_AVAILABLE' "
			 	+ "  and (select distinct sum(cii.item_quantity) from cart_item cii where cii.cart_sgid=c.sgid and (cii.status='IS_AVAILABLE' or cii.status='INITIATED'))>:cartSize"
			    + " group by c.sgid ",
			  nativeQuery = true) 
	Page<Long> findUnRegisteredCartByFiltersNew(Integer cartSize,Pageable paging); 
	 
	 @Query(value ="select distinct c.sgid from cart c " 
			    + " inner join cart_token ct on ct.cart_sgid=c.sgid "
			    + " inner join brewer b on b.sgid=ct.brewer_sgid "
			    + " inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			    + " inner join cart_item ci on ci.cart_sgid=c.sgid "
			 	+ " where ct.ops_dashboard_order_id is not null and ct.status ='IS_AVAILABLE' "
			    + " and (select distinct sum(cii.item_quantity) from cart_item cii where cii.cart_sgid=c.sgid and (cii.status='IS_AVAILABLE' or cii.status='INITIATED'))>:cartSize "
			    + " group by c.sgid ",
			  nativeQuery = true) 
	Page<Long> findRegisteredCartByFiltersNew(Integer cartSize,Pageable paging); 
	 
	 @Query(value ="select distinct c.sgid from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			    + " inner join cart_item ci on ci.cart_sgid=c.sgid "
			 	+" where ct.status ='IS_AVAILABLE' and "
			 	+ " ( UPPER(b.first_name) like %:searchStr% or UPPER(b.last_name) like %:searchStr% "
			 	+ " or UPPER(bb.email_address) like %:searchStr% "
			 	+ " or bb.mobile_number like %:searchStr% or bb.home_phone_number like %:searchStr%  )  "
			    + "  and (select distinct sum(cii.item_quantity) from cart_item cii where cii.cart_sgid=c.sgid and (cii.status='IS_AVAILABLE' or cii.status='INITIATED'))>:cartSize"
			    + " group by c.sgid ",
			  nativeQuery = true) 
	Page<Long> findAllCartByFiltersNew(String searchStr,Integer cartSize,Pageable paging); 
	 
	 @Query(value ="select distinct c.sgid from cart c " 
			   + "inner join cart_token ct on ct.cart_sgid=c.sgid  "
			    + "inner join brewer b on b.sgid=ct.brewer_sgid "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid "
			    + " inner join cart_item ci on ci.cart_sgid=c.sgid "
			 	+" where ct.status ='IS_AVAILABLE' "
			 	+ "  and (select distinct sum(cii.item_quantity) from cart_item cii where cii.cart_sgid=c.sgid and (cii.status='IS_AVAILABLE' or cii.status='INITIATED'))>:cartSize"
			    + " group by c.sgid ",
			  nativeQuery = true) 
	Page<Long> findCartByCartSizeNew(Integer cartSize,Pageable paging);  
}
