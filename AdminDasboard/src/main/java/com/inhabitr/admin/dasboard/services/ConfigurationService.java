package com.inhabitr.admin.dasboard.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.inhabitr.admin.dasboard.model.BrewerEntity;
import com.inhabitr.admin.dasboard.model.ConfigurationEntity;
import com.inhabitr.admin.dasboard.model.ItemEntity;
import com.inhabitr.admin.dasboard.model.PackageEntity;
import com.inhabitr.admin.dasboard.repository.ConfigurationRepository;

@Service
public class ConfigurationService<configurationRepository>  {
	@Autowired
	private ConfigurationRepository configurationRepository;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
	public List<ConfigurationEntity> findAll(Integer pageNo, Integer pageSize,String sortDir,String sortBy) {
		/*
		 * Iterable<BrewerEntity> findAll = cartConfigurationRepository.findAll();
		 * Iterator<BrewerEntity> iterator = findAll.iterator(); List<BrewerEntity>
		 * itemList = new ArrayList<BrewerEntity>();
		 * 
		 * while (iterator.hasNext()) { itemList.add(iterator.next()); }
		 * 
		 * return itemList;
		 */
		getLogger().info("............Inside findAll for CartConfigurationService...........");
		Pageable paging = PageRequest.of(pageNo, pageSize,
	            sortDir.equals("asc") ? Sort.by(sortBy).ascending()
                        : Sort.by("sgid").descending());
		 
        Page<ConfigurationEntity> pagedResult = configurationRepository.findAll(paging);
        getLogger().info("pagedResult list Size=="+pagedResult.getContent().size());
        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<ConfigurationEntity>();
        }
	}
	
	
	public String updateConfiguration(String paramName, String paramValue) {
		try {
			if(paramValue!=null && !paramValue.equals("")) {
			configurationRepository.updateConfiguration(paramValue);
			}
		}catch(Exception e) {
			e.printStackTrace();
			return "failure";
		}
		return "success";
			
	}
	
	public List<ConfigurationEntity> findAll() {
		logger.info("Inside findPackageCount() Starting.....");
		Iterable<ConfigurationEntity> findAll = configurationRepository.findAll();
		Iterator<ConfigurationEntity> iterator = findAll.iterator();
		List<ConfigurationEntity> itemList = new ArrayList<ConfigurationEntity>();

		while (iterator.hasNext()) {
			itemList.add(iterator.next());
		}

		return itemList;
	}
}
