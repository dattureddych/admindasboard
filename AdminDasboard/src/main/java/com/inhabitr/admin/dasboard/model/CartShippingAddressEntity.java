package com.inhabitr.admin.dasboard.model;


import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity @Table(name = "cart_shipping_address")
public class CartShippingAddressEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	private @Column(name = "cart_sgid") Long cartSgid;
	private @Column(name = "brewer_sgid") Long brewerSgid;
    private @Column(name = "email") String email;
    private @Column(name = "shipping_first_name") String shippingFirstName;
    private @Column(name = "shipping_last_name") String shippingLastName;
    private @Column(name = "ship_address") String shipAddress;
    private @Column(name = "shipping_city") String shippingCity;
    private @Column(name = "shipping_state") String shippingState;
    private @Column(name = "shipping_country") String shippingCountry;
    private @Column(name = "shipping_zip") String shippingZip;
    private @Column(name = "billing_address") Boolean billingAddress;
    private @Column(name = "shipping_telephone") String shippingTelephone;
    private @Column(name = "building_name") String buildingName;
    private @Column(name = "building_unit_no") String buildingUnitNo;
    private @Column(name = "is_alternare")Integer isAlternate;
    private @Column(name = "order_reference")String orderReference;
    
	public Integer getIsAlternate() {
		return isAlternate;
	}
	public void setIsAlternate(Integer isAlternate) {
		this.isAlternate = isAlternate;
	}
	public String getOrderReference() {
		return orderReference;
	}
	public void setOrderReference(String orderReference) {
		this.orderReference = orderReference;
	}
	public String getBuildingName() {
		return buildingName;
	}
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}
	public String getBuildingUnitNo() {
		return buildingUnitNo;
	}
	public void setBuildingUnitNo(String buildingUnitNo) {
		this.buildingUnitNo = buildingUnitNo;
	}
	public Boolean getBillingAddress() {
		return billingAddress;
	}
	public void setBillingAddress(Boolean billingAddress) {
		this.billingAddress = billingAddress;
	}
	public Long getCartSgid() {
		return cartSgid;
	}
	public void setCartSgid(Long cartSgid) {
		this.cartSgid = cartSgid;
	}
	public Long getBrewerSgid() {
		return brewerSgid;
	}
	public void setBrewerSgid(Long brewerSgid) {
		this.brewerSgid = brewerSgid;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getShippingFirstName() {
		return shippingFirstName;
	}
	public void setShippingFirstName(String shippingFirstName) {
		this.shippingFirstName = shippingFirstName;
	}
	public String getShippingLastName() {
		return shippingLastName;
	}
	public void setShippingLastName(String shippingLastName) {
		this.shippingLastName = shippingLastName;
	}
	public String getShipAddress() {
		return shipAddress;
	}
	public void setShipAddress(String shipAddress) {
		this.shipAddress = shipAddress;
	}
	public String getShippingCity() {
		return shippingCity;
	}
	public void setShippingCity(String shippingCity) {
		this.shippingCity = shippingCity;
	}
	public String getShippingState() {
		return shippingState;
	}
	public void setShippingState(String shippingState) {
		this.shippingState = shippingState;
	}
	public String getShippingCountry() {
		return shippingCountry;
	}
	public void setShippingCountry(String shippingCountry) {
		this.shippingCountry = shippingCountry;
	}
	public String getShippingZip() {
		return shippingZip;
	}
	public void setShippingZip(String shippingZip) {
		this.shippingZip = shippingZip;
	}
	public String getShippingTelephone() {
		return shippingTelephone;
	}
	public void setShippingTelephone(String shippingTelephone) {
		this.shippingTelephone = shippingTelephone;
	}
	public Long getSgid() {
		return sgid;
	}
	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}
    
    
   
}
