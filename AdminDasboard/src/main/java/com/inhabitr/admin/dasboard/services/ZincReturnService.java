package com.inhabitr.admin.dasboard.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.inhabitr.admin.dasboard.model.ZincReturnEntity;
import com.inhabitr.admin.dasboard.repository.ZincReturnRepository;

@Service
public class ZincReturnService {
	@Autowired
	private ZincReturnRepository zincReturnRepository;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
	
	public List<ZincReturnEntity> searchByFilters(String status,String searchStr,Integer pageNo, Integer pageSize, String sortDir,
			String sortBy) {
		getLogger().info("Inside searchByFilters()");
		Pageable paging = PageRequest.of(pageNo, pageSize,
				sortDir.equals("asc") ? Sort.by(sortBy).ascending() : Sort.by("sgid").descending());
		Page<ZincReturnEntity> customerList = null;
		
		switch(status){
		case "RETURN INITIATED": status="PENDING";break;
		case "LABEL SENT TO CUSTOMER": status="LABEL";break;
		case "AMAZON TO INHABITR REFUND INITIATED": status="REFUND ISSUED";break;
		case "AMAZON TO INHABITR REFUND SUCCESSFUL": status="REFUND SUCCESSFUL";break;
		case "Return Failed": status="FAILED";break;
		case "CLOSED": status="CLOSED" ; break;
		
		}
		if ((!(status.equals("")) && status.equals("LABEL"))&& (searchStr != null && !searchStr.equals(""))) {
			customerList = zincReturnRepository.findByFilters1(searchStr.toUpperCase(), paging);
		}
		if ((!(status.equals("")) && !status.equals("LABEL"))&& (searchStr != null && !searchStr.equals(""))) {
			customerList = zincReturnRepository.findByFilters(status, searchStr.toUpperCase(), paging);
		}
		if ((status.equals("")) && (searchStr != null && !searchStr.equals(""))) {
			customerList = zincReturnRepository.findByFilters(searchStr.toUpperCase(), paging);
		}
		if ((status.equals("")) && (searchStr == null || searchStr.equals(""))) {
			customerList = zincReturnRepository.findByFilters(paging);
		}
		if ((!(status.equals("")) && status.equals("LABEL")) && (searchStr == null || searchStr.equals(""))) {
			customerList = zincReturnRepository.findByStatus1(paging);
		}
		if ((!(status.equals("")) && !status.equals("LABEL")) && (searchStr == null || searchStr.equals(""))) {
			customerList = zincReturnRepository.findByStatus(status,paging);
		}
		getLogger().info("Exit searchByFilters()");
		if (customerList.hasContent()) {
			return customerList.getContent();
		} else {
			return new ArrayList<ZincReturnEntity>();
		}
	}
}
