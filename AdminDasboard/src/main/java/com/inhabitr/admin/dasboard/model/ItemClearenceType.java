package com.inhabitr.admin.dasboard.model;

public enum ItemClearenceType implements KoinPlusEnum<String> {
    BELOW_TWENTY_PRICE("BELOW_TWENTY_PRICE"),
    UPTO_FIFTY_OFFER("UPTO_FIFTY_OFFER");

    //private static final KoinPlusEnumEnhancer<ItemClearenceType> enhancer = new KoinPlusEnumEnhancer<ItemClearenceType>(values());

    private String value;

    private ItemClearenceType(String name) {
        this.value = name;
    }

    public String getValue() {
        return value;
    }

    
}
