package com.inhabitr.admin.dasboard.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "package")
public class PackageEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;

	private @Column(name = "category_id") Long categorySgid;
	private @Column(name = "saffron_item_sgid") Long saffronItemSgid;
	private @Column(name = "sku") String sku;
	private @Column(name = "name") String name;
	private @Column(name = "slug") String slug;
	private @Column(name = "stock") Integer stock;
	private @Column(name = "pricing_asset_value") BigDecimal pricingAssetValue;
	private @Column(name = "base64_image") String base64Image;
	private @Column(name = "is_featured") Integer isFeatured;
	private @Column(name = "clearance") Integer clearance;
	private @Column(name = "sale") Integer sale;
	//private @Column(name = "available_online") Integer availableOnline;
	private @Column(name = "is_global") Integer isGlobal;
	private @Column(name = "notes") String notes;
	private @Column(name = "is_deleted") Integer isDeleted;
	private @Column(name = "status") Integer status;
	private @Column(name = "created_by") Integer createdBy;
	private @Column(name = "updated_by") Integer updatedBy;
	private @Column(name = "created_at") @Temporal(TemporalType.TIMESTAMP) Date createdAt;
	private @Column(name = "is_floorplan") Integer isFloorPlan;
	
	 @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "category_id", insertable=false, updatable=false)
       private CategoryEntity category;

    @Transient
	private List<PackageProductRelationEntity> packageproductrelationentity;
    @Transient
    private String categoryName;  
    @Transient
    private Double	buyMultiplier;
    @Transient
	private Double	buyAffirmMultiplier;
	 
    @Transient
	private Integer availbleItemCount;
	
	@Transient
	private Integer unavailbleItemCount;

	public Long getSgid() {
		return sgid;
	}

	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}

	public Long getCategorySgid() {
		return categorySgid;
	}

	public void setCategorySgid(Long categorySgid) {
		this.categorySgid = categorySgid;
	}

	public Long getSaffronItemSgid() {
		return saffronItemSgid;
	}

	public void setSaffronItemSgid(Long saffronItemSgid) {
		this.saffronItemSgid = saffronItemSgid;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public BigDecimal getPricingAssetValue() {
		return pricingAssetValue;
	}

	public void setPricingAssetValue(BigDecimal pricingAssetValue) {
		this.pricingAssetValue = pricingAssetValue;
	}

	public String getBase64Image() {
		return base64Image;
	}

	public void setBase64Image(String base64Image) {
		this.base64Image = base64Image;
	}

	public Integer getIsFeatured() {
		return isFeatured;
	}

	public void setIsFeatured(Integer isFeatured) {
		this.isFeatured = isFeatured;
	}

	public Integer getClearance() {
		return clearance;
	}

	public void setClearance(Integer clearance) {
		this.clearance = clearance;
	}

	public Integer getSale() {
		return sale;
	}

	public void setSale(Integer sale) {
		this.sale = sale;
	}

	public Integer getIsGlobal() {
		return isGlobal;
	}

	public void setIsGlobal(Integer isGlobal) {
		this.isGlobal = isGlobal;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Integer getIsFloorPlan() {
		return isFloorPlan;
	}

	public void setIsFloorPlan(Integer isFloorPlan) {
		this.isFloorPlan = isFloorPlan;
	}
	
	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	
	public List<PackageProductRelationEntity> getPackageproductrelationentity() {
		return packageproductrelationentity;
	}

	public void setPackageproductrelationentity(List<PackageProductRelationEntity> packageproductrelationentity) {
		this.packageproductrelationentity = packageproductrelationentity;
	}

	public Integer getAvailbleItemCount() {
		return availbleItemCount;
	}

	public void setAvailbleItemCount(Integer availbleItemCount) {
		this.availbleItemCount = availbleItemCount;
	}

	public Integer getUnavailbleItemCount() {
		return unavailbleItemCount;
	}

	public void setUnavailbleItemCount(Integer unavailbleItemCount) {
		this.unavailbleItemCount = unavailbleItemCount;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	/*public CategoryEntity getCategory() {
		return category;
	}

	public void setCategory(CategoryEntity category) {
		this.category = category;
	}*/

	public Double getBuyMultiplier() {
		return buyMultiplier;
	}

	public void setBuyMultiplier(Double buyMultiplier) {
		this.buyMultiplier = buyMultiplier;
	}

	public Double getBuyAffirmMultiplier() {
		return buyAffirmMultiplier;
	}

	public void setBuyAffirmMultiplier(Double buyAffirmMultiplier) {
		this.buyAffirmMultiplier = buyAffirmMultiplier;
	}


}
