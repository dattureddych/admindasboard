package com.inhabitr.admin.dasboard.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * @author Anshu Gupta
 */
@Entity @Table(name = "states")
public class StateEntity extends TrendableEntity<StateEntity> {
	//private @Column(name = "name") String name;
	private @Column(name = "code") String code;
	private @Column(name = "country_id") Long countrySgid;
	private @Column(name = "sale_tax_rate") BigDecimal salesTaxRate;
	private @Column(name = "enable_tax") Integer enableTax;
	private @Column(name = "no_of_crew") Integer noOfCrew;
	private @Column(name = "status") Integer status;
	private @Column(name = "created_datetime") @Temporal(TemporalType.TIMESTAMP) Date createdDatetime;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "state")
    private List<ZipcodeEntity> zipcodes;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "state")
    private List<CityEntity> cities;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "state")
    private List<UniversityEntity> university;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Long getCountrySgid() {
		return countrySgid;
	}
	public void setCountrySgid(Long countrySgid) {
		this.countrySgid = countrySgid;
	}
	public BigDecimal getSalesTaxRate() {
		return salesTaxRate;
	}
	public void setSalesTaxRate(BigDecimal salesTaxRate) {
		this.salesTaxRate = salesTaxRate;
	}
	public Integer getEnableTax() {
		return enableTax;
	}
	public void setEnableTax(Integer enableTax) {
		this.enableTax = enableTax;
	}
	public Integer getNoOfCrew() {
		return noOfCrew;
	}
	public void setNoOfCrew(Integer noOfCrew) {
		this.noOfCrew = noOfCrew;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Date getCreatedDatetime() {
		return createdDatetime;
	}
	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	public List<ZipcodeEntity> getZipcodes() {
		return zipcodes;
	}
	public void setZipcodes(List<ZipcodeEntity> zipcodes) {
		this.zipcodes = zipcodes;
	}
	public List<CityEntity> getCities() {
		return cities;
	}
	public void setCities(List<CityEntity> cities) {
		this.cities = cities;
	}
	public List<UniversityEntity> getUniversity() {
		return university;
	}
	public void setUniversity(List<UniversityEntity> university) {
		this.university = university;
	}
	
}

