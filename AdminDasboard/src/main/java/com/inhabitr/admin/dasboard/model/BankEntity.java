package com.inhabitr.admin.dasboard.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * @author Dattu Reddy
 */
@Entity @Table(name = "bank")
public class BankEntity extends GenericKoinPlusDataEntity<BankEntity> {
	private @Column(name = "account_number") Long accountNumber;
	private @Column(name = "token") String token;
	private @Column(name = "customer_id") String customerId;
	private @Column(name = "fingerprint") String fingerprint;
	private @Column(name = "bank_account_id") String bankAccountId;
	private @Column(name = "account_type") String accountType;
	private @Column(name = "source_bank_name") String sourceBankName;
	private @Column(name = "source_country") String sourceCountry;
	private @Column(name = "source_cvc_check") String sourceCvcCheck;
	private @Column(name = "last4") String last4;
	private @Column(name = "routing_number") String routingNumber;
	private @Column(name = "account_holder_name") String accountHolderName;
	private @Column(name = "account_holder_type") String accountHolderType;
	private @Column(name = "status") Integer status;
	private @Column(name = "is_default") Integer isDefault;
    private @Column(name = "created_datetime") @Temporal(TemporalType.TIMESTAMP) Date createdDatetime;
    private @Column(name = "deleted_datetime") @Temporal(TemporalType.TIMESTAMP) Date deletedDatetime;
    @OneToOne(fetch = FetchType.LAZY) @JoinColumn(name = "user_sgid")
    private BrewerEntity brewer;
	public Long getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getFingerprint() {
		return fingerprint;
	}
	public void setFingerprint(String fingerprint) {
		this.fingerprint = fingerprint;
	}
	public String getBankAccountId() {
		return bankAccountId;
	}
	public void setBankAccountId(String bankAccountId) {
		this.bankAccountId = bankAccountId;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getSourceBankName() {
		return sourceBankName;
	}
	public void setSourceBankName(String sourceBankName) {
		this.sourceBankName = sourceBankName;
	}
	public String getSourceCountry() {
		return sourceCountry;
	}
	public void setSourceCountry(String sourceCountry) {
		this.sourceCountry = sourceCountry;
	}
	public String getSourceCvcCheck() {
		return sourceCvcCheck;
	}
	public void setSourceCvcCheck(String sourceCvcCheck) {
		this.sourceCvcCheck = sourceCvcCheck;
	}
	public String getLast4() {
		return last4;
	}
	public void setLast4(String last4) {
		this.last4 = last4;
	}
	public String getRoutingNumber() {
		return routingNumber;
	}
	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}
	public String getAccountHolderName() {
		return accountHolderName;
	}
	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}
	public String getAccountHolderType() {
		return accountHolderType;
	}
	public void setAccountHolderType(String accountHolderType) {
		this.accountHolderType = accountHolderType;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(Integer isDefault) {
		this.isDefault = isDefault;
	}
	public Date getCreatedDatetime() {
		return createdDatetime;
	}
	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	public Date getDeletedDatetime() {
		return deletedDatetime;
	}
	public void setDeletedDatetime(Date deletedDatetime) {
		this.deletedDatetime = deletedDatetime;
	}
	public BrewerEntity getBrewer() {
		return brewer;
	}
	public void setBrewer(BrewerEntity brewer) {
		this.brewer = brewer;
	}
}
