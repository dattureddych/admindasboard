package com.inhabitr.admin.dasboard.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Anshu Gupta
 */
@Entity
@Table(name = "product_sku_variation")
public class ProductCombinationsEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;

	@Column(name = "product_id")
	private Long productId;

	@Column(name = "asset_value")
	private Double assetValue;

	@Column(name = "pricing_asset_value")
	private Double pricingAssetValue;
	
	private @Column(name = "status") Integer status;

	private @Column(name = "quantity") Integer quantity;
	private @Column(name = "stock") Integer stock;
	
	private @Column(name = "is_best_combination") Integer isBestCombination;

	@Column(name = "updated_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdatedAt;
	
	  private @Column(name = "sku") String sku;
	  
	  private @Column(name = "image_sku") String imageSku;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "product_id", insertable = false, updatable = false)
    @JsonIgnore
	private ProductEntity product;
	
	 @OneToMany(fetch = FetchType.LAZY, mappedBy = "productSkuVariation") 
	 private List<ProductImageEntity> productImages;
	 
	  @OneToMany(fetch = FetchType.LAZY, mappedBy = "productCombination")
	  @JsonIgnore
	  private List<AttributeSkuVariationEntity> productAttributes;
	  
	  @Transient
	  private Double buyNewPrice =0.0;
	  @Transient
		private Double buyNewAffirmPrice = 0.0;
	  
	  @Transient
		private String variation_detail = "";

	public ProductCombinationsEntity() {
		super();
	}

	public Long getSgid() {
		return sgid;
	}

	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Double getAssetValue() {
		return assetValue;
	}

	public void setAssetValue(Double assetValue) {
		this.assetValue = assetValue;
	}

	public Double getPricingAssetValue() {
		return pricingAssetValue;
	}

	public void setPricingAssetValue(Double pricingAssetValue) {
		this.pricingAssetValue = pricingAssetValue;
	}

	public Date getLastUpdatedAt() {
		return lastUpdatedAt;
	}

	public void setLastUpdatedAt(Date lastUpdatedAt) {
		this.lastUpdatedAt = lastUpdatedAt;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public ProductEntity getProduct() {
		return product;
	}

	public void setProduct(ProductEntity product) {
		this.product = product;
	}

	
	
	public List<ProductImageEntity> getProductImages() {
		return productImages;
	}
	public void setProductImages(List<ProductImageEntity> productImages) {
		this.productImages = productImages;
	}

	public Integer getIsBestCombination() {
		return isBestCombination;
	}

	public void setIsBestCombination(Integer isBestCombination) {
		this.isBestCombination = isBestCombination;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getImageSku() {
		return imageSku;
	}

	public void setImageSku(String imageSku) {
		this.imageSku = imageSku;
	}

	public Double getBuyNewPrice() {
		return buyNewPrice;
	}

	public void setBuyNewPrice(Double buyNewPrice) {
		this.buyNewPrice = buyNewPrice;
	}

	public Double getBuyNewAffirmPrice() {
		return buyNewAffirmPrice;
	}

	public void setBuyNewAffirmPrice(Double buyNewAffirmPrice) {
		this.buyNewAffirmPrice = buyNewAffirmPrice;
	}

	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getVariation_detail() {
		return variation_detail;
	}

	public void setVariation_detail(String variation_detail) {
		this.variation_detail = variation_detail;
	}
}
