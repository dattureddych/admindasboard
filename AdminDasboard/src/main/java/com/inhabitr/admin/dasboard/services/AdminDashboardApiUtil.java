package com.inhabitr.admin.dasboard.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class AdminDashboardApiUtil {
   private final String OPS_DB_API_END_POINT = "https://apitest.inhabitr.com";//test
	//private final String OPS_DB_API_END_POINT = "https://apps.inhabitr.com";
	

	private String zincUrl;

	private String token;

	
	private String authKey;

	private RestTemplate restTemplate;
	private ObjectMapper mapper = new ObjectMapper();

	private static final Logger logger = LoggerFactory.getLogger(AdminDashboardApiUtil.class);


public ResponseEntity<String> createUserRequest(String request) {
	restTemplate = new RestTemplate();
	logger.info("Into synchUser " + request);
	String endPoint = OPS_DB_API_END_POINT + "/api/auth/register";
	HttpHeaders header = new HttpHeaders();
	header.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	header.add("Content-Type", "application/json");
	header.add("Authorization", token);

	String requestJson=this.getJsonString(request);
	HttpEntity<String> entity = new HttpEntity<>(requestJson, header);

	ResponseEntity<String> response = restTemplate.exchange(endPoint, HttpMethod.POST, entity, String.class);
	return response;
}
public static String getJsonString(Object object) {
    try {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        String json = mapper.writeValueAsString(object);
        return json;
    } catch (JsonProcessingException ex) {
       ex.printStackTrace(); 
    }
    return null;
}
}
