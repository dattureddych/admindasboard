package com.inhabitr.admin.dasboard.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity @Table(name = "product_package_variation_group")
public class ProductPackageVariationGroupEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	
	private @Column(name = "package_sgid") Long packageSgid;
    private @Column(name = "group_name") String groupName;
    private @Column(name = "attribute_type") String attributeType;
    private @Column(name = "status") Integer status;
    private @Column(name = "created_at") @Temporal(TemporalType.TIMESTAMP) Date createdAt;
	
    @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "package_sgid",insertable=false, updatable=false)
    private PackageEntity packages;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "productPackageVariationGroupEntity")
    private List<ProductPackageVariationsEntity> productPackageVariationsEntity;
    
    public List<ProductPackageVariationsEntity> getProductPackageVariationsEntity() {
		return productPackageVariationsEntity;
	}
	public void setProductPackageVariationsEntity(List<ProductPackageVariationsEntity> productPackageVariationsEntity) {
		this.productPackageVariationsEntity = productPackageVariationsEntity;
	}
	public PackageEntity getPackages() {
		return packages;
	}
	public void setPackages(PackageEntity packages) {
		this.packages = packages;
	}
	public Long getPackageSgid() {
		return packageSgid;
	}
	public void setPackageSgid(Long packageSgid) {
		this.packageSgid = packageSgid;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getAttributeType() {
		return attributeType;
	}
	public void setAttributeType(String attributeType) {
		this.attributeType = attributeType;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Long getSgid() {
		return sgid;
	}
	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}
	
}
