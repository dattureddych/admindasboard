package com.inhabitr.admin.dasboard.services;

import java.io.UnsupportedEncodingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inhabitr.admin.dasboard.beans.IContentType;
import com.inhabitr.admin.dasboard.beans.IEmailConstants;
import com.inhabitr.admin.dasboard.model.ConfigurationEntity;
import com.inhabitr.admin.dasboard.repository.ConfigurationRepository;
import com.inhabitr.admin.dasboard.repository.NotificationTemplateRepostiory;

@Service
public class EmailService {

	@Autowired
	private ConfigurationRepository configurationRepository;

	@Autowired
	private NotificationTemplateRepostiory notificationTemplateRepostiory;

	
	private final Logger logger = LoggerFactory.getLogger(EmailService.class);

	
	public void sendEmailNotification(String emailAddress, String subject, String body) {
		ConfigurationEntity notificationEmail = configurationRepository.findByName("INHABTR_NOTIFICATION_EMAIL");
		ConfigurationEntity notificationEmailPwd = configurationRepository
				.findByName("INHABTR_NOTIFICATION_EMAIL_PWD");
		String EMAIL_LOGIN =null;
		String EMAIL_PASSWORD = null;
		if (notificationEmail != null && notificationEmail.getParamValue() != null && notificationEmailPwd != null
				&& notificationEmailPwd.getParamValue() != null) {
			EMAIL_LOGIN = notificationEmail.getParamValue();
			EMAIL_PASSWORD = notificationEmailPwd.getParamValue();
		}
		logger.info("Email address is {}", emailAddress);
		ConfigurationEntity configuration = configurationRepository.findByName("EMAIL_NOTIFICATION_FLAG");
		if (EMAIL_LOGIN != null && EMAIL_PASSWORD != null) {
			postMail(emailAddress, subject, body, EMAIL_LOGIN, EMAIL_PASSWORD, EMAIL_LOGIN,
					IEmailConstants.DEFAULT_PROVIDER, IContentType.HTML);
		} else

		if (configuration.getParamValue().equalsIgnoreCase("TRUE")) {
			postMail(emailAddress, subject, body, IEmailConstants.EMAIL_LOGIN, IEmailConstants.EMAIL_PASSWORD,
					IEmailConstants.EMAIL_LOGIN, IEmailConstants.DEFAULT_PROVIDER, IContentType.HTML);
			logger.info("Email sent");
		} else {
			logger.info("Email Notification is Disabled");
		}
	}

	@Async("threadPoolTaskExecutor")
	private void postMail(String recipient, String subject, String message, final String fromEmail,
			final String fromPassword, String replyTo, String emailProvider, String contentType) {

		logger.info("Posting Email");
		
		// Set smpt properties
		Properties props = new Properties();

		if (emailProvider.equalsIgnoreCase("GMAIL")) {
			props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");
			props.put("mail.smtp.ssl.protocols", "TLSv1.2");
		}
		
		// Create session and password authentication
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, fromPassword);
			}
		});

		Message msg = new MimeMessage(session);

		Address[] replyToAddr = new Address[1];

		try {
			replyToAddr[0] = new InternetAddress(replyTo);
			InternetAddress addressFrom = new InternetAddress(fromEmail);
			addressFrom.setPersonal("Inhabitr");
			InternetAddress addressTo = new InternetAddress(recipient);
			msg.setFrom(addressFrom);
			msg.setRecipient(Message.RecipientType.TO, addressTo);
			msg.setSubject(subject);
			msg.setContent(message, contentType);
			msg.setReplyTo(replyToAddr);
			Transport.send(msg);
		} catch (AddressException e) {
			logger.error("Address Exception {}", e.getMessage());
		} catch (MessagingException e) {
			logger.error("Messaging Exception {}", e.getMessage());
		} catch (UnsupportedEncodingException e) {
			logger.error("Unsupported Encoding Exception {}", e.getMessage());
		}
		logger.info("Message sent");
		logger.info("end postmail method");
	}
}

