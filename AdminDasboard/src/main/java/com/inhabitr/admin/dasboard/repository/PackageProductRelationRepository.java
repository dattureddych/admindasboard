package com.inhabitr.admin.dasboard.repository;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


import com.inhabitr.admin.dasboard.model.PackageProductRelationEntity;
@Repository
public interface PackageProductRelationRepository  extends PagingAndSortingRepository<PackageProductRelationEntity, Long> {
	@Query(value = "select * from package_product_relation pp "
			+" inner join package pac on pp.package_id=pac.sgid "
				+ "inner join product p on pp.product_id=p.sgid "
				+ " where pp.package_id= :pacId and pp.status=1 ", nativeQuery = true)
		List<PackageProductRelationEntity> getIncludedItemsList(Integer pacId);
}
