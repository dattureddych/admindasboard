package com.inhabitr.admin.dasboard.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity @Table(name = "cart_token")
public class CartTokenEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	
	private @Column(name = "last_access_datetime") @Temporal(TemporalType.TIMESTAMP) Date lastAccessDate;
    private @Column(name = "token") String token;
    private @Column(name = "brewer_sgid") Long brewerSgid;
    private @Column(name = "status") String status;
    private @Column(name = "location_sgid") Long locationSgid;
    private @Column(name = "city_sgid") Long citySgid;
    private @Column(name = "ops_dashboard_cart_id") Long opsDashboardCartId;
    private @Column(name = "ops_dashboard_order_id") Long opsDashboardOrderId;
    private @Column(name = "ops_dashboard_user_id") Long opsDashboardUserId;
    private @Column(name = "ops_dashboard_cart_status") String opsDashboardCartStatus;
    private @Column(name = "order_sgid") Long  orderSgid;
    private @Column(name = "order_reference") String dashboardOrderReference;
    private @Column(name = "promo_code") String promoCode;
    private @Column(name = "staging_fee_flag") Boolean stagingFeeFlag;
    private @Column(name = "is_affirm_success") Boolean isAffirmSuccess;
    private @Column(name = "reward_transaction_id") Long rewardTransactionSgid;
    private @Column(name = "brewer_reward_sgid") Long rewardSgid;
    private @Column(name = "reward_amount") BigDecimal rewardAmount;
    private @Column(name = "cart_total_amount") BigDecimal cartTotalAmount;
    private @Column(name = "payment_type") String paymentType;
    private @Column(name = "payment_amount") BigDecimal cartPaymentAmount;
    private @Column(name = "affirm_total_payment_amount") BigDecimal affirmtotalPaymentAmount;
    
    
	@ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "cart_sgid")
	@JsonIgnore
    private CartEntity cartEntity;
	
   // @OneToOne(fetch = FetchType.LAZY) @JoinColumn(name = "order_sgid")
   // @OneToOne(fetch = FetchType.LAZY) @JoinColumn(name = "order_sgid", insertable=false, updatable=false)
   // private OrderEntity orders;
     
	public CartEntity getCartEntity() {
		return cartEntity;
	}
	public void setCartEntity(CartEntity cartEntity) {
		this.cartEntity = cartEntity;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Date getLastAccessDate() {
		return lastAccessDate;
	}
	public void setLastAccessDate(Date lastAccessDate) {
		this.lastAccessDate = lastAccessDate;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getLocationSgid() {
		return locationSgid;
	}
	public void setLocationSgid(Long locationSgid) {
		this.locationSgid = locationSgid;
	}
	public Long getCitySgid() {
		return citySgid;
	}
	public void setCitySgid(Long citySgid) {
		this.citySgid = citySgid;
	}
	public Long getOrderSgid() {
		return orderSgid;
	}
	public void setOrderSgid(Long orderSgid) {
		this.orderSgid = orderSgid;
	}
	public Long getOpsDashboardCartId() {
		return opsDashboardCartId;
	}
	public void setOpsDashboardCartId(Long opsDashboardCartId) {
		this.opsDashboardCartId = opsDashboardCartId;
	}
	public String getOpsDashboardCartStatus() {
		return opsDashboardCartStatus;
	}
	public void setOpsDashboardCartStatus(String opsDashboardCartStatus) {
		this.opsDashboardCartStatus = opsDashboardCartStatus;
	}
	public String getDashboardOrderReference() {
		return dashboardOrderReference;
	}
	public void setDashboardOrderReference(String dashboardOrderReference) {
		this.dashboardOrderReference = dashboardOrderReference;
	}
	public String getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	public Boolean getStagingFeeFlag() {
		return stagingFeeFlag;
	}
	public void setStagingFeeFlag(Boolean stagingFeeFlag) {
		this.stagingFeeFlag = stagingFeeFlag;
	}
	public Long getOpsDashboardOrderId() {
		return opsDashboardOrderId;
	}
	public void setOpsDashboardOrderId(Long opsDashboardOrderId) {
		this.opsDashboardOrderId = opsDashboardOrderId;
	}
	public Long getOpsDashboardUserId() {
		return opsDashboardUserId;
	}
	public void setOpsDashboardUserId(Long opsDashboardUserId) {
		this.opsDashboardUserId = opsDashboardUserId;
	}
	public Boolean getIsAffirmSuccess() {
		return isAffirmSuccess;
	}
	public void setIsAffirmSuccess(Boolean isAffirmSuccess) {
		this.isAffirmSuccess = isAffirmSuccess;
	}
	public Long getRewardTransactionSgid() {
		return rewardTransactionSgid;
	}
	public void setRewardTransactionSgid(Long rewardTransactionSgid) {
		this.rewardTransactionSgid = rewardTransactionSgid;
	}
	public Long getRewardSgid() {
		return rewardSgid;
	}
	public void setRewardSgid(Long rewardSgid) {
		this.rewardSgid = rewardSgid;
	}
	public BigDecimal getRewardAmount() {
		return rewardAmount;
	}
	public void setRewardAmount(BigDecimal rewardAmount) {
		this.rewardAmount = rewardAmount;
	}
	public BigDecimal getCartTotalAmount() {
		return cartTotalAmount;
	}
	public void setCartTotalAmount(BigDecimal cartTotalAmount) {
		this.cartTotalAmount = cartTotalAmount;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public BigDecimal getCartPaymentAmount() {
		return cartPaymentAmount;
	}
	public void setCartPaymentAmount(BigDecimal cartPaymentAmount) {
		this.cartPaymentAmount = cartPaymentAmount;
	}
	public BigDecimal getAffirmtotalPaymentAmount() {
		return affirmtotalPaymentAmount;
	}
	public void setAffirmtotalPaymentAmount(BigDecimal affirmtotalPaymentAmount) {
		this.affirmtotalPaymentAmount = affirmtotalPaymentAmount;
	}
	public Long getSgid() {
		return sgid;
	}
	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}
	public Long getBrewerSgid() {
		return brewerSgid;
	}
	public void setBrewerSgid(Long brewerSgid) {
		this.brewerSgid = brewerSgid;
	}
		
}
