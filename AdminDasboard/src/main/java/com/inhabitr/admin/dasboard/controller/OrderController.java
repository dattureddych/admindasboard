package com.inhabitr.admin.dasboard.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.inhabitr.admin.dasboard.model.BankEntity;
import com.inhabitr.admin.dasboard.model.BrewerContactEntity;
import com.inhabitr.admin.dasboard.model.BrewerEntity;
import com.inhabitr.admin.dasboard.model.BrewerRewardEntity;
import com.inhabitr.admin.dasboard.model.BrewerRewardTransactionEntity;
import com.inhabitr.admin.dasboard.model.CardEntity;
import com.inhabitr.admin.dasboard.model.CardPaymentRequestResponseEntity;
import com.inhabitr.admin.dasboard.model.CartEntity;
import com.inhabitr.admin.dasboard.model.CartItemEntity;
import com.inhabitr.admin.dasboard.model.CartOptionsEntity;
import com.inhabitr.admin.dasboard.model.CartPackageProductsEntity;
import com.inhabitr.admin.dasboard.model.CartTokenEntity;
import com.inhabitr.admin.dasboard.model.ConfigurationEntity;
import com.inhabitr.admin.dasboard.model.ItemEntity;
import com.inhabitr.admin.dasboard.model.ItemType;
import com.inhabitr.admin.dasboard.model.OrderEntity;
import com.inhabitr.admin.dasboard.model.PromoCodesEntity;
import com.inhabitr.admin.dasboard.model.TwoTapCartRequestResponseEntity;
import com.inhabitr.admin.dasboard.model.CartShippingAddressEntity;
import com.inhabitr.admin.dasboard.model.ZincOrderEntity;
import com.inhabitr.admin.dasboard.model.ZipcodeEntity;
import com.inhabitr.admin.dasboard.repository.CardRepository;
import com.inhabitr.admin.dasboard.repository.ZipcodeRepository;
import com.inhabitr.admin.dasboard.response.SearchResponse;
import com.inhabitr.admin.dasboard.services.BrewerService;
import com.inhabitr.admin.dasboard.services.CartItemService;
import com.inhabitr.admin.dasboard.services.CartOptionsService;
import com.inhabitr.admin.dasboard.services.CartService;
import com.inhabitr.admin.dasboard.services.CartTokenService;
import com.inhabitr.admin.dasboard.services.ConfigurationService;
//import com.inhabitr.admin.dasboard.services.ConfigurationService;
import com.inhabitr.admin.dasboard.services.OrderService;
import com.inhabitr.admin.dasboard.services.SearchService;
import com.inhabitr.admin.dasboard.services.TwoTapCartRequestResponseService;
import com.inhabitr.admin.dashboard.commons.exception.InhabitrException;
import com.inhabitr.admin.dashboard.dto.CartData;

//import com.inhabitr.admin.dashboard.commons.exception.KoinPlusCommonExceptionCode;

import com.koinplus.common.util.KoinPlusDateUtil;
import com.koinplus.common.util.KoinPlusStringUtils;
import com.koinplus.common.webservice.GenericRestClient;

//import com.koinplus.common.util.KoinPlusStringUtils;

import com.stripe.model.Charge;

@RestController
@CrossOrigin
@RequestMapping("/apps/api/order")
public class OrderController {
	@Autowired
	private OrderService service;

	@Autowired
	private SearchService searchService;
	
	@Autowired
	private GenericRestClient genericRestClient;
	
	@Autowired 
	private KoinPlusDateUtil koinPlusDateUtil;
	@Autowired 
	private KoinPlusStringUtils koinPlusStringUtils;
	
	//@Autowired
	//private KoinPlusCommonExceptionCode koinPlusCommonExceptionCode;
	@Autowired
	private CartService cartService;
    @Autowired
    private CartTokenService cartTokenService; 
    
    @Autowired
    private BrewerService brewerService; 
	
	@Autowired
	private CartItemService cartItemService;
	@Autowired 
	private CardRepository cardRepository;
	
	@Autowired 
	private CartOptionsService cartOptionsService;
	
	@Autowired
	private ConfigurationService configurationService;
	@Autowired
	private ZipcodeRepository zipcodeDAO;
	
	@Autowired
	private TwoTapCartRequestResponseService twoTapCartRequestResponseService;
	
	private final String OPS_DB_API_END_POINT = "https://apitest.inhabitr.com";//test
	//private final String OPS_DB_API_END_POINT = "https://apps.inhabitr.com";
	//private GenericRestClient genericRestClient;
	//private KoinPlusStringUtils koinPlusStringUtils;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}

	@PostMapping("/")
	public List<ZincOrderEntity> getAllOrder(@RequestParam(defaultValue = "") Boolean isRegistered,
			@RequestParam(defaultValue = "") String status, @RequestParam(defaultValue = "") String searchStr,
			@RequestParam(defaultValue = "0") Integer pageNo, @RequestParam(defaultValue = "10") Integer pageSize,
			@RequestParam(defaultValue = "asc") String sortDir,
			@RequestParam(defaultValue = "order_sgid") String sortBy) {
		getLogger().info("Inside /apps/api/order/?pageNo=" + pageNo + "&pageSize=" + pageSize);
		List<ZincOrderEntity> ll = (List<ZincOrderEntity>) service.searchByFilters(isRegistered, status, searchStr,
				pageNo, pageSize, sortDir, sortBy);
		List<CartEntity> cartList=new ArrayList<CartEntity>();
		for (ZincOrderEntity zinOrder : ll) {
			OrderEntity orderObj= zinOrder.getOrders();
			if(orderObj!=null) {
			cartList = cartService.getCart(orderObj.getCustomerSgid());
			orderObj.setCartEntity(cartList.get(0));
		 }	
		}
		return ll;
	}

	@PostMapping("/search/{name}")
	public ResponseEntity<List<SearchResponse>> search(@PathVariable("name") String name) {
		getLogger().info("Inside /apps/api/order/search/" + name);
		return new ResponseEntity<List<SearchResponse>>(searchService.searchOrder(name), HttpStatus.OK);
	}

	@GetMapping("/searchItem/")
	public List<ZincOrderEntity> getByStock(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(defaultValue = "asc") String sortDir,
			@RequestParam(defaultValue = "firstName") String sortBy,
			@RequestParam(defaultValue = "1") Integer stockAvailibilty,
			@RequestParam(defaultValue = "") Integer categoryId) {
		getLogger().info("Inside /apps/api/order/?pageNo=" + pageNo + "&pageSize=" + pageSize);
		List<ZincOrderEntity> l1 = service.getAllOrder(pageNo, pageSize, sortDir, sortBy);
		return l1;
	}

	@PostMapping("/getAllOrderStatus/")
	public Map<String, String> getAllOrderStatus() {
		getLogger().info("Inside /apps/api/order/orderStatus/");
		Map<String, String> ll = (Map<String, String>) service.getAllOrderStatus();
		return ll;
	}
	
	
	/*public Map<String, Object> createStudentFurnitureOpsDBOrder(@RequestParam(defaultValue = "") Long sgid,
			String deliveryDate, String deliveryDate1,
			String deliveryDate2, String deliveryTime, String deliveryTime1, String deliveryTime2,
		//	CartEntity cartEntity, ShippingAddressData shippingAddressData, 
		    Boolean checkLeaseAggrement,
		//	CartData cartData, BankEntity bank, CardEntity card, 
			String paymentType,
			Map<String, Object> affirmChargeObject, Map<String, Object> affirmCaptureObject, String paymentToken
		//	,BrewerRewardTransactionEntity brewerRewardTransactionEntity, 
			,Charge charge
			) throws InhabitrException {*/
	
	
/*@PutMapping("/syncOps/")
	public String placeOrder(@RequestParam("brewerId") Long brewerId, @RequestParam("cartId") Long cartId, @RequestParam("token") String token, 
	        @RequestParam("checkLeaseAggrement") Boolean checkLeaseAggrement,
	        @RequestParam("accountId") Long accountId, @RequestParam("paymentType") String paymentType,
	        @RequestParam("location") Long location, @RequestParam("zipcode") Integer zipcode, @RequestParam("cityId") Long cityId,
	        @RequestParam("propertyId") Long propertyId) throws InhabitrException{
	*/

	@PutMapping("/syncOpsOrder")
	public String syncOrders(@RequestParam(defaultValue = "")Long orderSgid,@RequestParam(defaultValue = "")Long brewerId,@RequestParam(defaultValue = "") Long cartId,
			@RequestParam(defaultValue = "") String token,@RequestParam(defaultValue = "")  Boolean checkLeaseAggrement,
			@RequestParam(defaultValue = "") Long accountId,@RequestParam(defaultValue = "") String paymentType,@RequestParam(defaultValue = "") Long location,
			@RequestParam(defaultValue = "") Integer zipcode,@RequestParam(defaultValue = "") Long cityId,
			@RequestParam(defaultValue = "") Long propertyId) {
	try {
	
			CartData cartData = getUsedFurniturePurchase(orderSgid,brewerId, cartId, token, checkLeaseAggrement, accountId,
					paymentType, location, zipcode, cityId, propertyId);
			/*	if (cartResponse != null) {
					return Response.status(Status.OK).entity(cartResponse).build();
				} else {
					String message = "{\"message\":\"Something Went Wrong Please Try Again.\"}";
					return Response.ok(message).build();
				}*/
			} catch (InhabitrException ex) {
				getLogger().error("Could place Order - {}", ex.getCode());
				String message = "{\"message\":\"" + ex.getCode() + "\"}";
				
				return "failure";
			
	} 
		return "success";
}


	public CartData getUsedFurniturePurchase(Long orderSgid,Long brewerId, Long cartId, String token, Boolean checkLeaseAggrement,
			Long accountId, String paymentType, Long location, Integer zipcode, Long cityId, Long propertyId) throws InhabitrException {
		CartData responseCartData = new CartData();
		BigDecimal totalPrice = BigDecimal.ZERO;
		Integer totalQty = 0;
		String responseMessage="";
		Long opsDashboardId= this.service.findMaxDashboardOrderId();
		List<CartEntity> cartEntitys = this.cartService.getCart(brewerId);
		List<ZincOrderEntity> orderList= this.service.getOrderDetail(orderSgid);
		if(paymentType != null && paymentType.contains("AFFIRM")) {
			paymentType="CARD";
		}
		if (zipcode != null) {
			
			try {
				if (CollectionUtils.isNotEmpty(cartEntitys)) {
					String orderNo = null;
					CartEntity cartEntity = cartEntitys.get(0);
					CartTokenEntity cartTokenEntity =this.cartTokenService.getActiveCart(cartEntity.getSgid(),
							"IS_AVAILABLE", location);
					if (cartTokenEntity != null) {
						cartEntity.setToken(cartTokenEntity.getToken());
						responseCartData.setIsAffirmSuccess(cartTokenEntity.getIsAffirmSuccess());
						//this.cartDAO.update(cartEntity);
					} else {
						throw new InhabitrException();
					}
					List<CartItemEntity> cartItemsData = new ArrayList<CartItemEntity>();
					List<CartItemEntity> trendbrewCartItemEntitys = this.cartItemService.findCartItemDetails(
							cartEntity.getSgid(), Boolean.TRUE, Boolean.TRUE, null, "ORDER_CREATED");
					boolean CART_STATUS = Boolean.TRUE;
					if (CollectionUtils.isNotEmpty(trendbrewCartItemEntitys)) {
						BigDecimal rentTotalCartPrice = BigDecimal.ZERO;
						BigDecimal buyTotalCartPrice = BigDecimal.ZERO;
						BigDecimal totalCheckoutPrice = BigDecimal.ZERO;
						BigDecimal affirmSubTotalPrice = BigDecimal.ZERO;
						BigDecimal affirmTotalPrice = BigDecimal.ZERO;
						Boolean checkHasRent = Boolean.FALSE;
						String opsDbCartId = null;
						Integer opsCartItemId = null;
						Map<String, Object> opsDBCartResponse = null;
						try {
							opsDBCartResponse = this.createOPsDBStudentCart(cartEntity,
									trendbrewCartItemEntitys, cartTokenEntity.getToken(), cityId);
							if (MapUtils.isNotEmpty(opsDBCartResponse) && opsDBCartResponse.containsKey("cart")) {
								Map<String, Object> opsDBCart = (Map<String, Object>) opsDBCartResponse.get("cart");
								if (MapUtils.isNotEmpty(opsDBCart) && opsDBCart.containsKey("id")) {
									opsDbCartId = opsDBCart.get("id").toString();
									cartTokenEntity.setOpsDashboardCartId(Long.valueOf(opsDbCartId));
									cartTokenEntity
											.setOpsDashboardCartStatus("OPS_CART_SUCCESS");
									//this.cartTokenDAO.update(cartTokenEntity);
								}
							}
						} catch (Exception e) {
							cartTokenEntity.setOpsDashboardCartStatus("OPS_CART_FAILED");
							//this.cartTokenDAO.update(cartTokenEntity);
						}
						for (CartItemEntity cartItemEntity : trendbrewCartItemEntitys) {
							if (MapUtils.isNotEmpty(opsDBCartResponse) && opsDBCartResponse.containsKey("cart")) {
								List<Map<String, Object>> cartItems = (List<Map<String, Object>>) opsDBCartResponse
										.get("cartItems");
								if (CollectionUtils.isNotEmpty(cartItems)) {
									for (Map<String, Object> cartItemOps : cartItems) {
										if (cartItemOps != null && cartItemOps.containsKey("sku")
												&& cartItemEntity.getSku().equals(cartItemOps.get("sku").toString())) {
											
											opsCartItemId = (Integer) cartItemOps.get("item_id");
											if (opsCartItemId != null && opsCartItemId != 0) {
												cartItemEntity.setOpsDbCartItemSgid(Long.valueOf(opsCartItemId));
												cartItemEntity
														.setOpsDbCartStatus("OPS_CART_SUCCESS");
												//this.cartItemDAO.update(cartItemEntity);
												if (cartItemEntity.getItemType() != null
														&& cartItemEntity.getItemType().equals(ItemType.PRODUCT)) {

													List<CartPackageProductsEntity> cartPackageProductsEntity = cartItemEntity
															.getCartPackageProducts();
													CartPackageProductsEntity cartPackageProductEntity = cartPackageProductsEntity
															.get(0);
													cartPackageProductEntity.setOpsDbCartPackageProductsSgid(
															Long.valueOf(opsCartItemId));
													

												} else if (cartItemEntity.getItemType() != null
														&& cartItemEntity.getItemType().equals(ItemType.PACKAGE)) {
													Map<String, Object> packageProductsMap = new HashMap<String, Object>();
													if (cartItemOps != null
															&& cartItemOps.containsKey("packageItems")) {
														List<Map<String, Object>> cartOpsPackageItems = (List<Map<String, Object>>) cartItemOps
																.get("packageItems");
														for (Map<String, Object> cartOpsPackageItem : cartOpsPackageItems) {
															packageProductsMap.put(
																	cartOpsPackageItem.get("sku").toString(),
																	cartOpsPackageItem.get("item_id"));
														}
													}

												}
											}
										}
									}
								}
							}
							if (cartItemEntity.getItemQuantity() != null && cartItemEntity.getItemQuantity() != 0) {
								totalQty = totalQty + cartItemEntity.getItemQuantity();
							}
							if (!(cartItemEntity.getCheckoutFlag() && cartItemEntity.getStatus()
									.equalsIgnoreCase("INITIATED"))) {
								CART_STATUS = Boolean.FALSE;
							}
							if (cartItemEntity.getIsRent()) {
								checkHasRent = Boolean.TRUE;
								rentTotalCartPrice = rentTotalCartPrice.add(cartItemEntity.getTotalFinalPrice());
							} else {
								buyTotalCartPrice = buyTotalCartPrice.add(cartItemEntity.getTotalFinalPrice());
							}
							if (cartItemEntity.getTotalFinalPrice() != null) {
								totalCheckoutPrice = totalCheckoutPrice.add(cartItemEntity.getTotalFinalPrice());
							}
							if (cartItemEntity.getItemAffirmTotalPrice() != null) {
								affirmTotalPrice = affirmTotalPrice.add(cartItemEntity.getItemAffirmTotalPrice());
							}
						}
						CartData cart = new CartData();
						cart.setId(cartEntity.getSgid());
						cart.setBuyTotalPrice(buyTotalCartPrice);
						cart.setRentTotalPrice(rentTotalCartPrice);
						cart.setTotalPrice(totalCheckoutPrice);
						cart.setTotalAffirmPrice(affirmTotalPrice);
						if (rentTotalCartPrice != null && buyTotalCartPrice != null) {
							cart.setSubTotalPrice(buyTotalCartPrice.add(rentTotalCartPrice));
						} else {
							cart.setSubTotalPrice(totalCheckoutPrice);
						}
						responseCartData.setBuyTotalPrice(buyTotalCartPrice);
						responseCartData.setRentTotalPrice(rentTotalCartPrice);

						if (cartTokenEntity.getPromoCode() != null) {
							Map<String, Object> applypromocodeResponseMap = this.applyPromocode(cartTokenEntity.getToken(), cartTokenEntity.getPromoCode());
							if (MapUtils.isNotEmpty(applypromocodeResponseMap)) {
								if (applypromocodeResponseMap.containsKey("statusCode")
										&& applypromocodeResponseMap.get("statusCode") != null && Integer.parseInt(
												applypromocodeResponseMap.get("statusCode").toString()) != 200) {
								} else {
									Map<String, Object> promocodeResponseMap = this.caliculatePromocodeDiscount(
											brewerId, cartId, cartTokenEntity.getPromoCode(), Boolean.FALSE,
											trendbrewCartItemEntitys);
									if (MapUtils.isNotEmpty(promocodeResponseMap)
											&& promocodeResponseMap.containsKey("discount")
											&& promocodeResponseMap.get("discount") != null
											&& promocodeResponseMap.containsKey("promocodeSgid")
											&& promocodeResponseMap.get("promocodeSgid") != null) {
										BigDecimal discount = new BigDecimal(
												promocodeResponseMap.get("discount").toString());
										cart.setPromocodeDiscountAmount(discount);
										cart.setPromocodesgid(
												Long.valueOf(promocodeResponseMap.get("promocodeSgid").toString()));
										cart.setPromocode(cartTokenEntity.getPromoCode());
										responseCartData.setPromocode(cartTokenEntity.getPromoCode());
										responseCartData.setPromocodeDiscountAmount(discount);
									}
								}
							}
							/*
							 * CartData applyPromocode =
							 * this.applyPromocode(brewerId, cartId, promocode,
							 * true, null, CartStatusType.ORDER_CREATED.name(),
							 * location, cartTokenEntity.getSgid());
							 * 
							 * totalCheckoutPrice =
							 * applyPromocode.getTotalPrice();
							 */

						}
						CartData cartCaliculationData = this.caliculateStudentFurnitiureCartFee(cart, checkHasRent,
								Boolean.FALSE, null, cartTokenEntity.getSgid(), zipcode, totalQty);
						totalCheckoutPrice = cartCaliculationData.getTotalPrice();
						affirmTotalPrice = cartCaliculationData.getTotalAffirmPrice();
						cartTokenEntity.setCartTotalAmount(totalCheckoutPrice);
						responseCartData.setTotalPrice(totalCheckoutPrice);

						if (totalCheckoutPrice.compareTo(BigDecimal.ZERO) == 1) {
							totalPrice = totalCheckoutPrice;
							Charge charge = null;
							Map<String, Object> affirmChargeResponse = null;
							Map<String, Object> affirmCaptureResponse = null;
							String chargeId = null;
							BankEntity bankEntity = null;
							BrewerRewardEntity rewardEntity = new BrewerRewardEntity();
							BrewerRewardTransactionEntity brewerRewardTransactionEntity = new BrewerRewardTransactionEntity();
							BrewerEntity brewer = new BrewerEntity();
							BigDecimal rewardAmount = BigDecimal.ZERO;
							if (cartEntity.getBrewer() != null) {
								brewer = cartEntity.getBrewer();
							} else if (brewerId != null) {
								brewer = this.brewerService.find(brewerId);
							}
							/*if (brewer != null && brewer.getTotalRewardBalance() != null
									&& brewer.getTotalRewardBalance()>0) {
								rewardAmount = brewer.getTotalRewardBalance().b;
							}*/
							if (paymentType != null && paymentType.contains("REWARD")) {
								if (rewardAmount != null && cartTokenEntity.getRewardAmount() != null
										&& cartTokenEntity.getRewardAmount().compareTo(BigDecimal.ZERO) == 1) {

									List<BrewerRewardEntity> rewardEntityList =null;
											//this.brewerRewardDAO.getUserRewards(brewerId, null, null);
									if (CollectionUtils.isNotEmpty(rewardEntityList)) {
										rewardEntity = rewardEntityList.get(0);
										if (rewardEntity != null) {
											if(propertyId!=null){
												brewerRewardTransactionEntity.setPropertySgid(propertyId);
											}else{
											brewerRewardTransactionEntity.setPropertySgid(rewardEntity.getPropertySgid());
											}
											brewerRewardTransactionEntity.setResidentId(rewardEntity.getResidentId());
											 // brewerRewardTransactionEntity.setBrewerRewardSgid(rewardEntity.getSgid());
											brewerRewardTransactionEntity.setPropertyUserRewardSgid(rewardEntity.getPropertyUserRewardSgid());
											//brewerRewardTransactionEntity.setRewardId(rewardEntity.getRewardId());
											brewerRewardTransactionEntity.setPropertyUserSgid(rewardEntity.getPropertyUserSgid());
											brewerRewardTransactionEntity.setCartTotalAmount(totalCheckoutPrice);
										}
									}
									responseCartData.setRewardAmount(rewardAmount);
									responseCartData.setRewardSgid(cartTokenEntity.getRewardSgid());
									if (totalCheckoutPrice.compareTo(rewardAmount) == 1) {
										BigDecimal totalAmount = totalCheckoutPrice.subtract(rewardAmount).setScale(0,
												RoundingMode.HALF_UP);
										responseCartData.setTotalCartPrice(totalAmount);
										totalPrice = totalAmount;
										totalCheckoutPrice = totalAmount;
										cartTokenEntity.setRewardAmount(rewardAmount);
									} else if (totalCheckoutPrice.compareTo(rewardAmount) == 0) {
										BigDecimal totalAmount = totalCheckoutPrice.subtract(rewardAmount).setScale(0,
												RoundingMode.HALF_UP);
										responseCartData.setTotalCartPrice(totalAmount);
										totalPrice = totalAmount;
										totalCheckoutPrice = totalAmount;
										cartTokenEntity.setRewardAmount(rewardAmount);
									} else {
										responseCartData.setRewardAmount(totalCheckoutPrice);
										cartTokenEntity.setRewardAmount(totalCheckoutPrice);
										rewardAmount = totalCheckoutPrice;
										responseCartData.setTotalCartPrice(BigDecimal.ZERO);
										totalPrice = BigDecimal.ZERO;
										totalCheckoutPrice = BigDecimal.ZERO;

									}

									brewerRewardTransactionEntity.setBrewerSgid(brewerId);
									brewerRewardTransactionEntity.setPaymentStatus("PENDING");
									brewerRewardTransactionEntity.setPaymentType("REWARD_PAYMENT");
									brewerRewardTransactionEntity
											.setPaymentToken(RandomStringUtils.randomAlphanumeric(20).toUpperCase());
									brewerRewardTransactionEntity.setRewardTransactionAmount(rewardAmount);
								//	brewerRewardTransactionEntity = this.brewerRewardTransactionDAO.insert(brewerRewardTransactionEntity);
									cartTokenEntity.setRewardTransactionSgid(brewerRewardTransactionEntity.getSgid());
									cartTokenEntity.setCartPaymentAmount(totalCheckoutPrice);
									if (paymentType.contains("CARD")) {
										cartTokenEntity.setPaymentType("CARD");
									} else if (paymentType.contains("BANK")) {
										cartTokenEntity.setPaymentType("BANK");
									} else if (paymentType.contains("AFFIRM")) {
										cartTokenEntity.setPaymentType("AFFIRM");
										if(affirmTotalPrice!=null && affirmTotalPrice.compareTo(BigDecimal.ZERO)==1){
											cartTokenEntity.setAffirmtotalPaymentAmount(affirmTotalPrice);
										}
									}
									//this.cartTokenDAO.update(cartTokenEntity);

								}
							}
							if (totalCheckoutPrice != null && totalCheckoutPrice.compareTo(BigDecimal.ZERO) == 1) {
								if (paymentType != null && paymentType.contains("BANK") && accountId != null) {
									
								} else if (paymentType != null && paymentType.contains("CARD")) {
								
								} else if (paymentType != null && paymentType.contains("AFFIRM")) {
									try {
										

										if (MapUtils.isNotEmpty(affirmChargeResponse)) {
												if(affirmChargeResponse.containsKey("amount") && affirmChargeResponse.get("amount")!=null){
													cartTokenEntity.setPaymentType("AFFIRM");
													BigDecimal affirmTotalPayMentAmount = new BigDecimal(affirmChargeResponse.get("amount").toString());
													if(affirmTotalPayMentAmount!=null && affirmTotalPayMentAmount.compareTo(BigDecimal.ZERO)==1){
														cartTokenEntity.setAffirmtotalPaymentAmount(affirmTotalPayMentAmount.divide(new BigDecimal(100)));
													}
												//	this.cartTokenDAO.update(cartTokenEntity);
												}
											
											if (affirmChargeResponse.containsKey("id")) {
												chargeId = affirmChargeResponse.get("id").toString();
											}
										
										} else {
											cartTokenEntity.setIsAffirmSuccess(Boolean.FALSE);
										//	this.cartTokenDAO.update(cartTokenEntity);
											responseCartData.setIsAffirmSuccess(Boolean.FALSE);
											responseCartData.setId(cartEntity.getSgid());
											//responseCartData.setUpdatedDate(cartEntity.getLastUpdate());
											responseCartData.setLastAccessDate(cartEntity.getLastAccessDate());
											responseCartData.setTotalPrice(totalPrice);
											responseCartData.setNumCartItems(new Long(cartItemsData.size()));
											responseCartData.setOrderNumber(orderNo);
											//responseCartData.setCartItem(cartItemsData);
											responseCartData
													.setOpsDasdBoardcartId(cartTokenEntity.getOpsDashboardCartId());
											//return responseCartData;
										}
										
									} catch (Exception e) {
										cartTokenEntity.setIsAffirmSuccess(Boolean.FALSE);
									//	this.cartTokenDAO.update(cartTokenEntity);
										responseCartData.setIsAffirmSuccess(Boolean.FALSE);
										responseCartData.setId(cartEntity.getSgid());
										//responseCartData.setUpdatedDate(cartEntity.getLastUpdate());
										responseCartData.setLastAccessDate(cartEntity.getLastAccessDate());
										responseCartData.setTotalPrice(totalPrice);
										responseCartData.setNumCartItems(new Long(cartItemsData.size()));
										responseCartData.setOrderNumber(orderNo);
									//	responseCartData.setCartItem(cartItemsData);
										responseCartData.setOpsDasdBoardcartId(cartTokenEntity.getOpsDashboardCartId());
										//return responseCartData;
									}

								}
							}

							//List<CartShippingAddressEntity> shippingAdressEntity = this.shippingAddressDAO
								//	.getCartShippingAddress(cartEntity.getSgid(), true);

							Map<String, Object> orderResponse = new HashMap<String, Object>();
						//	OrderData orderData = new OrderData();
						//	List<CartOptionsEntity> cartOptionsEntitys = this.cartOptionsDAO
								//	.getCartOptions(cartEntity.getSgid(), cartTokenEntity.getSgid(), Boolean.FALSE);

							Date date1 = this.addDay(8, new Date().getTime());
							Date date2 = this.addDay(9, new Date().getTime());
							Date date3 = this.addDay(10, new Date().getTime());

							if (date1.getDay() == 0) {
								date1 = this.addDay(9, new Date().getTime());
								date2 = this.addDay(10, new Date().getTime());
								date3 = this.addDay(11, new Date().getTime());
							}
							if (date2.getDay() == 0) {
								date2 = this.addDay(10, new Date().getTime());
								date3 = this.addDay(11, new Date().getTime());
							}
							if (date3.getDay() == 0) {
								date3 = this.addDay(11, new Date().getTime());
							}
							SimpleDateFormat sDF = new SimpleDateFormat("dd/MM/YYYY");

							String d1 = date1.getTime() + "";
							String d2 = date2.getTime() + "";
							String d3 = date3.getTime() + "";

							// if (charge != null && charge.getStatus() != null)
							// {
							CardEntity addCardInformation = null;
							if (paymentType != null && paymentType.contains("CARD")) {
								//addCardInformation = this.addCardInformation(charge, cartEntity);
							}
							

							orderResponse = this.createStudentFurnitureOpsDBOrder(d1, d2, d3, null, null,
									null, cartEntity, orderList, checkLeaseAggrement, cartCaliculationData,
									bankEntity, addCardInformation, paymentType, affirmChargeResponse,
									affirmCaptureResponse, token, brewerRewardTransactionEntity, charge);
							
							
							JSONObject responseJson = new JSONObject(orderResponse);
							String responseArray=null;
							String[] responseArry=null;							String[] responseArry1=null;
							Map<String, String> orderResponseJson = new HashMap<String, String>();
							if(responseJson!=null){
								if(orderResponse.containsKey("order")) {
									 orderResponse.get("order");
								}
							//	orderResponseJson= (Map<String, String>) orderResponse.get("order").equals("id");
							//	System.out.println(responseArry[0]);//.contentEquals("id");
								opsDashboardId=opsDashboardId+1;
							}
							if(opsDashboardId!=null) {
								responseMessage=	service.updateOrderInfo(opsDashboardId, orderSgid);
							}
							
					
						}
						responseCartData.setMessage(responseMessage);
						responseCartData.setId(cartEntity.getSgid());
						//responseCartData.setUpdatedDate(cartEntity.getLastUpdate());
						responseCartData.setLastAccessDate(cartEntity.getLastAccessDate());
						responseCartData.setTotalPrice(totalPrice);
						responseCartData.setNumCartItems(new Long(cartItemsData.size()));
						responseCartData.setOrderNumber(orderNo);
						//responseCartData.setCartItem(cartItemsData);
						responseCartData.setOpsDasdBoardcartId(cartTokenEntity.getOpsDashboardCartId());
						
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new InhabitrException();
			}
		} else {

		}
		return responseCartData;
	}
	
	
public CardEntity addCardInformation(Charge charge, CartEntity cart) {
	CardEntity carddata = new CardEntity();
	CardEntity card = new CardEntity();
	Map<String, Object> responseMapC = KoinPlusStringUtils.getJsonMap(charge.toJson());
	getLogger().info("CARD DETAILS CHARGE OBJ{}", responseMapC);
	getLogger().info("CARD DETAILS CHARGE OBJ{}", charge);
	try {
		// todo not confirm
		card.setCustomerId(charge.getId());
		if (cart.getBrewer() != null && cart.getBrewer().getSgid() != null) {
			card.setBrewer(cart.getBrewer());
			BrewerEntity brewer = cart.getBrewer();
			getLogger().info("CARD DETAILS SOURCE OBJ{}", charge.getSource());
			if (charge.getSource() != null) {
				Map<String, Object> responseMap = KoinPlusStringUtils.getJsonMap(charge.getSource().toJson());
				getLogger().info("CARD DETAILS {}", responseMap);
				if (MapUtils.isNotEmpty(responseMap) && responseMap.containsKey("name")) {

					if (responseMap.containsKey("id") && responseMap.get("id") != null)
						card.setCardId(responseMap.get("id").toString());

					if (responseMap.containsKey("fingerprint") && responseMap.get("fingerprint") != null)
						card.setFingerprint(responseMap.get("fingerprint").toString());

					if (responseMap.containsKey("last4") && responseMap.get("last4") != null)
						card.setLast4(responseMap.get("last4").toString());

					if (responseMap.containsKey("brand") && responseMap.get("brand") != null)
						card.setSourceBrand(responseMap.get("brand").toString());
					if (responseMap.containsKey("funding") && responseMap.get("funding") != null) {
						card.setCardType("card");
					} else if (responseMap.containsKey("card") && responseMap.get("card") != null) {
						card.setCardType("card");
					}

					if (responseMap.containsKey("country") && responseMap.get("country") != null)
						card.setSourceCountry(responseMap.get("country").toString());

					if (responseMap.containsKey("cvc_check") && responseMap.get("cvc_check") != null)
						card.setSourceCvcCheck(responseMap.get("cvc_check").toString());

					if (responseMap.containsKey("exp_month") && responseMap.get("exp_month") != null)
						card.setExpMonth(Integer.valueOf(responseMap.get("exp_month").toString()));

					if (responseMap.containsKey("exp_year") && responseMap.get("exp_year") != null)
						card.setExpYear(Integer.valueOf(responseMap.get("exp_year").toString()));

					if (responseMap.containsKey("name") && responseMap.get("name") != null)
						card.setAccountHolderName(responseMap.get("name").toString());

					card.setCreatedDatetime(Calendar.getInstance().getTime());
					card.setIsInhSourceCard(0);
					List<CardEntity> cardEntityList = this.cardRepository.getCard(cart.getBrewer().getSgid(),
							responseMap.get("last4").toString());
			        if(cardEntityList!=null) {
					carddata = cardEntityList.get(0);
			        }
				
					CardPaymentRequestResponseEntity cardPaymentRequestResponseEntity = new CardPaymentRequestResponseEntity();
					cardPaymentRequestResponseEntity
							.setCardNumber(Long.valueOf(responseMap.get("last4").toString()));
					cardPaymentRequestResponseEntity.setCart(cart);
					cardPaymentRequestResponseEntity.setBrewer(cart.getBrewer());
					
					cardPaymentRequestResponseEntity.setCardType(responseMap.get("brand").toString());
					cardPaymentRequestResponseEntity.setToken(charge.getId());
					cardPaymentRequestResponseEntity.setIsactive(1);
				
					cardPaymentRequestResponseEntity.setResponseObject(charge.toJson());
				
				}
			}
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
	return carddata;
}

/*private CartData caliculateStudentFurnitiureCartFee(CartData cart, Boolean checkHasRent, Boolean hasReferance,
		String referance, Long cartTokenSgid, Integer zipcode, Integer totalQty) throws InhabitrException {
	CartData cartData = new CartData();
	if (cart != null && cart.getTotalPrice() != null && !cart.getTotalPrice().equals(BigDecimal.ZERO)) {
		cartData = cart;
		BigDecimal addOnsTotalFee = BigDecimal.ZERO;
		BigDecimal totalShippingHandlingFee = BigDecimal.ZERO;
		BigDecimal totalCartPrice = cart.getTotalPrice();
		BigDecimal totalCartAffirmPrice = cart.getTotalAffirmPrice();
		BigDecimal shippingFee = BigDecimal.ZERO;
		BigDecimal stagingFee = BigDecimal.ZERO;
		BigDecimal stagingFeePerMonth = BigDecimal.ZERO;
		Boolean checkHasStagingFeePerMonth = Boolean.FALSE;
		Boolean checkHasStagingFee = Boolean.FALSE;
		List<CartOptionsEntity> cartOptionsEntitys = new ArrayList<CartOptionsEntity>();
		if (hasReferance != null && hasReferance && referance != null) {
			cartOptionsEntitys = this.cartOptionsDAO.getCartOptions(cart.getId(), referance);
		} else {
			cartOptionsEntitys = this.cartOptionsDAO.getCartOptions(cart.getId(), cartTokenSgid, hasReferance);
		}

		if (CollectionUtils.isNotEmpty(cartOptionsEntitys)) {
			Boolean checkRegularShipping = Boolean.TRUE;
			for (CartOptionsEntity cartOptionsEntity : cartOptionsEntitys) {
				if (cartOptionsEntity.getOptionType().equals("checkRegularShipping")) {
					if (cartOptionsEntity.getOptionTypeValue() != null
							&& cartOptionsEntity.getOptionTypeValue().equalsIgnoreCase("false")) {
						checkRegularShipping = Boolean.FALSE;
					}
					break;
				}
			}
			for (CartOptionsEntity cartOptionEntity : cartOptionsEntitys) {
				if (!cartOptionEntity.getOptionType().equals("month")
						&& !cartOptionEntity.getOptionType().equals("checkRegularShipping")) {
					cartData.addCartOptionsMap(cartOptionEntity.getOptionType(),
							new BigDecimal(cartOptionEntity.getOptionTypeValue()));
				}
				if (cartOptionEntity.getOptionType().equals("shippingFee")) {
					shippingFee = new BigDecimal(cartOptionEntity.getOptionTypeValue());

					if (!shippingFee.equals(BigDecimal.ZERO)) {
						Map<String, Object> getOptions = getCartOptions(zipcode);
						if (getOptions.containsKey("SHIPPING & HANDLING FEE")) {
							Map<String, Object> shipMap = (Map<String, Object>) getOptions
									.get("SHIPPING & HANDLING FEE");
							if (checkRegularShipping != null && checkRegularShipping) {
								cartData.addShippingAndHandlingFeeMap("checkRegularShipping", checkRegularShipping);
								if (shipMap.containsKey("CURB_SIDE_DELIVERY") && shippingFee
										.equals(new BigDecimal(shipMap.get("CURB_SIDE_DELIVERY").toString()))) {
									cartData.addCartOptionsMap("SHIPPING_HANDLING_FEE_TYPE", "CURB_SIDE_DELIVERY");
									cartData.addShippingAndHandlingFeeMap("SHIPPING_HANDLING_FEE_TYPE",
											"CURB_SIDE_DELIVERY");
								} else if (shipMap.containsKey("IN_HOME_DELIVERY") && shippingFee
										.equals(new BigDecimal(shipMap.get("IN_HOME_DELIVERY").toString()))) {
									cartData.addCartOptionsMap("SHIPPING_HANDLING_FEE_TYPE", "IN_HOME_DELIVERY");
									cartData.addShippingAndHandlingFeeMap("SHIPPING_HANDLING_FEE_TYPE",
											"IN_HOME_DELIVERY");
								} else {
									if (totalQty != null && totalQty != 0) {
										shippingFee = shippingFee.multiply(new BigDecimal(totalQty)).setScale(2,
												BigDecimal.ROUND_HALF_UP);
									}
									cartData.addCartOptionsMap("SHIPPING_HANDLING_FEE_TYPE",
											"WHITE_GLOVE_AND_ASSEMBLY_DELIVERY");
									cartData.addShippingAndHandlingFeeMap("SHIPPING_HANDLING_FEE_TYPE",
											"WHITE_GLOVE_AND_ASSEMBLY_DELIVERY");
								}
							} else if (checkRegularShipping != null && !checkRegularShipping) {
								cartData.addShippingAndHandlingFeeMap("checkRegularShipping", checkRegularShipping);
								if (shipMap.containsKey("EXPEDITED_CURB_SIDE_DELIVERY") && shippingFee.equals(
										new BigDecimal(shipMap.get("EXPEDITED_CURB_SIDE_DELIVERY").toString()))) {
									cartData.addCartOptionsMap("SHIPPING_HANDLING_FEE_TYPE",
											"EXPEDITED_CURB_SIDE_DELIVERY");
									cartData.addShippingAndHandlingFeeMap("SHIPPING_HANDLING_FEE_TYPE",
											"EXPEDITED_CURB_SIDE_DELIVERY");
								} else if (shipMap.containsKey("EXPEDITED_IN_HOME_DELIVERY") && shippingFee.equals(
										new BigDecimal(shipMap.get("EXPEDITED_IN_HOME_DELIVERY").toString()))) {
									cartData.addCartOptionsMap("SHIPPING_HANDLING_FEE_TYPE",
											"EXPEDITED_IN_HOME_DELIVERY");
									cartData.addShippingAndHandlingFeeMap("SHIPPING_HANDLING_FEE_TYPE",
											"EXPEDITED_IN_HOME_DELIVERY");
								} else {
									if (totalQty != null && totalQty != 0) {
										shippingFee = shippingFee.multiply(new BigDecimal(totalQty)).setScale(2,
												BigDecimal.ROUND_HALF_UP);
									}
									cartData.addCartOptionsMap("SHIPPING_HANDLING_FEE_TYPE",
											"EXPEDITED_WHITE_GLOVE_AND_ASSEMBLY_DELIVERY");
									cartData.addShippingAndHandlingFeeMap("SHIPPING_HANDLING_FEE_TYPE",
											"EXPEDITED_WHITE_GLOVE_AND_ASSEMBLY_DELIVERY");
								}
							} else {
								if (shipMap.containsKey("CURB_SIDE_DELIVERY") && shippingFee
										.equals(new BigDecimal(shipMap.get("CURB_SIDE_DELIVERY").toString()))) {
									cartData.addCartOptionsMap("SHIPPING_HANDLING_FEE_TYPE", "CURB_SIDE_DELIVERY");
									cartData.addShippingAndHandlingFeeMap("SHIPPING_HANDLING_FEE_TYPE",
											"CURB_SIDE_DELIVERY");
								} else if (shipMap.containsKey("IN_HOME_DELIVERY") && shippingFee
										.equals(new BigDecimal(shipMap.get("IN_HOME_DELIVERY").toString()))) {
									cartData.addCartOptionsMap("SHIPPING_HANDLING_FEE_TYPE", "IN_HOME_DELIVERY");
									cartData.addShippingAndHandlingFeeMap("SHIPPING_HANDLING_FEE_TYPE",
											"IN_HOME_DELIVERY");
								} else {
									if (totalQty != null && totalQty != 0) {
										shippingFee = shippingFee.multiply(new BigDecimal(totalQty)).setScale(2,
												BigDecimal.ROUND_HALF_UP);
									}
									cartData.addCartOptionsMap("SHIPPING_HANDLING_FEE_TYPE",
											"WHITE_GLOVE_AND_ASSEMBLY_DELIVERY");
									cartData.addShippingAndHandlingFeeMap("SHIPPING_HANDLING_FEE_TYPE",
											"WHITE_GLOVE_AND_ASSEMBLY_DELIVERY");
								}
							}
						}
						totalCartPrice = totalCartPrice.add(shippingFee);
						if (totalCartAffirmPrice != null) {
							totalCartAffirmPrice = totalCartAffirmPrice.add(shippingFee);
						}
						totalShippingHandlingFee = totalShippingHandlingFee.add(shippingFee);
						cartData.addShippingAndHandlingFeeMap("deliveryFee", shippingFee);
					} else {
						totalCartPrice = totalCartPrice.add(shippingFee);
						if (totalCartAffirmPrice != null) {
							totalCartAffirmPrice = totalCartAffirmPrice.add(shippingFee);
						}
						totalShippingHandlingFee = totalShippingHandlingFee.add(shippingFee);
						cartData.addShippingAndHandlingFeeMap("deliveryFee", shippingFee);
					}
				}

				if (cartOptionEntity.getOptionType().equals("stagingFee")) {
					if (new BigDecimal(cartOptionEntity.getOptionTypeValue()).compareTo(new BigDecimal(0)) == 1) {
						checkHasStagingFee = Boolean.TRUE;
						stagingFee = new BigDecimal(cartOptionEntity.getOptionTypeValue());
					}
				}

				if (cartOptionEntity.getOptionType().equals("stagingFeePerMonth")) {
					if (new BigDecimal(cartOptionEntity.getOptionTypeValue()).compareTo(new BigDecimal(0)) == 1) {
						checkHasStagingFeePerMonth = Boolean.TRUE;
						stagingFeePerMonth = new BigDecimal(cartOptionEntity.getOptionTypeValue());
					}
				}

				if (checkHasRent) {
					if (cartOptionEntity.getOptionType().equalsIgnoreCase("month")) {
						cartData.setLeaseLength(Integer.valueOf(cartOptionEntity.getOptionTypeValue()));
					}
				}
			}

		}
		if (checkHasRent) {
			cartData.setType("RENT");
		} else {
			cartData.setType("BUY");
		}
		if (cart.getBuyTotalPrice() != null && cart.getBuyTotalPrice().compareTo(BigDecimal.ZERO) == 1
				&& checkHasRent && cart.getRentTotalPrice() != null
				&& cart.getRentTotalPrice().compareTo(BigDecimal.ZERO) == 1) {
			cartData.setType("HYBRID");
		}

		// promocode discount value
		if (cart.getPromocodeDiscountAmount() != null) {
			totalCartPrice = totalCartPrice.subtract(cart.getPromocodeDiscountAmount());
			if (totalCartAffirmPrice != null) {
				totalCartAffirmPrice = totalCartAffirmPrice.add(cart.getPromocodeDiscountAmount());
			}
			cartData.setPromocodeDiscountAmount(cart.getPromocodeDiscountAmount());
		}
		if (checkHasStagingFee) {
			cartData.addAddOnFeeMap("stagingFee", stagingFee);
			totalCartPrice = totalCartPrice.add(stagingFee);
			if (totalCartAffirmPrice != null) {
				totalCartAffirmPrice = totalCartAffirmPrice.add(stagingFee);
			}
		}
		if (checkHasStagingFeePerMonth) {
			cartData.addAddOnFeeMap("stagingFeePerMonth", stagingFeePerMonth);
		}

		if (addOnsTotalFee.compareTo(BigDecimal.ZERO) == 1) {
			cartData.addAddOnFeeMap("addOnsTotalFee", addOnsTotalFee.setScale(2, BigDecimal.ROUND_HALF_UP));
		}

		if (totalShippingHandlingFee.compareTo(BigDecimal.ZERO) == 1) {
			cartData.addShippingAndHandlingFeeMap("totalShippingHandlingFee",
					totalShippingHandlingFee.setScale(2, BigDecimal.ROUND_HALF_UP));
		}

		cartData.setTotalPrice(totalCartPrice.setScale(2, BigDecimal.ROUND_HALF_UP));
		if (totalCartAffirmPrice != null && totalCartAffirmPrice.compareTo(BigDecimal.ZERO) == 1) {
			cartData.setTotalAffirmPrice(totalCartAffirmPrice.setScale(2, BigDecimal.ROUND_HALF_UP));
		}
	}
	return cartData;
}*/


public Map<String, Object> createStudentFurnitureOpsDBOrder(String deliveryDate, String deliveryDate1,
		String deliveryDate2, String deliveryTime, String deliveryTime1, String deliveryTime2,
		CartEntity cartEntity, List<ZincOrderEntity> orderList, Boolean checkLeaseAggrement,CartData cartData,
		BankEntity bank,CardEntity card, String paymentType,
		Map<String, Object> affirmChargeObject, Map<String, Object> affirmCaptureObject, String paymentToken,
		BrewerRewardTransactionEntity brewerRewardTransactionEntity, Charge charge) throws InhabitrException {


	
	Map<String, Object> responseMap = null;
	try {
		if (cartEntity != null && cartEntity.getToken() != null && paymentType != null) {
			String endPoint = OPS_DB_API_END_POINT + "/api/student/order/create";
			Map<String, Object> scrapeInfo = new HashMap<String, Object>();
			Map<String, Object> paymentMap = null;

			scrapeInfo.put("payment_mode", paymentType);
			if (bank != null && paymentType != null && paymentType.contains("BANK")) {
				paymentMap = new HashMap<String, Object>();
				paymentMap.put("stripe_customer_id", bank.getCustomerId());
				paymentMap.put("fingerprint", bank.getFingerprint());
				paymentMap.put("card_id", bank.getBankAccountId());
				paymentMap.put("card_type", "bank_account");
				
				paymentMap.put("source_brand", bank.getSourceBankName());
				paymentMap.put("source_country", bank.getSourceCountry());
				paymentMap.put("source_cvc_check", bank.getSourceCvcCheck());//
				paymentMap.put("exp_month", null);
				paymentMap.put("exp_year", null);
				paymentMap.put("last4", bank.getLast4());
				paymentMap.put("routing_number", bank.getRoutingNumber());
				paymentMap.put("account_holder_name", null);
				paymentMap.put("account_holder_type", null);
				paymentMap.put("payment_response", charge);
				scrapeInfo.put("payment_mode", "bank");

				if (paymentMap != null) {
					scrapeInfo.put("payment_bank", paymentMap);
					// payment_stripe
				}
			}
			if (card != null && paymentType != null && paymentType.contains("CARD")) {
				paymentMap = new HashMap<String, Object>();
				paymentMap.put("stripe_customer_id", card.getCustomerId());
				paymentMap.put("fingerprint", card.getFingerprint());
				paymentMap.put("card_id", card.getCardId());
				paymentMap.put("card_type", card.getCardType());
				paymentMap.put("source_brand", card.getSourceBrand());
				paymentMap.put("source_country", card.getSourceCountry());
				paymentMap.put("source_cvc_check", card.getSourceCvcCheck());
				paymentMap.put("exp_month", card.getExpMonth());
				paymentMap.put("exp_year", card.getExpYear());
				paymentMap.put("last4", card.getLast4());
				paymentMap.put("routing_number", null);
				paymentMap.put("account_holder_name", null);
				paymentMap.put("account_holder_type", null);
				paymentMap.put("payment_response", charge);
				scrapeInfo.put("payment_mode", "stripe");
				if (paymentMap != null) {
					scrapeInfo.put("payment_stripe", paymentMap);
					// payment_stripe
				}
			}

		/*	if (paymentType != null && paymentType.contains("AFFIRM")) {
				scrapeInfo.put("payment_mode", "affirm");
				paymentMap = new HashMap<String, Object>();
				paymentMap.put("affirm_charge_response", affirmChargeObject);
				paymentMap.put("affirm_capture_response", affirmCaptureObject);
				scrapeInfo.put("payment_affirm", paymentMap);

				if (paymentToken != null) {
					scrapeInfo.put("payment_token", paymentToken);
				}
			}

			if (paymentType != null && paymentType.contains("REWARD") && brewerRewardTransactionEntity != null
					&& brewerRewardTransactionEntity.getPaymentToken() != null
					&& brewerRewardTransactionEntity.getRewardTransactionAmount() != null) {
				Map<String, Object> rewardPaymentMap = new HashMap<String, Object>();
				rewardPaymentMap.put("transaction_id", brewerRewardTransactionEntity.getPaymentToken());
				rewardPaymentMap.put("amount", brewerRewardTransactionEntity.getRewardTransactionAmount());
				scrapeInfo.put("payment_reward", rewardPaymentMap);
				if (paymentType.contains("AFFIRM")) {
					scrapeInfo.put("payment_mode", "affirm,reward");
				} else if (paymentType.contains("CARD")) {
					scrapeInfo.put("payment_mode", "stripe,reward");
				} else if (paymentType.contains("BANK")) {
					scrapeInfo.put("payment_mode", "bank,reward");
				} else {
					scrapeInfo.put("payment_mode", "reward");
				}
			}
*/
			scrapeInfo.put("token", cartEntity.getToken());
			// scrapeInfo.put("used_furniture",1);
			scrapeInfo.put("student", 1);
			scrapeInfo.put("delivery_date1", null);
			scrapeInfo.put("delivery_date2",null);
			scrapeInfo.put("delivery_date3", null);
			if (checkLeaseAggrement != null && checkLeaseAggrement) {
				String request = null;
				/*if (shippingAddressData.getShippingFirstName() != null
						&& shippingAddressData.getShippingLastName() != null) {
					request = shippingAddressData.getShippingFirstName() + " "
							+ shippingAddressData.getShippingLastName();
				}*/
				if (request != null) {
					scrapeInfo.put("signature", request);
				} else {
					scrapeInfo.put("signature", "Shirish");
				}
			}

		    if (deliveryTime != null)
				scrapeInfo.put("delivery_hour", deliveryTime);
			if (deliveryTime1 != null)
				scrapeInfo.put("delivery_hour1", deliveryTime1);
			if (deliveryTime2 != null)
				scrapeInfo.put("delivery_hour2", deliveryTime2);
			if(orderList!=null && orderList.get(0).getOrders()!=null) {
			if (orderList.get(0).getOrders().getFirstName() != null) {
				scrapeInfo.put("first_name", orderList.get(0).getOrders().getFirstName());
			}
			if (orderList.get(0).getOrders().getLastName()!= null) {
				scrapeInfo.put("last_name", orderList.get(0).getOrders().getLastName());
			}
			if (orderList.get(0).getOrders().getEmail() != null) {
				scrapeInfo.put("email", orderList.get(0).getOrders().getEmail());
			}

			if (orderList.get(0).getOrders().getPhone() != null) {
				scrapeInfo.put("phone",orderList.get(0).getOrders().getPhone());
			
			}
			// scrapeInfo.put("apt_no", "B-502");//
			if (orderList.get(0).getOrders().getAddress() != null) {
				scrapeInfo.put("building_name", orderList.get(0).getOrders().getAddress());
			}
			if (orderList.get(0).getOrders().getBillingState()!= null) {
				scrapeInfo.put("state", orderList.get(0).getOrders().getBillingState());
			}
			if (orderList.get(0).getOrders().getBillingAptNo() != null) {
				scrapeInfo.put("unit_no", orderList.get(0).getOrders().getBillingAptNo());
			}
			//
			// scrapeInfo.put("state", "IL");
			if (orderList.get(0).getOrders().getBillingCity() != null) {
				scrapeInfo.put("city", orderList.get(0).getOrders().getBillingCity());
			}
			if (orderList.get(0).getOrders().getBillingZipCode() != null) {
				scrapeInfo.put("zip_code", orderList.get(0).getOrders().getBillingZipCode());
			}
			}
			Map<String, Object> shippingAndHandlingFees = cartData.getShippingAndHandlingFees();
			if (MapUtils.isNotEmpty(shippingAndHandlingFees)
					&& shippingAndHandlingFees.containsKey("deliveryFee")) {
				scrapeInfo.put("delivery_fee", shippingAndHandlingFees.get("deliveryFee"));
			} else {
				scrapeInfo.put("delivery_fee", 0);
			}
			if (MapUtils.isNotEmpty(shippingAndHandlingFees) && shippingAndHandlingFees.containsKey("pickupFee")) {
				scrapeInfo.put("pickup_fee", shippingAndHandlingFees.get("pickupFee"));
			} else {
				scrapeInfo.put("pickup_fee", 0);
			}
			if (MapUtils.isNotEmpty(shippingAndHandlingFees)
					&& shippingAndHandlingFees.containsKey("handlingFee")) {
				scrapeInfo.put("min_cart_surcharge", shippingAndHandlingFees.get("handlingFee"));
			} else {
				scrapeInfo.put("min_cart_surcharge", 0);
			}
			if (MapUtils.isNotEmpty(shippingAndHandlingFees)
					&& shippingAndHandlingFees.containsKey("out_of_city_fee")) {
				scrapeInfo.put("out_of_city_fee", shippingAndHandlingFees.get("out_of_city_fee"));
			} else {
				scrapeInfo.put("out_of_city_fee", 0);
			}
			if (MapUtils.isNotEmpty(shippingAndHandlingFees)
					&& shippingAndHandlingFees.containsKey("out_of_state_fee")) {
				scrapeInfo.put("out_of_state_fee", shippingAndHandlingFees.get("out_of_state_fee"));
			} else {
				scrapeInfo.put("out_of_state_fee", 0);
			}

			if (MapUtils.isNotEmpty(shippingAndHandlingFees)
					&& shippingAndHandlingFees.containsKey("SHIPPING_HANDLING_FEE_TYPE")) {
				if (shippingAndHandlingFees.get("SHIPPING_HANDLING_FEE_TYPE") != null) {
					String SHIPPING_HANDLING_FEE_TYPE = shippingAndHandlingFees.get("SHIPPING_HANDLING_FEE_TYPE")
							.toString();
					if (SHIPPING_HANDLING_FEE_TYPE.equalsIgnoreCase("RUSHED_DELIVERY")) {
						scrapeInfo.put("delivery_option", 1);
					} else if (SHIPPING_HANDLING_FEE_TYPE.equalsIgnoreCase("EXPEDITED_TWO_DAYS_DELIVERY")) {
						scrapeInfo.put("delivery_option", 2);
					} else {
						scrapeInfo.put("delivery_option", 0);
					}
				} else {
					scrapeInfo.put("delivery_option", 0);
				}
			}
			Map<String, Object> addOnFees = cartData.getAddOnFees();
			if (MapUtils.isNotEmpty(addOnFees) && addOnFees.containsKey("stagingFee")) {
				scrapeInfo.put("staging_fee", addOnFees.get("stagingFee"));
			} else {
				scrapeInfo.put("staging_fee", 0);
			}
			if (MapUtils.isNotEmpty(addOnFees) && addOnFees.containsKey("rentBreakFreeFee")) {
				scrapeInfo.put("break_free_fee", addOnFees.get("rentBreakFreeFee"));
			} else {
				scrapeInfo.put("break_free_fee", 0);
			}

			if (MapUtils.isNotEmpty(addOnFees) && addOnFees.containsKey("rentDeposit")) {
				scrapeInfo.put("deposit_amount", addOnFees.get("rentDeposit"));
			} else {
				scrapeInfo.put("deposit_amount", 0);
			}

			if (MapUtils.isNotEmpty(addOnFees) && addOnFees.containsKey("rentAndBuyProtectionFee")) {
				if (addOnFees.containsKey("rentAndBuyProtectionFee")) {
					scrapeInfo.put("coverage", "15");
					scrapeInfo.put("coverage_fee", addOnFees.get("rentAndBuyProtectionFee"));
				} else {
					scrapeInfo.put("coverage", 0);
					scrapeInfo.put("coverage_fee", 0);
				}
			} else if (MapUtils.isNotEmpty(addOnFees)) {
				Boolean buyProtectionFlag = null;
				Boolean rentProtectionFlag = null;
				Object rentProtectionFee = null;
				Object buyProtectionFee = null;
				if (addOnFees.containsKey("buyProtectionFlag")) {
					buyProtectionFlag = (Boolean) addOnFees.get("buyProtectionFlag");
				}
				if (addOnFees.containsKey("rentProtectionFlag")) {
					rentProtectionFlag = (Boolean) addOnFees.get("rentProtectionFlag");
				}

				if (addOnFees.containsKey("buyProtectionFee") && buyProtectionFlag != null && buyProtectionFlag) {
					buyProtectionFee = addOnFees.get("buyProtectionFee");
				}
				if (addOnFees.containsKey("rentProtectionFee") && rentProtectionFlag != null
						&& rentProtectionFlag) {
					rentProtectionFee = addOnFees.get("rentProtectionFee");
				}

				if (rentProtectionFlag != null && rentProtectionFlag && rentProtectionFee != null) {
					scrapeInfo.put("coverage", "15");
					scrapeInfo.put("coverage_fee", rentProtectionFee);
				} else {
					scrapeInfo.put("coverage", 0);
					scrapeInfo.put("coverage_fee", 0);
				}

				if (buyProtectionFlag != null && buyProtectionFlag && buyProtectionFee != null) {
					scrapeInfo.put("buy_coverage", "15");
					scrapeInfo.put("buy_coverage_fee", buyProtectionFee);
				} else {
					scrapeInfo.put("buy_coverage", 0);
					scrapeInfo.put("buy_coverage_fee", 0);
				}
			}
			scrapeInfo.put("pet_cleaning_fee", 0);
			scrapeInfo.put("after_hour_delivery_fee", 0);
			scrapeInfo.put("expedited_fee", 0);
			/*
			 * if(cartData.getPromocodeDiscountAmount()!=null)
			 * scrapeInfo.put("promocode_discount",
			 * cartData.getPromocodeDiscountAmount()); else
			 * scrapeInfo.put("promocode_amount",0);
			 */

			getLogger().info("create product/package Order Request is {}", scrapeInfo);
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("Content-Type", "application/json");
			getLogger().info("Order Request URL is {}", endPoint);
			String response = this.genericRestClient.doPost(endPoint, scrapeInfo, headers);
			getLogger().info("Order URL Response is {}", response);
			responseMap = new HashMap<String, Object>();
			responseMap = KoinPlusStringUtils.getJsonMap(response);
			if (Integer.parseInt(responseMap.get("statusCode").toString()) != 200) {
				throw new InhabitrException();
			}
		}
		return responseMap;
	} catch (Exception ex) {
		ex.printStackTrace();
		// throw new InhabitrException();
	}
	return responseMap;
}

private CartData caliculateStudentFurnitiureCartFee(CartData cart, Boolean checkHasRent, Boolean hasReferance,
		String referance, Long cartTokenSgid, Integer zipcode, Integer totalQty) throws InhabitrException {
	CartData cartData = new CartData();
	if (cart != null && cart.getTotalPrice() != null && !cart.getTotalPrice().equals(BigDecimal.ZERO)) {
		cartData = cart;
		BigDecimal addOnsTotalFee = BigDecimal.ZERO;
		BigDecimal totalShippingHandlingFee = BigDecimal.ZERO;
		BigDecimal totalCartPrice = cart.getTotalPrice();
		BigDecimal totalCartAffirmPrice = cart.getTotalAffirmPrice();
		BigDecimal shippingFee = BigDecimal.ZERO;
		BigDecimal stagingFee = BigDecimal.ZERO;
		BigDecimal stagingFeePerMonth = BigDecimal.ZERO;
		Boolean checkHasStagingFeePerMonth = Boolean.FALSE;
		Boolean checkHasStagingFee = Boolean.FALSE;
		List<CartOptionsEntity> cartOptionsEntitys = new ArrayList<CartOptionsEntity>();
		if (hasReferance != null && hasReferance && referance != null) {
			cartOptionsEntitys = this.cartOptionsService.findCartOptionDetailById(cart.getId(), referance);
		} else {
			cartOptionsEntitys = this.cartOptionsService.findCartOptionDetailById(cart.getId(), cartTokenSgid,hasReferance);
	
		}

		if (CollectionUtils.isNotEmpty(cartOptionsEntitys)) {
			Boolean checkRegularShipping = Boolean.TRUE;
			for (CartOptionsEntity cartOptionsEntity : cartOptionsEntitys) {
				if (cartOptionsEntity.getOptionType().equals("checkRegularShipping")) {
					if (cartOptionsEntity.getOptionTypeValue() != null
							&& cartOptionsEntity.getOptionTypeValue().equalsIgnoreCase("false")) {
						checkRegularShipping = Boolean.FALSE;
					}
					break;
				}
			}
			for (CartOptionsEntity cartOptionEntity : cartOptionsEntitys) {
				if (!cartOptionEntity.getOptionType().equals("month")
						&& !cartOptionEntity.getOptionType().equals("checkRegularShipping")) {
					cartData.addCartOptionsMap(cartOptionEntity.getOptionType(),
							new BigDecimal(cartOptionEntity.getOptionTypeValue()));
				}
				if (cartOptionEntity.getOptionType().equals("shippingFee")) {
					shippingFee = new BigDecimal(cartOptionEntity.getOptionTypeValue());

					if (!shippingFee.equals(BigDecimal.ZERO)) {
						Map<String, Object> getOptions = getCartOptions(zipcode);
						if (getOptions.containsKey("SHIPPING & HANDLING FEE")) {
							Map<String, Object> shipMap = (Map<String, Object>) getOptions
									.get("SHIPPING & HANDLING FEE");
							if (checkRegularShipping != null && checkRegularShipping) {
								cartData.addShippingAndHandlingFeeMap("checkRegularShipping", checkRegularShipping);
								if (shipMap.containsKey("CURB_SIDE_DELIVERY") && shippingFee
										.equals(new BigDecimal(shipMap.get("CURB_SIDE_DELIVERY").toString()))) {
									cartData.addCartOptionsMap("SHIPPING_HANDLING_FEE_TYPE", "CURB_SIDE_DELIVERY");
									cartData.addShippingAndHandlingFeeMap("SHIPPING_HANDLING_FEE_TYPE",
											"CURB_SIDE_DELIVERY");
								} else if (shipMap.containsKey("IN_HOME_DELIVERY") && shippingFee
										.equals(new BigDecimal(shipMap.get("IN_HOME_DELIVERY").toString()))) {
									cartData.addCartOptionsMap("SHIPPING_HANDLING_FEE_TYPE", "IN_HOME_DELIVERY");
									cartData.addShippingAndHandlingFeeMap("SHIPPING_HANDLING_FEE_TYPE",
											"IN_HOME_DELIVERY");
								} else {
									if (totalQty != null && totalQty != 0) {
										shippingFee = shippingFee.multiply(new BigDecimal(totalQty)).setScale(2,
												BigDecimal.ROUND_HALF_UP);
									}
									cartData.addCartOptionsMap("SHIPPING_HANDLING_FEE_TYPE",
											"WHITE_GLOVE_AND_ASSEMBLY_DELIVERY");
									cartData.addShippingAndHandlingFeeMap("SHIPPING_HANDLING_FEE_TYPE",
											"WHITE_GLOVE_AND_ASSEMBLY_DELIVERY");
								}
							} else if (checkRegularShipping != null && !checkRegularShipping) {
								cartData.addShippingAndHandlingFeeMap("checkRegularShipping", checkRegularShipping);
								if (shipMap.containsKey("EXPEDITED_CURB_SIDE_DELIVERY") && shippingFee.equals(
										new BigDecimal(shipMap.get("EXPEDITED_CURB_SIDE_DELIVERY").toString()))) {
									cartData.addCartOptionsMap("SHIPPING_HANDLING_FEE_TYPE",
											"EXPEDITED_CURB_SIDE_DELIVERY");
									cartData.addShippingAndHandlingFeeMap("SHIPPING_HANDLING_FEE_TYPE",
											"EXPEDITED_CURB_SIDE_DELIVERY");
								} else if (shipMap.containsKey("EXPEDITED_IN_HOME_DELIVERY") && shippingFee.equals(
										new BigDecimal(shipMap.get("EXPEDITED_IN_HOME_DELIVERY").toString()))) {
									cartData.addCartOptionsMap("SHIPPING_HANDLING_FEE_TYPE",
											"EXPEDITED_IN_HOME_DELIVERY");
									cartData.addShippingAndHandlingFeeMap("SHIPPING_HANDLING_FEE_TYPE",
											"EXPEDITED_IN_HOME_DELIVERY");
								} else {
									if (totalQty != null && totalQty != 0) {
										shippingFee = shippingFee.multiply(new BigDecimal(totalQty)).setScale(2,
												BigDecimal.ROUND_HALF_UP);
									}
									cartData.addCartOptionsMap("SHIPPING_HANDLING_FEE_TYPE",
											"EXPEDITED_WHITE_GLOVE_AND_ASSEMBLY_DELIVERY");
									cartData.addShippingAndHandlingFeeMap("SHIPPING_HANDLING_FEE_TYPE",
											"EXPEDITED_WHITE_GLOVE_AND_ASSEMBLY_DELIVERY");
								}
							} else {
								if (shipMap.containsKey("CURB_SIDE_DELIVERY") && shippingFee
										.equals(new BigDecimal(shipMap.get("CURB_SIDE_DELIVERY").toString()))) {
									cartData.addCartOptionsMap("SHIPPING_HANDLING_FEE_TYPE", "CURB_SIDE_DELIVERY");
									cartData.addShippingAndHandlingFeeMap("SHIPPING_HANDLING_FEE_TYPE",
											"CURB_SIDE_DELIVERY");
								} else if (shipMap.containsKey("IN_HOME_DELIVERY") && shippingFee
										.equals(new BigDecimal(shipMap.get("IN_HOME_DELIVERY").toString()))) {
									cartData.addCartOptionsMap("SHIPPING_HANDLING_FEE_TYPE", "IN_HOME_DELIVERY");
									cartData.addShippingAndHandlingFeeMap("SHIPPING_HANDLING_FEE_TYPE",
											"IN_HOME_DELIVERY");
								} else {
									if (totalQty != null && totalQty != 0) {
										shippingFee = shippingFee.multiply(new BigDecimal(totalQty)).setScale(2,
												BigDecimal.ROUND_HALF_UP);
									}
									cartData.addCartOptionsMap("SHIPPING_HANDLING_FEE_TYPE",
											"WHITE_GLOVE_AND_ASSEMBLY_DELIVERY");
									cartData.addShippingAndHandlingFeeMap("SHIPPING_HANDLING_FEE_TYPE",
											"WHITE_GLOVE_AND_ASSEMBLY_DELIVERY");
								}
							}
						}
						totalCartPrice = totalCartPrice.add(shippingFee);
						if (totalCartAffirmPrice != null) {
							totalCartAffirmPrice = totalCartAffirmPrice.add(shippingFee);
						}
						totalShippingHandlingFee = totalShippingHandlingFee.add(shippingFee);
						cartData.addShippingAndHandlingFeeMap("deliveryFee", shippingFee);
					} else {
						totalCartPrice = totalCartPrice.add(shippingFee);
						if (totalCartAffirmPrice != null) {
							totalCartAffirmPrice = totalCartAffirmPrice.add(shippingFee);
						}
						totalShippingHandlingFee = totalShippingHandlingFee.add(shippingFee);
						cartData.addShippingAndHandlingFeeMap("deliveryFee", shippingFee);
					}
				}

				if (cartOptionEntity.getOptionType().equals("stagingFee")) {
					if (new BigDecimal(cartOptionEntity.getOptionTypeValue()).compareTo(new BigDecimal(0)) == 1) {
						checkHasStagingFee = Boolean.TRUE;
						stagingFee = new BigDecimal(cartOptionEntity.getOptionTypeValue());
					}
				}

				if (cartOptionEntity.getOptionType().equals("stagingFeePerMonth")) {
					if (new BigDecimal(cartOptionEntity.getOptionTypeValue()).compareTo(new BigDecimal(0)) == 1) {
						checkHasStagingFeePerMonth = Boolean.TRUE;
						stagingFeePerMonth = new BigDecimal(cartOptionEntity.getOptionTypeValue());
					}
				}

				if (checkHasRent) {
					if (cartOptionEntity.getOptionType().equalsIgnoreCase("month")) {
						cartData.setLeaseLength(Integer.valueOf(cartOptionEntity.getOptionTypeValue()));
					}
				}
			}

		}
		if (checkHasRent) {
			cartData.setType("RENT");
		} else {
			cartData.setType("BUY");
		}
		if (cart.getBuyTotalPrice() != null && cart.getBuyTotalPrice().compareTo(BigDecimal.ZERO) == 1
				&& checkHasRent && cart.getRentTotalPrice() != null
				&& cart.getRentTotalPrice().compareTo(BigDecimal.ZERO) == 1) {
			cartData.setType("HYBRID");
		}

		// promocode discount value
		if (cart.getPromocodeDiscountAmount() != null) {
			totalCartPrice = totalCartPrice.subtract(cart.getPromocodeDiscountAmount());
			if (totalCartAffirmPrice != null) {
				totalCartAffirmPrice = totalCartAffirmPrice.add(cart.getPromocodeDiscountAmount());
			}
			cartData.setPromocodeDiscountAmount(cart.getPromocodeDiscountAmount());
		}
		if (checkHasStagingFee) {
			cartData.addAddOnFeeMap("stagingFee", stagingFee);
			totalCartPrice = totalCartPrice.add(stagingFee);
			if (totalCartAffirmPrice != null) {
				totalCartAffirmPrice = totalCartAffirmPrice.add(stagingFee);
			}
		}
		if (checkHasStagingFeePerMonth) {
			cartData.addAddOnFeeMap("stagingFeePerMonth", stagingFeePerMonth);
		}

		if (addOnsTotalFee.compareTo(BigDecimal.ZERO) == 1) {
			cartData.addAddOnFeeMap("addOnsTotalFee", addOnsTotalFee.setScale(2, BigDecimal.ROUND_HALF_UP));
		}

		if (totalShippingHandlingFee.compareTo(BigDecimal.ZERO) == 1) {
			cartData.addShippingAndHandlingFeeMap("totalShippingHandlingFee",
					totalShippingHandlingFee.setScale(2, BigDecimal.ROUND_HALF_UP));
		}

		cartData.setTotalPrice(totalCartPrice.setScale(2, BigDecimal.ROUND_HALF_UP));
		if (totalCartAffirmPrice != null && totalCartAffirmPrice.compareTo(BigDecimal.ZERO) == 1) {
			cartData.setTotalAffirmPrice(totalCartAffirmPrice.setScale(2, BigDecimal.ROUND_HALF_UP));
		}
	}
	return cartData;
}

public Map<String, Object> getCartOptions(Integer zipcode) {
	Map<String, Object> response = new HashMap<String, Object>();
	List<ConfigurationEntity> configEntities = this.configurationService.findAll(zipcode, zipcode, OPS_DB_API_END_POINT, OPS_DB_API_END_POINT);
	Map<String, Object> addOns = new HashMap<String, Object>();
	Map<String, Object> shippingHandlingFee = new HashMap<String, Object>();
	for (ConfigurationEntity configEntity : configEntities) {
		if (configEntity.getParamName().equalsIgnoreCase("COVERAGE_FEE")) {
			addOns.put(configEntity.getParamName(), configEntity.getParamValue());
		}
		if (configEntity.getParamName().equalsIgnoreCase("BREAK_FREE EARLY_FEE")) {
			addOns.put(configEntity.getParamName(), configEntity.getParamValue());
		}
		if (configEntity.getParamName().equalsIgnoreCase("CURB_SIDE_DELIVERY")) {// Curb-side
																					// delivery
			shippingHandlingFee.put(configEntity.getParamName(), configEntity.getParamValue());
		}
		if (configEntity.getParamName().equalsIgnoreCase("IN_HOME_DELIVERY")) {//
			shippingHandlingFee.put(configEntity.getParamName(), configEntity.getParamValue());
		}
		if (configEntity.getParamName().equalsIgnoreCase("WHITE_GLOVE_AND_ASSEMBLY_DELIVERY")) {
			shippingHandlingFee.put(configEntity.getParamName(), configEntity.getParamValue());
		}
		if (configEntity.getParamName().equalsIgnoreCase("EXPEDITED_CURB_SIDE_DELIVERY")) {// Curb-side
																							// delivery
			shippingHandlingFee.put(configEntity.getParamName(), configEntity.getParamValue());
		}
		if (configEntity.getParamName().equalsIgnoreCase("EXPEDITED_IN_HOME_DELIVERY")) {//
			shippingHandlingFee.put(configEntity.getParamName(), configEntity.getParamValue());
		}
		if (configEntity.getParamName().equalsIgnoreCase("EXPEDITED_WHITE_GLOVE_AND_ASSEMBLY_DELIVERY")) {
			shippingHandlingFee.put(configEntity.getParamName(), configEntity.getParamValue());
		}
		if (configEntity.getParamName().equalsIgnoreCase("STUDENT_STAGING_FEE")) {

			if (configEntity.getParamValue() != null) {
				BigDecimal stagingFee = new BigDecimal(configEntity.getParamValue());
				if (zipcode != null && stagingFee != null && stagingFee.compareTo(BigDecimal.ZERO) == 1) {
					ZipcodeEntity zipcodeEntity = this.zipcodeDAO.findZipcode(zipcode);
					if (zipcodeEntity != null && zipcodeEntity.getSalesTaxRate() != null) {

						BigDecimal stagingFeeSalesTaxPrice = stagingFee.multiply(zipcodeEntity.getSalesTaxRate())
								.divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP);
						stagingFee = stagingFee.add(stagingFeeSalesTaxPrice).setScale(0, BigDecimal.ROUND_HALF_UP);

						BigDecimal oneMonthStagingFee = stagingFee.divide(new BigDecimal(12), 0,
								BigDecimal.ROUND_HALF_UP);
						if (oneMonthStagingFee != null && oneMonthStagingFee.compareTo(BigDecimal.ZERO) == 1) {
							shippingHandlingFee.put("STUDENT_STAGING_FEE_PER_MONTH", oneMonthStagingFee);
						}
						shippingHandlingFee.put(configEntity.getParamName(), stagingFee);
					} else {
						BigDecimal oneMonthStagingFee = stagingFee.divide(new BigDecimal(12), 0,
								BigDecimal.ROUND_HALF_UP);
						if (oneMonthStagingFee != null && oneMonthStagingFee.compareTo(BigDecimal.ZERO) == 1) {
							shippingHandlingFee.put("STUDENT_STAGING_FEE_PER_MONTH", oneMonthStagingFee);
						}
						shippingHandlingFee.put(configEntity.getParamName(), stagingFee);
					}
				} else {
					BigDecimal oneMonthStagingFee = stagingFee.divide(new BigDecimal(12), 0,
							BigDecimal.ROUND_HALF_UP);
					if (oneMonthStagingFee != null && oneMonthStagingFee.compareTo(BigDecimal.ZERO) == 1) {
						shippingHandlingFee.put("STUDENT_STAGING_FEE_PER_MONTH", oneMonthStagingFee);
					}
					shippingHandlingFee.put(configEntity.getParamName(), stagingFee);
				}
			} else {
				shippingHandlingFee.put(configEntity.getParamName(), configEntity.getParamValue());
			}
		}
	}
	if (MapUtils.isNotEmpty(addOns)) {
		response.put("ADD-ONS", addOns);
	}
	if (MapUtils.isNotEmpty(shippingHandlingFee)) {
		response.put("SHIPPING & HANDLING FEE", shippingHandlingFee);
	}
	return response;
}
public Map<String, Object> createOPsDBStudentCart(CartEntity cartEntity, List<CartItemEntity> cartItemEntitys,
		String cartItemTokenNumber, Long cityId) throws InhabitrException {
	Map<String, Object> responseMap = new HashMap<String, Object>();
	try {
		if (cartEntity != null) {
			String endPoint = OPS_DB_API_END_POINT + "/api/product/order/createCartAndItems";
			Map<String, Object> request = new HashMap<String, Object>();
			request.put("token", cartItemTokenNumber);
			List<Map<String, Object>> scrapesInfo = new ArrayList<Map<String, Object>>();
			for (CartItemEntity cartItemEntity : cartItemEntitys) {
				if (cartItemEntity.getItemType().equals(ItemType.PRODUCT)) {
					Map<String, Object> scrapeInfo = new HashMap<String, Object>();
					scrapeInfo.put("sku", cartItemEntity.getSku());
					scrapeInfo.put("type", "product");
					scrapeInfo.put("sale_price", cartItemEntity.getItemPrice());

					if (cartItemEntity.getItemQuantity() != null && cartItemEntity.getItemQuantity() > 0) {
						scrapeInfo.put("quantity", cartItemEntity.getItemQuantity());
					} else {
						scrapeInfo.put("quantity", 1);
					}					
					if (cartItemEntity.getIsRent() != null && cartItemEntity.getIsRent()) {
						scrapeInfo.put("rent_type", 0);

						if (cartItemEntity.getMonth() != null) {
							scrapeInfo.put("months", cartItemEntity.getMonth());
						} else {
							scrapeInfo.put("months", "3");
						}
					} else if (cartItemEntity.getIsBuyUsed() != null && cartItemEntity.getIsBuyUsed()) {
						scrapeInfo.put("rent_type", 1);
					} else if (cartItemEntity.getIsBuyNew() != null && cartItemEntity.getIsBuyNew()) {
						scrapeInfo.put("rent_type", 2);
					}

					scrapesInfo.add(scrapeInfo);

				} else if (cartItemEntity.getItemType().equals(ItemType.PACKAGE)) {

					if(cartItemEntity.getCartPackageProducts() != null && CollectionUtils
							.isNotEmpty(cartItemEntity.getCartPackageProducts())){
						for (CartPackageProductsEntity cartPackageProductsEntity : cartItemEntity.getCartPackageProducts()) {

							Map<String, Object> scrapeInfo = new HashMap<String, Object>();
							scrapeInfo.put("sku", cartPackageProductsEntity.getSku());
							scrapeInfo.put("type", "product");
							scrapeInfo.put("sale_price", cartPackageProductsEntity.getPrice());

							if (cartItemEntity.getItemQuantity() != null && cartItemEntity.getItemQuantity() > 0) {
								scrapeInfo.put("quantity", cartItemEntity.getItemQuantity());
							} else {
								scrapeInfo.put("quantity", 1);
							}
							// scrapeInfo.put("rent_type", 3);
							if (cartItemEntity.getIsRent() != null && cartItemEntity.getIsRent()) {
								scrapeInfo.put("rent_type", 0);

								if (cartItemEntity.getMonth() != null) {
									scrapeInfo.put("months", cartItemEntity.getMonth());
								} else {
									scrapeInfo.put("months", "3");
								}
							} else if (cartItemEntity.getIsBuyUsed() != null && cartItemEntity.getIsBuyUsed()) {
								scrapeInfo.put("rent_type", 1);
							} else if (cartItemEntity.getIsBuyNew() != null && cartItemEntity.getIsBuyNew()) {
								scrapeInfo.put("rent_type", 2);
							}
							scrapesInfo.add(scrapeInfo);
						}
					}else{
						Map<String, Object> scrapeInfo = new HashMap<String, Object>();
						scrapeInfo.put("sku", cartItemEntity.getSku());
						if (cartItemEntity.getProductPackageVariationSgid() != null)
							scrapeInfo.put("variation", cartItemEntity.getProductPackageVariationSgid());
							scrapeInfo.put("sale_price", cartItemEntity.getItemPrice());
							scrapeInfo.put("type", "package");
							if (cartItemEntity.getItemQuantity() != null && cartItemEntity.getItemQuantity() > 0) {
								scrapeInfo.put("quantity", cartItemEntity.getItemQuantity());
							} else {
								scrapeInfo.put("quantity", 1);
							}
							
							if (cartItemEntity.getIsRent() != null && cartItemEntity.getIsRent()) {
								scrapeInfo.put("rent_type", 0);

							if (cartItemEntity.getMonth() != null) {
								scrapeInfo.put("months", cartItemEntity.getMonth());
							} else {
								scrapeInfo.put("months", "3");
							}
							} else if (cartItemEntity.getIsBuyUsed() != null && cartItemEntity.getIsBuyUsed()) {
								scrapeInfo.put("rent_type", 1);
							} else if (cartItemEntity.getIsBuyNew() != null && cartItemEntity.getIsBuyNew()) {
								scrapeInfo.put("rent_type", 2);
							}
							scrapesInfo.add(scrapeInfo);
					}
				} else if (cartItemEntity.getItemType().equals(ItemType.FLOOR_PACKAGE)) {
					Map<String, Object> scrapeInfo = new HashMap<String, Object>();
					scrapeInfo.put("sku", cartItemEntity.getSku());
					if (cartItemEntity.getProductPackageVariationSgid() != null)
						scrapeInfo.put("variation", cartItemEntity.getProductPackageVariationSgid());
					scrapeInfo.put("sale_price", cartItemEntity.getItemPrice());
					scrapeInfo.put("type", "floor_package");
					if (cartItemEntity.getItemQuantity() != null && cartItemEntity.getItemQuantity() > 0) {
						scrapeInfo.put("quantity", cartItemEntity.getItemQuantity());
					} else {
						scrapeInfo.put("quantity", 1);
					}

					if (cartItemEntity.getIsRent() != null && cartItemEntity.getIsRent()) {
						scrapeInfo.put("rent_type", 0);

						if (cartItemEntity.getMonth() != null) {
							scrapeInfo.put("months", cartItemEntity.getMonth());
						} else {
							scrapeInfo.put("months", "3");
						}
					} else if (cartItemEntity.getIsBuyUsed() != null && cartItemEntity.getIsBuyUsed()) {
						scrapeInfo.put("rent_type", 1);
					} else if (cartItemEntity.getIsBuyNew() != null && cartItemEntity.getIsBuyNew()) {
						scrapeInfo.put("rent_type", 2);
					}
					scrapesInfo.add(scrapeInfo);
				}
			}
			request.put("cart_flow", 30);
			
			request.put("id_city", cityId);
			
			request.put("cartItems", scrapesInfo);
			getLogger().info("create product/package Cart Request is {}", request);
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("Content-Type", "application/json");
			getLogger().info("Cart Request URL is {}", endPoint);
			String response = this.genericRestClient.doPost(endPoint, request, headers);
			getLogger().info("create product/package Cart Response is {}", response);
			responseMap = KoinPlusStringUtils.getJsonMap(response);

			if (responseMap.containsKey("statusCode")) {
				if (Integer.parseInt(responseMap.get("statusCode").toString()) != 200) {
					throw new InhabitrException();
				}
			} else if (responseMap.containsKey("errors")) {
				throw new InhabitrException();
			}
		}
		return responseMap;
	} catch (Exception ex) {
		throw new InhabitrException();
	}
}
public Map<String, Object> applyPromocode(String cartTokenNumber, String code) throws InhabitrException {
	Map<String, Object> responseMap = new HashMap<String, Object>();

	try {
		if (cartTokenNumber != null && code != null) {
			String endPoint = OPS_DB_API_END_POINT + "/api/cart/promo/" + cartTokenNumber;
			Map<String, Object> request = new HashMap<String, Object>();
			request.put("code", code);

			getLogger().info("Apply Promocode discount to Cart Request is {}", request);
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("Content-Type", "application/json");
			getLogger().info("Apply Promocode discount to Cart Request URL is {}", endPoint);
			String response = this.genericRestClient.doPost(endPoint, request, headers);
			getLogger().info("Apply Promocode discount to Cart Response is {}", response);
			responseMap = KoinPlusStringUtils.getJsonMap(response);

		
		}
	} catch (Exception ex) {
		ex.printStackTrace();
	
	}
	return responseMap;
}

private Map<String, Object> caliculatePromocodeDiscount(Long brewerId, Long cartId, String promoCode,
		Boolean isOrderProccessed, List<CartItemEntity> trendbrewCartItemEntitys) throws InhabitrException {
	Map<String, Object> response = new HashMap<String, Object>();
	if (CollectionUtils.isNotEmpty(trendbrewCartItemEntitys)) {
		BigDecimal rentTotalCartPrice = BigDecimal.ZERO;
		BigDecimal buyTotalCartPrice = BigDecimal.ZERO;
		BigDecimal totalCheckoutPrice = BigDecimal.ZERO;

		for (CartItemEntity cartItemEntity : trendbrewCartItemEntitys) {
			if (cartItemEntity.getItemQuantity() != null && cartItemEntity.getItemQuantity() != 0) {
			}
			if (cartItemEntity.getIsRent()) {
				rentTotalCartPrice = rentTotalCartPrice.add(cartItemEntity.getTotalFinalPrice());
			} else {
				buyTotalCartPrice = buyTotalCartPrice.add(cartItemEntity.getTotalFinalPrice());
			}
			if (cartItemEntity.getTotalFinalPrice() != null) {
				totalCheckoutPrice = totalCheckoutPrice.add(cartItemEntity.getTotalFinalPrice());
			}
		}
		BigDecimal discount = BigDecimal.ZERO;
		
	}
	return response;
}

private Date addDay(int count, Long date) {

	Calendar addDate = Calendar.getInstance();
	if (date != null)
		addDate.setTime(new Date(date));
	else
		addDate.setTime(new Date(date));
	addDate.add(Calendar.DATE, count);
	return (Date) addDate.getTime();
}

private String getDay(Long date) {
	DateFormat format2 = new SimpleDateFormat("EEEE");
	return format2.format(new Date(date));
}
}