package com.inhabitr.admin.dasboard.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;




/**
 * @author Anshu Gupta
 */
@Entity @Table(name = "card")
public class CardEntity  {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	
	private @Column(name = "id_customer") String customerId;
	private @Column(name = "fingerprint") String fingerprint;
	private @Column(name = "id_card") String cardId;
	private @Column(name = "card_type") String cardType;
	private @Column(name = "source_brand") String sourceBrand;
	private @Column(name = "source_country") String sourceCountry;
	private @Column(name = "source_cvc_check") String sourceCvcCheck;
	private @Column(name = "exp_month") Integer expMonth;
	private @Column(name = "exp_year") Integer expYear;
	private @Column(name = "last4") String last4;
	private @Column(name = "routing_number") String routingNumber;
	private @Column(name = "account_holder_name") String accountHolderName;
	private @Column(name = "account_holder_type") String accountHolderType;
	private @Column(name = "status") Integer status;
	private @Column(name = "is_default") Integer isDefault;
	private @Column(name = "is_inh_source_card") Integer isInhSourceCard;
    private @Column(name = "created_datetime") @Temporal(TemporalType.TIMESTAMP) Date createdDatetime;
    private @Column(name = "deleted_datetime") @Temporal(TemporalType.TIMESTAMP) Date deletedDatetime;
    @OneToOne(fetch = FetchType.LAZY) @JoinColumn(name = "user_sgid")
    private BrewerEntity brewer;
	
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getFingerprint() {
		return fingerprint;
	}
	public void setFingerprint(String fingerprint) {
		this.fingerprint = fingerprint;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getSourceBrand() {
		return sourceBrand;
	}
	public void setSourceBrand(String sourceBrand) {
		this.sourceBrand = sourceBrand;
	}
	public String getSourceCountry() {
		return sourceCountry;
	}
	public void setSourceCountry(String sourceCountry) {
		this.sourceCountry = sourceCountry;
	}
	public String getSourceCvcCheck() {
		return sourceCvcCheck;
	}
	public void setSourceCvcCheck(String sourceCvcCheck) {
		this.sourceCvcCheck = sourceCvcCheck;
	}
	public Integer getExpMonth() {
		return expMonth;
	}
	public void setExpMonth(Integer expMonth) {
		this.expMonth = expMonth;
	}
	public Integer getExpYear() {
		return expYear;
	}
	public void setExpYear(Integer expYear) {
		this.expYear = expYear;
	}
	public String getLast4() {
		return last4;
	}
	public void setLast4(String last4) {
		this.last4 = last4;
	}
	public String getRoutingNumber() {
		return routingNumber;
	}
	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}
	public String getAccountHolderName() {
		return accountHolderName;
	}
	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}
	public String getAccountHolderType() {
		return accountHolderType;
	}
	public void setAccountHolderType(String accountHolderType) {
		this.accountHolderType = accountHolderType;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(Integer isDefault) {
		this.isDefault = isDefault;
	}
	public Integer getIsInhSourceCard() {
		return isInhSourceCard;
	}
	public void setIsInhSourceCard(Integer isInhSourceCard) {
		this.isInhSourceCard = isInhSourceCard;
	}
	public Date getCreatedDatetime() {
		return createdDatetime;
	}
	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	public Date getDeletedDatetime() {
		return deletedDatetime;
	}
	public void setDeletedDatetime(Date deletedDatetime) {
		this.deletedDatetime = deletedDatetime;
	}
	public BrewerEntity getBrewer() {
		return brewer;
	}
	public void setBrewer(BrewerEntity brewer) {
		this.brewer = brewer;
	}
	public Long getSgid() {
		return sgid;
	}
	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}
	
}
