package com.inhabitr.admin.dasboard.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.BrewerEntity;
import com.inhabitr.admin.dasboard.model.OrderEntity;

@Repository
public interface PricingRepository extends PagingAndSortingRepository<BrewerEntity, Long> {
	Page<BrewerEntity> findAll(Pageable paging);
}
