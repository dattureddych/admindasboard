package com.inhabitr.admin.dasboard.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inhabitr.admin.dasboard.model.PackageProductRelationEntity;
import com.inhabitr.admin.dasboard.services.PackageProductRelationService;

@RestController
@RequestMapping("/apps/api/packageProductrelation")
public class PackageProductRelationController {
	@Autowired
	private PackageProductRelationService service;

	
	@GetMapping("/")
	public List<PackageProductRelationEntity> findAll() {
		return (List<PackageProductRelationEntity>) service.findAll();
	}
	 
}
