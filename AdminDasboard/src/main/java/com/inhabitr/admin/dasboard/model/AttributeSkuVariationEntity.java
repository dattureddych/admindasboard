package com.inhabitr.admin.dasboard.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Table(name="attribute_sku_variation")
public class AttributeSkuVariationEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	
	private @Column(name = "sku_variation_id") Long skuvariationId;
	private @Column(name = "attribute_id") Long attributeId;
	private @Column(name = "record_status") Integer recordStatus;
	private @Column(name = "created_by") Integer createdBy;
	private @Column(name = "isactive") Boolean isactive;
    private @Column(name = "updated_by") Integer updatedBy;
    private @Column(name = "created_at") @Temporal(TemporalType.TIMESTAMP) Date createdAt;
    private @Column(name = "category_id") Integer categoryId;
    
    @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "sku_variation_id", insertable=false, updatable=false)
    @JsonIgnore
    private ProductCombinationsEntity productCombination;
    
    @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "attribute_id", insertable=false, updatable=false)
    private AttributeEntity attribute;
    
    @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "category_id", insertable=false, updatable=false)
    private CategoryEntity category;
       
    

	public AttributeEntity getAttribute() {
		return attribute;
	}

	public Boolean getIsactive() {
		return isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public void setAttribute(AttributeEntity attribute) {
		this.attribute = attribute;
	}

	public Long getSkuvariationId() {
		return skuvariationId;
	}

	public void setSkuvariationId(Long skuvariationId) {
		this.skuvariationId = skuvariationId;
	}

	public Long getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(Long attributeId) {
		this.attributeId = attributeId;
	}

	public Integer getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(Integer recordStatus) {
		this.recordStatus = recordStatus;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public CategoryEntity getCategory() {
		return category;
	}


	public void setCategory(CategoryEntity category) {
		this.category = category;
	}

	public Long getSgid() {
		return sgid;
	}

	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}
	
}
