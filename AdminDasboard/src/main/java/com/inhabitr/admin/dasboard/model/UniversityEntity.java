package com.inhabitr.admin.dasboard.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Anshu Gupta
 */
@Entity @Table(name = "university")
public class UniversityEntity extends GenericKoinPlusDataEntity<UniversityEntity> {
	private @Column(name = "name") String name;
	private @Column(name = "city_name") String cityName;
	private @Column(name = "state_sgid") Long stateSgid;
	private @Column(name = "state_name") String stateCode;
	private @Column(name = "city_sgid") Long citySgid;
	private @Column(name = "created_datetime") @Temporal(TemporalType.TIMESTAMP) Date createdDatetime;
	@ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "state_sgid", insertable=false, updatable=false)
    private StateEntity state;
	@ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "city_sgid", insertable=false, updatable=false)
    private CityEntity city;
	
	/*@OneToMany(fetch = FetchType.LAZY, mappedBy = "university")
    private List<ItemEntity> items;*/
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public Long getStateSgid() {
		return stateSgid;
	}
	public void setStateSgid(Long stateSgid) {
		this.stateSgid = stateSgid;
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public Long getCitySgid() {
		return citySgid;
	}
	public void setCitySgid(Long citySgid) {
		this.citySgid = citySgid;
	}
	public Date getCreatedDatetime() {
		return createdDatetime;
	}
	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	public CityEntity getCity() {
		return city;
	}
	public void setCity(CityEntity city) {
		this.city = city;
	}
	public StateEntity getState() {
		return state;
	}
	public void setState(StateEntity state) {
		this.state = state;
	}

}

