package com.inhabitr.admin.dasboard.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inhabitr.admin.dasboard.model.CategoryEntity;
import com.inhabitr.admin.dasboard.services.CategoryService;


@RestController
@CrossOrigin
@RequestMapping("/apps/api/category")
public class CategoryController {
	@Autowired
	private CategoryService service;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    public Logger getLogger() {
		return logger;
	}
	
	@PostMapping("/getPackageCategoryList")
	public List<CategoryEntity> getPackageCategoryList() {
		getLogger().info("Inside /apps/api/category/getCategoryList");
		
		List<CategoryEntity> l=service.getPackageCategoryList();
		return l;
	
	}
	
	@PostMapping("/getProductCategoryList")
	public List<CategoryEntity> getProductCategoryList() {
		getLogger().info("Inside /apps/api/category/getCategoryList");
		
		List<CategoryEntity> l=service.getProductCategoryList();
		return l;
	
	}
	
	//@PostMapping("/getCategoryListById")
	public CategoryEntity getCategoryListById(Long category_sgid) {
		getLogger().info("Inside /apps/api/category/getCategoryList");
		
		CategoryEntity l=service.getCategory(category_sgid);
		return l;
	
	}
}
