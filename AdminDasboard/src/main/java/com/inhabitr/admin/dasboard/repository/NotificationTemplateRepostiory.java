package com.inhabitr.admin.dasboard.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.NotificationTemplateEntity;





@Repository
public interface NotificationTemplateRepostiory extends CrudRepository<NotificationTemplateEntity, Long> {
	NotificationTemplateEntity findByName(String name);
}
