package com.inhabitr.admin.dasboard.model;


import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "orders")
public class OrderEntity{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;

	private @Column(name = "cart_id") Long cartSgid;
	private @Column(name = "ops_db_cart_id") Long opsDbCartSgid;
	private @Column(name = "payment_type_id") Long paymentTypeSgid;
	private @Column(name = "customer_id") Long customerSgid;
	private @Column(name = "first_name") String firstName;
	private @Column(name = "last_name") String lastName;
	private @Column(name = "phone") String phone;
	private @Column(name = "address") String address;
	private @Column(name = "apt_no") String aptNo;
	private @Column(name = "city") String city;
	private @Column(name = "state") String state;
	private @Column(name = "zip_code") String zipCode;
	private @Column(name = "payment_type") String paymentType;
	private @Column(name = "email") String email;
	private @Column(name = "oder_created_date") String orderCreatedDate;
	private @Column(name = "total") BigDecimal total;
	private @Column(name = "ops_dashboard_order_id") Long opsDashboardOrderId;
	
	private @Column(name = "user_id") Long userSgid;
	private @Column(name = "reference") String reference;
	private @Column(name = "order_type") Integer orderType;
	private @Column(name = "cust_billing_address") String custBillingAddress;
	private @Column(name = "billing_apt_no") String billingAptNo;
	private @Column(name = "billing_state") String billingState;
	private @Column(name = "billing_city") String billingCity;
	private @Column(name = "billing_zip_code") String billingZipCode;
	private @Column(name = "billing_company") String billingCompany;
	private @Column(name = "order_total") BigDecimal orderTotal;
	private @Column(name = "reward_amount") BigDecimal rewardAmount;
	private @Column(name = "order_monthly_total") BigDecimal orderMonthlyTotal;
	private @Column(name = "sales_tax_rate") BigDecimal salesTaxRate;
	private @Column(name = "sales_tax_amount") BigDecimal salesTaxAmount;
	private @Column(name = "signed_at") Integer signedAt;
	private @Column(name = "delivery_person") String deliveryPerson;
	private @Column(name = "delivery_person_contact") String deliveryPersonContact;
	
	private @Column(name = "delivery_date") String deliveryDate;
	private @Column(name = "actual_delivery_date") String actualDeliveryDate;
	private @Column(name = "delivery_hour") String deliveryHour;
	private @Column(name = "delivery_status") String deliveryStatus;
	
		
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "orders")
	private List<OrderItemEntity> orderItem;
	
	
  //  @OneToOne(fetch = FetchType.LAZY) @JoinColumn(name = "cart_id")
  //  @OneToOne(fetch = FetchType.LAZY) @JoinColumn(name = "cart_id", insertable=false, updatable=false)
	//private CartEntity cartEntity;
	@Transient
	private CartEntity cartEntity;
	
	@Transient
	private Long packagePrice;
	
	@Transient
	private CartShippingAddressEntity ShippingAddressData;
	
	public OrderEntity() {
		super();
	}

	public OrderEntity(Long sgid, String firstName, String lastName, String phone, String address, String aptNo, String city,
			String state, String zipCode, String paymentType, String email, String orderCreatedDate) {
		super();
		this.sgid = sgid;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
		this.address = address;
		this.aptNo = aptNo;
		this.city = city;
		this.state = state;
		this.zipCode = zipCode;
		this.paymentType = paymentType;
		this.email = email;
		this.orderCreatedDate = orderCreatedDate;
	}

	public Long getSgid() {
		return sgid;
	}

	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	
	public String getAptNo() {
		return aptNo;
	}

	public void setAptNo(String aptNo) {
		this.aptNo = aptNo;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOrderCreatedDate() {
		return orderCreatedDate;
	}

	public void setOrderCreatedDate(String orderCreatedDate) {
		this.orderCreatedDate = orderCreatedDate;
	}
	

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	
	public Long getOpsDashboardOrderId() {
		return opsDashboardOrderId;
	}

	public void setOpsDashboardOrderId(Long opsDashboardOrderId) {
		this.opsDashboardOrderId = opsDashboardOrderId;
	}
	
	

	public List<OrderItemEntity> getOrderItem() {
		return orderItem;
	}

	public void setOrderItem(List<OrderItemEntity> orderItem) {
		this.orderItem = orderItem;
	}

	@Override
	public String toString() {
		return "Orders [sgid=" + sgid + ", firstName=" + firstName + ", lastName=" + lastName + ", phone=" + phone
				+ ", address=" + address + ", aptNo=" + aptNo + ", city=" + city + ", state=" + state + ", zipCode="
				+ zipCode + ", paymentType=" + paymentType + ", email=" + email + ", orderCreatedDate="
				+ orderCreatedDate + "]";
	}

	public CartEntity getCartEntity() {
		return cartEntity;
	}

	public void setCartEntity(CartEntity cartEntity) {
		this.cartEntity = cartEntity;
	}

	

	public CartShippingAddressEntity getShippingAddressData() {
		return ShippingAddressData;
	}

	public void setShippingAddressData(CartShippingAddressEntity shippingAddressData) {
		ShippingAddressData = shippingAddressData;
	}

	public Long getUserSgid() {
		return userSgid;
	}

	public void setUserSgid(Long userSgid) {
		this.userSgid = userSgid;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Integer getOrderType() {
		return orderType;
	}

	public void setOrderType(Integer orderType) {
		this.orderType = orderType;
	}

	public String getCustBillingAddress() {
		return custBillingAddress;
	}

	public void setCustBillingAddress(String custBillingAddress) {
		this.custBillingAddress = custBillingAddress;
	}

	public String getBillingAptNo() {
		return billingAptNo;
	}

	public void setBillingAptNo(String billingAptNo) {
		this.billingAptNo = billingAptNo;
	}

	public String getBillingState() {
		return billingState;
	}

	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}

	public String getBillingCity() {
		return billingCity;
	}

	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}

	public String getBillingZipCode() {
		return billingZipCode;
	}

	public void setBillingZipCode(String billingZipCode) {
		this.billingZipCode = billingZipCode;
	}

	public String getBillingCompany() {
		return billingCompany;
	}

	public void setBillingCompany(String billingCompany) {
		this.billingCompany = billingCompany;
	}

	public BigDecimal getOrderTotal() {
		return orderTotal;
	}

	public void setOrderTotal(BigDecimal orderTotal) {
		this.orderTotal = orderTotal;
	}

	public BigDecimal getRewardAmount() {
		return rewardAmount;
	}

	public void setRewardAmount(BigDecimal rewardAmount) {
		this.rewardAmount = rewardAmount;
	}

	public BigDecimal getOrderMonthlyTotal() {
		return orderMonthlyTotal;
	}

	public void setOrderMonthlyTotal(BigDecimal orderMonthlyTotal) {
		this.orderMonthlyTotal = orderMonthlyTotal;
	}

	public BigDecimal getSalesTaxRate() {
		return salesTaxRate;
	}

	public void setSalesTaxRate(BigDecimal salesTaxRate) {
		this.salesTaxRate = salesTaxRate;
	}

	public BigDecimal getSalesTaxAmount() {
		return salesTaxAmount;
	}

	public void setSalesTaxAmount(BigDecimal salesTaxAmount) {
		this.salesTaxAmount = salesTaxAmount;
	}

	public Integer getSignedAt() {
		return signedAt;
	}

	public void setSignedAt(Integer signedAt) {
		this.signedAt = signedAt;
	}

	public String getDeliveryPerson() {
		return deliveryPerson;
	}

	public void setDeliveryPerson(String deliveryPerson) {
		this.deliveryPerson = deliveryPerson;
	}

	public String getDeliveryPersonContact() {
		return deliveryPersonContact;
	}

	public void setDeliveryPersonContact(String deliveryPersonContact) {
		this.deliveryPersonContact = deliveryPersonContact;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getActualDeliveryDate() {
		return actualDeliveryDate;
	}

	public void setActualDeliveryDate(String actualDeliveryDate) {
		this.actualDeliveryDate = actualDeliveryDate;
	}

	public String getDeliveryHour() {
		return deliveryHour;
	}

	public void setDeliveryHour(String deliveryHour) {
		this.deliveryHour = deliveryHour;
	}

	public String getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}
	public Long getPackagePrice() {
		return packagePrice;
	}

	public void setPackagePrice(Long packagePrice) {
		this.packagePrice = packagePrice;
	}

	public Long getCartSgid() {
		return cartSgid;
	}

	public void setCartSgid(Long cartSgid) {
		this.cartSgid = cartSgid;
	}

	public Long getOpsDbCartSgid() {
		return opsDbCartSgid;
	}

	public void setOpsDbCartSgid(Long opsDbCartSgid) {
		this.opsDbCartSgid = opsDbCartSgid;
	}

	public Long getPaymentTypeSgid() {
		return paymentTypeSgid;
	}

	public void setPaymentTypeSgid(Long paymentTypeSgid) {
		this.paymentTypeSgid = paymentTypeSgid;
	}

	public Long getCustomerSgid() {
		return customerSgid;
	}

	public void setCustomerSgid(Long customerSgid) {
		this.customerSgid = customerSgid;
	}
	
	
}
