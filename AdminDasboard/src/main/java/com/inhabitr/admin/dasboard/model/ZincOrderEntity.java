package com.inhabitr.admin.dasboard.model;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "zinc_order")
public class ZincOrderEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;

	private @Column(name = "order_sgid") Long orderSgid;
	private @Column(name = "reference") String reference;
	private @Column(name = "zinc_status") String zincStatus;

	@Column(name = "updated_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDateTime;
	
	@Column(name = "delivery_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deliveryDateTime;

	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "zinc_order_sgid", referencedColumnName = "sgid")
	private List<ZincOrderItemEntity> zincItems;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "order_sgid", referencedColumnName = "sgid", insertable = false, updatable = false)
	private OrderEntity orders;

	public ZincOrderEntity() {
	}

	public ZincOrderEntity(Long sgid, Long orderSgid, String reference, String zincStatus, Date updatedDateTime,
			List<ZincOrderItemEntity> zincItems, OrderEntity orders) {
		super();
		this.sgid = sgid;
		this.orderSgid = orderSgid;
		this.reference = reference;
		this.zincStatus = zincStatus;
		this.updatedDateTime = updatedDateTime;
		this.zincItems = zincItems;
		this.orders = orders;
	}

	public Long getSgid() {
		return sgid;
	}

	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}

	public Long getOrderSgid() {
		return orderSgid;
	}

	public void setOrderSgid(Long orderSgid) {
		this.orderSgid = orderSgid;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getZincStatus() {
		return zincStatus;
	}

	public void setZincStatus(String zincStatus) {
		this.zincStatus = zincStatus;
	}

	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}

	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	public List<ZincOrderItemEntity> getZincItems() {
		return zincItems;
	}

	public void setZincItems(List<ZincOrderItemEntity> zincItems) {
		this.zincItems = zincItems;
	}

	public OrderEntity getOrders() {
		return orders;
	}

	public void setOrders(OrderEntity orders) {
		this.orders = orders;
	}

	
	@Override
	public String toString() {
		return "ZincOrders [sgid=" + sgid + ", orderSgid=" + orderSgid + ", reference=" + reference + ", zincStatus="
				+ zincStatus + ", updatedDateTime=" + updatedDateTime + ", zincItems=" + zincItems + ", orders="
				+ orders + "]";
	}

}