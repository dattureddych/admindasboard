package com.inhabitr.admin.dasboard.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.inhabitr.admin.dasboard.model.CategoryEntity;
import com.inhabitr.admin.dasboard.model.ItemEntity;
import com.inhabitr.admin.dasboard.model.CartEntity;
import com.inhabitr.admin.dasboard.model.PackageEntity;
import com.inhabitr.admin.dasboard.repository.AbandonmentCartRepository;

@Service
public class AbandonmentCartService<abandonmentCartRepository> {
	@Autowired
	private AbandonmentCartRepository abandonmentCartRepository;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}

	public List<CartEntity> searchByFilters(Integer cartSize, Boolean isRegistered, String searchStr, Integer pageNo,
			Integer pageSize, String sortDir, String sortBy) {
		getLogger().info("Inside searchPackage()");
		Pageable paging = PageRequest.of(pageNo, pageSize,
				sortDir.equals("asc") ? Sort.by(sortBy).ascending() : Sort.by("sgid").descending());
		Page<CartEntity> searchList = null;
		Page<Long> searchList1 = null;
		Integer cart_size = 0;
		if (cartSize!=null && cartSize == 1) {
			if (cartSize != null && (isRegistered != null && isRegistered == false)
					&& (searchStr != null && !searchStr.equals(""))) {
				cart_size = 5;
				searchList1 = abandonmentCartRepository.findUnRegisteredCartByFilters(searchStr.toUpperCase(),
						cart_size, paging);
				searchList = abandonmentCartRepository.findUnRegisteredCartByFilters(searchStr.toUpperCase(),
						searchList1.getContent(), paging);
			}
			if (cartSize != null && (isRegistered != null && isRegistered == true)
					&& (searchStr != null && !searchStr.equals(""))) {
				cart_size = 5;
				
				searchList1 = abandonmentCartRepository.findRegisteredCartByFilters(searchStr.toUpperCase(), cart_size,
						paging);
				searchList = abandonmentCartRepository.findRegisteredCartByFilters(searchStr.toUpperCase(),
						searchList1.getContent(), paging);
			}
		}

		if (cartSize!=null && cartSize == 2) {
			cart_size = 5;
			if (cartSize != null && (isRegistered != null && isRegistered == false)
					&& (searchStr != null && !searchStr.equals(""))) {
				searchList1 = abandonmentCartRepository.findUnRegisteredCartByFiltersNew(searchStr.toUpperCase(),
						cart_size, paging);
				searchList = abandonmentCartRepository.findUnRegisteredCartByFilters(searchStr.toUpperCase(),
						searchList1.getContent(), paging);
			}
			if (cartSize != null && (isRegistered != null && isRegistered == true)
					&& (searchStr != null && !searchStr.equals(""))) {
				searchList1 = abandonmentCartRepository.findRegisteredCartByFiltersNew(searchStr.toUpperCase(),
						cart_size, paging);
				searchList = abandonmentCartRepository.findRegisteredCartByFilters(searchStr.toUpperCase(),
						searchList1.getContent(), paging);
			}
		}

		if (cartSize!=null && cartSize == 1) {
			cart_size = 5;
			if (cartSize != null && (isRegistered != null && isRegistered == false)
					&& (searchStr == null || searchStr.equals(""))) {
				searchList1 = abandonmentCartRepository.findUnRegisteredCartByFilters(cart_size, paging);
				searchList = abandonmentCartRepository.findUnRegisteredCartByFilters(searchList1.getContent(), paging);
			}
			if (cartSize != null && (isRegistered != null && isRegistered == true)
					&& (searchStr == null || searchStr.equals(""))) {
				searchList1 = abandonmentCartRepository.findRegisteredCartByFilters(cart_size, paging);
				searchList = abandonmentCartRepository.findRegisteredCartByFilters(searchList1.getContent(), paging);
			}
		}
		if (cartSize!=null && cartSize == 2) {
			cart_size = 5;
			if (cartSize != null && (isRegistered != null && isRegistered == false)
					&& (searchStr == null || searchStr.equals(""))) {
				searchList1 = abandonmentCartRepository.findUnRegisteredCartByFiltersNew(cart_size, paging);
				searchList = abandonmentCartRepository.findUnRegisteredCartByFilters(searchList1.getContent(), paging);
			}
			if (cartSize != null && (isRegistered != null && isRegistered == true)
					&& (searchStr == null || searchStr.equals(""))) {
				searchList1 = abandonmentCartRepository.findRegisteredCartByFiltersNew(cart_size, paging);
				searchList = abandonmentCartRepository.findRegisteredCartByFilters(searchList1.getContent(), paging);
			}
		}

		if (cartSize!=null && cartSize == 1) {
			cart_size = 5;
			if (cartSize != null && (isRegistered == null) && (searchStr != null && !searchStr.equals(""))) {
				searchList1 = abandonmentCartRepository.findAllCartByFilters(searchStr.toUpperCase(), cart_size,
						paging);
				searchList = abandonmentCartRepository.findAllCartByFilters(searchStr.toUpperCase(),
						searchList1.getContent(), paging);
			}
		}
		if (cartSize!=null && cartSize == 2) {
			cart_size = 5;
			if (cartSize != null && (isRegistered == null) && (searchStr != null && !searchStr.equals(""))) {
				searchList1 = abandonmentCartRepository.findAllCartByFiltersNew(searchStr.toUpperCase(), cart_size,
						paging);
				searchList = abandonmentCartRepository.findAllCartByFilters(searchStr.toUpperCase(),
						searchList1.getContent(), paging);
			}

		}
		if (cartSize == null && (isRegistered != null && isRegistered == false)
				&& (searchStr != null && !searchStr.equals(""))) {
			searchList1 = abandonmentCartRepository.searchUnRegisteredCart(searchStr.toUpperCase(), paging);
			searchList = abandonmentCartRepository.searchUnRegisteredCart(searchStr.toUpperCase(),
					searchList1.getContent(), paging);
		}
		if (cartSize == null && (isRegistered != null && isRegistered == true)
				&& (searchStr != null && !searchStr.equals(""))) {
			searchList1 = abandonmentCartRepository.searchRegisteredCart(searchStr.toUpperCase(), paging);
			searchList = abandonmentCartRepository.searchRegisteredCart(searchStr.toUpperCase(),
					searchList1.getContent(), paging);
		}
		if (cartSize!=null && cartSize == 1) {
			cart_size = 5;
			if (cartSize != null && (isRegistered == null) && (searchStr == null || searchStr.equals(""))) {

				searchList1 = abandonmentCartRepository.findCartByCartSize(cart_size, paging);
				searchList = abandonmentCartRepository.findCartByCartSize(searchList1.getContent(), paging);
			}
		}
		if (cartSize!=null && cartSize == 2) {
			cart_size = 5;
			if (cartSize != null && (isRegistered == null) && (searchStr == null || searchStr.equals(""))) {
				searchList1 = abandonmentCartRepository.findCartByCartSizeNew(cart_size, paging);
				searchList = abandonmentCartRepository.findCartByCartSize(searchList1.getContent(), paging);
			}
		}
		if (cartSize == null && (isRegistered != null && isRegistered == false)
				&& (searchStr == null || searchStr.equals(""))) {
			searchList1 = abandonmentCartRepository.findUnRegisteredCart(paging);
			searchList = abandonmentCartRepository.findUnRegisteredCart(searchList1.getContent(), paging);
		}
		if (cartSize == null && (isRegistered != null && isRegistered == true)
				&& (searchStr == null || searchStr.equals(""))) {
			searchList1 = abandonmentCartRepository.findRegisteredCart(paging);
			searchList = abandonmentCartRepository.findRegisteredCart(searchList1.getContent(), paging);
		}

		if (cartSize == null && (isRegistered == null) && (searchStr != null && !searchStr.equals(""))) {
			searchList1 = abandonmentCartRepository.searchCart(searchStr.toUpperCase(), paging);
			searchList = abandonmentCartRepository.searchCart(searchStr.toUpperCase(), searchList1.getContent(),
					paging);
		}
		if (cartSize == null && (isRegistered == null) && (searchStr == null || searchStr.equals(""))) {
			searchList1 = abandonmentCartRepository.findAllCart(paging);
			searchList = abandonmentCartRepository.findAllCart(searchList1.getContent(), paging);
		}
		if (searchList.hasContent()) {
			return searchList.getContent();
		} else {
			return new ArrayList<CartEntity>();
		}
	}

}
