package com.inhabitr.admin.dasboard.model;


import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity @Table(name = "order_item")
public class OrderItemEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	private @Column(name = "cart_sgid") Long cartSgid;
	private @Column(name = "order_sgid") Long orderSgid;
	private @Column(name = "item_name") String itemName;
	private @Column(name = "sku") String sku;
	private @Column(name = "item_sgid") Long itemSgid;
	private @Column(name = "cart_item_sgid") Long cartItemSgid;
   	private @Column(name = "monthly_rent_sgid") Long monthlyRentSgid;
    private @Column(name = "item_image_url") String itemImageUrl;
    private @Column(name = "item_original_price") BigDecimal itemOriginalPrice;
    private @Column(name = "trendbrew_order_number") String trendbrewOrderNumber;
    private @Column(name = "item_price") BigDecimal itemPrice;
    private @Column(name = "retailer_name") String retailerName;
    private @Column(name = "retailer_sgid") Long retailerSgid;
    private @Column(name = "retailer_logo_url") String retailerLogoUrl;
    private @Column(name = "total_final_price") BigDecimal totalFinalPrice;
    private @Column(name = "is_rent") Boolean isRent;
    private @Column(name = "order_processed") Boolean orderProcessed;
    private @Column(name = "is_purchase_confirmed") Boolean isPurchaseConfirmed;
    private @Column(name = "tp_cart_id") String retailerOrderNumber;
    private @Column(name = "tp_purchase_id") String thirdPartyPurchaseId;
    private @Column(name = "tp_order_confirm_id") String orderConfirmationId;
    private @Column(name = "status") String status;
    private @Column(name = "status_reason") String statusReason;
    private @Column(name = "final_message") String finalMessage;
    private @Column(name = "retailer_contact") String retailerContact;
    private @Column(name = "tp_order_short_id") String retailerOrderShortNumber;
    private @Column(name = "supplier_sgid") Long supplierSgid;
    private @Column(name = "item_source") String itemSource;
    private @Column(name = "delivery_date1") Long deliveryDate1;
    private @Column(name = "delivery_date2") Long deliveryDate2;
    private @Column(name = "delivery_date3") Long deliveryDate3;
    private @Column(name = "delivery_hour") String deliveryHour;
    private @Column(name = "delivery_hour1") String deliveryHour1;
    private @Column(name = "delivery_hour2") String deliveryHour2;
    private @Column(name = "month") Integer month;
    private @Column(name = "monthly_rent") BigDecimal rentalPrice;
    private @Column(name = "item_quantity") Integer itemQuantity;
    private @Column(name = "is_buy_used") Boolean isBuyUsed;
    private @Column(name = "is_buy_new") Boolean isBuyNew;
    private @Column(name = "is_reassigned") Boolean isReassigned;
    private @Column(name = "is_returnable") Boolean isReturnable = Boolean.FALSE;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "order_sgid", referencedColumnName = "sgid", insertable = false, updatable = false)
	@JsonIgnore
	private OrderEntity orders;
	
    
	public Long getCartSgid() {
		return cartSgid;
	}
	public void setCartSgid(Long cartSgid) {
		this.cartSgid = cartSgid;
	}
	
	public Boolean getIsReturnable() {
		return isReturnable;
	}
	public void setIsReturnable(Boolean isReturnable) {
		this.isReturnable = isReturnable;
	}
	public Long getOrderSgid() {
		return orderSgid;
	}
	public void setOrderSgid(Long orderSgid) {
		this.orderSgid = orderSgid;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public Long getItemSgid() {
		return itemSgid;
	}
	public void setItemSgid(Long itemSgid) {
		this.itemSgid = itemSgid;
	}
	public Long getCartItemSgid() {
		return cartItemSgid;
	}
	public void setCartItemSgid(Long cartItemSgid) {
		this.cartItemSgid = cartItemSgid;
	}
	public Long getMonthlyRentSgid() {
		return monthlyRentSgid;
	}
	public void setMonthlyRentSgid(Long monthlyRentSgid) {
		this.monthlyRentSgid = monthlyRentSgid;
	}
	public String getItemImageUrl() {
		return itemImageUrl;
	}
	public void setItemImageUrl(String itemImageUrl) {
		this.itemImageUrl = itemImageUrl;
	}
	public BigDecimal getItemOriginalPrice() {
		return itemOriginalPrice;
	}
	public void setItemOriginalPrice(BigDecimal itemOriginalPrice) {
		this.itemOriginalPrice = itemOriginalPrice;
	}
	public String getTrendbrewOrderNumber() {
		return trendbrewOrderNumber;
	}
	public void setTrendbrewOrderNumber(String trendbrewOrderNumber) {
		this.trendbrewOrderNumber = trendbrewOrderNumber;
	}
	public BigDecimal getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(BigDecimal itemPrice) {
		this.itemPrice = itemPrice;
	}
	public String getRetailerName() {
		return retailerName;
	}
	public void setRetailerName(String retailerName) {
		this.retailerName = retailerName;
	}
	public Long getRetailerSgid() {
		return retailerSgid;
	}
	public void setRetailerSgid(Long retailerSgid) {
		this.retailerSgid = retailerSgid;
	}
	public String getRetailerLogoUrl() {
		return retailerLogoUrl;
	}
	public void setRetailerLogoUrl(String retailerLogoUrl) {
		this.retailerLogoUrl = retailerLogoUrl;
	}
	public BigDecimal getTotalFinalPrice() {
		return totalFinalPrice;
	}
	public void setTotalFinalPrice(BigDecimal totalFinalPrice) {
		this.totalFinalPrice = totalFinalPrice;
	}
	public Boolean getIsRent() {
		return isRent;
	}
	public void setIsRent(Boolean isRent) {
		this.isRent = isRent;
	}
	public Boolean getOrderProcessed() {
		return orderProcessed;
	}
	public void setOrderProcessed(Boolean orderProcessed) {
		this.orderProcessed = orderProcessed;
	}
	public Boolean getIsPurchaseConfirmed() {
		return isPurchaseConfirmed;
	}
	public void setIsPurchaseConfirmed(Boolean isPurchaseConfirmed) {
		this.isPurchaseConfirmed = isPurchaseConfirmed;
	}
	public String getRetailerOrderNumber() {
		return retailerOrderNumber;
	}
	public void setRetailerOrderNumber(String retailerOrderNumber) {
		this.retailerOrderNumber = retailerOrderNumber;
	}
	public String getThirdPartyPurchaseId() {
		return thirdPartyPurchaseId;
	}
	public void setThirdPartyPurchaseId(String thirdPartyPurchaseId) {
		this.thirdPartyPurchaseId = thirdPartyPurchaseId;
	}
	public String getOrderConfirmationId() {
		return orderConfirmationId;
	}
	public void setOrderConfirmationId(String orderConfirmationId) {
		this.orderConfirmationId = orderConfirmationId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusReason() {
		return statusReason;
	}
	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}
	public String getFinalMessage() {
		return finalMessage;
	}
	public void setFinalMessage(String finalMessage) {
		this.finalMessage = finalMessage;
	}
	public String getRetailerContact() {
		return retailerContact;
	}
	public void setRetailerContact(String retailerContact) {
		this.retailerContact = retailerContact;
	}
	public String getRetailerOrderShortNumber() {
		return retailerOrderShortNumber;
	}
	public void setRetailerOrderShortNumber(String retailerOrderShortNumber) {
		this.retailerOrderShortNumber = retailerOrderShortNumber;
	}
	public Long getSupplierSgid() {
		return supplierSgid;
	}
	public void setSupplierSgid(Long supplierSgid) {
		this.supplierSgid = supplierSgid;
	}
	public String getItemSource() {
		return itemSource;
	}
	public void setItemSource(String itemSource) {
		this.itemSource = itemSource;
	}
	public Long getDeliveryDate1() {
		return deliveryDate1;
	}
	public void setDeliveryDate1(Long deliveryDate1) {
		this.deliveryDate1 = deliveryDate1;
	}
	public Long getDeliveryDate2() {
		return deliveryDate2;
	}
	public void setDeliveryDate2(Long deliveryDate2) {
		this.deliveryDate2 = deliveryDate2;
	}
	public Long getDeliveryDate3() {
		return deliveryDate3;
	}
	public void setDeliveryDate3(Long deliveryDate3) {
		this.deliveryDate3 = deliveryDate3;
	}
	public String getDeliveryHour() {
		return deliveryHour;
	}
	public void setDeliveryHour(String deliveryHour) {
		this.deliveryHour = deliveryHour;
	}
	public String getDeliveryHour1() {
		return deliveryHour1;
	}
	public void setDeliveryHour1(String deliveryHour1) {
		this.deliveryHour1 = deliveryHour1;
	}
	public String getDeliveryHour2() {
		return deliveryHour2;
	}
	public void setDeliveryHour2(String deliveryHour2) {
		this.deliveryHour2 = deliveryHour2;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public BigDecimal getRentalPrice() {
		return rentalPrice;
	}
	public void setRentalPrice(BigDecimal rentalPrice) {
		this.rentalPrice = rentalPrice;
	}
	public Integer getItemQuantity() {
		return itemQuantity;
	}
	public void setItemQuantity(Integer itemQuantity) {
		this.itemQuantity = itemQuantity;
	}
	public Boolean getIsBuyUsed() {
		return isBuyUsed;
	}
	public void setIsBuyUsed(Boolean isBuyUsed) {
		this.isBuyUsed = isBuyUsed;
	}
	public Boolean getIsBuyNew() {
		return isBuyNew;
	}
	public void setIsBuyNew(Boolean isBuyNew) {
		this.isBuyNew = isBuyNew;
	}
	public Boolean getIsReassigned() {
		return isReassigned;
	}
	public void setIsReassigned(Boolean isReassigned) {
		this.isReassigned = isReassigned;
	}
	public OrderEntity getOrders() {
		return orders;
	}
	public void setOrders(OrderEntity orders) {
		this.orders = orders;
	}
	public Long getSgid() {
		return sgid;
	}
	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}

	
	
	
}
