package com.inhabitr.admin.dasboard.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;




/**
 * @author Dattu Reddy
 */

@Entity @Table(name = "property_user_rewards")
public class PropertyUserRewardEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	
	private @Column(name = "reward_id") String rewardId;
	private @Column(name = "first_name") String firstName;
    private @Column(name = "last_name") String lastName;
    private @Column(name = "email") String email;
    private @Column(name = "property_user_sgid") Long propertyUserSgid;
	private @Column(name = "resident_id") String residentId;
	private @Column(name = "property_sgid") Long propertySgid;
	private @Column(name = "brewer_sgid") Long brewerSgid;
	private @Column(name = "reward_amount") BigDecimal rewardAmount;
	private @Column(name = "reward_balance") BigDecimal rewardBalance;
	private @Column(name = "auth_token") String authToken;
	private @Column(name = "status") Integer status;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "userReward")
	private List<PropertyUserRewardHistoryEntity> userRewardHistory;
	
	@OneToOne(fetch = FetchType.LAZY) @JoinColumn(name = "property_sgid", insertable=false, updatable=false)
	private CASLPropertyEntity property;
	 
	public BigDecimal getRewardBalance() {
		return rewardBalance;
	}
	public void setRewardBalance(BigDecimal rewardBalance) {
		this.rewardBalance = rewardBalance;
	}
	public String getRewardId() {
		return rewardId;
	}
	public void setRewardId(String rewardId) {
		this.rewardId = rewardId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getPropertyUserSgid() {
		return propertyUserSgid;
	}
	public void setPropertyUserSgid(Long propertyUserSgid) {
		this.propertyUserSgid = propertyUserSgid;
	}
	public String getResidentId() {
		return residentId;
	}
	public void setResidentId(String residentId) {
		this.residentId = residentId;
	}
	public Long getPropertySgid() {
		return propertySgid;
	}
	public void setPropertySgid(Long propertySgid) {
		this.propertySgid = propertySgid;
	}
	public Long getBrewerSgid() {
		return brewerSgid;
	}
	public void setBrewerSgid(Long brewerSgid) {
		this.brewerSgid = brewerSgid;
	}
	public BigDecimal getRewardAmount() {
		return rewardAmount;
	}
	public void setRewardAmount(BigDecimal rewardAmount) {
		this.rewardAmount = rewardAmount;
	}
	public String getAuthToken() {
		return authToken;
	}
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public CASLPropertyEntity getProperty() {
		return property;
	}
	public void setProperty(CASLPropertyEntity property) {
		this.property = property;
	}
	public List<PropertyUserRewardHistoryEntity> getUserRewardHistory() {
		return userRewardHistory;
	}
	public void setUserRewardHistory(List<PropertyUserRewardHistoryEntity> userRewardHistory) {
		this.userRewardHistory = userRewardHistory;
	}
	public Long getSgid() {
		return sgid;
	}
	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}
	

}
