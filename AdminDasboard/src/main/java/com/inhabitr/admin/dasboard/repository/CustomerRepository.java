package com.inhabitr.admin.dasboard.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.BrewerEntity;
import com.inhabitr.admin.dasboard.model.ItemEntity;


//@Repository
//public interface CustomerRepository  extends PagingAndSortingRepository<BrewerEntity, Long> {
	@Repository
	public interface CustomerRepository extends PagingAndSortingRepository<BrewerEntity, Long> {
	Page<BrewerEntity> findAll(Pageable paging);	
	Page<BrewerEntity> findByFirstNameContainingIgnoreCaseOrMiddleNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(String firstName,String middleName,String lastname,Pageable paging);
	 @Query(value ="select * from brewer b "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid"
			  + " where b.registered_in_ops =:isRegistered and UPPER(b.first_name) like %:searchStr% or UPPER(b.middle_name) like %:searchStr% or UPPER(b.last_name) like %:searchStr%"
			  + " or UPPER(bb.email_address) like %:searchStr% or bb.mobile_number like %:searchStr% or bb.home_phone_number like %:searchStr% " ,
			  nativeQuery = true) 
	Page<BrewerEntity> findByFilters(Boolean isRegistered,String searchStr, Pageable paging);
	
	 @Query(value ="select * from brewer b "
			   + " inner join brewer_contact bb on bb.brewer_sgid=b.sgid"
			  + " where UPPER(b.first_name) like %:searchStr% or UPPER(b.middle_name) like %:searchStr% or UPPER(b.last_name) like %:searchStr%"
			  + " or UPPER(bb.email_address) like %:searchStr% or bb.mobile_number like %:searchStr% or bb.home_phone_number like %:searchStr%" ,   
			  nativeQuery = true) 
	Page<BrewerEntity> findByFilters(String searchStr, Pageable paging);

	 @Query(value ="select * from brewer b "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid",
			   nativeQuery = true) 
	Page<BrewerEntity> findByFilters(Pageable paging);
	 
	 @Query(value ="select * from brewer b "
			   + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid where b.registered_in_ops =:isRegistered",
			   nativeQuery = true) 
	Page<BrewerEntity> findByStatus(Boolean isRegistered,Pageable paging);
	 
	List<BrewerEntity> findBySgid(Long sgid);
	
	@Modifying
	@Transactional
	@Query(value= "update brewer set registered_in_ops=:isRegistered ,ops_dashboard_user_id= :opsDashboardUserId where sgid=:sgid", nativeQuery = true)
	public void updateBrewerInfo(@Param("isRegistered") Boolean isRegistered,@Param("opsDashboardUserId") Long opsDashboardUserId,@Param("sgid") Long sgid);
}
