package com.inhabitr.admin.dasboard.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.BrewerEntity;
import com.inhabitr.admin.dasboard.model.ItemEntity;

@Repository
public interface BrewerRepository extends PagingAndSortingRepository<BrewerEntity, Long> {
	@Query(value = "select * from brewer  "
			+ "WHERE sgid =:sgid ", nativeQuery = true)
public BrewerEntity findByBrewerId(@Param("sgid") Long sgid);
}
