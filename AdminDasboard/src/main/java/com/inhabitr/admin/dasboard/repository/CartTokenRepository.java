package com.inhabitr.admin.dasboard.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.CartTokenEntity;

@Repository
public interface CartTokenRepository extends PagingAndSortingRepository<CartTokenEntity, Long> {

	@Query(value = "select * from cart_token  "
				+ "WHERE cart_sgid =:cartSgid and status =:status ", nativeQuery = true)
	public CartTokenEntity getActiveCart(@Param("cartSgid") Long cartSgid,@Param("status") String status);

	
	@Query(value = "select * from cart_token  "
			+ "WHERE cart_sgid =:cartSgid and UPPER(token) like %:token% ", nativeQuery = true)
public CartTokenEntity getTokenDetails(@Param("cartSgid") Long cartSgid,@Param("token") String token);
}
