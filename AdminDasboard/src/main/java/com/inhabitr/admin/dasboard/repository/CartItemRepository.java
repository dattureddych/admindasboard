package com.inhabitr.admin.dasboard.repository;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.CartItemEntity;
import com.inhabitr.admin.dasboard.model.CategoryEntity;
import com.inhabitr.admin.dasboard.model.ItemEntity;
import com.inhabitr.admin.dasboard.model.PackageEntity;

@Repository
public interface CartItemRepository extends PagingAndSortingRepository<CartItemEntity, Long> {
	@Query(value ="select * from cart_item ci " 
		  	+" where (ci.status='IS_AVAILABLE' or ci.status='INITIATED') and ci.trendbrew_order_number is null  "
		    + " and ci.cart_sgid =:cartSgid",
		  nativeQuery = true) 
	public List<CartItemEntity> findCartItemDetails(@Param("cartSgid") Long cartSgid);
	
	@Query(value ="select * from cart_item ci " 
		  	+" where ci.status='ORDER_CREATED' and ci.trendbrew_order_number is null  "
		    + " and ci.cart_sgid =:cartSgid",
		  nativeQuery = true) 
	public List<CartItemEntity> findCartDetails(@Param("cartSgid") Long cartSgid);
}
