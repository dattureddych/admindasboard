package com.inhabitr.admin.dasboard.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.inhabitr.admin.dasboard.model.ZincOrderItemEntity;
import com.inhabitr.admin.dasboard.repository.ZincOrderRepository;

@Service
public class ZincOrderService {
	@Autowired
	private ZincOrderRepository orderRepository;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
	public ZincOrderItemEntity findAmazonPricebyOrderItemId(Long orderSgid,Long itemSgid) {
		getLogger().info(".................Inside getAllOrder..........");
		ZincOrderItemEntity obj = orderRepository.findAmazonPricebyOrderItemId(orderSgid,itemSgid);
      //  getLogger().info("pagedResult list Size=="+pagedResult.getContent().size());
      return obj;
	}

}
