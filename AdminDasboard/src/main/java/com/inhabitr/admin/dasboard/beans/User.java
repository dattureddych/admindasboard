package com.inhabitr.admin.dasboard.beans;

import java.util.Date;

public class User {

	private Integer sgid;
	private String ca_admin_id;
	private String firstName;
    private String lastName;
    private String middleName;
    private String email;
    private String password;
    private String salt;
    private String tempPassword;
    private String passwordPlain;
    private Date updateDateTime;
    private Integer status;
    private Integer disabled;
    private String message;
    
    
    public String getCa_admin_id() {
		return ca_admin_id;
	}

	public void setCa_admin_id(String ca_admin_id) {
		this.ca_admin_id = ca_admin_id;
	}
    
    
    public String getSalt() {
        return salt;
    }

    public Date getUpdateDateTime() {
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	public void setSalt(String salt) {
        this.salt = salt;
    }
    
    
    public String getTempPassword() {
        return tempPassword;
    }

    public Integer getSgid() {
		return sgid;
	}

	public void setSgid(Integer sgid) {
		this.sgid = sgid;
	}

	public void setTempPassword(String tempPassword) {
        this.tempPassword = tempPassword;
    }

    public String getPasswordPlain() {
        return passwordPlain;
    }

    public void setPasswordPlain(String passwordPlain) {
        this.passwordPlain = passwordPlain;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    
    public Integer getDisabled() {
        return disabled;
    }

    public void setDisabled(Integer disabled) {
        this.disabled = disabled;
    }

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}   

}
