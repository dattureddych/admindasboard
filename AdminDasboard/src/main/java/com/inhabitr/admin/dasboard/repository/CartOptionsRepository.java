package com.inhabitr.admin.dasboard.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.CartOptionsEntity;

@Repository
public interface CartOptionsRepository extends PagingAndSortingRepository<CartOptionsEntity, Long> {
	 @Query(value ="select * from cart_options   "
			  + "where cart_sgid=:sgid and reference=:reference" ,
			 nativeQuery = true)  
	List<CartOptionsEntity> findCartOptionDetailById(Long sgid,String reference);
	 
	 @Query(value ="select * from cart_options "
			  + " where cart_sgid=:sgid and cart_token_sgid=:cartTokenSgid and reference is not null " ,
			 nativeQuery = true)  
	List<CartOptionsEntity> findCartOptionDetailById(Long sgid,Long cartTokenSgid);

	 @Query(value ="select * from cart_options "
			  + " where cart_sgid=:sgid and cart_token_sgid=:cartTokenSgid and reference is null " ,
			 nativeQuery = true)  
	List<CartOptionsEntity> findCartOptionDetailBySgid(Long sgid,Long cartTokenSgid);

	 @Query(value ="select * from cart_options "
			  + " where cart_sgid=:sgid and cart_token_sgid=:cartTokenSgid " ,
			 nativeQuery = true)  
	List<CartOptionsEntity> findCartOptionPaymentySgid(Long sgid,Long cartTokenSgid);
	 
}
