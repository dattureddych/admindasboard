package com.inhabitr.admin.dasboard.model;

public interface Trendable<T extends TrendableEntity> {
    public T getTrendableObject();
}
