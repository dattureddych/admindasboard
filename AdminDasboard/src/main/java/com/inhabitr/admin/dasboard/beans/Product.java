package com.inhabitr.admin.dasboard.beans;


import java.math.BigDecimal;

import javax.persistence.Column;



/**
 * @author Anshu Gupta
 */
public class Product {
	private String name;
	private String url;
	private String image;
    private String description;
    private String category;
    private String brand;
    private String retailer;
    private BigDecimal originalPrice;
    private BigDecimal salePrice;
    private Integer avalibility;
    private Integer isProcured;
    private String sku;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getRetailer() {
		return retailer;
	}
	public void setRetailer(String retailer) {
		this.retailer = retailer;
	}
	public BigDecimal getOriginalPrice() {
		return originalPrice;
	}
	public void setOriginalPrice(BigDecimal originalPrice) {
		this.originalPrice = originalPrice;
	}
	public BigDecimal getSalePrice() {
		return salePrice;
	}
	public void setSalePrice(BigDecimal salePrice) {
		this.salePrice = salePrice;
	}
	
	public Integer getAvalibility() {
		return avalibility;
	}
	public void setAvalibility(Integer avalibility) {
		this.avalibility = avalibility;
	}
	public Integer getIsProcured() {
		return isProcured;
	}
	public void setIsProcured(Integer isProcured) {
		this.isProcured = isProcured;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	
	@Override
	public String toString() {
		return "ProductEntity [name=" + name + ", sku=" + sku + ", image=" + image + "]";
	}
    
}
