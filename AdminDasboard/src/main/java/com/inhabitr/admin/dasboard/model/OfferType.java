package com.inhabitr.admin.dasboard.model;

public enum OfferType implements KoinPlusEnum {

    GENERAL("GENERAL"),
    PERSONAL("PERSONAL");
    
   // private static final KoinPlusEnumEnhancer<OfferType> enhancer = new KoinPlusEnumEnhancer<OfferType>(values());
    
    private String value;
    
    private OfferType(String name) {
        this.value = name;
    }
    
    public String getValue() {
        return value;
    }
   
}
