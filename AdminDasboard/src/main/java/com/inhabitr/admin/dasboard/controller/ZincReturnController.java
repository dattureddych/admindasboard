package com.inhabitr.admin.dasboard.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.HttpClient;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.inhabitr.admin.dasboard.model.CategoryEntity;
import com.inhabitr.admin.dasboard.model.CustomerServiceRequestsEntity;
import com.inhabitr.admin.dasboard.model.ItemEntity;
import com.inhabitr.admin.dasboard.model.ProductCombinationsEntity;
import com.inhabitr.admin.dasboard.model.ProductEntity;
import com.inhabitr.admin.dasboard.model.ProductImageEntity;
import com.inhabitr.admin.dasboard.model.ZincOrderItemEntity;
import com.inhabitr.admin.dasboard.model.ZincReturnEntity;
import com.inhabitr.admin.dasboard.services.CategoryService;
import com.inhabitr.admin.dasboard.services.ItemService;
import com.inhabitr.admin.dasboard.services.ProductService;
import com.inhabitr.admin.dasboard.services.ServiceRequestService;
import com.inhabitr.admin.dasboard.services.ZincOrderService;
import com.inhabitr.admin.dasboard.services.ZincReturnService;


@RestController
@CrossOrigin
@RequestMapping("/apps/api/orderReturns")
public class ZincReturnController {
	private HttpClient httpClient;
	@Autowired
	private ZincReturnService service;
	
	@Autowired
	private ProductService prodService;
	
	@Autowired
	private ItemService itemService;
	
	@Autowired
	private ServiceRequestService serviceRequestService;
	
	@Autowired
	private CategoryService catService;
	
	@Autowired
	private ZincOrderService zincOrderService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private ObjectMapper mapper = new ObjectMapper();
	
	public Logger getLogger() {
		return logger;
	}
	
	@PostMapping("/")
	public List<ZincReturnEntity> getCustomerList(@RequestParam(defaultValue = "") String status,
			@RequestParam(defaultValue = "") String searchStr,@RequestParam(defaultValue = "0") Integer pageNo, 
            @RequestParam(defaultValue = "10") Integer pageSize,@RequestParam(defaultValue = "asc") String sortDir,
            @RequestParam(defaultValue = "sgid") String sortBy) {
		getLogger().info("Inside /apps/api/orderReturns/?pageNo=" + pageNo + "&pageSize="+ pageSize);
		List<ZincReturnEntity> customerList=new ArrayList<ZincReturnEntity>();
		CategoryEntity categoryEntity = null;
		customerList= (List<ZincReturnEntity>) service.searchByFilters(status,searchStr,pageNo, pageSize,sortDir,sortBy);
		ProductEntity prodDetail= new ProductEntity();
		for (ZincReturnEntity returnObj : customerList) {
		prodDetail=  prodService.getItemDetailsBysgid(returnObj.getItemSgid());
		categoryEntity=  catService.getCategory(prodDetail.getCategoryId());
		if(prodDetail!=null && prodDetail.getProductCombinations()!=null){
			for (int j = 0; j < prodDetail.getProductCombinations().size(); j++) {
				ProductCombinationsEntity combination=prodDetail.getProductCombinations().get(j);
				if(prodDetail.getProductCombinations().get(j).getAssetValue()!=null) {
					Double assetPrice=	prodDetail.getProductCombinations().get(j).getAssetValue();
					if(categoryEntity.getBuyMultiplier()!=null) {
					assetPrice=(((assetPrice)*categoryEntity.getBuyMultiplier().doubleValue()));
					}else {
						assetPrice=(((assetPrice)*1.3));
					}
					assetPrice=assetPrice+(assetPrice*0.10);
				Integer	assetPrice1=assetPrice.intValue();
				prodDetail.getProductCombinations().get(j).setAssetValue(assetPrice1.doubleValue());
				}
				if(prodDetail.getProductCombinations().get(j).getStatus()!=null &&
						prodDetail.getProductCombinations().get(j).getStatus()==1 &&
						prodDetail.getProductCombinations().get(j).getIsBestCombination()!=null && 
						prodDetail.getProductCombinations().get(j).getIsBestCombination()==1) {
				
					returnObj.setAssetValue(combination.getPricingAssetValue());
				}
			
			}
				
			}
		returnObj.setProduct(prodDetail);
		
		
		
		returnObj.setCategory(categoryEntity);
		ZincOrderItemEntity obj=zincOrderService.findAmazonPricebyOrderItemId(returnObj.getOrderSgid(),returnObj.getItemSgid());
		if(obj!=null) {
		returnObj.setAmazonPrice(obj.getAmazonPrice());
		returnObj.setInhabitrPrice(obj.getPrice());
		}
		String imageUrl=  itemService.getImagePath(returnObj.getItemSgid());
		returnObj.setImageUrl(imageUrl);
		String serviceRequestNumberObj=  serviceRequestService.getServiceRequestDetail(returnObj.getReference(),returnObj.getItemSgid());
		returnObj.setServiceRequestNumber(serviceRequestNumberObj);
		
		}		
				
		

	
		return customerList;
	}
	
	
}
