package com.inhabitr.admin.dasboard.beans;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.inhabitr.admin.dasboard.model.BrewerContactEntity;

public class BrewerEntity_ {
	
    private String firstName;
    private String lastName;
    private String middleName;
    private String salutation;
	private Date signUpDate;
	
	private BrewerContactEntity contact;
private String vendor="Amazon";

private String category= "Decor";
//private String signUpDate="Wed 25th Jan 2021";

private Long multiplier=(long) 2.0;

public String getFirstName() {
	return firstName;
}

public void setFirstName(String firstName) {
	this.firstName = firstName;
}

public String getLastName() {
	return lastName;
}

public void setLastName(String lastName) {
	this.lastName = lastName;
}

public String getMiddleName() {
	return middleName;
}

public void setMiddleName(String middleName) {
	this.middleName = middleName;
}

public String getSalutation() {
	return salutation;
}

public void setSalutation(String salutation) {
	this.salutation = salutation;
}

public Date getSignUpDate() {
	return signUpDate;
}

public void setSignUpDate(Date signUpDate) {
	this.signUpDate = signUpDate;
}

public BrewerContactEntity getContact() {
	return contact;
}

public void setContact(BrewerContactEntity contact) {
	this.contact = contact;
}

public String getVendor() {
	return vendor;
}

public void setVendor(String vendor) {
	this.vendor = vendor;
}

public Long getMultiplier() {
	return multiplier;
}

public void setMultiplier(Long multiplier) {
	this.multiplier = multiplier;
}

public String getCategory() {
	return category;
}

public void setCategory(String category) {
	this.category = category;
}


}

