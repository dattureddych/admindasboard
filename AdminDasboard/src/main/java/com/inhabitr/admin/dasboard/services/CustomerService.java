package com.inhabitr.admin.dasboard.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import com.inhabitr.admin.dasboard.model.BrewerEntity;
import com.inhabitr.admin.dasboard.model.ItemEntity;
import com.inhabitr.admin.dasboard.model.PackageEntity;
import com.inhabitr.admin.dasboard.repository.CustomerRepository;

@Service
public class CustomerService {
	@Autowired
	private CustomerRepository customerRepository;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
	
	public List<BrewerEntity> findAll(Integer pageNo, Integer pageSize,String sortDir,String sortBy) {
		logger.info("........Inside findAll of customerService.............");
		Pageable paging = PageRequest.of(pageNo, pageSize,
	            sortDir.equals("asc") ? Sort.by(sortBy).ascending()
                        : Sort.by("sgid").descending());
		 
        Page<BrewerEntity> pagedResult = customerRepository.findAll(paging);
        getLogger().info("pagedResult list Size=="+pagedResult.getContent().size());
        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<BrewerEntity>();
        }
	}
	
	public List<BrewerEntity> searchCustomer(String queryParam, Integer pageNo, Integer pageSize, String sortDir,
			String sortBy) {
		getLogger().info("Inside searchPackage()");
		Pageable paging = PageRequest.of(pageNo, pageSize,
				sortDir.equals("asc") ? Sort.by(sortBy).ascending() : Sort.by("sgid").descending());
		Page<BrewerEntity> searchList = customerRepository.findByFirstNameContainingIgnoreCaseOrMiddleNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(queryParam,queryParam,queryParam,paging);
		
		getLogger().info("searchList size=" + searchList.getContent().size());
		if (searchList.hasContent()) {
			return searchList.getContent();
		} else {
			return new ArrayList<BrewerEntity>();
		}
	}
	
	public List<BrewerEntity> searchByFilters(Boolean isRegistered,String searchStr,Integer pageNo, Integer pageSize, String sortDir,
			String sortBy) {
		getLogger().info("Inside searchByFilters()");
		Pageable paging = PageRequest.of(pageNo, pageSize,
				sortDir.equals("asc") ? Sort.by(sortBy).ascending() : Sort.by("sgid").descending());
		Page<BrewerEntity> customerList = null;
		Page<BrewerEntity> customerList1 = null;
		Long quantity = 0l;

		// customerList =
		// customerRepository.findByFilters(isRegistered,searchStr,paging);

		if ((isRegistered != null) && (searchStr != null && !searchStr.equals(""))) {
			// return (List<BrewerEntity>) service.findAll(pageNo, pageSize,sortDir,sortBy);
			customerList = customerRepository.findByFilters(isRegistered, searchStr.toUpperCase(), paging);
		}
		if ((isRegistered == null) && (searchStr != null && !searchStr.equals(""))) {
			customerList = customerRepository.findByFilters(searchStr.toUpperCase(), paging);
		}
		if ((isRegistered == null) && (searchStr == null || searchStr.equals(""))) {
			customerList = customerRepository.findByFilters(paging);
		}
		if ((isRegistered != null) && (searchStr == null || searchStr.equals(""))) {
			customerList = customerRepository.findByStatus(isRegistered,paging);
		}
		if (customerList.hasContent()) {
			return customerList.getContent();
		} else {
			return new ArrayList<BrewerEntity>();
		}
	}
	
	public List<BrewerEntity>  searchBySgid(Long sgid) {
		getLogger().info("Inside searchByFilters()");
		
		List<BrewerEntity>  customerInfo = null;
		 List<BrewerEntity> customerList = customerRepository.findBySgid(sgid);
	  
		Iterator<BrewerEntity> iterator = customerList.iterator();
		List<BrewerEntity> itemList = new ArrayList<BrewerEntity>();

		//while (iterator.hasNext()) {
			//customerInfo.add(iterator.next());
		//}

	
		return customerList;
	}
	
	public String updateBrewerInfo(Boolean isRegistered,Long opsDashboardUserId,Long sgid) {
		logger.info("Inside /apps/api/package/updateBrewerInfo/isAvailable&sgid"+sgid);		
		try {
			customerRepository.updateBrewerInfo(isRegistered, opsDashboardUserId, sgid);
		}catch(Exception e) {
			e.printStackTrace();
			return "failure";
		}
		return "success";
			
		}
	
}
