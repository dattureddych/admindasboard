package com.inhabitr.admin.dasboard.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.BrewerEntity;
import com.inhabitr.admin.dasboard.model.ConfigurationEntity;

@Repository
public interface ConfigurationRepository extends PagingAndSortingRepository<ConfigurationEntity, Long> {
	Page<ConfigurationEntity> findAll(Pageable paging);	
	
	@Modifying
	@Transactional
	@Query(value= "update configuration set parameter_val =:paramValue where parameter_name='STUDENT_STAGING_FEE'", nativeQuery = true)
	public void updateConfiguration(String paramValue);
	
	//ConfigurationEntity findByName(String name);
	
	@Query(value = "select * from configuration "
				+ "WHERE parameter_name =:name", nativeQuery = true)
	public ConfigurationEntity findByName(@Param("name") String name);
}
