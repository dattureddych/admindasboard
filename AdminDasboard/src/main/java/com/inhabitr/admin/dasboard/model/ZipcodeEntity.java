package com.inhabitr.admin.dasboard.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Anshu Gupta
 */
@Entity @Table(name = "zipcode")
public class ZipcodeEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	private @Column(name = "zipcode") Integer zipcode;
	private @Column(name = "state") String stateName;
	private @Column(name = "id_state") Long stateSgid;
	private @Column(name = "county") String county;
	private @Column(name = "city") String city;
	private @Column(name = "distance_from_60611") Integer distanceFrom60611;
	private @Column(name = "charge_amount") Integer chargeAmount;
    private @Column(name = "config_id") String configId;
    private @Column(name = "id_storage") Integer storageId;
    private @Column(name = "sales_tax_rate") BigDecimal salesTaxRate;
    private @Column(name = "status") Integer status;
    private @Column(name = "created_datetime") @Temporal(TemporalType.TIMESTAMP) Date createdDatetime;
    @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "id_state", insertable=false, updatable=false)
    private StateEntity state;
	public Integer getZipcode() {
		return zipcode;
	}
	public void setZipcode(Integer zipcode) {
		this.zipcode = zipcode;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public Long getStateSgid() {
		return stateSgid;
	}
	public void setStateSgid(Long stateSgid) {
		this.stateSgid = stateSgid;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Integer getDistanceFrom60611() {
		return distanceFrom60611;
	}
	public void setDistanceFrom60611(Integer distanceFrom60611) {
		this.distanceFrom60611 = distanceFrom60611;
	}
	public Integer getChargeAmount() {
		return chargeAmount;
	}
	public void setChargeAmount(Integer chargeAmount) {
		this.chargeAmount = chargeAmount;
	}
	public String getConfigId() {
		return configId;
	}
	public void setConfigId(String configId) {
		this.configId = configId;
	}
	public Integer getStorageId() {
		return storageId;
	}
	public void setStorageId(Integer storageId) {
		this.storageId = storageId;
	}
	public BigDecimal getSalesTaxRate() {
		return salesTaxRate;
	}
	public void setSalesTaxRate(BigDecimal salesTaxRate) {
		this.salesTaxRate = salesTaxRate;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Date getCreatedDatetime() {
		return createdDatetime;
	}
	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	public StateEntity getState() {
		return state;
	}
	public void setState(StateEntity state) {
		this.state = state;
	}
	public Long getSgid() {
		return sgid;
	}
	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}
	
}
