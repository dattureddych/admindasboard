package com.inhabitr.admin.dasboard.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.HttpClient;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.inhabitr.admin.dasboard.model.BrewerEntity;
import com.inhabitr.admin.dasboard.model.TwoTapCartRequestResponseEntity;
import com.inhabitr.admin.dasboard.response.SearchResponse;
import  com.inhabitr.admin.dasboard.services.AdminDashboardApiUtil;
import com.inhabitr.admin.dasboard.services.CustomerService;
import com.inhabitr.admin.dasboard.services.SearchService;
import com.inhabitr.admin.dasboard.services.TwoTapCartRequestResponseService;
import com.koinplus.common.webservice.GenericRestClient;


@RestController
@CrossOrigin
@RequestMapping("/apps/api/customers")
public class CustomerController {
	private HttpClient httpClient;
	@Autowired
	private CustomerService service;
	@Autowired
	private SearchService searchService;
	
	@Autowired
	private GenericRestClient genericRestClient;
	
	@Autowired
	private AdminDashboardApiUtil adminUtil;	
	
	@Autowired
	private TwoTapCartRequestResponseService twoTapCartRequestResponseService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final String OPS_DB_API_END_POINT = "https://apitest.inhabitr.com";//test
	//private final String OPS_DB_API_END_POINT = "https://apps.inhabitr.com";
	private ObjectMapper mapper = new ObjectMapper();
	
	public Logger getLogger() {
		return logger;
	}
	
	@PostMapping("/")
	public List<BrewerEntity> getCustomerList(@RequestParam(defaultValue = "") Boolean isRegistered,
			@RequestParam(defaultValue = "") String searchStr,@RequestParam(defaultValue = "0") Integer pageNo, 
            @RequestParam(defaultValue = "10") Integer pageSize,@RequestParam(defaultValue = "asc") String sortDir,
            @RequestParam(defaultValue = "sgid") String sortBy) {
		getLogger().info("Inside /apps/api/customers/?pageNo=" + pageNo + "&pageSize="+ pageSize);
		List<BrewerEntity> customerList=new ArrayList<BrewerEntity>();
		customerList= (List<BrewerEntity>) service.searchByFilters(isRegistered,searchStr,pageNo, pageSize,sortDir,sortBy);
	
		return customerList;
	}
	
	@PostMapping("/search/{queryParam}")
	public ResponseEntity<List<SearchResponse>> search(@PathVariable("queryParam") String queryParam) {
		getLogger().info("Inside /apps/api/customers/search/"+queryParam);
		return new ResponseEntity<List<SearchResponse>>(searchService.searchCustomer(queryParam), HttpStatus.OK);
	}
	
	@PutMapping("/login/")
	public void login(@RequestParam String email,@RequestParam(defaultValue = "") String password) {
		logger.info("Inside /apps/api/package/hideorShowProduct/isAvailable" +email+ "&sgid"+password);
		//service.loginUser(email,password);
		return;
	}
	
	@PostMapping("/search")
	public List<BrewerEntity> search(@RequestParam(defaultValue = "") Long sgid,@RequestParam("name") String name,@RequestParam(defaultValue = "0") Integer pageNo, 
            @RequestParam(defaultValue = "10000") Integer pageSize,@RequestParam(defaultValue = "asc") String sortDir,
            @RequestParam(defaultValue = "name") String sortBy) {
		getLogger().info("Inside /apps/api/package/search/" + name);
		return (service.searchCustomer(name,pageNo, pageSize,sortDir,sortBy));
	}
	
	@PutMapping("/syncOps")
	public String syncOps(@RequestParam(defaultValue = "") Long sgid,@RequestParam(defaultValue = "") String firstName,
			@RequestParam(defaultValue = "") String lastName,@RequestParam(defaultValue = "") String mobileNumber,
			@RequestParam(defaultValue = "") String email,@RequestParam(defaultValue = "") String password,@RequestParam(defaultValue = "") String about,
			@RequestParam(defaultValue = "") String isStudent,@RequestParam(defaultValue = "") String organization,
			@RequestParam(defaultValue = "") String sendEmail,@RequestParam(defaultValue = "") String sendSms,@RequestParam(defaultValue = "") String student) {
		Map<String, Object> responseMap = null;
        String responseMessage=null;
		try {
			Map<String, Object> request = new HashMap<String, Object>();
		
			request.put("first_name", firstName);
			request.put("last_name", lastName);
			if (mobileNumber != null) {
				request.put("mobile", mobileNumber);
			} else {
				request.put("mobile",mobileNumber);
			}
			request.put("email", email);
			request.put("password", "Abc123456");
			request.put("about", "");
			request.put("is_student", "no");
			request.put("organization", "1");
			request.put("send_email", Boolean.FALSE);
			request.put("send_sms", Boolean.TRUE);
			// request.put("used_furniture",1);
			request.put("student", "yes");
			//request= new HashMap<String, Object>();
					
					
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("Content-Type", "application/json");
			String endPoint = OPS_DB_API_END_POINT + "/api/auth/register";
			getLogger().info("Cart Request URL is {}", endPoint);
			getLogger().info("create User Request is {}", request);
			String response =this.genericRestClient.doPost(endPoint, request, headers);
			 /* "
			  * 
			  * 
			  * {password:abc123, "
			  *
			 
					+ "student:1,"
					+ " organization:1, "
					+ " mobile: 8700075228,"
					+ " about:Anshu, "
					+" last_name:Gupta,"
					+" send_email:false," 
					+"first_name:Anshu,"
					+"is_student:no,"
					+"send_sms:true,"
					+"email:anshugupta3@gmail.com}";
					//genericRestClient.doPost(endPoint, request, headers);*/
		//	String request1 ="{password"+":"+password+",student:1"+" mobile:"+mobileNumber+",about:"+about+" ,last_name:"+lastName+",firstName:"+firstName+
			//		",send_email:false,"+"is_student:no,send_sms:true,email:"+email+"}";
					//getLogger().info("create User Request is {}", request);
					
					//change t
		//	String request1 =null;
					//String content = "{"id":1,"name":"ram"}";
					//JSONObject jsonObject= new JSONObject(request.);
					//String amazonRequest = mapper.writeValueAsString(adminUtil.createRequest(request));
//					logger.info("Generated Amazon Request " + amazonRequest);
					//ResponseEntity<String> response = this.adminUtil.createUserRequest(request.toString());
					//JSONObject responseJson = new JSONObject(response.getBody());
					//String zincRequestId = responseJson.getString("request_id");
			TwoTapCartRequestResponseEntity twoTapCartRequestResponseEntity = new TwoTapCartRequestResponseEntity();
			twoTapCartRequestResponseEntity.setRequest(request.toString());
			twoTapCartRequestResponseEntity.setResposne(response.toString());
			twoTapCartRequestResponseEntity.setService("CREATE_USER");
			getLogger().info("Saving Create User Response");
			this.twoTapCartRequestResponseService.saveCart(twoTapCartRequestResponseEntity);
			Long opsDashboardId=null;
			JSONObject responseJson = new JSONObject(response);
			String[] responseArray=null;
			String[] responseArry=null;
			if(responseJson!=null){
				responseArray=response.split(":");
				responseArry=responseArray[1].split(",");
				System.out.println(responseArry[0]);//.contentEquals("id");
				opsDashboardId=Long.parseLong(responseArry[0]);
			}
			if(opsDashboardId!=null) {
				responseMessage=	service.updateBrewerInfo(true, opsDashboardId, sgid);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseMessage;
	}

    public static String getJsonString(Object object) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
            String json = mapper.writeValueAsString(object);
            return json;
        } catch (JsonProcessingException ex) {
            
        }
        return null;
    }
}
