package com.inhabitr.admin.dasboard.model;

/**
 * @author Anshu Gupta
 */
public enum WebsiteType implements KoinPlusEnum<String> {
    BLOG("BLOG"),
    VIDEO("VIDEO"),
    YOUTUBE("YOUTUBE"),
    FACEBOOK("FACEBOOK"),
    TWITTER("TWITTER"),
    INSTAGRAM("INSTAGRAM"),
    SNAPCHAT("SNAPCHAT"),
    PINTEREST("PINTEREST"),;

   // private static final KoinPlusEnumEnhancer<WebsiteType> enhancer = new KoinPlusEnumEnhancer<WebsiteType>(values());

    private String value;

    private WebsiteType(String name) {
        this.value = name;
    }

    public String getValue() {
        return value;
    }

    // This is delegation.
  
}

