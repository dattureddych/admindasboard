package com.inhabitr.admin.dasboard.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inhabitr.admin.dasboard.repository.ServiceRequestRepository;

@Service
public class AdminDashboardUserService {
	@Autowired
	private ServiceRequestRepository serviceRequestRepository;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
}
  