package com.inhabitr.admin.dasboard.model;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;


@Entity @DiscriminatorValue("CATEGORY")
public class CategoryTrendEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;

	/*
	 * @OneToOne(fetch = FetchType.LAZY) @JoinColumn(name =
	 * "category_sgid") @JsonIgnore private CategoryEntity trendableObject;
	 * 
	 * //@Override public CategoryEntity getTrendableObject() { return
	 * trendableObject; }
	 * 
	 * public void setTrendableObject(CategoryEntity trendableObject) {
	 * this.trendableObject = trendableObject; }
	 */
	public Long getSgid() {
		return sgid;
	}

	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}
    
    
}
