package com.inhabitr.admin.dasboard.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import com.inhabitr.admin.dasboard.model.CustomerServiceRequestsEntity;
import com.inhabitr.admin.dasboard.model.ZincOrderEntity;
import com.inhabitr.admin.dasboard.repository.ServiceRequestRepository;

@Service
public class ServiceRequestService {
	@Autowired
	private ServiceRequestRepository serviceRequestRepository;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}

	public List<CustomerServiceRequestsEntity> getAllServiceRequest(String subject,String status,String searchStr,Integer pageNo, Integer pageSize,String sortDir,String sortBy) {
	
		getLogger().info("Inside getAllServiceRequest()");
	/*	Pageable paging = PageRequest.of(pageNo, pageSize,
	            sortDir.equals("asc") ? Sort.by(sortBy).ascending()
                        : Sort.by("sgid").descending());
		 
        Page<CustomerServiceRequestsEntity> pagedResult = serviceRequestRepository.findAll(paging);
        getLogger().info("pagedResult list Size=="+pagedResult.getContent().size());
        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<CustomerServiceRequestsEntity>();
        }*/
		Pageable paging = PageRequest.of(pageNo, pageSize,
				sortDir.equals("asc") ? Sort.by(sortBy).ascending() : Sort.by("sgid").descending());
		Page<CustomerServiceRequestsEntity> customerServiceRequestsEntityList = null;
		
	
		if ((subject != null && !subject.equals("")) && (status != null && !status.equals("")) && (searchStr != null && !searchStr.equals(""))) {
			if(status.equalsIgnoreCase("HYBRID")) {
			customerServiceRequestsEntityList = serviceRequestRepository.findHybridServiceRequest(subject,searchStr.toUpperCase(), paging);
			}else {
				customerServiceRequestsEntityList = serviceRequestRepository.findServiceRequest(subject,status, searchStr.toUpperCase(), paging);	
			}	
			
		}
		if ((subject != null && !subject.equals("")) && (status != null && !status.equals("")) && (searchStr == null || searchStr.equals(""))) {
			if(status.equalsIgnoreCase("HYBRID")) {
			customerServiceRequestsEntityList = serviceRequestRepository.findHybridServiceRequest(subject,paging);
			}else {
			customerServiceRequestsEntityList = serviceRequestRepository.findServiceRequest(subject,status, paging);
			}
		}
		if ((subject != null && !subject.equals("")) && (status == null || status.equals("")) && (searchStr != null && !searchStr.equals(""))) {
			customerServiceRequestsEntityList = serviceRequestRepository.searchServiceRequest(subject, searchStr.toUpperCase(), paging);	
		}
		if ((subject == null || subject.equals("")) && (status != null && !status.equals("")) && (searchStr != null && !searchStr.equals(""))) {
			if(status.equalsIgnoreCase("HYBRID")) {
			customerServiceRequestsEntityList = serviceRequestRepository.searchHybridServiceRequestByStatus(searchStr.toUpperCase(), paging);		
			}else {
				customerServiceRequestsEntityList = serviceRequestRepository.searchServiceRequestByStatus(status, searchStr.toUpperCase(), paging);
			}
		}
		if ((subject == null || subject.equals("")) && (status == null || status.equals("")) && (searchStr != null && !searchStr.equals(""))) {
			customerServiceRequestsEntityList = serviceRequestRepository.findWithSearch(searchStr.toUpperCase(), paging);		
		}
		if ((subject == null || subject.equals("")) && (status != null && !status.equals("")) && (searchStr == null || searchStr.equals(""))) {
			if(status.equalsIgnoreCase("HYBRID")) {
			customerServiceRequestsEntityList = serviceRequestRepository.findHybridServiceRequest(paging);
			}else {
			customerServiceRequestsEntityList = serviceRequestRepository.findServiceRequest(status, paging);	
			}
		}
		if ((subject != null && !subject.equals("")) && (status == null || status.equals("")) && (searchStr == null || searchStr.equals(""))) {
			customerServiceRequestsEntityList = serviceRequestRepository.findServiceRequestBySubject(subject, paging);		
		}
		
       if ((subject == null || subject.equals("")) && (status == null || status.equals("")) && (searchStr == null ||searchStr.equals(""))) {
    	   customerServiceRequestsEntityList = serviceRequestRepository.findAllServiceRequest(paging);		
		}
		
		if (customerServiceRequestsEntityList.hasContent()) {
			return customerServiceRequestsEntityList.getContent();
		} else {
			return new ArrayList<CustomerServiceRequestsEntity>();
		}
	}
	
	
	public String getServiceRequestDetail(String orderReference,Long itemSgid) {
		
		getLogger().info("Inside getServiceRequestDetail()");
	
		String obj = serviceRequestRepository.findServiceRequestDetail(orderReference,itemSgid);
		if (obj!=null) {
			return obj;
		} else {
			return "";
		}
	}
}
