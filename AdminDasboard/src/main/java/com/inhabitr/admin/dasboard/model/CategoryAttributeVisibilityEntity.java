package com.inhabitr.admin.dasboard.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



@Entity @Table(name = "categrory_attribute_visibility")
public class CategoryAttributeVisibilityEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	
	private @Column(name = "category_id") Long categoryId;
    private @Column(name = "attribute_type_id") Long attributeTypeId;
    private @Column(name = "is_visible") Boolean isVisible;
    private @Column(name = "display_name") String displayName;
    private @Column(name = "created_at") @Temporal(TemporalType.TIMESTAMP) Date createdAt;
    @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "attribute_type_id", insertable=false, updatable=false)
    private AttributeListEntity attributeList;
    
  //  @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "category_id", insertable=false, updatable=false)
   // private CategoryEntity category;
    

	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public Long getAttributeTypeId() {
		return attributeTypeId;
	}
	public void setAttributeTypeId(Long attributeTypeId) {
		this.attributeTypeId = attributeTypeId;
	}
	public Boolean getIsVisible() {
		return isVisible;
	}
	public void setIsVisible(Boolean isVisible) {
		this.isVisible = isVisible;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public AttributeListEntity getAttributeList() {
		return attributeList;
	}
	public void setAttributeList(AttributeListEntity attributeList) {
		this.attributeList = attributeList;
	}
	public Long getSgid() {
		return sgid;
	}
	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}
	    

}
