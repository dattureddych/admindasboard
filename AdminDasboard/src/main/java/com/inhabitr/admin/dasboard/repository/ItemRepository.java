package com.inhabitr.admin.dasboard.repository;

import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.CategoryEntity;
import com.inhabitr.admin.dasboard.model.ItemEntity;
import com.inhabitr.admin.dasboard.model.PackageEntity;

@Repository
public interface ItemRepository extends PagingAndSortingRepository<ItemEntity, Long> {
Page<ItemEntity> findByproductType(String productType, Pageable paging);
Page<ItemEntity> findByproductTypeAndSgid(String productType, Long sgid,Pageable paging);
//	Page<ItemEntity> findAll(Pageable paging);
	

  @Query(value ="select i.sgid,i.name as itemName,i.image_url, c.name as categoryNamee  from package_product_relation pp "
  +"inner join product p on pp.product_id=p.sgid inner join item i on i.sgid=p.saffron_item_sgid "
  + "inner join category c on c.sgid=p.category_id where pp.product_id=:sgid and i.type =:productType WHERE  ORDER BY ?#{#pageable}",
  countQuery ="select count(*)  from package_product_relation pp "
  		 		+ "  inner join product p on pp.product_id=p.sgid inner join item i on i.sgid=p.saffron_item_sgid "
  		+ " inner join category c on c.sgid=p.category_id where pp.product_id=:sgid and i.type =:productType ",
  nativeQuery = true) 
  Page<ItemEntity> findByproductType1(String productType,Pageable paging);
 
	@Query(value = "select distinct pac.sgid as Package_Id, pac.name as Package_Name,i.image_url from package_product_relation pp "
			+" inner join package pac on pp.package_id=pac.sgid "
			+ "inner join product p on pp.product_id=p.sgid "
			+ "inner join item i on i.sgid=p.saffron_item_sgid "
			+ "inner join category c on c.sgid=p.category_id "
			+ "WHERE i.sgid =:sgid", nativeQuery = true)
	public List<Object> findPackageCount(@Param("sgid") Long sgid);
	
	@Query(value = "select count(*) from package_product_relation pp "
			+" inner join package pac on pp.package_id=pac.sgid "
			+ "inner join product p on pp.product_id=p.sgid "
			+ "inner join item i on i.sgid=p.saffron_item_sgid "
			+ "inner join category c on c.sgid=p.category_id "
			+ "WHERE i.sgid =:sgid", nativeQuery = true)
	public Integer findPackagesCount(@Param("sgid") Long sgid);
	
	@Modifying
	@Transactional
	@Query(value= "update item set is_available=:isAvailable where sgid=:sgid", nativeQuery = true)
	public void updateisAvailable(@Param("isAvailable") Boolean isAvailable,@Param("sgid") Long sgid);
	
	Page<ItemEntity> findByproductTypeAndSgidOrDisplayNameContainingIgnoreCaseOrDisplayNameContainingIgnoreCase(String productType,Long sgid,String name, String displayName,Pageable paging);
	
	 @Query(value ="select *  from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			  + "inner join category c on c.sgid=p.category_id where i.type =:productType and p.category_id=:cat_id  and i.sgid in (:sgids)",
			  nativeQuery = true) 
	List<ItemEntity> findByFilters1(String productType,Long cat_id,List<Long> sgids);
	
	 
	 @Query(value ="select *  from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			  + "inner join category c on c.sgid=p.category_id where i.type =:productType and i.sgid in (:sgids) and ( UPPER(i.display_name) like %:searchStr% or UPPER(c.name) like %:searchStr% )",
			  nativeQuery = true) 
	List<ItemEntity> findAvailableProductsBySearch(String productType,String searchStr,List<Long> sgids);
	
	 @Query(value ="select distinct i.sgid from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			  + "inner join category c on c.sgid=p.category_id where i.type =:productType and p.category_id=:cat_id",
			  nativeQuery = true) 
	 Page<Long> findProductByCategory(String productType,Long cat_id,Pageable paging);
	 
	 @Query(value ="select *  from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			  + "inner join category c on c.sgid=p.category_id where i.type =:productType"
			  + " and p.category_id=:cat_id and i.sgid in (:sgids) ",
			  nativeQuery = true) 
	List<ItemEntity> findByCategory(String productType,Long cat_id,List<Long> sgids);
	 
	 
	 @Query(value ="select distinct i.sgid from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			  + "inner join product_sku_variation sku on sku.product_id=p.sgid  "
			   + "inner join product_images img on img.sku_variation_id=sku.sgid  "
			  + "inner join category c on c.sgid=p.category_id where i.type =:productType and p.category_id=:cat_id and sku.quantity>0 ",
			  nativeQuery = true) 
	Page<Long> findAvailableProducts(String productType,Long cat_id, Pageable paging);
	 
	 @Query(value ="select distinct i.sgid from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			  + "inner join product_sku_variation sku on sku.product_id=p.sgid  "
			   + "inner join product_images img on img.sku_variation_id=sku.sgid  "
			  + "inner join category c on c.sgid=p.category_id where i.type =:productType and p.category_id=:cat_id and sku.quantity=0 ",
			  nativeQuery = true) 
	Page<Long> findUnAvailableProducts(String productType,Long cat_id, Pageable paging);
	 
	 @Query(value ="select distinct i.sgid from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			  + "inner join product_sku_variation sku on sku.product_id=p.sgid  "
			   + "inner join product_images img on img.sku_variation_id=sku.sgid  "
			  + "inner join category c on c.sgid=p.category_id where i.type =:productType "
			  + "and p.category_id=:cat_id and ( UPPER(i.display_name) like %:searchStr% or "
			  + "UPPER(c.name) like %:searchStr% ) and sku.quantity>0 ",
			  nativeQuery = true) 
	Page<Long> findAvailableProducts(String productType,Long cat_id, String searchStr,Pageable paging);
	 
	 @Query(value ="select distinct i.sgid from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			  + "inner join product_sku_variation sku on sku.product_id=p.sgid  "
			   + "inner join product_images img on img.sku_variation_id=sku.sgid  "
			  + "inner join category c on c.sgid=p.category_id where i.type =:productType and p.category_id=:cat_id "
			  + " and ( UPPER(i.display_name) like %:searchStr% or UPPER(c.name) like %:searchStr% ) and sku.status=1",
			  nativeQuery = true) 
	Page<Long> findUnAvailableProducts(String productType,Long cat_id, String searchStr,Pageable paging);
	 
	 @Query(value ="select distinct i.sgid from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			  + "inner join product_sku_variation sku on sku.product_id=p.sgid  "
			   + "inner join product_images img on img.sku_variation_id=sku.sgid  "
			  + "inner join category c on c.sgid=p.category_id where i.type =:productType and p.category_id=:cat_id  and sku.status=0 "
			  + " and ( UPPER(i.display_name) like %:searchStr% or UPPER(c.name) like %:searchStr% )",
			  nativeQuery = true) 
	Page<Long> findUnAvailableProducts(String productType,String searchStr,Pageable paging);
	 
	 @Query(value ="select distinct i.sgid from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			  + "inner join product_sku_variation sku on sku.product_id=p.sgid  "
			   + "inner join product_images img on img.sku_variation_id=sku.sgid  "
			  + "inner join category c on c.sgid=p.category_id where i.type =:productType and sku.status=1 "
			  + " and ( UPPER(i.display_name) like %:searchStr% or UPPER(c.name) like %:searchStr% )",
			  nativeQuery = true) 
	Page<Long> findAvailableProducts(String productType,String searchStr,Pageable paging);
	 
	 @Query(value ="select distinct i.sgid from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			  + "inner join product_sku_variation sku on sku.product_id=p.sgid  "
			   + "inner join product_images img on img.sku_variation_id=sku.sgid  "
			  + "inner join category c on c.sgid=p.category_id where i.type =:productType "
			  + " and ( UPPER(i.display_name) like %:searchStr% or UPPER(c.name) like %:searchStr% )",
			  nativeQuery = true) 
	Page<Long> findProductsBySearch(String productType,String searchStr,Pageable paging);
	 
	 @Query(value ="select distinct i.sgid from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			  + "inner join product_sku_variation sku on sku.product_id=p.sgid  "
			   + "inner join product_images img on img.sku_variation_id=sku.sgid  "
			  + "inner join category c on c.sgid=p.category_id where i.type =:productType and p.category_id=:cat_id ",
			  nativeQuery = true) 
	Page<Long> findProductsByCategory(String productType,Long cat_id, Pageable paging);
	 
	 
	 @Query(value ="select *  from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			  + "inner join category c on c.sgid=p.category_id where i.type =:productType ",
			  nativeQuery = true) 
	List<ItemEntity> findByAvailability(String productType,Pageable paging); 
	 
	 @Query(value ="select *  from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			   + "inner join product_sku_variation sku on sku.product_id=p.sgid  "
			   + "inner join product_images img on img.sku_variation_id=sku.sgid  "
			  + "inner join category c on c.sgid=p.category_id where i.type =:productType and p.category_id=:cat_id and sku.status = 0 ",
			  nativeQuery = true) 
	List<ItemEntity> findByFilters11(String productType,Long cat_id,Pageable paging);
	 
	 
	 @Query(value ="select * from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			  + "inner join category c on c.sgid=p.category_id where i.type =:productType and p.category_id=:cat_id "
			  + " and ( UPPER(i.display_name) like %:searchStr% or UPPER(c.name) like %:searchStr% ) and i.sgid in (:sgids)",
			  nativeQuery = true) 
	List<ItemEntity> findByFilters1(String productType,Long cat_id,String searchStr,List<Long> sgids);
	 	 
	@Query(value ="select distinct i.sgid from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			  + "inner join category c on c.sgid=p.category_id where i.type =:productType and p.category_id=:cat_id "
			  + " and ( UPPER(i.display_name) like %:searchStr% or UPPER(c.name) like %:searchStr% ) ",
			  nativeQuery = true) 
	Page<Long> findCountByFilters1(String productType,Long cat_id,String searchStr,Pageable paging);
	 
	 
	 @Query(value ="select  *  from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
		 	   + "inner join category c on c.sgid=p.category_id where i.type =:productType "
		 	   + " and i.sgid in (:sgids) ",
			  nativeQuery = true) 
	List<ItemEntity> findByFilters(String productType,List<Long> sgids);
	 
	 @Query(value ="select distinct i.sgid from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
		   	   + "inner join category c on c.sgid=p.category_id where i.type =:productType ",
			  nativeQuery = true) 
	 Page<Long> findCountByFilters(String productType,Pageable paging);
	 
	 @Query(value ="select *  from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			//   + "inner join product_sku_variation sku on sku.product_id=p.sgid  "
			//   + "inner join product_images img on img.sku_variation_id=sku.sgid  "
			  + "inner join category c on c.sgid=p.category_id "
			  + " where i.type =:productType and p.category_id=:cat_id and sku.status = 0 "
			   +" and (UPPER(i.display_name) like ':str'  ) ",
			  nativeQuery = true) 
	List<ItemEntity> findBySearchStr(String productType,Long cat_id,String str,Pageable paging);
	 
	 @Query(value ="select *  from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			   + "inner join product_sku_variation sku on sku.product_id=p.sgid  "
			   + "inner join product_images img on img.sku_variation_id=sku.sgid  "
			  + "inner join category c on c.sgid=p.category_id "
			  + " where i.type =:productType and p.category_id=:cat_id and sku.status = 1"
			 +" and (UPPER(i.display_name) like ':str' ) ",
			  nativeQuery = true) 
	List<ItemEntity> findBySearchStr1(String productType,Long cat_id,String str,Pageable paging);
	
	 @Query(value ="select  count(*)  from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			   + "inner join product_sku_variation sku on sku.product_id=p.sgid  "
			  + "inner join category c on c.sgid=p.category_id WHERE i.type =:productType "
			  + "and sku.status = 0 and i.sgid =:sgid ",
			  nativeQuery = true) 
	Integer findUnavailableVariantsCount(String productType,@Param("sgid") Long sgid);
	 
	 @Query(value ="select distinct i.sgid from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			  + "inner join product_sku_variation sku on sku.product_id=p.sgid  "
			   + "inner join product_images img on img.sku_variation_id=sku.sgid  "
			  + "inner join category c on c.sgid=p.category_id where i.type =:productType and sku.status = 1 ",
			  nativeQuery = true) 
	Page<Long> findAvailableProducts(String productType, Pageable paging);
	 
	 @Query(value ="select distinct i.sgid from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			  + "inner join product_sku_variation sku on sku.product_id=p.sgid  "
			   + "inner join product_images img on img.sku_variation_id=sku.sgid  "
			  + "inner join category c on c.sgid=p.category_id where i.type =:productType and sku.status =0 ",
			  nativeQuery = true) 
	Page<Long> findUnAvailableProducts(String productType, Pageable paging);
	 
	 @Query(value ="select *  from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			  + "inner join category c on c.sgid=p.category_id where i.type =:productType "
			  + " and ( UPPER(i.display_name) like %:searchStr% or UPPER(c.name) like %:searchStr% ) and i.sgid in (:sgids)",
			  nativeQuery = true) 	 
	List<ItemEntity> findAvailableProductsBySearch(String productType,String searchStr,List<Long> sgids,Pageable paging);
	 
	 @Query(value ="select distinct i.sgid from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			  + "inner join category c on c.sgid=p.category_id where i.type =:productType "
			  + " and ( UPPER(i.display_name) like %:searchStr% or UPPER(c.name) like %:searchStr% )",
			  nativeQuery = true) 	 
	List<ItemEntity> findCountAvailableProductsBySearch(String productType,String searchStr,Pageable paging);
	 
	 @Query(value ="select * from item i "
			   + "inner join product p on i.sgid=p.saffron_item_sgid  "
			  + "inner join category c on c.sgid=p.category_id where i.type =:productType  "
			  + " and i.sgid in (:sgids)",
			  nativeQuery = true) 
	List<ItemEntity> findByFilters1(String productType,List<Long> sgids,Pageable paging);
	/* @Query(value ="select  *  from category "
			    + " WHERE sgid =:sgid ",
			   nativeQuery = true) 
	 CategoryEntity findMultipliers(@Param("sgid") Long sgid);*/
	 
		@Query(value = "select count(*) from package_product_relation pp "
				+" inner join package pac on pp.package_id=pac.sgid "
				+ "inner join product p on pp.product_id=p.sgid "
				+ "inner join item i on i.sgid=p.saffron_item_sgid "
				+ "inner join category c on c.sgid=p.category_id "
				+ "WHERE pac.sgid =:sgid and pp.status=1 ", nativeQuery = true)
		public Integer findAvailbleIncludedItemCount(@Param("sgid") Long sgid);
		
		@Query(value = "select count(*) from package_product_relation pp "
				+" inner join package pac on pp.package_id=pac.sgid "
				+ "inner join product p on pp.product_id=p.sgid "
				+ "inner join item i on i.sgid=p.saffron_item_sgid "
				+ "inner join category c on c.sgid=p.category_id "
				+ "WHERE pac.sgid =:sgid and pp.status=1 ", nativeQuery = true)
		public Integer finUnAvailbleIncludedItemCount(@Param("sgid") Long sgid);
	 
		@Query(value = "select count(*) from product p "
				    + "inner join item i on i.sgid=p.saffron_item_sgid "
					+ "WHERE p.sgid =:productId and i.is_available=true ", nativeQuery = true)
		public Integer findAvailbleIncludedItemCount(@Param("productId") Integer productId);
		
		@Query(value = "select count(*) from package_product_relation pp "
				+" inner join package pac on pp.package_id=pac.sgid "
			//	+ "inner join product p on pp.product_id=p.sgid  "
				+ "inner join product_sku_variation sku on sku.sku=pp.sku "
				+ "WHERE  pp.package_id =:pacId and sku.status=1 and pp.product_id =:productId ", nativeQuery = true)
	public Integer findAvailbleIncludedVariantsCount(@Param("productId") Integer productId,@Param("pacId") Integer pacId);
		
		/*@Query(value = "select count(*) from item i "
				+ "inner join product p on i.sgid=p.saffron_item_sgid "
				 + "inner join product_sku_variation sku on sku.product_id=p.sgid  "
				+ "WHERE sku.product_id =:productId and sku.status=1  and i.is_available=true ", nativeQuery = true)
	public Integer findAvailbleIncludedVariantsCount1(@Param("productId") Integer productId);*/
		
		 @Query(value ="select *  from item i "
				   + "inner join product p on i.sgid=p.saffron_item_sgid  "
				  + "inner join category c on c.sgid=p.category_id where i.type =:productType and p.category_id=:cat_id  and i.sgid in (:sgids)",
				  nativeQuery = true) 
		ItemEntity getItemDetailsBysgid(Long sgid);
		 
		 @Query(value = "select image_url from item "
						+ "WHERE sgid =:sgid ", nativeQuery = true)
		public String getImagePath(@Param("sgid") Long sgid);
			
		@Query(value ="select attribute_value from attribute_sku_variation asv "
				   + "inner join attribute a on asv.attribute_id=a.sgid "
				   + "inner join product_sku_variation sku on asv.sku_variation_id=sku.sgid  "
				  + " where category_id	 =:categoryId "
				  + "and sku.product_id =:productId and asv.record_status=1 and sku.sgid =:combinationId",
				  nativeQuery = true) 
		List<String> getVariationAttributeDetail(Long productId,Long categoryId,Long combinationId);		
}
