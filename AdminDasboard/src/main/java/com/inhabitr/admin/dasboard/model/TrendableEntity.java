package com.inhabitr.admin.dasboard.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 *
 * @author abhij
 */
@MappedSuperclass
public class TrendableEntity<T>{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;

    private @Column(name = "image_url") String imageUrl;
    private @Column(name = "name") String name;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // Overridden methods
    @Override public int hashCode() {
        return this.name.toLowerCase().hashCode();
    }

	public Long getSgid() {
		return sgid;
	}

	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}



}

