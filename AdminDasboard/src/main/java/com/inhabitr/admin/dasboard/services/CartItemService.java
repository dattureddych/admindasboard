package com.inhabitr.admin.dasboard.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.inhabitr.admin.dasboard.model.BrewerEntity;
import com.inhabitr.admin.dasboard.model.CategoryEntity;
import com.inhabitr.admin.dasboard.model.CartItemEntity;
import com.inhabitr.admin.dasboard.model.PackageEntity;
import com.inhabitr.admin.dasboard.repository.CartItemRepository;
import com.inhabitr.admin.dasboard.repository.ItemRepository;


@Service
public class CartItemService<cartItemRepository> {
	@Autowired
	private CartItemRepository cartItemRepository;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
	
	public List<CartItemEntity>findCartItemDetails(Long cartSgid) {
		getLogger().info("Inside searchByFilters()");
		
		List<CartItemEntity> customerList = cartItemRepository.findCartItemDetails(cartSgid);
	  
		return customerList;
	}
	
	public List<CartItemEntity>findCartItemDetails(Long cartSgid, Boolean isTrendbrew, Boolean isOrderProccessed,
			String trendbrewOrderNumber, String status) {
		getLogger().info("Inside searchByFilters()");
		
		List<CartItemEntity> customerList = cartItemRepository.findCartDetails(cartSgid);
	  
		return customerList;
	}
}
