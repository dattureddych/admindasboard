package com.inhabitr.admin.dasboard.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity @Table(name = "customer_service_requests")
public class CustomerServiceRequestsEntity{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	
	@Column(name="brewer_id")
	private Long brewerId;
	
	@Column(name="order_id")
	private String orderId;
	
	@Column(name="service_reference_id")
	private String serviceReferenceId;
	
	@Column(name="product_name")
	private String productName;
	
	@Column(name="item_sgid")
	private Long itemSgid;
	
	@Column(name="subject")
	private String subject;
	
	@Column(name="message")
	private String message;
	
	@Column(name="status")
	private String status;
	
    private @Column(name = "created_datetime") @Temporal(TemporalType.TIMESTAMP) Date createdDatetime;
    
    
    @Column(name="assignedTo")
	private String assignedTo;
    	
	//@ManyToOne(fetch = FetchType.EAGER)
	//@JoinColumn(name = "brewer_id", insertable = false, updatable = false)
//	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	//private BrewerEntity brewer;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "brewer_id", referencedColumnName = "sgid", insertable = false, updatable = false)
	private BrewerEntity brewer;
	
	@Transient
	private String paymentType;
	
	public Long getBrewerId() {
		return brewerId;
	}

	public void setBrewerId(Long brewerId) {
		this.brewerId = brewerId;
	}

	public Long getSgid() {
		return sgid;
	}

	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}

	public BrewerEntity getBrewer() {
		return brewer;
	}

	public void setBrewer(BrewerEntity brewer) {
		this.brewer = brewer;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getServiceReferenceId() {
		return serviceReferenceId;
	}

	public void setServiceReferenceId(String serviceReferenceId) {
		this.serviceReferenceId = serviceReferenceId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Long getItemSgid() {
		return itemSgid;
	}

	public void setItemSgid(Long itemSgid) {
		this.itemSgid = itemSgid;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	
	
	
}

