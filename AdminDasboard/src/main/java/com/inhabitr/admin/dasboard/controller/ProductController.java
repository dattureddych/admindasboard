package com.inhabitr.admin.dasboard.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inhabitr.admin.dasboard.model.ProductEntity;
import com.inhabitr.admin.dasboard.services.ProductService;


@RestController
@RequestMapping("/apps/api/product")
public class ProductController {

	@Autowired
	private ProductService service;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}

	@PostMapping("/")
	public List<ProductEntity> getAllProducts(@RequestParam(defaultValue = "0") Integer pageNo, 
            @RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(defaultValue = "asc") String sortOrder,
            @RequestParam(defaultValue = "name") String sortBy) {
	getLogger().info("Inside /apps/api/package/?pageNo=" + pageNo + "&pageSize="+ pageSize);
	List<ProductEntity> l1= service.getAllProducts(pageNo,pageSize,sortOrder,sortBy); 
	getLogger().info("Exit /apps/api/package/?pageNo=" + pageNo + "&pageSize="+ pageSize);
	return l1;
	}

}

