package com.inhabitr.admin.dasboard.model;


import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity @Table(name = "cart_package_products")
public class CartPackageProductsEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	
	private @Column(name = "product_name") String productName;
	private @Column(name = "product_image")  String productImage;
	private @Column(name = "ordered_qty")  Integer orderedQty;
	private @Column(name = "cart_item_sgid") Long cartItemSgid;
	private @Column(name = "package_item_sgid") Long packageItemSgid;
	private @Column(name = "product_item_sgid") Long productItemSgid;
	private @Column(name = "sku")  String sku;
	private @Column(name = "pa_sku")  String paSku;
	private @Column(name = "price") BigDecimal price;
	private @Column(name = "item_type") String itemtype;
	private @Column(name = "is_parent") Boolean isParent;
	private @Column(name = "ops_db_cart_package_products_sgid") Long opsDbCartPackageProductsSgid;
	private @Column(name = "is_returnable") Boolean isReturnable;
	private @Column(name = "amazon_url") String productUrl;
	
	@ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "cart_item_sgid" , insertable=false, updatable=false)
	@JsonIgnore
	private CartItemEntity cartItem;
	
	public String getProductUrl() {
		return productUrl;
	}
	public void setProductUrl(String productUrl) {
		this.productUrl = productUrl;
	}
	public Boolean getIsReturnable() {
		return isReturnable;
	}
	public void setIsReturnable(Boolean isReturnable) {
		this.isReturnable = isReturnable;
	}
	public String getPaSku() {
		return paSku;
	}
	public void setPaSku(String paSku) {
		this.paSku = paSku;
	}
	public Long getOpsDbCartPackageProductsSgid() {
		return opsDbCartPackageProductsSgid;
	}
	public void setOpsDbCartPackageProductsSgid(Long opsDbCartPackageProductsSgid) {
		this.opsDbCartPackageProductsSgid = opsDbCartPackageProductsSgid;
	}
	public Boolean getIsParent() {
		return isParent;
	}
	public void setIsParent(Boolean isParent) {
		this.isParent = isParent;
	}
	public String getItemtype() {
		return itemtype;
	}
	public void setItemtype(String itemtype) {
		this.itemtype = itemtype;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductImage() {
		return productImage;
	}
	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}
	public Integer getOrderedQty() {
		return orderedQty;
	}
	public void setOrderedQty(Integer orderedQty) {
		this.orderedQty = orderedQty;
	}
	public Long getCartItemSgid() {
		return cartItemSgid;
	}
	public void setCartItemSgid(Long cartItemSgid) {
		this.cartItemSgid = cartItemSgid;
	}
	public Long getPackageItemSgid() {
		return packageItemSgid;
	}
	public void setPackageItemSgid(Long packageItemSgid) {
		this.packageItemSgid = packageItemSgid;
	}
	public Long getProductItemSgid() {
		return productItemSgid;
	}
	public void setProductItemSgid(Long productItemSgid) {
		this.productItemSgid = productItemSgid;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public CartItemEntity getCartItem() {
		return cartItem;
	}
	public void setCartItem(CartItemEntity cartItem) {
		this.cartItem = cartItem;
	}
	public Long getSgid() {
		return sgid;
	}
	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}
	
	
}

