package com.inhabitr.admin.dasboard.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity @Table(name = "brewer")
public class BrewerEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	
    private @Column(name = "first_name") String firstName;
    private @Column(name = "last_name") String lastName;
    private @Column(name = "middle_name") String middleName;
    private @Column(name = "salutation") String salutation;
    private @Column(name = "registered_in_ops") String registeredInOps;
    private @Column(name = "ops_dashboard_user_id") Long opsDashboardUserId;
	@Column(name = "updated_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date signUpDate;
	
	private @Column(name = "total_reward_balance") Double  totalRewardBalance;
	
	  @OneToOne(fetch = FetchType.LAZY, mappedBy = "brewer")
	  @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	  private BrewerContactEntity contact;
    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return this.firstName + ' ' + this.lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
	public String getName() {
        return this.firstName + ' ' + this.lastName;
    }
    
    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }   

   public BrewerContactEntity getContact() {
		return contact;
	}

	public void setContact(BrewerContactEntity contact) {
		this.contact = contact;
	}

public Long getSgid() {
		return sgid;
	}

	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}

	
	/*
	 * public List<CustomerServiceRequestsEntity> getCustomerservicerequestsentity()
	 * { return customerservicerequestsentity; }
	 * 
	 * public void
	 * setCustomerservicerequestsentity(List<CustomerServiceRequestsEntity>
	 * customerservicerequestsentity) { this.customerservicerequestsentity =
	 * customerservicerequestsentity; }
	 */



public Date getSignUpDate() {
		return signUpDate;
	}

	public void setSignUpDate(Date signUpDate) {
		this.signUpDate = signUpDate;
	}
	

@Override public boolean equals(Object obj) {
        if (obj instanceof BrewerEntity) {
            return ((BrewerEntity) obj).getSgid().equals(this.getSgid());
        }
        return false;
    }

   @Override
    public String toString() {
        return "BrewerEntity{" + "firstName=" + firstName + '}';
    }

public Double getTotalRewardBalance() {
	return totalRewardBalance;
}

public void setTotalRewardBalance(Double totalRewardBalance) {
	this.totalRewardBalance = totalRewardBalance;
}

public String getRegisteredInOps() {
	return registeredInOps;
}

public void setRegisteredInOps(String registeredInOps) {
	this.registeredInOps = registeredInOps;
}

public Long getOpsDashboardUserId() {
	return opsDashboardUserId;
}

public void setOpsDashboardUserId(Long opsDashboardUserId) {
	this.opsDashboardUserId = opsDashboardUserId;
}
   

    
}
