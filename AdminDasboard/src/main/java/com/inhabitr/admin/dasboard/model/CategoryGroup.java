package com.inhabitr.admin.dasboard.model;


import java.math.BigDecimal;

public class CategoryGroup {

	private Long categoryId;
	private Long countProduct;
	
	

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Long getCountProduct() {
		return countProduct;
	}

	public void setCountProduct(Long countProduct) {
		this.countProduct = countProduct;
	}

	public CategoryGroup(Long categoryId, Long countProduct) {
		this.categoryId = categoryId;
		this.countProduct = countProduct;
	}

	public CategoryGroup() {
	}

}