package com.inhabitr.admin.dasboard.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.NotificationTemplateEntity;





@Repository
public interface NotificationTemplateRepository  extends PagingAndSortingRepository<NotificationTemplateEntity, Long> {
//NotificationTemplateEntity findByName(String name);
@Query(value = "select * from notification_templates "
		+ "WHERE name =:name", nativeQuery = true)
public NotificationTemplateEntity findByName(@Param("name") String name);
}
