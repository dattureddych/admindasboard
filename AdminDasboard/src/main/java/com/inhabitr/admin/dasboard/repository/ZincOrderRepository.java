package com.inhabitr.admin.dasboard.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.ZincOrderItemEntity;

@Repository
public interface ZincOrderRepository extends PagingAndSortingRepository<ZincOrderItemEntity, Long> {
	 @Query(value ="select  * from zinc_order_item where order_sgid=:orderSgid and item_sgid=:itemSgid limit 1",
			 nativeQuery = true)  
	 ZincOrderItemEntity findAmazonPricebyOrderItemId(Long orderSgid,Long itemSgid); 
}
