package com.inhabitr.admin.dasboard.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inhabitr.admin.dasboard.model.BrewerEntity;
import com.inhabitr.admin.dasboard.model.ConfigurationEntity;
import com.inhabitr.admin.dasboard.response.SearchResponse;
import com.inhabitr.admin.dasboard.services.ConfigurationService;
import com.inhabitr.admin.dasboard.services.SearchService;

@RestController
@CrossOrigin
@RequestMapping("/apps/api/cartConfiguration")
public class ConfigurationController {
	@Autowired
	private ConfigurationService service;

	@Autowired
	private SearchService searchService;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
		
	
	@PostMapping("/")
	public List<ConfigurationEntity> getCartConfiguration(@RequestParam(defaultValue = "0") Integer pageNo, 
            @RequestParam(defaultValue = "10") Integer pageSize,@RequestParam(defaultValue = "asc") String sortDir,
            @RequestParam(defaultValue = "firstName") String sortBy) {
		getLogger().info("Inside /apps/api/cartConfiguration/?pageNo=" + pageNo + "&pageSize="+ pageSize);
		return (List<ConfigurationEntity>) service.findAll(pageNo,pageSize,sortDir,sortBy);
	}

	@PutMapping("/updateConfiguration/")
	public String updateConfigurationByName(@RequestParam(defaultValue = "STUDENT_STAGING_FEE") String paramName,
			@RequestParam(defaultValue = "") String paramValue) {
		try {
			service.updateConfiguration(paramName,paramValue);
		} catch (Exception e) {
			e.printStackTrace();
			return "failure";
		}
		return "success";
	}
	
}
