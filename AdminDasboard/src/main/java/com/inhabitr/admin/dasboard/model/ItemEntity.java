package com.inhabitr.admin.dasboard.model;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import java.util.HashSet;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.inhabitr.admin.dasboard.beans.Product;




@Entity
@Table(name = "item")
public class ItemEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	
	 @Column(name = "display_name") 
	 private String displayName;
	 
	 private @Column(name = "slug") String slug;

	@Column(name = "type", updatable = false, nullable = false)
	private String productType;

	@Column(name = "is_prime")
	private boolean isPrime;

	@Column(name = "updated_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdatedAt;

	@Column(name = "source_url")
	private String source_url;
	
	@Column(name = "image_url")
	private String image_url;
	
	private @Column(name = "product_id") 
	String productId;
	
	/* @ManyToMany @JoinTable(name = "item_category", joinColumns = {
     @JoinColumn(name = "item_sgid")}, inverseJoinColumns = { @JoinColumn(name = "category_sgid")})
	 private Set<CategoryEntity> categories;*/
	 
	 @ManyToMany @JoinTable(name = "item_category", joinColumns = {
		        @JoinColumn(name = "item_sgid")}, inverseJoinColumns = {
		        @JoinColumn(name = "category_sgid")})
		    private Set<CategoryEntity> categories;
	
	@OneToOne(fetch = FetchType.EAGER, mappedBy = "item")
    private ProductEntity product;  
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "is_Available")
	private String isAvailable;
	@Transient
	private Integer packageCount;
	
	@Transient
	private Integer unavailbleVariantsCount;
	
	@Transient
	private Long packagePrice;
	
    /*@OneToOne(fetch = FetchType.LAZY, mappedBy = "item")
    @JsonIgnore
	private PackageEntity packege;*/
	
	public ItemEntity() {
		super();
	}
	
	
	  public Set<CategoryEntity> getCategories() {
	        return categories;
	    }

	    public void setCategories(Set<CategoryEntity> categories) {
	        this.categories = categories;
	    }
	
	    public void addCategory(CategoryEntity category) {
	        if (this.categories == null)
	            this.categories = new HashSet<CategoryEntity>();
	        if (!this.categories.contains(category))
	            this.categories.add(category);
	    }

	public Long getSgid() {
		return sgid;
	}

	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}
	   

	public boolean isPrime() {
		return isPrime;
	}

	public void setPrime(boolean isPrime) {
		this.isPrime = isPrime;
	}

	public Date getLastUpdatedAt() {
		return lastUpdatedAt;
	}

	public void setLastUpdatedAt(Date lastUpdatedAt) {
		this.lastUpdatedAt = lastUpdatedAt;
	}

	public String getSource_url() {
		return source_url;
	}

	public void setSource_url(String source_url) {
		this.source_url = source_url;
	}	
	public String getImage_url() {
			return image_url;
		}

		public void setImage_url(String image_url) {
			this.image_url = image_url;
		}
		
		public ProductEntity getProduct() {
			return product;
		}

		public void setProduct(ProductEntity product) {
			this.product = product;
		}


	public String getDisplayName() {
			return displayName;
		}

		public void setDisplayName(String displayName) {
			this.displayName = displayName;
		}

		
		/*
		 * public PackageEntity getPackege() { return packege; }
		 * 
		 * public void setPackege(PackageEntity packege) { this.packege = packege; }
		 */
		

	public String getProductId() {
			return productId;
		}

		public void setProductId(String productId) {
			this.productId = productId;
		}
		
		

	public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		
	public String getIsAvailable() {
			return isAvailable;
		}

		public void setIsAvailable(String isAvailable) {
			this.isAvailable = isAvailable;
		}

	/*@Override
	public String toString() {
		return "Item [sgid=" + sgid + ", productType=" + productType + ", isPrime=" + isPrime + ", lastUpdatedAt="
				+ lastUpdatedAt + ", source_url=" + source_url 
				//+ ", products=" + products
				+ "]";
	}
*/
	public Integer getPackageCount() {
		return packageCount;
	}

	public void setPackageCount(Integer packageCount) {
		this.packageCount = packageCount;
	}

	public Integer getUnavailbleVariantsCount() {
		return unavailbleVariantsCount;
	}

	public void setUnavailbleVariantsCount(Integer unavailbleVariantsCount) {
		this.unavailbleVariantsCount = unavailbleVariantsCount;
	}

	public Long getPackagePrice() {
		return packagePrice;
	}

	public void setPackagePrice(Long packagePrice) {
		this.packagePrice = packagePrice;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	

	
}
