package com.inhabitr.admin.dasboard.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inhabitr.admin.dasboard.beans.BrewerEntity_;
import com.inhabitr.admin.dasboard.model.BrewerEntity;
import com.inhabitr.admin.dasboard.response.SearchResponse;
import com.inhabitr.admin.dasboard.services.PricingService;
import com.inhabitr.admin.dasboard.services.SearchService;

@RestController
@CrossOrigin
@RequestMapping("/apps/api/pricing")
public class PricingController {
	@Autowired
	private PricingService service;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
	
	@PostMapping("/")
	public List<BrewerEntity> findAll(@RequestParam(defaultValue = "0") Integer pageNo, 
            @RequestParam(defaultValue = "10") Integer pageSize,@RequestParam(defaultValue = "first_name") String sortDir,
            @RequestParam(defaultValue = "asc") String sortBy) {
		getLogger().info("Inside /apps/api/pricing/findAll?pageNo=" + pageNo + "&pageSize="
				+ pageSize);
		return (List<BrewerEntity>) service.findAll(pageNo, pageSize,sortDir,sortBy);
	}
	
	@Autowired
	private SearchService searchService;
	
	@PostMapping("/search/{name}")
	public ResponseEntity<List<SearchResponse>> search(@PathVariable("name") String name) {
		getLogger().info("Inside /apps/api/pricing/search/"+name);
		return new ResponseEntity<List<SearchResponse>>(searchService.searchPricing(name), HttpStatus.OK);
	}
}
