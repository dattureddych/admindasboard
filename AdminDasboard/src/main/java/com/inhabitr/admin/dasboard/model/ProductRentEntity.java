package com.inhabitr.admin.dasboard.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity @Table(name = "price")
public class ProductRentEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	private @Column(name = "price_calculation_id") Long priceCalculationId;
    private @Column(name = "asset_price") BigDecimal assetPrice;
    private @Column(name = "rental_price") BigDecimal rentalPrice;
    private @Column(name = "selling_price") BigDecimal sellingPrice;
    private @Column(name = "rent_to_own_price") BigDecimal rentToOwnPrice;
    private @Column(name = "discount") BigDecimal discount;
    private @Column(name = "discount_rate") BigDecimal discountRate;
    private @Column(name = "item_type") Integer itemType;
    private @Column(name = "month") Integer month;
    private @Column(name = "isactive") Boolean isActive;
    private @Column(name = "created_by") Long createdBy;
    private @Column(name = "updated_by") Long updatedBy;
    private @Column(name = "sku_variation_id") Long skuVariationId;
    private @Column(name = "package_id") Long packageId;
    private @Column(name = "created_datetime") @Temporal(TemporalType.TIMESTAMP) Date createdAt;
	
  /*  @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "package_variation_id",insertable=false, updatable=false)
    private PackageProductVariationEntity packageProductVariation;
    
    @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "sku_variation_id",insertable=false, updatable=false)
    private ProductCombinationsEntity productSkuVariation;
    
    @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "package_id",insertable=false, updatable=false)
    private PackageEntity packege; */
    
	public Long getPackageId() {
		return packageId;
	}

	public void setPackageId(Long packageId) {
		this.packageId = packageId;
	}

	public Long getSkuVariationId() {
		return skuVariationId;
	}

	public void setSkuVariationId(Long skuVariationId) {
		this.skuVariationId = skuVariationId;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	/*
	 * public PackageEntity getPackege() { return packege; }
	 * 
	 * public void setPackege(PackageEntity packege) { this.packege = packege; }
	 */

	public Long getPriceCalculationId() {
		return priceCalculationId;
	}

	public void setPriceCalculationId(Long priceCalculationId) {
		this.priceCalculationId = priceCalculationId;
	}

	public BigDecimal getAssetPrice() {
		return assetPrice;
	}

	public void setAssetPrice(BigDecimal assetPrice) {
		this.assetPrice = assetPrice;
	}

	public BigDecimal getRentalPrice() {
		return rentalPrice;
	}

	public void setRentalPrice(BigDecimal rentalPrice) {
		this.rentalPrice = rentalPrice;
	}

	public BigDecimal getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(BigDecimal sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public BigDecimal getRentToOwnPrice() {
		return rentToOwnPrice;
	}

	public void setRentToOwnPrice(BigDecimal rentToOwnPrice) {
		this.rentToOwnPrice = rentToOwnPrice;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public BigDecimal getDiscountRate() {
		return discountRate;
	}

	public void setDiscountRate(BigDecimal discountRate) {
		this.discountRate = discountRate;
	}

	public Integer getItemType() {
		return itemType;
	}

	public void setItemType(Integer itemType) {
		this.itemType = itemType;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/*public PackageProductVariationEntity getPackageProductVariation() {
		return packageProductVariation;
	}

	public void setPackageProductVariation(PackageProductVariationEntity packageProductVariation) {
		this.packageProductVariation = packageProductVariation;
	}

	public ProductCombinationsEntity getProductSkuVariation() {
		return productSkuVariation;
	}

	public void setProductSkuVariation(ProductCombinationsEntity productSkuVariation) {
		this.productSkuVariation = productSkuVariation;
	}*/

	public Long getSgid() {
		return sgid;
	}

	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}
	
	
}
