package com.inhabitr.admin.dasboard.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Anshu Gupta
 */
@Entity @Table(name = "product_images")
public class ProductImageEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	
	private @Column(name = "package_id") Long packageSgid;
    private @Column(name = "sku_variation_id") Long combinationSgid;
    private @Column(name = "image") String image;
   
    @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "sku_variation_id",insertable=false, updatable=false)
    @JsonIgnore
    private ProductCombinationsEntity productSkuVariation;
    
	public ProductCombinationsEntity getProductSkuVariation() {
		return productSkuVariation;
	}
	public void setProductSkuVariation(ProductCombinationsEntity productSkuVariation) {
		this.productSkuVariation = productSkuVariation;
	}
	public Long getPackageSgid() {
		return packageSgid;
	}
	public void setPackageSgid(Long packageSgid) {
		this.packageSgid = packageSgid;
	}
	public Long getCombinationSgid() {
		return combinationSgid;
	}
	public void setCombinationSgid(Long combinationSgid) {
		this.combinationSgid = combinationSgid;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public Long getSgid() {
		return sgid;
	}
	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}
	
}

