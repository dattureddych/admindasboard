package com.inhabitr.admin.dasboard.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.inhabitr.admin.dasboard.model.CategoryEntity;
import com.inhabitr.admin.dasboard.repository.CategoryRepository;



@Service
public class CategoryService<catRepository> {
	@Autowired
	private CategoryRepository catRepository;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
		

	public List<CategoryEntity> getPackageCategoryList() {
		logger.info("...............Inside findByproductType() Starting...................");
			
		getLogger().info("............Inside getAllPackages....................");
		
		Iterable<CategoryEntity> findAll = catRepository.getPackageCategoryList();
		Iterator<CategoryEntity> iterator = findAll.iterator();
		List<CategoryEntity> itemList = new ArrayList<CategoryEntity>();

		while (iterator.hasNext()) {
			itemList.add(iterator.next());
		}

		return itemList;
	}
	
	
	public List<CategoryEntity> getProductCategoryList() {
		logger.info("...............Inside findByproductType() Starting...................");
			
		getLogger().info("............Inside getAllPackages....................");
		
		Iterable<CategoryEntity> findAll = catRepository.getProductCategoryList();
		Iterator<CategoryEntity> iterator = findAll.iterator();
		List<CategoryEntity> itemList = new ArrayList<CategoryEntity>();

		while (iterator.hasNext()) {
			itemList.add(iterator.next());
		}

		return itemList;
	}
	
	public CategoryEntity getCategory(Long sgid) {
		logger.info("...............Inside findByproductType() Starting...................");
			
		getLogger().info("............Inside getAllPackages....................");
		
		Iterable<CategoryEntity> findAll = catRepository.findBySgid(sgid);
		Iterator<CategoryEntity> iterator = findAll.iterator();
		List<CategoryEntity> itemList = new ArrayList<CategoryEntity>();

		while (iterator.hasNext()) {
			itemList.add(iterator.next());
		}

		return itemList.get(0);
	}
		
	}
