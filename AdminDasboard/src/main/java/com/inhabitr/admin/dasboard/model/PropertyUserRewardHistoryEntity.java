package com.inhabitr.admin.dasboard.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



@Entity @Table(name = "property_user_reward_history")
public class PropertyUserRewardHistoryEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	
	private @Column(name = "user_reward_sgid") Long userRewardSgid;
	private @Column(name = "reward_id") String rewardId;
	private @Column(name = "resident_id") String residentId;
	private @Column(name = "property_sgid") Long propertySgid;
	private @Column(name = "brewer_sgid") Long brewerSgid;
	private @Column(name = "reward_amount") BigDecimal rewardAmount;
	private @Column(name = "total_reward_balance") BigDecimal rewardBalance;
	private @Column(name = "status") Integer status;
	private @Column(name = "delete_status") Integer deleteStatus;
	private @Column(name = "start_datetime") @Temporal(TemporalType.TIMESTAMP) Date rewardStartDate;
	private @Column(name = "end_datetime") @Temporal(TemporalType.TIMESTAMP) Date rewardEndDate;
	
	@ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "user_reward_sgid", insertable=false, updatable=false)
    private  PropertyUserRewardEntity userReward;
	
	@OneToOne(fetch = FetchType.LAZY) @JoinColumn(name = "property_sgid", insertable=false, updatable=false)
	private CASLPropertyEntity property;

	public Long getUserRewardSgid() {
		return userRewardSgid;
	}

	public void setUserRewardSgid(Long userRewardSgid) {
		this.userRewardSgid = userRewardSgid;
	}

	public String getRewardId() {
		return rewardId;
	}

	public void setRewardId(String rewardId) {
		this.rewardId = rewardId;
	}

	public String getResidentId() {
		return residentId;
	}

	public void setResidentId(String residentId) {
		this.residentId = residentId;
	}

	public Long getPropertySgid() {
		return propertySgid;
	}

	public void setPropertySgid(Long propertySgid) {
		this.propertySgid = propertySgid;
	}

	public Long getBrewerSgid() {
		return brewerSgid;
	}

	public void setBrewerSgid(Long brewerSgid) {
		this.brewerSgid = brewerSgid;
	}

	public BigDecimal getRewardAmount() {
		return rewardAmount;
	}

	public void setRewardAmount(BigDecimal rewardAmount) {
		this.rewardAmount = rewardAmount;
	}

	public BigDecimal getRewardBalance() {
		return rewardBalance;
	}

	public void setRewardBalance(BigDecimal rewardBalance) {
		this.rewardBalance = rewardBalance;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(Integer deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	public Date getRewardStartDate() {
		return rewardStartDate;
	}

	public void setRewardStartDate(Date rewardStartDate) {
		this.rewardStartDate = rewardStartDate;
	}

	public Date getRewardEndDate() {
		return rewardEndDate;
	}

	public void setRewardEndDate(Date rewardEndDate) {
		this.rewardEndDate = rewardEndDate;
	}

	public PropertyUserRewardEntity getUserReward() {
		return userReward;
	}

	public void setUserReward(PropertyUserRewardEntity userReward) {
		this.userReward = userReward;
	}

	public CASLPropertyEntity getProperty() {
		return property;
	}

	public void setProperty(CASLPropertyEntity property) {
		this.property = property;
	}

	public Long getSgid() {
		return sgid;
	}

	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}	
	 
}
