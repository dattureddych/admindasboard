package com.inhabitr.admin.dasboard.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.inhabitr.admin.dasboard.model.ItemEntity;
import com.inhabitr.admin.dasboard.model.PackageEntity;
import com.inhabitr.admin.dasboard.model.ProductEntity;
import com.inhabitr.admin.dasboard.repository.ProductRepository;

@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
	public List<ProductEntity> getAllProducts(Integer pageNo, Integer pageSize,String sortDir,String sortBy) {
		/*
		 * Iterable<ProductEntity> findAll = productRepository.findAll();
		 * Iterator<ProductEntity> iterator = findAll.iterator(); List<ProductEntity>
		 * productList1 = new ArrayList<ProductEntity>(); while (iterator.hasNext()) {
		 * productList1.add(iterator.next()); }
		 * 
		 * return productList1;
		 */
		getLogger().info("Inside getAllProducts");
		
		Pageable paging = PageRequest.of(pageNo, pageSize,
	            sortDir.equals("asc") ? Sort.by(sortBy).ascending()
                        : Sort.by(sortBy).descending());
		 
        Page<ProductEntity> pagedResult = productRepository.findAll(paging);
        getLogger().info("pagedResult list Size=="+pagedResult.getContent().size());
        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<ProductEntity>();
        }
	}

	
	public ProductEntity getItemDetailsBysgid(Long sgid) {
		getLogger().info("Inside searchPackage()");

		ProductEntity searchList = productRepository.getItemDetailsBysgid(sgid);
		
		if (searchList!=null) {
			return searchList;
		} else {
			return new ProductEntity();
		}
	}
}
