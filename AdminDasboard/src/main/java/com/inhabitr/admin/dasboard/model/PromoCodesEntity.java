package com.inhabitr.admin.dasboard.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.koinplus.common.data.GenericKoinPlusDataEntity;

@Entity @Table(name = "product_coupons")
public class PromoCodesEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	
	@Column(name = "user_id") 
	 private Integer userId ;   
	@Column(name = "code") 
	 private String code;       
	@Column(name = "quantity") 
	 private Integer quantity;  
	@Column(name = "quantity_per_user") 
	 private Integer quantityPerUser ;
	@Column(name = "title") 
	 private String title;      
	@Column(name = "description") 
	 private String description;
	@Column(name = "amount") 
	 private BigDecimal amount; 
	@Column(name = "max_amount") 
	 private BigDecimal maxAmount;
	@Column(name = "type") 
	 private String type;      
	@Column(name = "minimum_cart_value") 
	 private BigDecimal minimumCartValue;
	@Column(name = "ambassador") 
	 private Integer ambassador ;
	@Column(name = "start_date") 
	 private Date startDate;    
	@Column(name = "end_date") 
	 private Date endDate;      
	@Column(name = "option_type") 
	 private String optionType; 
	@Column(name = "number_of_months") 
	 private Integer numberOfMonths ;
	@Column(name = "status") 
	 private Integer status;
	@Column(name = "dashboard_id") 
	private Long dashboardId;
	@Column(name = "brewer_id") 
	 private Long brewerId;
   private @Column(name = "created_datetime") @Temporal(TemporalType.TIMESTAMP) Date createdDatetime;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "promoCodesEntity")
	private List<PromoCodesOptionsEntity> promoCodesOptionsEntity = new ArrayList<PromoCodesOptionsEntity>();
	 
	public Long getDashboardId() {
		return dashboardId;
	}
	public void setDashboardId(Long dashboardId) {
		this.dashboardId = dashboardId;
	}

	public Long getBrewerId() {
		return brewerId;
	}
	public void setBrewerId(Long brewerId) {
		this.brewerId = brewerId;
	}
	public List<PromoCodesOptionsEntity> getPromoCodesOptionsEntity() {
		return promoCodesOptionsEntity;
	}

	public void setPromoCodesOptionsEntity(List<PromoCodesOptionsEntity> promoCodesOptionsEntity) {
		this.promoCodesOptionsEntity = promoCodesOptionsEntity;
	}
	 
	public Date getCreatedDatetime() {
		return createdDatetime;
	}
	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getQuantityPerUser() {
		return quantityPerUser;
	}
	public void setQuantityPerUser(Integer quantityPerUser) {
		this.quantityPerUser = quantityPerUser;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getMaxAmount() {
		return maxAmount;
	}
	public void setMaxAmount(BigDecimal maxAmount) {
		this.maxAmount = maxAmount;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public BigDecimal getMinimumCartValue() {
		return minimumCartValue;
	}
	public void setMinimumCartValue(BigDecimal minimumCartValue) {
		this.minimumCartValue = minimumCartValue;
	}
	public Integer getAmbassador() {
		return ambassador;
	}
	public void setAmbassador(Integer ambassador) {
		this.ambassador = ambassador;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getOptionType() {
		return optionType;
	}
	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}
	public Integer getNumberOfMonths() {
		return numberOfMonths;
	}
	public void setNumberOfMonths(Integer numberOfMonths) {
		this.numberOfMonths = numberOfMonths;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
