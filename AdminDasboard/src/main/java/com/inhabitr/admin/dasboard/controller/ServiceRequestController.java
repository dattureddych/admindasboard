package com.inhabitr.admin.dasboard.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inhabitr.admin.dasboard.model.CartOptionsEntity;
import com.inhabitr.admin.dasboard.model.CartTokenEntity;
import com.inhabitr.admin.dasboard.model.CustomerServiceRequestsEntity;
import com.inhabitr.admin.dasboard.response.SearchResponse;
import com.inhabitr.admin.dasboard.services.OrderService;
import com.inhabitr.admin.dasboard.services.SearchService;
import com.inhabitr.admin.dasboard.services.ServiceRequestService;

@RestController
@CrossOrigin
@RequestMapping("/apps/api/serviceRequest")
public class ServiceRequestController {
	@Autowired
	private ServiceRequestService service;
	
	@Autowired
	private OrderService orderService;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}

	@PostMapping("/")
	public List<CustomerServiceRequestsEntity> getAllServiceRequest(@RequestParam(defaultValue = "") String subject,
			@RequestParam(defaultValue = "") String status,
			@RequestParam(defaultValue = "") String searchStr,@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(defaultValue = "id") String sortOrder,
            @RequestParam(defaultValue = "firstName") String sortBy) {
		getLogger().info("Inside /apps/api/serviceRequest/?pageNo=" + pageNo + "&pageSize="+ pageSize);
		String paymentType="";
		String statusValue="";
		if(status.equalsIgnoreCase("HYBRID REFUND")) {
			statusValue= "HYBRID";
		}
		else if(status.equalsIgnoreCase("BANK REFUND")) {
			statusValue= "BANK";
		}else if(status.equalsIgnoreCase("CARD REFUND")) {
			statusValue= "CARD";
		}else if(status.equalsIgnoreCase("REWARD REFUND")) {
			statusValue= "REWARD";
		}else if(status.equalsIgnoreCase("AFFIRM REFUND")) {
			statusValue= "AFFIRM";
		}else {
			statusValue= status;
		}
		List<CustomerServiceRequestsEntity> list=
				service.getAllServiceRequest(subject,statusValue,searchStr,pageNo, pageSize,sortOrder,sortBy);
		
	
				for (CustomerServiceRequestsEntity request : list) {
					paymentType=orderService.findOrderPaymentDetail(request.getOrderId());
					request.setPaymentType(paymentType);
				}

					
			
		return list;
	}
	
	@Autowired
	private SearchService searchService;
	
	@PostMapping("/search/{name}")
	public ResponseEntity<List<SearchResponse>> search(@PathVariable("name") String name) {
		getLogger().info("Inside /apps/api/serviceRequest/search/" + name);
		return new ResponseEntity<List<SearchResponse>>(searchService.searchServiceRequest(name), HttpStatus.OK);
	}
}
