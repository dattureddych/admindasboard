package com.inhabitr.admin.dasboard.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.TwoTapCartRequestResponseEntity;

@Repository
public interface TwoTapCartRequestResponseRepository extends PagingAndSortingRepository<TwoTapCartRequestResponseEntity, Long> {
/*	@Modifying
	@Transactional
	@Query(value= "update configuration set parameter_val =:paramValue where parameter_name='STUDENT_STAGING_FEE'", nativeQuery = true)
	public void updateTwoTapCartRequest(String paramValue);*/
}
