package com.inhabitr.admin.dasboard.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inhabitr.admin.dasboard.model.BrewerEntity;
import com.inhabitr.admin.dasboard.model.CartEntity;
import com.inhabitr.admin.dasboard.model.CartItemEntity;
import com.inhabitr.admin.dasboard.model.CartOptionsEntity;
import com.inhabitr.admin.dasboard.model.CartTokenEntity;
import com.inhabitr.admin.dasboard.model.CategoryEntity;
import com.inhabitr.admin.dasboard.model.ItemEntity;
import com.inhabitr.admin.dasboard.model.ProductCombinationsEntity;
import com.inhabitr.admin.dasboard.model.ProductImageEntity;
import com.inhabitr.admin.dasboard.services.AbandonmentCartService;
import com.inhabitr.admin.dasboard.services.CartItemService;
import com.inhabitr.admin.dasboard.services.CartOptionsService;
import com.inhabitr.admin.dasboard.services.CartTokenService;
import com.inhabitr.admin.dasboard.services.CategoryService;
import com.inhabitr.admin.dasboard.services.CustomerService;
import com.inhabitr.admin.dasboard.services.ItemService;

@RestController
@CrossOrigin
@RequestMapping("/apps/api/abandonmentCart")
public class AbandonmentCartController {
	
	@Autowired
	private AbandonmentCartService cartService;
	
	@Autowired
	private CustomerService service;
	
	@Autowired
	private CartItemService cartItemService;
	
	@Autowired
	private CartOptionsService cartOptionsService;
	@Autowired
	private CartTokenService cartTokenService;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	public static final String imageURL="https://d12kqwzvfrkt5o.cloudfront.net/products/";
	public Logger getLogger() {
		return logger;
	}

	@PostMapping("/")
	public List<CartEntity> getAbandonmentCart(@RequestParam(defaultValue = "") Integer cartSize, @RequestParam(defaultValue = "") Boolean isAvailable,
			@RequestParam(defaultValue = "") String searchStr, @RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(defaultValue = "asc") String sortDir,
			@RequestParam(defaultValue = "display_name") String sortBy) {
		getLogger().info("Inside /apps/api/abandonmentCart/{" + cartSize + "}?pageNo=" + pageNo + "&pageSize="
				+ pageSize);
		List<BrewerEntity> brewer = new ArrayList<BrewerEntity>();
		List<CartItemEntity> cartItem = new ArrayList<CartItemEntity>();
		Integer unavailableSkuCount = 0;
		Double cartAmountTotal=0.0;
		String stagingFees="";
		List<CartEntity> l = cartService.searchByFilters(cartSize, isAvailable, searchStr,
				pageNo, pageSize, sortDir,sortBy);
		
	    CategoryEntity categoryEntity = null;
		String token="";
		List<CartEntity> ll = new ArrayList<CartEntity>();
		
		for(int i=0; i<l.size();i++) {
			cartItem = cartItemService.findCartItemDetails(l.get(i).getSgid());	
			List<CartTokenEntity> tokenEntity1=l.get(i).getCartTokenEntity();
		//	if(tokenEntity!=null && tokenEntity.size()>0) {
			/*for (CartTokenEntity cartToken : tokenEntity) {
				//brewer = service.searchBySgid(cartToken.getBrewerSgid());	
				//cartToken.setBrewer(brewer.get(0));
			}*/

				
		//}
			if(tokenEntity1!=null && tokenEntity1.size()>0) {
				for (CartTokenEntity cartToken : tokenEntity1) {
					if(cartToken.getStatus().equals("IS_AVAILABLE")) {
						token=cartToken.getToken();
					}
				}

					
			}
			CartTokenEntity tokenEntity=cartTokenService.getTokenDetails(l.get(i).getSgid(), token);
			l.get(i).setCartItemEntity(cartItem);
			
			List<CartOptionsEntity> otherCharges= cartOptionsService.findCartOptionPaymentySgid(l.get(i).getSgid(), tokenEntity!=null?tokenEntity.getSgid():0);
			if(otherCharges!=null && otherCharges.size()>0) {
				for(int k=0;k<otherCharges.size();k++) {
			if(otherCharges.get(k).getOptionType().equalsIgnoreCase("stagingFee")) {
				stagingFees=otherCharges.get(k).getOptionTypeValue();
			}
				}
			}
			
			for(int j=0; j<cartItem.size();j++) {
				if(cartItem.get(j).getItemPrice()!=null) {
				cartAmountTotal=cartAmountTotal+(cartItem.get(j).getItemPrice().doubleValue()*cartItem.get(j).getItemQuantity());
				}
			}
			if(!stagingFees.equals("")) {
			cartAmountTotal= cartAmountTotal+Double.parseDouble(stagingFees);
			}
			brewer = service.searchBySgid(l.get(i).getBrewerSgid());	
			l.get(i).setBrewer(brewer.get(0));
			l.get(i).setCartAmount(cartAmountTotal.doubleValue());
		}
		return l;

	}
	
	@PostMapping("/getCartByOrderId")
	public List<CartEntity> getAbandonmentCart1(@RequestParam(defaultValue = "") Integer cartSize, @RequestParam(defaultValue = "") Boolean isAvailable,
			@RequestParam(defaultValue = "") String searchStr, @RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(defaultValue = "asc") String sortDir,
			@RequestParam(defaultValue = "display_name") String sortBy) {
		getLogger().info("Inside /apps/api/abandonmentCart/{" + cartSize + "}?pageNo=" + pageNo + "&pageSize="
				+ pageSize);
		List<BrewerEntity> brewer = new ArrayList<BrewerEntity>();
		List<CartItemEntity> cartItem = new ArrayList<CartItemEntity>();
		List<CartOptionsEntity> cartOptionsEntitys = new ArrayList<CartOptionsEntity>();
		Integer unavailableSkuCount = 0;
		Double cartAmountTotal=0.0;
		List<CartEntity> l = cartService.searchByFilters(cartSize, isAvailable, searchStr,
				pageNo, pageSize, sortDir,sortBy);
		
	    CategoryEntity categoryEntity = null;
		
		List<CartEntity> ll = new ArrayList<CartEntity>();
		
		for(int i=0; i<l.size();i++) {
			cartItem = cartItemService.findCartItemDetails(l.get(i).getSgid());	
		/*	List<CartTokenEntity> tokenEntity=l.get(i).getCartTokenEntity();
			if(tokenEntity!=null && tokenEntity.size()>0) {
			for (CartTokenEntity cartToken : tokenEntity) {
				brewer = service.searchBySgid(cartToken.getBrewerSgid());	
				cartToken.setBrewer(brewer.get(0));
			}
			
		}*/
			l.get(i).setCartItemEntity(cartItem);
			//cartOptionsEntitys = this.cartOptionsService.findCartOptionPaymentySgid(l.get(i).getSgid(), l.get(i).getCartTokenEntity().get);
			for(int j=0; j<cartItem.size();j++) {
				if(cartItem.get(j).getItemPrice()!=null) {
				cartAmountTotal=cartAmountTotal+(cartItem.get(j).getItemPrice().doubleValue());
				}
			}
			brewer = service.searchBySgid(l.get(i).getBrewerSgid());	
			l.get(i).setBrewer(brewer.get(0));
			l.get(i).setCartAmount(cartAmountTotal.doubleValue());
		}
		return l;

	}
}
