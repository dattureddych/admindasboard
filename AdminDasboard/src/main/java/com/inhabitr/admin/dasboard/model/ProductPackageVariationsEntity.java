package com.inhabitr.admin.dasboard.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



@Entity
@Table(name = "product_package_variations")
public class ProductPackageVariationsEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	private @Column(name = "id_group") Long idGroup;
	private @Column(name = "package_sgid") Long packageSgid;
	private @Column(name = "product_sgid") Long productSgid;
	private @Column(name = "sku_variation_id") Long skuVariationId;
	private @Column(name = "product_connected_id") Long productConnectedId;
	private @Column(name = "combination_connected_id") Long combinationConnectedId;
	private @Column(name = "price") BigDecimal price;
	private @Column(name = "status") Integer status;
	private @Column(name = "created_at") @Temporal(TemporalType.TIMESTAMP) Date createdAt;
	
	@ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "id_group",insertable=false, updatable=false)
    private ProductPackageVariationGroupEntity productPackageVariationGroupEntity;
	
  
	public ProductPackageVariationGroupEntity getProductPackageVariationGroupEntity() {
		return productPackageVariationGroupEntity;
	}
	public void setProductPackageVariationGroupEntity(
			ProductPackageVariationGroupEntity productPackageVariationGroupEntity) {
		this.productPackageVariationGroupEntity = productPackageVariationGroupEntity;
	}
	public Long getIdGroup() {
		return idGroup;
	}
	public void setIdGroup(Long idGroup) {
		this.idGroup = idGroup;
	}
	public Long getPackageSgid() {
		return packageSgid;
	}
	public void setPackageSgid(Long packageSgid) {
		this.packageSgid = packageSgid;
	}
	public Long getProductSgid() {
		return productSgid;
	}
	public void setProductSgid(Long productSgid) {
		this.productSgid = productSgid;
	}
	public Long getSkuVariationId() {
		return skuVariationId;
	}
	public void setSkuVariationId(Long skuVariationId) {
		this.skuVariationId = skuVariationId;
	}
	public Long getProductConnectedId() {
		return productConnectedId;
	}
	public void setProductConnectedId(Long productConnectedId) {
		this.productConnectedId = productConnectedId;
	}
	public Long getCombinationConnectedId() {
		return combinationConnectedId;
	}
	public void setCombinationConnectedId(Long combinationConnectedId) {
		this.combinationConnectedId = combinationConnectedId;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Long getSgid() {
		return sgid;
	}
	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}
	
}
