package com.inhabitr.admin.dasboard.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inhabitr.admin.dasboard.model.CartEntity;
import com.inhabitr.admin.dasboard.repository.CartRepository;


@Service
public class CartService<cartRepository> {
	@Autowired
	private CartRepository cartRepository;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
	public List<CartEntity> getCart(Long brewerId) {
		logger.info("Inside getActiveCart() Starting.....");
		List<CartEntity> searchList = cartRepository.getCart(brewerId);
		logger.info("...searchList Count..."+searchList);
	
		logger.info("Exit getActiveCart() !!!");
		return searchList;
	}	
	

		
	
}
