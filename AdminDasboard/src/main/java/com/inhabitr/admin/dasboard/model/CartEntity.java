package com.inhabitr.admin.dasboard.model;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
@Table(name = "cart")
public class CartEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	
	  private @Column(name = "last_access_datetime") @Temporal(TemporalType.TIMESTAMP) Date lastAccessDate;
	    
	    private @Column(name = "token") String token;
	    private @Column(name = "is_rent") Boolean isRent;
	    private @Column(name = "is_guest") Boolean isGuest;
	    private @Column(name = "brewer_sgid") Long brewerSgid;
		
	    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cartEntity")
	    private List<CartTokenEntity> cartTokenEntity;
	    
	    @Transient
	    private List<CartItemEntity> cartItemEntity;
	    
	    @Transient
	    private Double cartAmount;
	    
	    @Transient
	    private Double rewardBalance;
	    
	    @Transient
		private BrewerEntity brewer;
	    
	    @Transient
	    private Map<String, Object> shippingAndHandlingFees = new HashMap<String, Object>();
	    @Transient
	    private Map<String, Object> addOnFees = new HashMap<String, Object>();
	     
	    private @Column(name = "updated_datetime")
	    @Temporal(TemporalType.TIMESTAMP) Date signUpDate;
	    
		public List<CartTokenEntity> getCartTokenEntity() {
			return cartTokenEntity;
		}
		public void setCartTokenEntity(List<CartTokenEntity> cartTokenEntity) {
			this.cartTokenEntity = cartTokenEntity;
		}
		public Boolean getIsGuest() {
			return isGuest;
		}
		public void setIsGuest(Boolean isGuest) {
			this.isGuest = isGuest;
		}
		public String getToken() {
			return token;
		}
		public void setToken(String token) {
			this.token = token;
		}
		public Boolean getIsRent() {
			return isRent;
		}
		public void setIsRent(Boolean isRent) {
			this.isRent = isRent;
		}
		public Date getLastAccessDate() {
			return lastAccessDate;
		}
		public void setLastAccessDate(Date lastAccessDate) {
			this.lastAccessDate = lastAccessDate;
		}
		public Long getSgid() {
			return sgid;
		}
		public void setSgid(Long sgid) {
			this.sgid = sgid;
		}
		public List<CartItemEntity> getCartItemEntity() {
			return cartItemEntity;
		}
		public void setCartItemEntity(List<CartItemEntity> cartItemEntity) {
			this.cartItemEntity = cartItemEntity;
		}
		public BrewerEntity getBrewer() {
			return brewer;
		}
		public void setBrewer(BrewerEntity brewer) {
			this.brewer = brewer;
		}
		public Double getCartAmount() {
			return cartAmount;
		}
		public void setCartAmount(Double cartAmount) {
			this.cartAmount = cartAmount;
		}
		public Double getRewardBalance() {
			return rewardBalance;
		}
		public void setRewardBalance(Double rewardBalance) {
			this.rewardBalance = rewardBalance;
		}
		public Date getSignUpDate() {
			return signUpDate;
		}
		public void setSignUpDate(Date signUpDate) {
			this.signUpDate = signUpDate;
		}
		public Long getBrewerSgid() {
			return brewerSgid;
		}
		public void setBrewerSgid(Long brewerSgid) {
			this.brewerSgid = brewerSgid;
		}
		public Map<String, Object> getShippingAndHandlingFees() {
			return shippingAndHandlingFees;
		}
		public void setShippingAndHandlingFees(Map<String, Object> shippingAndHandlingFees) {
			this.shippingAndHandlingFees = shippingAndHandlingFees;
		}
		public Map<String, Object> getAddOnFees() {
			return addOnFees;
		}
		public void setAddOnFees(Map<String, Object> addOnFees) {
			this.addOnFees = addOnFees;
		}
		
		
}
