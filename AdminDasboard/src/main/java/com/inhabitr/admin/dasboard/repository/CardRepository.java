package com.inhabitr.admin.dasboard.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.CardEntity;

@Repository
public interface CardRepository extends PagingAndSortingRepository<CardEntity, Long> {
@Query(value = "select * from card "
			+ "WHERE user_sgid =:brewerId and last4 =:cardNumber", nativeQuery = true)
public List<CardEntity> getCard(@Param("brewerId") Long brewerId,@Param("cardNumber") String cardNumber);
}
