package com.inhabitr.admin.dasboard.model;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "product")
public class ProductEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;

	@Column(name = "name")
	private String displayName;

	@Column(name = "saffron_item_sgid")
	private Long safronItemSgid;

	@Column(name = "category_id")
	private Long categoryId;

	@Column(name = "updated_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdatedAt;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "saffron_item_sgid", insertable = false, updatable = false)
	@JsonIgnore
	private ItemEntity item;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
	@JsonIgnoreProperties
	private List<ProductCombinationsEntity> productCombinations;
	
	@Column(name = "features")
	private String features;
	
	@Column(name = "benefits")
	private String benefits;
	
	  private @Column(name = "sku") String sku;
	  private @Column(name = "slug") String slug;

	/*
	 * public ProductEntity() { super(); }
	 * 
	 * public ProductEntity(Long sgid, String displayName, String shortDescription,
	 * String description, String features, String benefits, String supplierSku,
	 * Long safronItemSgid, Date lastUpdatedAt, List<ProductCombinationsEntity>
	 * productCombinations) { super(); this.sgid = sgid; this.displayName =
	 * displayName; this.safronItemSgid = safronItemSgid; this.lastUpdatedAt =
	 * lastUpdatedAt; this.productCombinations = productCombinations; }
	 */

	public Long getSgid() {
		return sgid;
	}

	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Long getSafronItemSgid() {
		return safronItemSgid;
	}

	public void setSafronItemSgid(Long safronItemSgid) {
		this.safronItemSgid = safronItemSgid;
	}

	public Date getLastUpdatedAt() {
		return lastUpdatedAt;
	}

	public void setLastUpdatedAt(Date lastUpdatedAt) {
		this.lastUpdatedAt = lastUpdatedAt;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public List<ProductCombinationsEntity> getProductCombinations() {
		return productCombinations;
	}

	public void setProductCombinations(List<ProductCombinationsEntity> productCombinations) {
		this.productCombinations = productCombinations;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public ItemEntity getItem() {
		return item;
	}

	public void setItem(ItemEntity item) {
		this.item = item;
	}

	public String getFeatures() {
		return features;
	}

	public void setFeatures(String features) {
		this.features = features;
	}

	public String getBenefits() {
		return benefits;
	}

	public void setBenefits(String benefits) {
		this.benefits = benefits;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	
	/*
	 * @Override public String toString() { return "Product [sgid=" + sgid +
	 * ", displayName=" + displayName + ", safronItemSgid=" + safronItemSgid +
	 * ", lastUpdatedAt=" + lastUpdatedAt + ", productCombinations=" +
	 * productCombinations + "]"; }
	 */
}
