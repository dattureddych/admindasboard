package com.inhabitr.admin.dasboard.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.BrewerEntity;
import com.inhabitr.admin.dasboard.model.ItemEntity;
import com.inhabitr.admin.dasboard.model.ProductEntity;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<ProductEntity, Long> {
	Page<ProductEntity> findAll(Pageable paging);	

	 @Query(value ="select *  from product p "
			  + " where p.saffron_item_sgid =:sgid",
			  nativeQuery = true) 
	 ProductEntity getItemDetailsBysgid(Long sgid);
}
