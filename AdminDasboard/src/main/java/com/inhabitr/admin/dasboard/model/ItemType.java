package com.inhabitr.admin.dasboard.model;

public enum ItemType implements KoinPlusEnum<String> {
    PRODUCT("PRODUCT"),
    PACKAGE("PACKAGE"),
	FLOOR_PACKAGE("FLOOR_PACKAGE");
   // private static final KoinPlusEnumEnhancer<ItemType> enhancer = new KoinPlusEnumEnhancer<ItemType>(values());

    private String value;

    private ItemType(String name) {
        this.value = name;
    }

    public String getValue() {
        return value;
    }

   
}