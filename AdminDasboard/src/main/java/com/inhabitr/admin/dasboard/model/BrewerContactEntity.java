package com.inhabitr.admin.dasboard.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity @Table(name = "brewer_contact")
public class BrewerContactEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	private @Column(name = "email_address") String emailAddress;
    private @Column(name = "mobile_number") String mobileNumber;
    private @Column(name = "home_phone_number") String homePhoneNumber;
    
    @OneToOne(fetch = FetchType.LAZY) @JoinColumn(name = "brewer_sgid")
    @JsonIgnore
    private BrewerEntity brewer;

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getHomePhoneNumber() {
        return homePhoneNumber;
    }

    public void setHomePhoneNumber(String homePhoneNumber) {
        this.homePhoneNumber = homePhoneNumber;
    }
  	public Long getSgid() {
		return sgid;
	}

	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}

	public BrewerEntity getBrewer() {
		return brewer;
	}

	public void setBrewer(BrewerEntity brewer) {
		this.brewer = brewer;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
    
    
    }

