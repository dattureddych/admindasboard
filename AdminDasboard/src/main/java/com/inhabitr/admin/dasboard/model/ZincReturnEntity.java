package com.inhabitr.admin.dasboard.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "zinc_return")
public class ZincReturnEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;

	@Column(name = "order_sgid")
	private Long orderSgid;

	@Column(name = "reference")
	private String reference;

	@Column(name = "item_sgid")
	private Long itemSgid;

	@Column(name = "product_id")
	private String productId;

	@Column(name = "status")
	private String status;

	@Column(name = "request_id")
	private String requestId;

	@Column(name = "message")
	private String message;

	@Column(name = "response")
	private String response;

	@Column(name = "return_initiated_mail")
	private Boolean returnIntiatedMailSent;

	@Column(name = "return_completed")
	private Boolean returnCompletedMailSent;

	@Column(name = "label_link")
	private String labelLink;

	@Column(name = "return_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date returnDate;

	@Column(name = "created_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDateTime;

	@Column(name = "updated_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDateTime;

//	@OneToOne(fetch = FetchType.EAGER)
//	@JoinColumn(name = "order_sgid", referencedColumnName = "order_sgid", insertable = false, updatable = false)
//	private ZincOrderEntity zincOrder;
	
	@Column(name = "refund_mail_sent")
	private Boolean refundCompletedMailSent;
	
	 @Column(name = "quantity") 
	 Integer quantity=0;
	 
	 @Transient
	private ProductEntity product; 
	
	@Transient
	private CategoryEntity category; 

	
	@Transient
	private Double assetValue; 
	
	@Transient
	private Double amazonPrice; 
	
	@Transient
	private Double inhabitrPrice; 
	
	@Transient
	private String imageUrl; 
	
	@Transient
	private String serviceRequestNumber;
	
	public ZincReturnEntity() {
		super();
	}

	public ZincReturnEntity(Long sgid, Long orderSgid, String reference, Long itemSgid, String productId, String status,
			String requestId, String message, String response, Boolean returnIntiatedMailSent,
			Boolean returnCompletedMailSent, String labelLink, Date returnDate, Date createdDateTime,
			Date updatedDateTime, ZincOrderEntity zincOrder,Boolean refundCompletedMailSent,Integer quantity) {
		super();
		this.sgid = sgid;
		this.orderSgid = orderSgid;
		this.reference = reference;
		this.itemSgid = itemSgid;
		this.productId = productId;
		this.status = status;
		this.requestId = requestId;
		this.message = message;
		this.response = response;
		this.returnIntiatedMailSent = returnIntiatedMailSent;
		this.returnCompletedMailSent = returnCompletedMailSent;
		this.labelLink = labelLink;
		this.returnDate = returnDate;
		this.createdDateTime = createdDateTime;
		this.updatedDateTime = updatedDateTime;
		//this.zincOrder = zincOrder;
		this.refundCompletedMailSent=refundCompletedMailSent;
		this.quantity= quantity;
	}

	public Long getSgid() {
		return sgid;
	}

	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}

	public Long getOrderSgid() {
		return orderSgid;
	}

	public void setOrderSgid(Long orderSgid) {
		this.orderSgid = orderSgid;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Long getItemSgid() {
		return itemSgid;
	}

	public void setItemSgid(Long itemSgid) {
		this.itemSgid = itemSgid;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public Boolean getReturnIntiatedMailSent() {
		return returnIntiatedMailSent;
	}

	public void setReturnIntiatedMailSent(Boolean returnIntiatedMailSent) {
		this.returnIntiatedMailSent = returnIntiatedMailSent;
	}

	public Boolean getReturnCompletedMailSent() {
		return returnCompletedMailSent;
	}

	public void setReturnCompletedMailSent(Boolean returnCompletedMailSent) {
		this.returnCompletedMailSent = returnCompletedMailSent;
	}

	public String getLabelLink() {
		return labelLink;
	}

	public void setLabelLink(String labelLink) {
		this.labelLink = labelLink;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}

	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	/*public ZincOrderEntity getZincOrder() {
		return zincOrder;
	}

	public void setZincOrder(ZincOrderEntity zincOrder) {
		this.zincOrder = zincOrder;
	}*/
	
	public Boolean getRefundCompletedMailSent() {
		return refundCompletedMailSent;
	}

	public void setRefundCompletedMailSent(Boolean refundCompletedMailSent) {
		this.refundCompletedMailSent = refundCompletedMailSent;
	}	

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	
	@Override
	public String toString() {
		return "ZincReturn [sgid=" + sgid + ", orderSgid=" + orderSgid + ", reference=" + reference + ", itemSgid="
				+ itemSgid + ", productId=" + productId + ", status=" + status + ", requestId=" + requestId
				+ ", message=" + message + ", response=" + response + ", returnIntiatedMailSent="
				+ returnIntiatedMailSent + ", returnCompletedMailSent=" + returnCompletedMailSent + ", labelLink="
				+ labelLink + ", returnDate=" + returnDate + ", createdDateTime=" + createdDateTime
				+ ", updatedDateTime=" + updatedDateTime + ", refundCompletedMailSent=" + refundCompletedMailSent +", quantity=" + quantity+ "]";
	}

	public ProductEntity getProduct() {
		return product;
	}

	public void setProduct(ProductEntity product) {
		this.product = product;
	}

	public CategoryEntity getCategory() {
		return category;
	}

	public void setCategory(CategoryEntity category) {
		this.category = category;
	}

	public Double getAssetValue() {
		return assetValue;
	}

	public void setAssetValue(Double assetValue) {
		this.assetValue = assetValue;
	}

	public Double getAmazonPrice() {
		return amazonPrice;
	}

	public void setAmazonPrice(Double amazonPrice) {
		this.amazonPrice = amazonPrice;
	}

	public Double getInhabitrPrice() {
		return inhabitrPrice;
	}

	public void setInhabitrPrice(Double inhabitrPrice) {
		this.inhabitrPrice = inhabitrPrice;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getServiceRequestNumber() {
		return serviceRequestNumber;
	}

	public void setServiceRequestNumber(String serviceRequestNumber) {
		this.serviceRequestNumber = serviceRequestNumber;
	}	
	
	}
