package com.inhabitr.admin.dasboard.repository;

import java.util.Collections;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.CartEntity;
import com.inhabitr.admin.dasboard.model.CartItemEntity;


@Repository
public interface CartRepository extends PagingAndSortingRepository<CartEntity, Long> {
	@Query(value = "select * from cart "
			+ "WHERE  brewer_sgid=:brewerId ", nativeQuery = true)
public List<CartEntity> getCart(@Param("brewerId") Long brewerId);
	

}
