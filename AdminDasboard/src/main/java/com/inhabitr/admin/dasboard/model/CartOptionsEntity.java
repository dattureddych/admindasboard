package com.inhabitr.admin.dasboard.model;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity @Table(name = "cart_options")
public class CartOptionsEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	
	private @Column(name = "cart_sgid") Long cartSgid;
	private @Column(name = "cart_token_sgid") Long cartTokenSgid;
	private @Column(name = "option_type") String optionType;
    private @Column(name = "option_type_value") String optionTypeValue;
    private @Column(name = "reference") String reference;
	public Long getCartSgid() {
		return cartSgid;
	}
	public void setCartSgid(Long cartSgid) {
		this.cartSgid = cartSgid;
	}
	public Long getCartTokenSgid() {
		return cartTokenSgid;
	}
	public void setCartTokenSgid(Long cartTokenSgid) {
		this.cartTokenSgid = cartTokenSgid;
	}
	public String getOptionType() {
		return optionType;
	}
	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}
	public String getOptionTypeValue() {
		return optionTypeValue;
	}
	public void setOptionTypeValue(String optionTypeValue) {
		this.optionTypeValue = optionTypeValue;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public Long getSgid() {
		return sgid;
	}
	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}
	
}

