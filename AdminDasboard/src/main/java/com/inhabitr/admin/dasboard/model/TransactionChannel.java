package com.inhabitr.admin.dasboard.model;

public enum TransactionChannel implements KoinPlusEnum<Integer> {
    WEB(0), MOBILE(1), API(2);

   // private static final KoinPlusEnumEnhancer<TransactionChannel> enhancer = new KoinPlusEnumEnhancer<TransactionChannel>(values());

    private Integer value;

    private TransactionChannel(int name) {
        this.value = name;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }

    
}
