package com.inhabitr.admin.dasboard.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.inhabitr.admin.dasboard.controller.PackageController;
import com.inhabitr.admin.dasboard.model.CategoryEntity;
import com.inhabitr.admin.dasboard.model.ItemEntity;
import com.inhabitr.admin.dasboard.model.PackageEntity;
import com.inhabitr.admin.dasboard.model.PackageProductRelationEntity;
import com.inhabitr.admin.dasboard.model.ProductCombinationsEntity;
import com.inhabitr.admin.dasboard.repository.ItemRepository;
import com.inhabitr.admin.dasboard.repository.PackageRepository;


@Service
public class ItemService<itemRepository> {
	@Autowired
	private ItemRepository itemRepository;
	@Autowired
	private PackageRepository packageRepository;
	@Autowired
	private PackageProductRelationService packageProductService;
	@Autowired
	private ProductCombinationService productCombinationService;
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private PackageController pacController;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
		

	public List<ItemEntity> findByproductType(String productType, Integer pageNo, Integer pageSize,String sortDir, String sortBy) {
		logger.info("...............Inside findByproductType() Starting...................");
			
		getLogger().info("............Inside getAllPackages....................");
		Pageable paging = PageRequest.of(pageNo, pageSize,
	            sortDir.equals("asc") ? Sort.by(sortBy).ascending()
                        : Sort.by("sgid").descending());
	
		Page<ItemEntity> pagedResult = itemRepository.findByproductType(productType, paging);
		logger.info("Item list Size==" + pagedResult.getContent().size() + " End.....");
		if (pagedResult.hasContent()) {
			return pagedResult.getContent();
		} else {
			return new ArrayList<ItemEntity>();
		}

	}
	
	
	public List<ItemEntity> findByproductTypeAndSgid(String productType,Long sgid, Integer pageNo, Integer pageSize,String sortDir, String sortBy) {
		logger.info("...............Inside findByproductType() Starting...................");
			
		getLogger().info("............Inside getAllPackages....................");
		Pageable paging = PageRequest.of(pageNo, pageSize,
	            sortDir.equals("asc") ? Sort.by(sortBy).ascending()
                        : Sort.by("sgid").descending());
	
		Page<ItemEntity> pagedResult = itemRepository.findByproductTypeAndSgid(productType,sgid, paging);
		logger.info("Item list Size==" + pagedResult.getContent().size() + " End.....");
		if (pagedResult.hasContent()) {
			return pagedResult.getContent();
		} else {
			return new ArrayList<ItemEntity>();
		}

	}
	

	public List<ItemEntity> findPackageCount(Long productId) {
		logger.info("Inside findPackageCount() Starting.....");
		Integer availbleItemCount = 0;
		Integer numberofUnAvailableItems = 0;
		Integer numberofAvailableItems = 0;
		CategoryEntity catObj=new CategoryEntity();
		List<PackageProductRelationEntity> includedItemList = new ArrayList<PackageProductRelationEntity>();
		
		List<Object> searchList1 = itemRepository.findPackageCount(productId);
		logger.info("...searchList Count..."+searchList1.size());
		
		Iterator<Object> packageItr = searchList1.iterator();
		//Object[] obj1 = (Object[]) packageItr.next();
		
		PackageEntity searchList = new PackageEntity();
		List<PackageEntity> packageList =new ArrayList<PackageEntity>();
		ItemEntity response = null;
		List<ItemEntity> itemList = new ArrayList<ItemEntity>();
		Iterator<Object> itr = searchList1.iterator();
		
		while (packageItr.hasNext()) {
			Object[] obj = (Object[]) packageItr.next();
		//	searchList = new ArrayList<PackageEntity>();
			searchList = packageRepository.findPackageCount(Long.valueOf(obj[0].toString()));
		//	response.setSgid(Long.valueOf(obj[0].toString()));
		//	response.setDisplayName(String.valueOf(obj[1]));
		//	response.setImage_url(String.valueOf(obj[2]));
		//	response.setPackagePrice(searchList.getSale().longValue());

			packageList.add(searchList);
		}
		
		
		if(packageList!=null && packageList.size()>0) {
			for (PackageEntity pac : packageList) {
				includedItemList =packageProductService.getIncludedItemsList(pac.getSgid().intValue());
				catObj =categoryService.getCategory(pac.getCategorySgid());
				System.out.println(includedItemList);
				pac.setPackageproductrelationentity(includedItemList);
				pac.setCategoryName(catObj.getDisplayName());
				pac.setBuyMultiplier(catObj.getBuyMultiplier()!=null?catObj.getBuyMultiplier():1.3);
				pac.setBuyAffirmMultiplier(catObj.getBuyAffirmMultiplier()!=null?catObj.getBuyAffirmMultiplier():1.3);
				}		
				availbleItemCount = 0;				
		}
			
			if (packageList!=null && packageList.size()>0) {
				Double tempPrice=0.0;
				Double buyNewPrice=0.0;
				Double buyNewAffirmPrice = 0.0;		
				for (PackageEntity pac : packageList) {
				for (PackageProductRelationEntity packageProductEntity : pac.getPackageproductrelationentity()) {
					if (packageProductEntity.getOptional()!=null && packageProductEntity.getOptional().equals(0) && packageProductEntity.getStatus()!=null && packageProductEntity.getStatus().equals(1)) {
						ProductCombinationsEntity productCombinationEntity =new ProductCombinationsEntity();// = packageProductEntity.getProductSkuVariation();
						productCombinationEntity= productCombinationService.findVariantDetail(packageProductEntity.getProductId());
						if (productCombinationEntity != null) {
							ProductCombinationsEntity productCombinationResult = pacController.productCombinationPriceCaliculations(null, productCombinationEntity);
							if(productCombinationResult.getAssetValue()!=null && productCombinationResult.getAssetValue()>0){
								buyNewPrice = buyNewPrice+(productCombinationResult.getAssetValue());							
							}						
							if(productCombinationResult.getBuyNewAffirmPrice()!=null && productCombinationResult.getBuyNewAffirmPrice()>0){
								buyNewAffirmPrice = buyNewAffirmPrice+(productCombinationResult.getBuyNewAffirmPrice());
							}										
						}
					}
				}	
				tempPrice=(buyNewPrice*10.25)/100;
				buyNewPrice=(buyNewPrice+tempPrice)*pac.getBuyMultiplier();
				pac.setSale(buyNewPrice.intValue());
				tempPrice=0.0;
				buyNewPrice=0.0;
				buyNewAffirmPrice = 0.0;		
			}
			}

		while (itr.hasNext()) {
			Object[] obj = (Object[]) itr.next();
			response = new ItemEntity();
			response.setSgid(Long.valueOf(obj[0].toString()));
			response.setDisplayName(String.valueOf(obj[1]));
			response.setImage_url(String.valueOf(obj[2]));
			if(packageList!=null && packageList.size()>0) {
			for(PackageEntity pac:packageList) {
			if(response.getSgid().equals(pac.getSgid())) {
			response.setPackagePrice(pac.getSale().longValue());
			}
			}
			}
			itemList.add(response);
		}
		logger.info("Exit findPackageCount() !!!");
		return itemList;
	}
	
	public Integer findPackagesCount(Long productId) {
		logger.info("Inside findPackageCount() Starting.....");
		Integer searchList = itemRepository.findPackagesCount(productId);
		logger.info("...searchList Count..."+searchList);
	
		logger.info("Exit findPackagesCount() !!!");
		return searchList;
	}
	
	public String hideOrShowProduct(Boolean isAvailable,Long sgid) {
		logger.info("Inside /apps/api/package/hideorShowProduct/isAvailable" +isAvailable+ "&sgid"+sgid);
		
		try {
			itemRepository.updateisAvailable(isAvailable,sgid);
		}catch(Exception e) {
			e.printStackTrace();
			return "failure";
		}
		return "success";
			
		}
	
	public List<ItemEntity> searchProduct(String queryParam, Long sgid,Integer pageNo, Integer pageSize, String sortDir,
			String sortBy) {
		getLogger().info("Inside searchPackage()");
		Pageable paging = PageRequest.of(pageNo, pageSize,
				sortDir.equals("asc") ? Sort.by(sortBy).ascending() : Sort.by("sgid").descending());
		Page<ItemEntity> searchList = itemRepository.findByproductTypeAndSgidOrDisplayNameContainingIgnoreCaseOrDisplayNameContainingIgnoreCase("PRODUCT",sgid,queryParam, queryParam,paging);
		
		getLogger().info("searchList size=" + searchList.getContent().size());
		if (searchList.hasContent()) {
			return searchList.getContent();
		} else {
			return new ArrayList<ItemEntity>();
		}
	}
	
	public List<ItemEntity> searchByFilters(Long categories, Boolean isAvailable,String searchStr,Integer pageNo, Integer pageSize, String sortDir,
			String sortBy) {
		getLogger().info("Inside searchPackage()");
		Pageable paging = PageRequest.of(pageNo, pageSize,
				sortDir.equals("asc") ? Sort.by(sortBy).ascending() : Sort.by("sgid").descending());
		List<ItemEntity> searchList = null;
		Page<Long> searchList1 = null;
		Long quantity=0l;
		if(categories!=null && (isAvailable!=null && isAvailable==false && (searchStr==null || searchStr.equals("")))) {
			quantity=0l;
		searchList1=itemRepository.findUnAvailableProducts("PRODUCT",categories,paging);
		 searchList = itemRepository.findByFilters1("PRODUCT",categories,searchList1.getContent());
		}
		if(categories!=null && (isAvailable!=null && isAvailable==true) && (searchStr==null || searchStr.equals(""))) {
			quantity=1l;
		searchList1=itemRepository.findAvailableProducts("PRODUCT",categories,paging);
		 searchList = itemRepository.findByFilters1("PRODUCT",categories, searchList1.getContent());
		
		
		 }
		if(categories!=null && (isAvailable!=null && isAvailable==false && (searchStr!=null && !searchStr.equals("")))) {
			quantity=0l;
		searchList1=itemRepository.findUnAvailableProducts("PRODUCT",categories,searchStr.toUpperCase(),paging);
		 searchList = itemRepository.findByFilters1("PRODUCT",categories,searchStr.toUpperCase(),searchList1.getContent());
		}
		if(categories!=null && (isAvailable!=null && isAvailable==true) && (searchStr!=null && !searchStr.equals(""))) {
			quantity=1l;
		 searchList1=itemRepository.findAvailableProducts("PRODUCT",categories,searchStr.toUpperCase(),paging);
		 searchList = itemRepository.findByFilters1("PRODUCT",categories, searchStr.toUpperCase(),searchList1.getContent());
		
		
		 }
		
		if(categories!=null && (isAvailable==null) && (searchStr==null || searchStr.equals(""))) {
			quantity=1l;			
		 searchList1 = itemRepository.findProductByCategory("PRODUCT",categories, paging);	
	   	 searchList = itemRepository.findByCategory("PRODUCT",categories, searchList1.getContent());		
		 }
		if(categories==null && (isAvailable!=null && isAvailable==false) && (searchStr!=null && !searchStr.equals(""))) {
			quantity=0l;
		 searchList1=itemRepository.findUnAvailableProducts("PRODUCT",searchStr.toUpperCase(),paging);
		 searchList = itemRepository.findByFilters1("PRODUCT",categories,searchStr.toUpperCase(),searchList1.getContent());
		}
		if(categories==null && (isAvailable!=null && isAvailable==true) && (searchStr!=null && !searchStr.equals(""))) {
			searchList1=itemRepository.findAvailableProducts("PRODUCT",searchStr.toUpperCase(),paging);
		 searchList = itemRepository.findAvailableProductsBySearch("PRODUCT",searchStr.toUpperCase(),searchList1.getContent());
		}
		if(categories==null && isAvailable==null && (searchStr==null || searchStr.equals(""))) {
			quantity=0l;
		 searchList1 = itemRepository.findCountByFilters("PRODUCT", paging);
		 searchList = itemRepository.findByFilters("PRODUCT", searchList1.getContent());
		}
		if(categories!=null && isAvailable==null && (searchStr!=null && !searchStr.equals(""))) {
			quantity=0l;
		searchList1 = itemRepository.findCountByFilters1("PRODUCT", categories,searchStr.toUpperCase(),paging);
		searchList = itemRepository.findByFilters1("PRODUCT", categories,searchStr.toUpperCase(),searchList1.getContent());
		}
		if(categories==null && (isAvailable!=null && isAvailable==false) && (searchStr==null || searchStr.equals(""))) {
			quantity=0l;
			searchList1=itemRepository.findUnAvailableProducts("PRODUCT",paging);
		 searchList = itemRepository.findByFilters1("PRODUCT",searchList1.getContent(),paging);
		}
		if(categories==null && (isAvailable!=null && isAvailable==true) && (searchStr==null || searchStr.equals(""))) {
	
		searchList1=itemRepository.findAvailableProducts("PRODUCT",paging);
		 searchList = itemRepository.findByFilters1("PRODUCT",searchList1.getContent(),paging);
		}
	
		if(categories==null && (isAvailable==null) && (searchStr!=null && !searchStr.equals(""))) {
		searchList1=itemRepository.findProductsBySearch("PRODUCT",searchStr.toUpperCase(),paging);
		 searchList = itemRepository.findAvailableProductsBySearch("PRODUCT",searchStr.toUpperCase(),searchList1.getContent());
		}
		if (searchList!=null && searchList.size()>0) {
			return searchList;
		} else {
			return new ArrayList<ItemEntity>();
		}
	}
	
	
	public Integer findUnavailableVariantsCount(Long sgid) {
		getLogger().info("Inside searchPackage()");
	
		Integer searchList = itemRepository.findUnavailableVariantsCount("PRODUCT", sgid);
		return searchList;
	}

	/*public CategoryEntity findMultipliers(Long sgid) {
		getLogger().info("Inside searchPackage()");
	
		CategoryEntity list = itemRepository.findMultipliers(sgid);
		return list;
	}*/

	
	
	
	public Integer findUnavailbleIncludedItemCount(Long packageId) {
		logger.info("Inside findPackageCount() Starting.....");
		Integer searchList = itemRepository.finUnAvailbleIncludedItemCount(packageId);
		logger.info("...searchList Count..."+searchList);
	
		logger.info("Exit findPackagesCount() !!!");
		return searchList;
	}
	
	public Integer findAvailbleIncludedItemCount(Integer productId) {
		logger.info("Inside findPackageCount() Starting.....");
		Integer searchList = itemRepository.findAvailbleIncludedItemCount(productId);
		logger.info("...searchList Count..."+searchList);
	
		logger.info("Exit findPackagesCount() !!!");
		return searchList;
	}
	
	public Integer findAvailbleIncludedVariationCount(Integer productId,Integer packageId) {
		logger.info("Inside findPackageCount() Starting.....");
		Integer searchList = itemRepository.findAvailbleIncludedVariantsCount(productId,packageId);
		logger.info("...searchList Count..."+searchList);
	
		logger.info("Exit findPackagesCount() !!!");
		return searchList;
	}
	
	public String getImagePath(Long sgid) {
		logger.info("Inside getImagePath() Starting.....");
		String imageUlr = itemRepository.getImagePath(sgid);
		logger.info("...imageUlr..."+imageUlr);
	
		logger.info("Exit getImagePath() !!!");
		return imageUlr;
	}
	
	public String getVariationAttributeDetail(Long productId,Long categoryId,Long combinationId) {
		getLogger().info("Inside searchPackage()");
		String variationDetail="";
		List<String> searchList = itemRepository.getVariationAttributeDetail(productId, categoryId,combinationId);
		for(int i=0;i<searchList.size();i++) {
			variationDetail=variationDetail + " "+searchList.get(i);
		}
		return variationDetail;
	}
	
}
