package com.inhabitr.admin.dasboard.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.inhabitr.admin.dasboard.model.BrewerEntity;
import com.inhabitr.admin.dasboard.model.OrderEntity;
import com.inhabitr.admin.dasboard.model.PackageEntity;
import com.inhabitr.admin.dasboard.model.ZincOrderEntity;
import com.inhabitr.admin.dasboard.repository.OrderRepository;

@Service
public class OrderService {

	@Autowired
	private OrderRepository orderRepository;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
	public List<ZincOrderEntity> getAllOrder(Integer pageNo, Integer pageSize,String sortDir,String sortBy) {
		getLogger().info(".................Inside getAllOrder..........");
		Pageable paging = PageRequest.of(pageNo, pageSize,
	            sortDir.equals("asc") ? Sort.by(sortBy).ascending()
                        : Sort.by("sgid").descending());
		 
       // Page<OrderEntity> pagedResult = orderRepository.findAll(paging);
		Page<Long> orderList1 = null;
		 List<ZincOrderEntity> pagedResult = orderRepository.findByFilters(orderList1.getContent());
       
        if(pagedResult!=null) {
            return pagedResult;
        } else {
            return new ArrayList<ZincOrderEntity>();
        }
	}
	
	
	public List<ZincOrderEntity> searchByFilters(Boolean isRegistered,String status,String searchStr,Integer pageNo, Integer pageSize, String sortDir,
			String sortBy) {
		getLogger().info("Inside searchByFilters()");
		Pageable paging = PageRequest.of(pageNo, pageSize,
				sortDir.equals("asc") ? Sort.by(sortBy).ascending() : Sort.by("order_sgid").descending());
		Pageable paging1 = PageRequest.of(pageNo, pageSize,
				sortDir.equals("asc") ? Sort.by(sortBy).ascending() : Sort.by("order_sgid").descending());
		List<ZincOrderEntity> orderList = null;
		Page<Long> orderList1 = null;
	
		if ((isRegistered != null) && (status != null && !status.equals("")) && (searchStr != null && !searchStr.equals(""))) {
			if(isRegistered==true) {
			orderList1=orderRepository.findOrders(status,searchStr.toUpperCase(),paging);
			orderList = orderRepository.findRegisteredOrders(status,searchStr.toUpperCase(),orderList1.getContent());
			}else {
				orderList1=orderRepository.findUnRegisteredOrderList(status,searchStr.toUpperCase(),paging);
				orderList = orderRepository.findUnRegisteredOrders(status,searchStr.toUpperCase(),orderList1.getContent());
			}
		}
    	if ((isRegistered != null) && (status != null  && !status.equals("")) && (searchStr == null || searchStr.equals(""))) {
    		if(isRegistered==true) {
    			orderList1=orderRepository.findRegisteredOrdersByStatus(status,paging);
    			orderList = orderRepository.findRegisteredOrders(status,orderList1.getContent());
    			}else {
    				orderList1=orderRepository.findUnRegisteredOrdersByStatus(status,paging);
    				orderList = orderRepository.findUnRegisteredOrders(status,orderList1.getContent());
    			}
		}
		if ((isRegistered != null) && (status == null  || status.equals("")) && (searchStr != null && !searchStr.equals(""))) {
			if(isRegistered==true) {
				orderList1=orderRepository.findRegisteredOrdersWithSearchStr(searchStr.toUpperCase(),paging);
    			orderList = orderRepository.findRegisteredOrdersWithSearch(searchStr.toUpperCase(),orderList1.getContent());
    			}else {
    				orderList1=orderRepository.findUnRegisteredOrdersWithSearchStr(searchStr.toUpperCase(),paging);
    				orderList = orderRepository.findUnRegisteredOrdersWithSearch(searchStr.toUpperCase(),orderList1.getContent());
    			}
		}
		/*if ((isRegistered == null) && (status == null  || status.equals("")) && (searchStr != null && !searchStr.equals(""))) {
			orderList1=orderRepository.findOrdersWithSearchStr(searchStr,paging);
			orderList = orderRepository.findByFilters(null,searchStr, paging);
		}*/
		if ((isRegistered != null) && (status == null || status.equals("")) && (searchStr == null || searchStr.equals(""))) {
			if(isRegistered==true) {
				orderList1=orderRepository.findRegisteredOrder(paging);
				orderList = orderRepository.findRegisteredOrders(orderList1.getContent());
				}else {
					orderList = orderRepository.findUnRegisteredOrders();
				}
		}
		if ((isRegistered == null) && (status == null || status.equals("")) && (searchStr != null && !searchStr.equals(""))) {
			orderList1=orderRepository.findOrderBySearch(searchStr.toUpperCase(),paging);
			orderList = orderRepository.findByFilters(searchStr.toUpperCase(),orderList1.getContent());
		}
		if ((isRegistered == null) && (status != null && !status.equals("")) && (searchStr == null || searchStr.equals(""))) {
			orderList1=orderRepository.findOrderList(status,paging);
			orderList = orderRepository.findByStatus(status,orderList1.getContent());
		}
		
		if ((isRegistered == null) && (status != null && !status.equals("")) && (searchStr != null && !searchStr.equals(""))) {
			orderList1=orderRepository.findOrderListByStatus(status,searchStr.toUpperCase(),paging);
			orderList = orderRepository.findByStatus(status,searchStr.toUpperCase(),orderList1.getContent());
		}
		if ((isRegistered == null) && (status == null || status.equals("")) && (searchStr == null || searchStr.equals(""))) {
			orderList1=orderRepository.findAllOrders(paging);
			orderList = orderRepository.findByFilters(orderList1.getContent());
		}
		
		if (orderList!=null) {
			return orderList;
		} else {
			return new ArrayList<ZincOrderEntity>();
		}
	}

	
	public Map<String,String> getAllOrderStatus() {
		Map<String, String> map = new HashMap<>();
		map.put("PENDING", "PENDING");
		map.put("SUBMITTED", "SUBMITTED");
		map.put("SUCCESS", "SUCCESS");
		map.put("DELIVERED", "DELIVERED");
		map.put("FAILED", "FAILED");
		map.put("ERROR", "ERROR");
		map.put("CANCELLED", "CANCELLED");
		return map;
	}
	
	public List<ZincOrderEntity> getOrderDetail(Long sgid) {
		getLogger().info(".................Inside getAllOrder..........");
    	List<ZincOrderEntity> orderList = orderRepository.findOrderDetailById(sgid);
      //  getLogger().info("pagedResult list Size=="+pagedResult.getContent().size());
      return orderList;
	}
	
	public String updateOrderInfo(Long opsDashboardUserId,Long sgid) {
		logger.info("Inside /apps/api/package/updateBrewerInfo/isAvailable&sgid"+sgid);		
		try {
			orderRepository.updateOrderInfo(opsDashboardUserId, sgid);
		}catch(Exception e) {
			e.printStackTrace();
			return "failure";
		}
		return "success";
			
		}
	public Long findMaxDashboardOrderId() {
		getLogger().info(".................Inside getAllOrder..........");
    	Long opsDashboardOrderId = orderRepository.findMaxDashboardOrderId();
      //  getLogger().info("pagedResult list Size=="+pagedResult.getContent().size());
      return opsDashboardOrderId;
	}
	
	
	public String findOrderPaymentDetail(String orderReference) {
		getLogger().info(".................Inside findOrderPaymentDetail..........");
    	String paymentType = orderRepository.findOrderPaymentDetail(orderReference);
        getLogger().info("findOrderPaymentDetail Exit");
      return paymentType;
	}
}
