package com.inhabitr.admin.dasboard.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.CategoryEntity;
import com.inhabitr.admin.dasboard.model.ItemEntity;

@Repository
public interface CategoryRepository extends PagingAndSortingRepository<CategoryEntity, Long> {
	//List<ItemEntity> findAll(String productType, Pageable paging);
	
	//List<CategoryEntity> findBySgid(Long sgid);
	@Query(value ="select * from category  "
			   + " where sgid =:sgid order by display_name asc ",
			  nativeQuery = true) 
	 List<CategoryEntity> findBySgid(Long sgid);
	
	 @Query(value ="select * from category  "
			   + " where is_display = 'true' and is_leaf='true' order by display_name asc ",
			  nativeQuery = true) 
	 List<CategoryEntity> getPackageCategoryList();
	 
	 @Query(value ="select * from category  "
			   + " where is_display = 'true' and is_leaf='false' order by display_name asc ",
			  nativeQuery = true) 
	 List<CategoryEntity> getProductCategoryList();
	}
