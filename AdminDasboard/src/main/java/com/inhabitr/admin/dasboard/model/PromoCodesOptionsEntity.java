package com.inhabitr.admin.dasboard.model;

import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.koinplus.common.data.GenericKoinPlusDataEntity;

@Entity @Table(name = "product_coupon_options")
public class PromoCodesOptionsEntity  extends GenericKoinPlusDataEntity<PromoCodesOptionsEntity> {

	@Column(name="coupon_id")
	private Long couponId;
	@Column(name="type")
	private String type;
	@Column(name="sku")
	private String sku;
	@Column(name="status")
	private Integer status;
	
    private @Column(name = "created_datetime") @Temporal(TemporalType.TIMESTAMP) Date createdDatetime;
	
    @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "coupon_id", insertable=false, updatable=false)
    private  PromoCodesEntity promoCodesEntity;

	public Long getCouponId() {
		return couponId;
	}

	public void setCouponId(Long couponId) {
		this.couponId = couponId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public PromoCodesEntity getPromoCodesEntity() {
		return promoCodesEntity;
	}

	public void setPromoCodesEntity(PromoCodesEntity promoCodesEntity) {
		this.promoCodesEntity = promoCodesEntity;
	}
    
}
