package com.inhabitr.admin.dasboard.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.CustomerServiceRequestsEntity;
import com.inhabitr.admin.dasboard.model.ZincOrderEntity;

@Repository
public interface ServiceRequestRepository extends PagingAndSortingRepository<CustomerServiceRequestsEntity, Long> {
	Page<CustomerServiceRequestsEntity> findAll(Pageable paging);
	
	@Query(value ="select * from customer_service_requests req  "
			  + "inner join brewer b on b.sgid=req.brewer_id "
			  + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid"
			  +" inner join orders o on o.reference=req.order_id "
			 + " where (req.status =:status or o.payment_type =:status) and req.subject =:subject and (UPPER(b.first_name) like %:searchStr% or UPPER(b.middle_name) like %:searchStr% "
			 + " or UPPER(b.last_name) like %:searchStr% or UPPER(bb.email_address) like %:searchStr%"
			 + " or bb.mobile_number like %:searchStr% or bb.home_phone_number like %:searchStr% "
			 + " or CONCAT(UPPER(b.first_name),' ',UPPER(b.last_name)) like %:searchStr% or req.service_reference_id like %:searchStr% )" ,			  
			 nativeQuery = true) 
	Page<CustomerServiceRequestsEntity> findServiceRequest(String subject,String status,String searchStr, Pageable paging);
		
	@Query(value ="select * from customer_service_requests req  "
			  + "inner join brewer b on b.sgid=req.brewer_id "
			  + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid"
			  +" inner join orders o on o.reference=req.order_id "
			 + " where ((o.payment_type like '%CARD,REWARD%' or o.payment_type like '%BANK,REWARD%' or o.payment_type like '%AFFIRM,REWARD%')) and req.subject =:subject and (UPPER(b.first_name) like %:searchStr% or UPPER(b.middle_name) like %:searchStr% "
			 + " or UPPER(b.last_name) like %:searchStr% or UPPER(bb.email_address) like %:searchStr%"
			 + " or bb.mobile_number like %:searchStr% or bb.home_phone_number like %:searchStr% "
			 + " or CONCAT(UPPER(b.first_name),' ',UPPER(b.last_name)) like %:searchStr% or req.service_reference_id like %:searchStr% )" ,			  
			 nativeQuery = true) 
	Page<CustomerServiceRequestsEntity> findHybridServiceRequest(String subject,String searchStr, Pageable paging);

	@Query(value ="select * from customer_service_requests req  "
			  + "inner join brewer b on b.sgid=req.brewer_id "
			  + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid"
			  +" inner join orders o on o.reference=req.order_id "
			 + " where (req.status =:status or o.payment_type =:status) and req.subject =:subject",		  
			 nativeQuery = true) 
	Page<CustomerServiceRequestsEntity> findServiceRequest(String subject,String status,Pageable paging);

	@Query(value ="select * from customer_service_requests req  "
			  + "inner join brewer b on b.sgid=req.brewer_id "
			  + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid"
			  +" inner join orders o on o.reference=req.order_id "
			 + " where ((o.payment_type like '%CARD,REWARD%' or o.payment_type like '%BANK,REWARD%' or o.payment_type like '%AFFIRM,REWARD%')) and req.subject =:subject",		  
			 nativeQuery = true) 
	Page<CustomerServiceRequestsEntity> findHybridServiceRequest(String subject,Pageable paging);

	
	@Query(value ="select * from customer_service_requests req  "
			  + "inner join brewer b on b.sgid=req.brewer_id "
			  + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid"
			  +" inner join orders o on o.reference=req.order_id "
			 + " where req.subject =:subject and (UPPER(b.first_name) like %:searchStr% or UPPER(b.middle_name) like %:searchStr% "
			 +  " or UPPER(b.last_name) like %:searchStr% or UPPER(bb.email_address) like %:searchStr% "
			 + "  or bb.mobile_number like %:searchStr% or bb.home_phone_number like %:searchStr%"
			 + " or CONCAT(UPPER(b.first_name),' ',UPPER(b.last_name)) like %:searchStr% or req.service_reference_id like %:searchStr% )" ,			  
			 nativeQuery = true) 
	Page<CustomerServiceRequestsEntity> searchServiceRequest(String subject,String searchStr,Pageable paging);
	
	@Query(value ="select * from customer_service_requests req  "
			  + "inner join brewer b on b.sgid=req.brewer_id "
			  + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid"
			  + " inner join orders o on o.reference=req.order_id "
			 + " where (req.status =:status or o.payment_type =:status) and (UPPER(b.first_name) like %:searchStr% or UPPER(b.middle_name) like %:searchStr% "
			 +  " or UPPER(b.last_name) like %:searchStr% or UPPER(bb.email_address) like %:searchStr% "
			 + "  or bb.mobile_number like %:searchStr% or bb.home_phone_number like %:searchStr%"
			 + " or CONCAT(UPPER(b.first_name),' ',UPPER(b.last_name)) like %:searchStr% or req.service_reference_id like %:searchStr% )" ,			  
			 nativeQuery = true) 
	Page<CustomerServiceRequestsEntity> searchServiceRequestByStatus(String status,String searchStr,Pageable paging);

	@Query(value ="select * from customer_service_requests req  "
			  + "inner join brewer b on b.sgid=req.brewer_id "
			  + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid"
			  + " inner join orders o on o.reference=req.order_id "
			 + " where ((o.payment_type like '%CARD,REWARD%' or o.payment_type like '%BANK,REWARD%' or o.payment_type like '%AFFIRM,REWARD%')) and (UPPER(b.first_name) like %:searchStr% or UPPER(b.middle_name) like %:searchStr% "
			 +  " or UPPER(b.last_name) like %:searchStr% or UPPER(bb.email_address) like %:searchStr% "
			 + "  or bb.mobile_number like %:searchStr% or bb.home_phone_number like %:searchStr%"
			 + " or CONCAT(UPPER(b.first_name),' ',UPPER(b.last_name)) like %:searchStr% or req.service_reference_id like %:searchStr% )" ,			  
			 nativeQuery = true) 
	Page<CustomerServiceRequestsEntity> searchHybridServiceRequestByStatus(String searchStr,Pageable paging);

	
	@Query(value ="select * from customer_service_requests req  "
			  + "inner join brewer b on b.sgid=req.brewer_id "
			  + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid"
			  +" inner join orders o on o.reference=req.order_id "
			 + " where (UPPER(b.first_name) like %:searchStr% or UPPER(b.middle_name) like %:searchStr% "
			 + " or UPPER(b.last_name) like %:searchStr% or UPPER(bb.email_address) like %:searchStr%"
			 + " or bb.mobile_number like %:searchStr% or bb.home_phone_number like %:searchStr%"
			 + " or CONCAT(UPPER(b.first_name),' ',UPPER(b.last_name)) like %:searchStr% or req.service_reference_id like %:searchStr% )" ,			  
			 nativeQuery = true) 
	Page<CustomerServiceRequestsEntity> findWithSearch(String searchStr, Pageable paging);
	
	@Query(value ="select * from customer_service_requests req  "
			  + "inner join brewer b on b.sgid=req.brewer_id "
			  + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid"
			  +" inner join orders o on o.reference=req.order_id "
			 + " where (req.status =:status or o.payment_type =:status) ",		  
			 nativeQuery = true) 
	Page<CustomerServiceRequestsEntity> findServiceRequest(String status, Pageable paging);
	
	@Query(value ="select * from customer_service_requests req  "
			  + "inner join brewer b on b.sgid=req.brewer_id "
			  + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid"
			  +" inner join orders o on o.reference=req.order_id "
			 + " where ((o.payment_type like '%CARD,REWARD%' or o.payment_type like '%BANK,REWARD%' or o.payment_type like '%AFFIRM,REWARD%')) ",		  
			 nativeQuery = true) 
	Page<CustomerServiceRequestsEntity> findHybridServiceRequest(Pageable paging);
	
	@Query(value ="select * from customer_service_requests req  "
			  + "inner join brewer b on b.sgid=req.brewer_id "
			  + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid"
			  +" inner join orders o on o.reference=req.order_id "
			 + " where req.subject =:subject",		  
			 nativeQuery = true) 
	Page<CustomerServiceRequestsEntity> findServiceRequestBySubject(String subject,Pageable paging);
	 
	@Query(value ="select * from customer_service_requests req  "
			  + "inner join brewer b on b.sgid=req.brewer_id "
			  + "inner join brewer_contact bb on bb.brewer_sgid=b.sgid"
			  +" inner join orders o on o.reference=req.order_id ", 
			 nativeQuery = true) 
	Page<CustomerServiceRequestsEntity> findAllServiceRequest(Pageable paging);
	
	@Query(value ="select service_reference_id from customer_service_requests req  "
			 	  + " where order_id =:orderReference and item_sgid =:itemSgid", 
			 nativeQuery = true) 
	String findServiceRequestDetail(String orderReference,Long itemSgid);
 }
