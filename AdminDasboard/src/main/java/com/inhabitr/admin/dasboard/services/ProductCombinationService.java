package com.inhabitr.admin.dasboard.services;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.inhabitr.admin.dasboard.model.ProductCombinationsEntity;
import com.inhabitr.admin.dasboard.model.ItemEntity;
import com.inhabitr.admin.dasboard.model.PackageEntity;
import com.inhabitr.admin.dasboard.repository.ItemRepository;
import com.inhabitr.admin.dasboard.repository.ProductCombinationRepository;


@Service
public class ProductCombinationService <productCombinationRepository> {
	@Autowired
	private ProductCombinationRepository productCombinationRepository;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
		
	public ProductCombinationsEntity findVariantDetail(Integer productId) {
		logger.info("Inside findPackageCount() Starting.....");
		ProductCombinationsEntity searchList = productCombinationRepository.findVariantDetail(productId);
		logger.info("...searchList Count..."+searchList);
	
		logger.info("Exit findPackagesCount() !!!");
		return searchList;
	}
}
