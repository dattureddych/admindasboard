package com.inhabitr.admin.dasboard.model;


import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



@Entity @Table(name = "vendor")
public class VendorEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	private @Column(name = "name") String name;
    private @Column(name = "email") String email;
    private @Column(name = "contact_person_name") String contactPersonName;
    private @Column(name = "contact_no") Long contactNumber;
    private @Column(name = "address") String address;
    private @Column(name = "id_state") String stateId;
    private @Column(name = "is_global") Integer isglobal;
    private @Column(name = "buy_used_multiplier") BigDecimal buyUsedMultiplier;
    private @Column(name = "buy_new_multiplier") BigDecimal buyNewMultiplier;
    private @Column(name = "override_month_multiplier") Integer overrideMonthMultiplier;
    private @Column(name = "buy_used_month_multiplier") Integer buyUsedMonthMultiplier;
    private @Column(name = "buy_new_month_multiplier") Integer buyNewMonthMultiplier;
    private @Column(name = "used_furniture_buy_used_multiplier") BigDecimal usedFurnitureBuyUsedMultiplier;
    private @Column(name = "business_pdm_buy_new_multiplier") BigDecimal businessPdmBuyNewMultiplier;
    private @Column(name = "business_pdm_buy_used_multiplier") BigDecimal businessPdmBuyUsedMultiplier;
    private @Column(name = "student_buy_new_multiplier") BigDecimal studentBuyNewMultiplier;
    private @Column(name = "student_buy_used_multiplier") BigDecimal studentBuyUsedMultiplier;    
    private @Column(name = "status") Integer status;
    private @Column(name = "created_datetime")  @Temporal(TemporalType.TIMESTAMP) Date createdDatetime;
    
    
	public Integer getIsglobal() {
		return isglobal;
	}
	public void setIsglobal(Integer isglobal) {
		this.isglobal = isglobal;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContactPersonName() {
		return contactPersonName;
	}
	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}
	public Long getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(Long contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getStateId() {
		return stateId;
	}
	public void setStateId(String stateId) {
		this.stateId = stateId;
	}
	public BigDecimal getBuyUsedMultiplier() {
		return buyUsedMultiplier;
	}
	public void setBuyUsedMultiplier(BigDecimal buyUsedMultiplier) {
		this.buyUsedMultiplier = buyUsedMultiplier;
	}
	public BigDecimal getBuyNewMultiplier() {
		return buyNewMultiplier;
	}
	public void setBuyNewMultiplier(BigDecimal buyNewMultiplier) {
		this.buyNewMultiplier = buyNewMultiplier;
	}
	public Integer getOverrideMonthMultiplier() {
		return overrideMonthMultiplier;
	}
	public void setOverrideMonthMultiplier(Integer overrideMonthMultiplier) {
		this.overrideMonthMultiplier = overrideMonthMultiplier;
	}
	public Integer getBuyUsedMonthMultiplier() {
		return buyUsedMonthMultiplier;
	}
	public void setBuyUsedMonthMultiplier(Integer buyUsedMonthMultiplier) {
		this.buyUsedMonthMultiplier = buyUsedMonthMultiplier;
	}
	public Integer getBuyNewMonthMultiplier() {
		return buyNewMonthMultiplier;
	}
	public void setBuyNewMonthMultiplier(Integer buyNewMonthMultiplier) {
		this.buyNewMonthMultiplier = buyNewMonthMultiplier;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Date getCreatedDatetime() {
		return createdDatetime;
	}
	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	public BigDecimal getUsedFurnitureBuyUsedMultiplier() {
		return usedFurnitureBuyUsedMultiplier;
	}
	public void setUsedFurnitureBuyUsedMultiplier(BigDecimal usedFurnitureBuyUsedMultiplier) {
		this.usedFurnitureBuyUsedMultiplier = usedFurnitureBuyUsedMultiplier;
	}
	public BigDecimal getBusinessPdmBuyNewMultiplier() {
		return businessPdmBuyNewMultiplier;
	}
	public void setBusinessPdmBuyNewMultiplier(BigDecimal businessPdmBuyNewMultiplier) {
		this.businessPdmBuyNewMultiplier = businessPdmBuyNewMultiplier;
	}
	public BigDecimal getBusinessPdmBuyUsedMultiplier() {
		return businessPdmBuyUsedMultiplier;
	}
	public void setBusinessPdmBuyUsedMultiplier(BigDecimal businessPdmBuyUsedMultiplier) {
		this.businessPdmBuyUsedMultiplier = businessPdmBuyUsedMultiplier;
	}
	public BigDecimal getStudentBuyNewMultiplier() {
		return studentBuyNewMultiplier;
	}
	public void setStudentBuyNewMultiplier(BigDecimal studentBuyNewMultiplier) {
		this.studentBuyNewMultiplier = studentBuyNewMultiplier;
	}
	public BigDecimal getStudentBuyUsedMultiplier() {
		return studentBuyUsedMultiplier;
	}
	public void setStudentBuyUsedMultiplier(BigDecimal studentBuyUsedMultiplier) {
		this.studentBuyUsedMultiplier = studentBuyUsedMultiplier;
	}
	public Long getSgid() {
		return sgid;
	}
	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}
	
}
