package com.inhabitr.admin.dasboard.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity @Table(name = "brewer_rewards")
public class BrewerRewardEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	
	private @Column(name = "brewer_sgid") Long brewerSgid;
	private @Column(name = "property_user_sgid") Long propertyUserSgid;
	private @Column(name = "property_user_reward_sgid") Long propertyUserRewardSgid;
	private @Column(name = "property_user_reward_history_sgid") Long propertyUserRewardHistorySgid;
	private @Column(name = "property_sgid") Long propertySgid;
	private @Column(name = "reward_id") String rewardId;
	private @Column(name = "resident_id") String residentId;
	private @Column(name = "total_reward_amount") BigDecimal rewardAmount;
	private @Column(name = "available_reward_amount") BigDecimal rewardBalance;
	private @Column(name = "status") Integer status;
	
	@ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "brewer_sgid", insertable=false, updatable=false)
    private BrewerEntity brewer;
	
	@OneToOne(fetch = FetchType.LAZY) @JoinColumn(name = "property_sgid", insertable=false, updatable=false)
	private CASLPropertyEntity property;
	
	@OneToOne(fetch = FetchType.LAZY) @JoinColumn(name = "property_user_reward_sgid", insertable=false, updatable=false)
    private PropertyUserRewardEntity userReward;
	 
	public String getRewardId() {
		return rewardId;
	}
	public void setRewardId(String rewardId) {
		this.rewardId = rewardId;
	}
	public Long getPropertyUserSgid() {
		return propertyUserSgid;
	}
	public void setPropertyUserSgid(Long propertyUserSgid) {
		this.propertyUserSgid = propertyUserSgid;
	}
	public String getResidentId() {
		return residentId;
	}
	public void setResidentId(String residentId) {
		this.residentId = residentId;
	}
	public Long getPropertySgid() {
		return propertySgid;
	}
	public void setPropertySgid(Long propertySgid) {
		this.propertySgid = propertySgid;
	}
	public Long getBrewerSgid() {
		return brewerSgid;
	}
	public void setBrewerSgid(Long brewerSgid) {
		this.brewerSgid = brewerSgid;
	}
	public BigDecimal getRewardAmount() {
		return rewardAmount;
	}
	public void setRewardAmount(BigDecimal rewardAmount) {
		this.rewardAmount = rewardAmount;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getPropertyUserRewardSgid() {
		return propertyUserRewardSgid;
	}
	public void setPropertyUserRewardSgid(Long propertyUserRewardSgid) {
		this.propertyUserRewardSgid = propertyUserRewardSgid;
	}
	public Long getPropertyUserRewardHistorySgid() {
		return propertyUserRewardHistorySgid;
	}
	public void setPropertyUserRewardHistorySgid(Long propertyUserRewardHistorySgid) {
		this.propertyUserRewardHistorySgid = propertyUserRewardHistorySgid;
	}
	public BigDecimal getRewardBalance() {
		return rewardBalance;
	}
	public void setRewardBalance(BigDecimal rewardBalance) {
		this.rewardBalance = rewardBalance;
	}
	public PropertyUserRewardEntity getUserReward() {
		return userReward;
	}
	public void setUserReward(PropertyUserRewardEntity userReward) {
		this.userReward = userReward;
	}
	public CASLPropertyEntity getProperty() {
		return property;
	}
	public void setProperty(CASLPropertyEntity property) {
		this.property = property;
	}
	public BrewerEntity getBrewer() {
		return brewer;
	}
	public void setBrewer(BrewerEntity brewer) {
		this.brewer = brewer;
	}
	public Long getSgid() {
		return sgid;
	}
	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}
	
	
}
