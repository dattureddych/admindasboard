package com.inhabitr.admin.dasboard.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.BrewerEntity;
import com.inhabitr.admin.dasboard.model.OrderEntity;
import com.inhabitr.admin.dasboard.model.PackageEntity;
import com.inhabitr.admin.dasboard.model.ZincOrderEntity;

@Repository
public interface OrderRepository extends PagingAndSortingRepository<ZincOrderEntity, Long> {
	Page<ZincOrderEntity> findAll(Pageable paging);
	
	 @Query(value ="select * from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid "
			//  + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid "
			 + " where o.ops_dashboard_order_id >0 "
			 + " and (UPPER(o.reference) like %:searchStr% or UPPER(o.first_name) like %:searchStr% or "
			 + "UPPER(o.last_name) like %:searchStr% or UPPER(o.email) "
			 + " like %:searchStr% ) and o.sgid in (:sgids)"
			  +" and zin.zinc_status =:status" ,
			 nativeQuery = true) 
	List<ZincOrderEntity> findRegisteredOrders(String status,String searchStr, List<Long> sgids);
		
	 @Query(value ="select * from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid "
		//	  + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid "
			 + " where o.ops_dashboard_order_id=null and (UPPER(o.reference) like %:searchStr% or UPPER(o.first_name) like %:searchStr% or UPPER(o.last_name) like %:searchStr% or UPPER(o.email) like %:searchStr%)"
			 +"  and o.sgid in (:sgids) and zin.zinc_status =:status" ,
			 nativeQuery = true) 
	List<ZincOrderEntity> findUnRegisteredOrders(String status,String searchStr,List<Long> sgids);
		
	 @Query(value ="select * from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid "
			//  + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid "
			 + " where o.ops_dashboard_order_id >0 "
			  +"  and o.sgid in (:sgids) and zin.zinc_status =:status" ,
			 nativeQuery = true) 
	List<ZincOrderEntity> findRegisteredOrders(String status,List<Long> sgids);
		
	 @Query(value ="select * from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid "
			//  + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid "
			 + " where o.ops_dashboard_order_id =null "
			 +" and o.sgid in (:sgids) and zin.zinc_status =:status" ,
			 nativeQuery = true) 
	List<ZincOrderEntity> findUnRegisteredOrders(String status,List<Long> sgids);
	 
	 @Query(value ="select * from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid "
			//  + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid "
			 + " where o.ops_dashboard_order_id >0 and (UPPER(o.reference) like %:searchStr% or UPPER(o.first_name) like %:searchStr% or UPPER(o.last_name) like %:searchStr% "
			 + "or UPPER(o.email) like %:searchStr% or o.phone like %:searchStr%) and o.sgid in (:sgids) ",
			  nativeQuery = true) 
	List<ZincOrderEntity> findRegisteredOrdersWithSearch(String searchStr,List<Long> sgids);
		
	 @Query(value ="select * from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid "
			 // + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid "
			 + " where o.ops_dashboard_order_id is NULL and (UPPER(o.reference) like %:searchStr% or UPPER(o.first_name) like %:searchStr% or UPPER(o.last_name) like %:searchStr% or UPPER(o.email) like %:searchStr% or o.phone like %:searchStr%)"
			 +" and o.sgid in (:sgids) " ,
			 nativeQuery = true) 
	List<ZincOrderEntity> findUnRegisteredOrdersWithSearch(String searchStr,List<Long> sgids);
		
		
	 @Query(value ="select * from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid "
			//  + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid "
			 + " where (UPPER(o.reference) like %:searchStr% or UPPER(o.first_name) like %:searchStr% or UPPER(o.last_name) like %:searchStr% or UPPER(o.email) like %:searchStr% or o.phone like %:searchStr%)"
			  +" and zin.zinc_status =:status" ,
			 nativeQuery = true) 
	Page<ZincOrderEntity> findByFilters(String status, String searchStr,Pageable paging);
	 
	 @Query(value ="select * from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid "
			//  + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid "
			 + " where (UPPER(o.reference) like %:searchStr% or UPPER(o.first_name) like %:searchStr% or UPPER(o.last_name) like %:searchStr% or UPPER(o.email) like %:searchStr% or o.phone like %:searchStr%)"
			  +" and o.sgid in (:sgids) " ,
			 nativeQuery = true) 
	List<ZincOrderEntity> findByFilters(String searchStr,List<Long> sgids);
	 
	 @Query(value ="select * from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid "
			//  + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid "
			 + " where zin.zinc_status =:status and o.sgid in (:sgids)"
			  +" " ,
			 nativeQuery = true) 
	List<ZincOrderEntity> findByStatus(String status,List<Long> sgids);
	 
	 @Query(value ="select * from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid "
		//	  + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid "
			 + " where zin.zinc_status =:status and (UPPER(o.reference) like %:searchStr% or UPPER(o.first_name) like %:searchStr% or UPPER(o.last_name) "
			 + " like %:searchStr% or UPPER(o.email) like %:searchStr% or o.phone like %:searchStr%)"
			  +" and o.sgid in (:sgids)" ,
			 nativeQuery = true) 
	List<ZincOrderEntity> findByStatus(String status,String searchStr,List<Long> sgids);
	 
	 @Query(value ="select * from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid "
		//	  + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid "
			 + " where o.ops_dashboard_order_id >0 and o.sgid in (:sgids) ",
			 nativeQuery = true) 
	List<ZincOrderEntity> findRegisteredOrders(List<Long> sgids);
		
	 @Query(value ="select * from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid "
		//	  + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid "
			 + " where o.ops_dashboard_order_id =null ",
			 nativeQuery = true) 
	List<ZincOrderEntity> findUnRegisteredOrders();
		 
	

	 @Query(value ="select * from zinc_order zin  "
			  + " inner join orders ord on zin.order_sgid=ord.sgid " 
			// + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid "
			  +  " where ord.sgid in (:sgids) ",
			 nativeQuery = true)  
	Page<ZincOrderEntity> findByFilters(List<Long> sgids,Pageable paging); 
	 
	 @Query(value ="select * from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid " 
			//  + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid "
			  + " where o.sgid in (:sgids) ",
			 nativeQuery = true)  
	List<ZincOrderEntity> findByFilters(List<Long> sgids); 
	 
	 @Query(value ="select distinct zin.order_sgid from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid "
			 + " where o.ops_dashboard_order_id >0 and (UPPER(o.reference) like %:searchStr% or UPPER(o.first_name) like %:searchStr% or UPPER(o.last_name) like %:searchStr% or UPPER(o.email) like %:searchStr%) "
			  +" and zin.zinc_status =:status " ,
			 nativeQuery = true) 
	Page<Long> findOrders(String status,String searchStr, Pageable paging);
	 
	 @Query(value ="select distinct zin.order_sgid from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid "
			//  + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid "
			 + " where o.ops_dashboard_order_id <0 and (UPPER(o.reference) like %:searchStr% or o.first_name like %:searchStr% or o.last_name like %:searchStr% or o.email like %:searchStr%) "
			  +" and zin.zinc_status =:status " ,
			 nativeQuery = true) 
	Page<Long> findUnRegisteredOrderList(String status,String searchStr, Pageable paging);
	 
	 @Query(value ="select"
	 		+ " distinct zin.order_sgid  from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid "
			//  + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid "
			 + " where o.ops_dashboard_order_id >0 "
			  +"  and zin.zinc_status =:status" ,
			 nativeQuery = true) 
	Page<Long> findRegisteredOrdersByStatus(String status,Pageable paging);
	 
	 @Query(value ="select distinct zin.order_sgid  from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid "
		//	  + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid "
			 + " where o.ops_dashboard_order_id =null "
			 +" and zin.zinc_status =:status" ,
			 nativeQuery = true) 
	Page<Long> findUnRegisteredOrdersByStatus(String status, Pageable paging);
	 
	 @Query(value ="select distinct zin.order_sgid  from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid "
			//  + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid "
			 + " where o.ops_dashboard_order_id >0 and (UPPER(o.reference) like %:searchStr% or UPPER(o.first_name) like %:searchStr% or UPPER(o.last_name) like %:searchStr% or UPPER(o.email) like %:searchStr% or o.phone like %:searchStr%)",
			  nativeQuery = true) 
	Page<Long> findRegisteredOrdersWithSearchStr(String searchStr, Pageable paging); 
	 
	 @Query(value ="select distinct zin.order_sgid  from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid "
		//	  + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid "
			 + " where o.ops_dashboard_order_id is NULL and (UPPER(o.reference) like %:searchStr% or UPPER(o.first_name) like %:searchStr% or UPPER(o.last_name) like %:searchStr% or UPPER(o.email) like %:searchStr% or o.phone like %:searchStr%)"
			 +" " ,
			 nativeQuery = true) 
	Page<Long> findUnRegisteredOrdersWithSearchStr(String searchStr, Pageable paging);
	 
	 @Query(value ="select distinct zin.order_sgid  from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid "
		//	  + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid "
			 + " where o.ops_dashboard_order_id >0",
			 nativeQuery = true) 
	Page<Long> findRegisteredOrder(Pageable paging);	
	 
	 @Query(value ="select distinct zin.order_sgid from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid "
			//  + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid "
			 + " where (UPPER(o.reference) like %:searchStr% or UPPER(o.first_name) like %:searchStr% or UPPER(o.last_name) like %:searchStr% or UPPER(o.email) like %:searchStr% or o.phone like %:searchStr%)"
			  +" " ,
			 nativeQuery = true) 
	Page<Long> findOrderBySearch(String searchStr,Pageable paging);
	 
	 @Query(value ="select distinct zin.order_sgid from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid "
			//  + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid "
			 + " where zin.zinc_status =:status"
			  +" " ,
			 nativeQuery = true) 
	Page<Long> findOrderList(String status,Pageable paging);
	 
	 @Query(value ="select distinct zin.order_sgid from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid "
			//  + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid "
			 + " where zin.zinc_status =:status and (UPPER(o.reference) like %:searchStr% or UPPER(o.first_name) like %:searchStr% or UPPER(o.last_name) "
			 + " like %:searchStr% or UPPER(o.email) like %:searchStr% or o.phone like %:searchStr%)"
			  +" " ,
			 nativeQuery = true) 
	Page<Long> findOrderListByStatus(String status,String searchStr,Pageable paging);
	 
	 @Query(value ="select  distinct zin.order_sgid from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid " ,
			//  + "inner join zinc_order_item zoi on zoi.zinc_order_sgid=zin.sgid ",
			 nativeQuery = true)  
	Page<Long> findAllOrders(Pageable paging); 
	 
	 @Query(value ="select * from zinc_order zin  "
			  + "inner join orders o on zin.order_sgid=o.sgid where o.sgid=:sgid" ,
			 nativeQuery = true)  
	List<ZincOrderEntity> findOrderDetailById(Long sgid); 
	 
	 @Modifying
		@Transactional
		@Query(value= "update orders set ops_dashboard_order_id=:opsDashboardOrderId where sgid=:sgid", nativeQuery = true)
		public void updateOrderInfo(@Param("opsDashboardOrderId") Long opsDashboardOrderId,@Param("sgid") Long sgid);
	
	 @Query(value ="select  max(ops_dashboard_order_id) from orders  ",
			 nativeQuery = true)  
     Long findMaxDashboardOrderId(); 
	 
	 @Query(value ="select payment_type from orders o  "
			  + " where o.reference like %:orderReference% " ,
			 nativeQuery = true)  
	String findOrderPaymentDetail(String orderReference); 
}
