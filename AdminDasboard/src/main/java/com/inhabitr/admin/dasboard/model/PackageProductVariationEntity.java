package com.inhabitr.admin.dasboard.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity @Table(name = "package_product_variation")

public class PackageProductVariationEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	  private @Column(name = "batch") Integer batch;
	    private @Column(name = "sku") String sku;
	    private @Column(name = "quantity") Integer quantity;
	    private @Column(name = "optional") Integer optional;
	    private @Column(name = "status") Integer status;
	    private @Column(name = "package_id") Integer package_id;
	    private @Column(name = "created_by") Integer createdBy;
	    private @Column(name = "updated_by") Integer updatedBy;
	    private @Column(name = "created_at") @Temporal(TemporalType.TIMESTAMP) Date createdAt;
	    
	    @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "sku_variation_id", insertable=false, updatable=false)
	    private ProductCombinationsEntity productSkuVariation;
	    
	    @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "package_id", insertable=false, updatable=false)
	    private PackageEntity packege;
	    
		/*
		 * @OneToMany(fetch = FetchType.LAZY, mappedBy = "packageProductVariation")
		 * private List<ProductRentEntity> productRents;
		 */

		public Integer getPackage_id() {
			return package_id;
		}

		public void setPackage_id(Integer package_id) {
			this.package_id = package_id;
		}

		public Integer getBatch() {
			return batch;
		}

		public void setBatch(Integer batch) {
			this.batch = batch;
		}

		public String getSku() {
			return sku;
		}

		public void setSku(String sku) {
			this.sku = sku;
		}

		public Integer getQuantity() {
			return quantity;
		}

		public void setQuantity(Integer quantity) {
			this.quantity = quantity;
		}

		public Integer getOptional() {
			return optional;
		}

		public void setOptional(Integer optional) {
			this.optional = optional;
		}

		public Integer getStatus() {
			return status;
		}

		public void setStatus(Integer status) {
			this.status = status;
		}

		public Integer getCreatedBy() {
			return createdBy;
		}

		public void setCreatedBy(Integer createdBy) {
			this.createdBy = createdBy;
		}

		public Integer getUpdatedBy() {
			return updatedBy;
		}

		public void setUpdatedBy(Integer updatedBy) {
			this.updatedBy = updatedBy;
		}

		public Date getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(Date createdAt) {
			this.createdAt = createdAt;
		}

		public ProductCombinationsEntity getProductSkuVariation() {
			return productSkuVariation;
		}

		public void setProductSkuVariation(ProductCombinationsEntity productSkuVariation) {
			this.productSkuVariation = productSkuVariation;
		}

		public PackageEntity getPackege() {
			return packege;
		}

		public void setPackege(PackageEntity packege) {
			this.packege = packege;
		}

		

		public Long getSgid() {
			return sgid;
		}

		public void setSgid(Long sgid) {
			this.sgid = sgid;
		}
		


}
