package com.inhabitr.admin.dasboard.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "attribute")
public class AttributeEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	
	private @Column(name = "variation_id") Long variationId;
	private @Column(name = "attribute_list_id") Long attributelistId;
	private @Column(name = "attribute_value") String attributeValue;
	private @Column(name = "slug") String slug;
	private @Column(name = "isactive") Boolean isactive;
	private @Column(name = "record_status") Integer recordstatus;
	private @Column(name = "name") String name;
    private @Column(name = "type") String type;
    private @Column(name = "created_by") Integer createdBy;
    private @Column(name = "updated_by") Integer updatedBy;
    private @Column(name = "created_at") @Temporal(TemporalType.TIMESTAMP) Date createdAt;
    
    @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "attribute_list_id", insertable=false, updatable=false)
    private AttributeListEntity attributeList;
	
    public Long getAttributelistId() {
		return attributelistId;
	}
	public void setAttributelistId(Long attributelistId) {
		this.attributelistId = attributelistId;
	}
	public AttributeListEntity getAttributeList() {
		return attributeList;
	}
	public void setAttributeList(AttributeListEntity attributeList) {
		this.attributeList = attributeList;
	}
	public Long getVariationId() {
		return variationId;
	}
	public void setVariationId(Long variationId) {
		this.variationId = variationId;
	}
	public Long getAttributeListId() {
		return attributelistId;
	}
	public void setAttributeListId(Long attributelistId) {
		this.attributelistId = attributelistId;
	}
	public String getAttributeValue() {
		return attributeValue;
	}
	public void setAttributeValue(String attributeValue) {
		this.attributeValue = attributeValue;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public Boolean getIsactive() {
		return isactive;
	}
	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}
	public Integer getRecordstatus() {
		return recordstatus;
	}
	public void setRecordstatus(Integer recordstatus) {
		this.recordstatus = recordstatus;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Long getSgid() {
		return sgid;
	}
	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}
	
}
