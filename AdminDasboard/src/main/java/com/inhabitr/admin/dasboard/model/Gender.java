package com.inhabitr.admin.dasboard.model;

public enum Gender implements KoinPlusEnum {
    ALL("ALL"),
    FEMALE("FEMALE"),
    MALE("MALE"),
    GIRL("GIRL"),
    BOY("BOY"),
    BABY_GIRL("BABY GIRL"),
    BABY_BOY("BABY BOY"),
    INFANT("INFANT");

//    private static final KoinPlusEnumEnhancer<Gender> enhancer = new KoinPlusEnumEnhancer<Gender>(values());

    private String value;

    private Gender(String name) {
        this.value = name;
    }

    @Override
    public String getValue() {
        return value;
    }

    // This is delegation.
   
}
