package com.inhabitr.admin.dasboard.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inhabitr.admin.dasboard.model.PackageProductRelationEntity;
import com.inhabitr.admin.dasboard.repository.PackageProductRelationRepository;

@Service
public class PackageProductRelationService {
	@Autowired
	private PackageProductRelationRepository repository;

	public List<PackageProductRelationEntity> findAll() {
		 Iterable<PackageProductRelationEntity> findAll = repository.findAll();
		 Iterator<PackageProductRelationEntity> iterator = findAll.iterator();
			List<PackageProductRelationEntity> productList1= new ArrayList<PackageProductRelationEntity>();
			while(iterator.hasNext()) {
			
				productList1.add(iterator.next());
			}
			
		return productList1;
	}
	
	public List<PackageProductRelationEntity> getIncludedItemsList(Integer pacId) {
		List<PackageProductRelationEntity> packageEntityList = null;
		packageEntityList = repository.getIncludedItemsList(pacId);
		return packageEntityList;

		}
	
	
}
