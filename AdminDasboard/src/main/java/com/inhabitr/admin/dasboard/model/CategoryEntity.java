package com.inhabitr.admin.dasboard.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "category")
public class CategoryEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;

	private @Column(name = "display_name") String displayName;
	private @Column(name = "slug") String slug;
	private @Column(name = "buy_multiplier") Double	buyMultiplier;
	private @Column(name = "buy_affirm_multiplier") Double	buyAffirmMultiplier;
	private @Column(name = "is_deleted")Integer	isDeleted;
	
//	@OneToMany(fetch = FetchType.LAZY, mappedBy = "category")
  //  private List<CategoryAttributeVisibilityEntity> attributeVisibilityList;
	
	public CategoryEntity() {
		super();
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Long getSgid() {
		return sgid;
	}

	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	
/*	public CategoryEntity getRootCategory() {
		return rootCategory;
	}

	public void setRootCategory(CategoryEntity rootCategory) {
		this.rootCategory = rootCategory;
	}

	public CategoryEntity getDisplayCategory() {
		return displayCategory;
	}

	public void setDisplayCategory(CategoryEntity displayCategory) {
		this.displayCategory = displayCategory;
	}

	public CategoryEntity getParentCategory() {
		return parentCategory;
	}

	public void setParentCategory(CategoryEntity parentCategory) {
		this.parentCategory = parentCategory;
	}*/

//	@Override
	/*public String toString() {
		return "Category[sgid=" + sgid + ", displayName=" + displayName + ", slug=" + slug + "]";
	}*/

	public Double getBuyMultiplier() {
		return buyMultiplier;
	}

	public void setBuyMultiplier(Double buyMultiplier) {
		this.buyMultiplier = buyMultiplier;
	}

	public Double getBuyAffirmMultiplier() {
		return buyAffirmMultiplier;
	}

	public void setBuyAffirmMultiplier(Double buyAffirmMultiplier) {
		this.buyAffirmMultiplier = buyAffirmMultiplier;
	}

	public Integer getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Integer isDeleted) {
		this.isDeleted = isDeleted;
	}
	
/*	public List<CategoryAttributeVisibilityEntity> getAttributeVisibilityList() {
		return attributeVisibilityList;
	}

	public void setAttributeVisibilityList(List<CategoryAttributeVisibilityEntity> attributeVisibilityList) {
		this.attributeVisibilityList = attributeVisibilityList;
	}*/

}
