package com.inhabitr.admin.dasboard.repository;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.PackageEntity;
import com.inhabitr.admin.dasboard.model.PackageProductRelationEntity;
import com.inhabitr.admin.dasboard.model.ZincOrderEntity;
import com.inhabitr.admin.dasboard.model.PackageEntity;

@Repository
public interface PackageRepository extends PagingAndSortingRepository<PackageEntity, Long> {
	
	Page<PackageEntity> findAll(Pageable paging);

	List<PackageEntity> findByNameContainingIgnoreCase(String name);

	Page<PackageEntity> findByNameContainingIgnoreCaseOrSkuContainingIgnoreCase(String name, String sku,Pageable paging);

	Page<PackageEntity> findByNameContainingIgnoreCase(String name, Pageable paging);

	@Modifying
	@Transactional
	@Query(value = "UPDATE item set is_available=:isAvailable where sgid=:sgid", nativeQuery = true)
	void updatestatus(@Param("isAvailable") Boolean isAvailable, @Param("sgid") Long sgid);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE package set status=:isAvailable where saffron_item_sgid=:sgid", nativeQuery = true)
	void updatePackageStatus(@Param("isAvailable") Integer isAvailable, @Param("sgid") Long sgid);

	
	
	@Query(value = "select distinct pac.sgid from  package pac "
			+ "inner join item i on i.sgid=pac.saffron_item_sgid "
		//	+ "inner join category c on c.sgid=p.category_id "
			+ "inner join category c1 on c1.sgid=pac.category_id "
			+ "WHERE (pac.category_id =:catId and i.is_available =:status) "
			+ "and ( UPPER(pac.name) like %:searchStr% or UPPER(c1.name) like %:searchStr% )", nativeQuery = true)
	Page<Long> findPackageByFilters(Long catId,Boolean status,String searchStr, Pageable paging);
	
	@Query(value = "select * from  package pac  "
			//+" inner join package pac on pp.package_id=pac.sgid "
			//+ "inner join product p on pp.product_id=p.sgid "
			+ "inner join item i on i.sgid=pac.saffron_item_sgid "
		//	+ "inner join category c on c.sgid=p.category_id "
			+ "inner join category c1 on c1.sgid=pac.category_id "
			+ "WHERE (pac.category_id =:catId and i.is_available =:status) "
			+ "and ( UPPER(pac.name) like %:searchStr% or UPPER(c1.name) like %:searchStr% )"
			+ " and pac.sgid in (:sgids)", nativeQuery = true)
	List<PackageEntity> findPackageByFilters(Long catId,Boolean status,String searchStr,List<Long> sgids);
	
	@Query(value = "select distinct pac.sgid from  package pac "
			+ "inner join item i on i.sgid=pac.saffron_item_sgid "
			+ "inner join category c1 on c1.sgid=pac.category_id "
			+ "WHERE pac.category_id =:catId and i.is_available =:status", nativeQuery = true)
	Page<Long> findPackageByFilters(Long catId,Boolean status,Pageable paging);
	
	@Query(value = "select * from  package pac "
			+ "inner join item i on i.sgid=pac.saffron_item_sgid "
			+ "inner join category c1 on c1.sgid=pac.category_id "
			+ "WHERE pac.category_id =:catId and i.is_available =:status and pac.sgid in (:sgids)", nativeQuery = true)
	List<PackageEntity> findPackageByFilters(Long catId,Boolean status,List<Long> sgids);
	
	@Query(value = "select distinct pac.sgid from  package pac "
			+ "inner join item i on i.sgid=pac.saffron_item_sgid "
			+ "inner join category c1 on c1.sgid=pac.category_id "
			+ "WHERE pac.category_id =:catId and ( UPPER(pac.name) like %:searchStr% or UPPER(c1.name) like %:searchStr% )", nativeQuery = true)
	Page<Long> findPackageBySearch(Long catId,String searchStr,Pageable paging);
	
	@Query(value = "select * from package pac "
			+ "inner join item i on i.sgid=pac.saffron_item_sgid "
			+ "inner join category c1 on c1.sgid=pac.category_id "
			+ "WHERE pac.category_id =:catId and ( UPPER(pac.name) like %:searchStr% "
			+ "or UPPER(c1.name) like %:searchStr% ) and pac.sgid in (:sgids)", nativeQuery = true)
	List<PackageEntity> findPackageBySearch(Long catId,String searchStr,List<Long> sgids);
	
	
	@Query(value = "select distinct pac.sgid from  package pac "
			+ "inner join item i on i.sgid=pac.saffron_item_sgid "
			+ "inner join category c1 on c1.sgid=pac.category_id "
			+ "WHERE i.is_available =:status and ( UPPER(pac.name) like %:searchStr% or UPPER(c1.name) like %:searchStr% )", nativeQuery = true)
	Page<Long> searchPackageByStatus(Boolean status,String searchStr,Pageable paging);
	
	@Query(value = "select * from package pac "
			+ "inner join item i on i.sgid=pac.saffron_item_sgid "
			+ "inner join category c1 on c1.sgid=pac.category_id "
			+ "WHERE i.is_available =:status and "
			+ "( UPPER(pac.name) like %:searchStr% or UPPER(c1.name) like %:searchStr% )"
			+ " and pac.sgid in (:sgids)", nativeQuery = true)
	List<PackageEntity> searchPackageByStatus(Boolean status,String searchStr,List<Long> sgids);

	@Query(value = "select distinct pac.sgid  from package pac "
			+ "inner join item i on i.sgid=pac.saffron_item_sgid "
			+ "inner join category c1 on c1.sgid=pac.category_id "
			+ "WHERE ( UPPER(pac.name) like %:searchStr% or UPPER(c1.name) like %:searchStr% )", nativeQuery = true)
	Page<Long> findWithSearch(String searchStr, Pageable paging);
	
	
	@Query(value = "select * from package pac "
			+ "inner join item i on i.sgid=pac.saffron_item_sgid "
			+ "inner join category c1 on c1.sgid=pac.category_id "
			+ "WHERE ( UPPER(pac.name) like %:searchStr% or UPPER(c1.name) like %:searchStr% ) and pac.sgid in (:sgids) ", nativeQuery = true)
	List<PackageEntity> findWithSearch(String searchStr, List<Long> sgids);
	
	@Query(value = "select distinct pac.sgid from package pac "
			+ "inner join item i on i.sgid=pac.saffron_item_sgid "
			+ "inner join category c1 on c1.sgid=pac.category_id "
			+ "WHERE i.is_available =:status", nativeQuery = true)
	Page<Long> findPackageByAvailability(Boolean status, Pageable paging);
	
	@Query(value = "select * from package pac "
			+ "inner join item i on i.sgid=pac.saffron_item_sgid "
			+ "inner join category c1 on c1.sgid=pac.category_id "
			+ "WHERE i.is_available =:status and pac.sgid in (:sgids)", nativeQuery = true)
	List<PackageEntity> findPackageByAvailability(Boolean status, List<Long> sgids);
		
	 @Query(value =" select distinct pac.sgid from package pac  "
			  + " where pac.category_id =:catId"
			  +" " ,
			 nativeQuery = true) 
	Page<Long> findPackageByCategory(Long catId,Pageable paging);
			
	@Query(value = "select * from package pac "
			+ "inner join item i on i.sgid=pac.saffron_item_sgid "
			+ "inner join category c1 on c1.sgid=pac.category_id "
			+ "WHERE pac.category_id =:catId and pac.sgid in (:sgids)", nativeQuery = true)
	List<PackageEntity> findPackageByCategory(Long catId,List<Long> sgids);

	
	@Query(value = "select distinct pac.sgid from package pac "
			+ "inner join item i on i.sgid=pac.saffron_item_sgid "
			+ "inner join category c1 on c1.sgid=pac.category_id ",nativeQuery = true)
			Page<Long> findAllPackageList(Pageable paging);
	
	@Query(value = "select * from package pp "
		+  " WHERE pp.sgid in (:sgids) ", nativeQuery = true)
	List<PackageEntity> findAllPackages(List<Long> sgids);
	
	@Query(value = "select * from package pac "
			+ "WHERE pac.sgid =:sgid ", nativeQuery = true)
	public PackageEntity findPackageCount(@Param("sgid") Long sgid);
}