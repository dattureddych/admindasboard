package com.inhabitr.admin.dasboard.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inhabitr.admin.dasboard.beans.User;
import com.inhabitr.admin.dasboard.model.UserEntity;
import com.inhabitr.admin.dasboard.services.UserManagementService;
import com.inhabitr.admin.dashboard.commons.exception.InhabitrException;
import com.koinplus.common.authentication.exception.InvalidLoginCredentialsException;
import com.koinplus.common.authentication.exception.InvalidResetPasswordException;
import com.koinplus.common.authentication.exception.PreRegisteredUserException;
import com.koinplus.common.authentication.exception.UnregisteredUserException;
import com.koinplus.common.exception.KoinPlusCommonExceptionCode;


@RestController
@CrossOrigin
@RequestMapping("/apps/api/signUp")
public class UserController {
	@Autowired
	private UserManagementService userManagementService;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	public Logger getLogger() {
		return logger;
	}
	
	public static final String SUCCESS = "{\"message\":\"SUCCESS\"}";
	public static final String FAILED = "{\"message\":\"FAILED\"}";
	
	@PostMapping("/signup")
	public User signup(@RequestBody User user) throws PreRegisteredUserException, InvalidLoginCredentialsException, UnregisteredUserException {
		return (userManagementService.signup(user));
	}
	@PostMapping("/login")
	public User login(@RequestBody User user) {
		//throws PreRegisteredUserException, InvalidLoginCredentialsException, UnregisteredUserException
	
		//return (userManagementService.login(user.getEmail(), user.getPassword()));
		String message=null;
		User userEntity=new User();
		try {
			
			userEntity= userManagementService.login(user.getEmail(), user.getPassword());
			if (userEntity != null) {
				 message = null;
				//return Response.ok(message).build();
				return userEntity;
			}else {
				userEntity=new User();
				message =  "Invalid username or password!!!";
				userEntity.setMessage(message);
				return userEntity;
			}
		} catch (UnregisteredUserException ex) {
			ex.printStackTrace();
			userEntity=new User();
			message =  "Invalid username or password!!!";
			userEntity.setMessage(message);
			return userEntity;
		}catch (InvalidLoginCredentialsException e) {
			e.printStackTrace();
			userEntity=new User();
			message =  "Invalid username or password!!!";
			userEntity.setMessage(message);
			return userEntity;
		}
		
	}
	
	/*@PostMapping("/user/forgotPassword")
	public String forgotPassword(@RequestParam(defaultValue = "") String email) throws PreRegisteredUserException, InvalidLoginCredentialsException, UnregisteredUserException, InhabitrException{
		return (userManagementService.forgotPassword(email));
	}*/
	@PostMapping("/user/forgotPassword")
	public String forgotPassword(String email) {
		String message=null;
		try {
			
			String tempPass = this.userManagementService.forgotPassword(email);
			if (tempPass != null) {
				 message = "{\"Temporary Password\":\"" + tempPass + "\"}";
				//return Response.ok(message).build();
				return message;
			}else {
				 message = "User not found!!!";
				return message;
			}
		} catch (UnregisteredUserException ex) {
			message="User not found!!!";
			getLogger().error(ex.getMessage());			
			return message;
		} catch (InhabitrException ex) {
			message="No loyalty Promo code found!!!";
			getLogger().error(ex.getMessage());
			
			return message;
		}
	//	return message;
       catch (InvalidLoginCredentialsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message="User not found!!!";
			getLogger().error(e.getMessage());
			
			return message;
		}
		
	}
	@PostMapping("/user/resetPassword")
	public String resetUserPassword(String email,String newPassword,String tempPassword) {
		String message=null;
		getLogger().info("Getting new Password {} for {}", newPassword, email);
		try {
			UserEntity brewer = this.userManagementService.resetUserPassword(email, tempPassword, newPassword);
			 message =  "{\"message\":\"Password reset done successfully.\"}";
     	     return message;
		} catch (InvalidLoginCredentialsException ex) {
			message =  "{\"message\":\"RESET PASSWORD LINK EXPIRED.\"}";
			getLogger().error(ex.getMessage());
			return message;
			
		} catch (InvalidResetPasswordException ex) {
			message =  "{\"message\":\"RESET PASSWORD LINK EXPIRED.\"}";
			getLogger().error(ex.getMessage());
			return message;
		} catch (PreRegisteredUserException pe) {
			getLogger().error(pe.getMessage());
			message = "{\"message\":\"Make sure the new password is different from the password used before.\"}";
			if (pe.getCode().equals(KoinPlusCommonExceptionCode.EXISTING_USER_PWD)) {
				message = pe.getOriginalExceptionMessage();
				getLogger().info(message);
			}
			return message;
		}
	//	return Response.status(Status.EXPECTATION_FAILED).entity(FAILED).build();
	}
}
