package com.inhabitr.admin.dasboard.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * @author Dattu Reddy
 */
@MappedSuperclass
public abstract class GenericKoinPlusDataEntity<T>  {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected @Column(name = "sgid", updatable = false, nullable = false) Long sgid = null;

    public Long getSgid() {
        return sgid;
    }

    public void setSgid(Long sgid) {
        this.sgid = sgid;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof GenericKoinPlusDataEntity)) {
            return false;
        }
        final GenericKoinPlusDataEntity other = (GenericKoinPlusDataEntity) obj;
        if (this.sgid != null && other.sgid != null) {
            if (!this.sgid.equals(other.sgid)) {
                return false;
            }
        }
        return true;
    }

}

