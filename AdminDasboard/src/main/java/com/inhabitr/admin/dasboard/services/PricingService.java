package com.inhabitr.admin.dasboard.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.inhabitr.admin.dasboard.model.BrewerEntity;
import com.inhabitr.admin.dasboard.beans.BrewerEntity_;
import com.inhabitr.admin.dasboard.repository.PricingRepository;

@Service
public class PricingService {
	@Autowired
	private PricingRepository pricingRepository;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
	public List<BrewerEntity> findAll(Integer pageNo, Integer pageSize,String sortDir,String sortBy) {
		/*
		 * Iterable<BrewerEntity> findAll = pricingRepository.findAll();
		 * Iterator<BrewerEntity> iterator = findAll.iterator(); List<BrewerEntity>
		 * itemList = new ArrayList<BrewerEntity>(); List<BrewerEntity_> itemList1 = new
		 * ArrayList<BrewerEntity_>(); BrewerEntity_ obj=new BrewerEntity_(); while
		 * (iterator.hasNext()) { itemList.add(iterator.next()); } for( BrewerEntity
		 * brewer : itemList ) { obj=new BrewerEntity_(); // System.out.print( brewer.
		 * ); obj.setFirstName(brewer.getFirstName());
		 * obj.setLastName(brewer.getLastName());
		 * obj.setMiddleName(brewer.getMiddleName());
		 * obj.setSalutation(brewer.getSalutation());
		 * obj.setSignUpDate(brewer.getSignUpDate());
		 * obj.setVendor("Amazon");//System.out.print(","); obj.setMultiplier(2l);
		 * obj.setCategory("Decor"); itemList1.add(obj); //
		 * itemList1.addAll((Collection<? extends BrewerEntity_>) brewer); }
		 * 
		 * return itemList1;
		 */
		getLogger().info("Inside findAll()");
		
		Pageable paging = PageRequest.of(pageNo, pageSize,
	            sortDir.equals("asc") ? Sort.by(sortBy).ascending()
                        : Sort.by(sortBy).descending());
		 
        Page<BrewerEntity> pagedResult = pricingRepository.findAll(paging);
        getLogger().info("pagedResult list Size=="+pagedResult.getContent().size());
        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<BrewerEntity>();
        }
	}
}
