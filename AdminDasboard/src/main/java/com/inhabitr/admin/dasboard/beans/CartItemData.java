package com.inhabitr.admin.dasboard.beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;



public class CartItemData {

	private Long cartSgid;
    private String itemName;
    private Long itemSgid;
    private Long itemTrendSgid;
    private String itemImageUrl;
    private BigDecimal itemOriginalPrice;
    private BigDecimal itemPrice;
    private BigDecimal itemAffirmPrice;
    private BigDecimal itemAffirmTotalPrice;
    private String retailerName;
    private Long retailerSgid;
    private String retailerLogoUrl;
    private String purchaseUrl;
    private BigDecimal shippingPrice;
    private BigDecimal saleTaxAmount;
    private BigDecimal totalFinalPrice;
    private BigDecimal affirmPrice = BigDecimal.ZERO;
    private String couponCode;
    private Boolean checkoutFlag;
	private Date  checkoutFlagDatetime;
	private Long brewPoints = 0l;
    private Date dateAdded;
    private Long minDeliveryDays = 0l;
    private Long maxDeliveryDays = 0l;
    private Boolean isClover;
    private Boolean isRent;
    private Boolean isTwotap;
    private Boolean isShopify;
    private Boolean isTicketrewards;
    private Boolean isTrendbrew;
    private Boolean orderProcessed;
    private String trendbrewOrderNumber;
    private String retailerOrderNumber;
    private String orderConfirmationNumber;
    private String twoTapPurchaseId;
    private String twoTapProductId;
    private String twoTapRetailerId;
    private String shippingOption;  
    private Boolean isAvailable;
    private Date isAvailableDatetime;
    private String ticketRewardsAvailableDate;
    private Date updatedDate;
    private Boolean isPurchaseConfirmed;
    private String status;
    private String statusReason;
    private String finalMessage;
    private String affiliateUrl;
    private Date orderConfirmedDatetime;
    private Integer stock;
    private Integer lockedQty;
    private Long ticketRewardsItemVariantSgid;
    private String eventAddress;
    private String ticketDeliveryMethod;
    private String ticketDeliveryMethodDescription;
    private String itemSource;
    private Long deliveryDate1;
    private Long deliveryDate2;
    private Long deliveryDate3;
    private String deliveryHour;
    private String deliveryHour1;
    private String deliveryHour2;
    private Boolean isBuyUsed;
    private Boolean isBuyNew;
    private Map<String, Object> monthlyRent = new HashMap<String, Object>();
    private Map<Long, String> categories = new HashMap<Long, String>();
    private Map<String, String> categorySlug = new HashMap<String, String>();  
    private String userEmail;
    private String userType;
    private String phNo;
    private String sku;
    private Integer itemQuantity;
    private Integer month;
    private String slug;
    private Long dashboardOrderId;
    private String orderNumber;
    private Long location;
    private BigDecimal shippingFee;
    private Map<String, Object> cartOptionsMap = new HashMap<String, Object>();
    private List<Map<String, Object>> cartItemAttributesList = new ArrayList<Map<String, Object>>();
    private String type;
    private Long attributeId;
    private Long attributeListId;
	private Long opsDbCartItemSgid;
    private Integer isPreAssigned;
    private Integer isShowOffer;
    private Long opsDbCartSgid;
    private Boolean isReturnable = Boolean.FALSE;
    
    public Boolean getIsReturnable() {
		return isReturnable;
	}

	public void setIsReturnable(Boolean isReturnable) {
		this.isReturnable = isReturnable;
	}

	public Long getOpsDbCartSgid() {
		return opsDbCartSgid;
	}

	public void setOpsDbCartSgid(Long opsDbCartSgid) {
		this.opsDbCartSgid = opsDbCartSgid;
	}

	public Integer getIsPreAssigned() {
		return isPreAssigned;
	}

	public void setIsPreAssigned(Integer isPreAssigned) {
		this.isPreAssigned = isPreAssigned;
	}

	public Integer getIsShowOffer() {
		return isShowOffer;
	}

	public void setIsShowOffer(Integer isShowOffer) {
		this.isShowOffer = isShowOffer;
	}

	public Long getOpsDbCartItemSgid() {
		return opsDbCartItemSgid;
	}

	public void setOpsDbCartItemSgid(Long opsDbCartItemSgid) {
		this.opsDbCartItemSgid = opsDbCartItemSgid;
	}

	public Long getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(Long attributeId) {
		this.attributeId = attributeId;
	}

	public Long getAttributeListId() {
		return attributeListId;
	}

	public void setAttributeListId(Long attributeListId) {
		this.attributeListId = attributeListId;
	}

	public Long getLocation() {
		return location;
	}

	public void setLocation(Long location) {
		this.location = location;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public BigDecimal getShippingFee() {
		return shippingFee;
	}

	public void setShippingFee(BigDecimal shippingFee) {
		this.shippingFee = shippingFee;
	}

	public Map<String, Object> getCartOptionsMap() {
		return cartOptionsMap;
	}

	public void setCartOptionsMap(Map<String, Object> cartOptionsMap) {
		this.cartOptionsMap = cartOptionsMap;
	}

	public List<Map<String, Object>> getCartItemAttributesList() {
		return cartItemAttributesList;
	}

	public void setCartItemAttributesList(List<Map<String, Object>> cartItemAttributesList) {
		this.cartItemAttributesList = cartItemAttributesList;
	}
	public void addCategory(Long id, String name) {
        if (this.categories == null)
            this.categories = new HashMap<Long, String>();
        this.categories.put(id, name);
    }
    
    public void addCategorySlug(String key, String name) {
        if (this.categorySlug == null)
            this.categorySlug = new HashMap<String, String>();
        this.categorySlug.put(key, name);
    }
  
	public Long getDashboardOrderId() {
		return dashboardOrderId;
	}

	public void setDashboardOrderId(Long dashboardOrderId) {
		this.dashboardOrderId = dashboardOrderId;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public Map<Long, String> getCategories() {
		return categories;
	}

	public void setCategories(Map<Long, String> categories) {
		this.categories = categories;
	}

	public Map<String, String> getCategorySlug() {
		return categorySlug;
	}

	public void setCategorySlug(Map<String, String> categorySlug) {
		this.categorySlug = categorySlug;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}
    public Boolean getIsBuyUsed() {
		return isBuyUsed;
	}
	public void setIsBuyUsed(Boolean isBuyUsed) {
		this.isBuyUsed = isBuyUsed;
	}
	public Boolean getIsBuyNew() {
		return isBuyNew;
	}
	public void setIsBuyNew(Boolean isBuyNew) {
		this.isBuyNew = isBuyNew;
	}
	public Long getCartSgid() {
		return cartSgid;
	}
	public void setCartSgid(Long cartSgid) {
		this.cartSgid = cartSgid;
	}
	
	public Long getItemTrendSgid() {
		return itemTrendSgid;
	}
	public void setItemTrendSgid(Long itemTrendSgid) {
		this.itemTrendSgid = itemTrendSgid;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Long getItemSgid() {
		return itemSgid;
	}
	public void setItemSgid(Long itemSgid) {
		this.itemSgid = itemSgid;
	}
	public String getItemImageUrl() {
		return itemImageUrl;
	}
	public void setItemImageUrl(String itemImageUrl) {
		this.itemImageUrl = itemImageUrl;
	}
	public BigDecimal getItemOriginalPrice() {
		return itemOriginalPrice;
	}
	public void setItemOriginalPrice(BigDecimal itemOriginalPrice) {
		this.itemOriginalPrice = itemOriginalPrice;
	}
	public BigDecimal getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(BigDecimal itemPrice) {
		this.itemPrice = itemPrice;
	}
	public BigDecimal getItemAffirmPrice() {
		return itemAffirmPrice;
	}

	public void setItemAffirmPrice(BigDecimal itemAffirmPrice) {
		this.itemAffirmPrice = itemAffirmPrice;
	}

	public BigDecimal getItemAffirmTotalPrice() {
		return itemAffirmTotalPrice;
	}

	public void setItemAffirmTotalPrice(BigDecimal itemAffirmTotalPrice) {
		this.itemAffirmTotalPrice = itemAffirmTotalPrice;
	}

	public String getRetailerName() {
		return retailerName;
	}
	public void setRetailerName(String retailerName) {
		this.retailerName = retailerName;
	}
	public Long getRetailerSgid() {
		return retailerSgid;
	}
	public void setRetailerSgid(Long retailerSgid) {
		this.retailerSgid = retailerSgid;
	}
	public String getRetailerLogoUrl() {
		return retailerLogoUrl;
	}
	public void setRetailerLogoUrl(String retailerLogoUrl) {
		this.retailerLogoUrl = retailerLogoUrl;
	}
	public String getPurchaseUrl() {
		return purchaseUrl;
	}
	public void setPurchaseUrl(String purchaseUrl) {
		this.purchaseUrl = purchaseUrl;
	}
	public BigDecimal getShippingPrice() {
		return shippingPrice;
	}
	public void setShippingPrice(BigDecimal shippingPrice) {
		this.shippingPrice = shippingPrice;
	}
	public BigDecimal getSaleTaxAmount() {
		return saleTaxAmount;
	}
	public void setSaleTaxAmount(BigDecimal saleTaxAmount) {
		this.saleTaxAmount = saleTaxAmount;
	}
	public BigDecimal getTotalFinalPrice() {
		return totalFinalPrice;
	}
	public void setTotalFinalPrice(BigDecimal totalFinalPrice) {
		this.totalFinalPrice = totalFinalPrice;
	}
	public BigDecimal getAffirmPrice() {
		return affirmPrice;
	}

	public void setAffirmPrice(BigDecimal affirmPrice) {
		this.affirmPrice = affirmPrice;
	}

	public String getCouponCode() {
		return couponCode;
	}
	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}
	public Boolean getCheckoutFlag() {
		return checkoutFlag;
	}
	public void setCheckoutFlag(Boolean checkoutFlag) {
		this.checkoutFlag = checkoutFlag;
	}
	public Date getCheckoutFlagDatetime() {
		return checkoutFlagDatetime;
	}
	public void setCheckoutFlagDatetime(Date checkoutFlagDatetime) {
		this.checkoutFlagDatetime = checkoutFlagDatetime;
	}
	public Long getBrewPoints() {
		return brewPoints;
	}
	public void setBrewPoints(Long brewPoints) {
		this.brewPoints = brewPoints;
	}
	public Date getDateAdded() {
		return dateAdded;
	}
	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}
	public Long getMinDeliveryDays() {
		return minDeliveryDays;
	}
	public void setMinDeliveryDays(Long minDeliveryDays) {
		this.minDeliveryDays = minDeliveryDays;
	}
	public Long getMaxDeliveryDays() {
		return maxDeliveryDays;
	}
	public void setMaxDeliveryDays(Long maxDeliveryDays) {
		this.maxDeliveryDays = maxDeliveryDays;
	}
	public Boolean getIsClover() {
		return isClover;
	}
	public void setIsClover(Boolean isClover) {
		this.isClover = isClover;
	}
	public Boolean getIsRent() {
		return isRent;
	}
	public void setIsRent(Boolean isRent) {
		this.isRent = isRent;
	}
	public Boolean getIsTwotap() {
		return isTwotap;
	}
	public void setIsTwotap(Boolean isTwotap) {
		this.isTwotap = isTwotap;
	}
	public Boolean getIsShopify() {
		return isShopify;
	}
	public void setIsShopify(Boolean isShopify) {
		this.isShopify = isShopify;
	}
	public Boolean getIsTicketrewards() {
		return isTicketrewards;
	}
	public void setIsTicketrewards(Boolean isTicketrewards) {
		this.isTicketrewards = isTicketrewards;
	}
	public Boolean getIsTrendbrew() {
		return isTrendbrew;
	}
	public void setIsTrendbrew(Boolean isTrendbrew) {
		this.isTrendbrew = isTrendbrew;
	}
	public Boolean getOrderProcessed() {
		return orderProcessed;
	}
	public void setOrderProcessed(Boolean orderProcessed) {
		this.orderProcessed = orderProcessed;
	}
	public String getTrendbrewOrderNumber() {
		return trendbrewOrderNumber;
	}
	public void setTrendbrewOrderNumber(String trendbrewOrderNumber) {
		this.trendbrewOrderNumber = trendbrewOrderNumber;
	}
	public String getRetailerOrderNumber() {
		return retailerOrderNumber;
	}
	public void setRetailerOrderNumber(String retailerOrderNumber) {
		this.retailerOrderNumber = retailerOrderNumber;
	}
	public String getOrderConfirmationNumber() {
		return orderConfirmationNumber;
	}
	public void setOrderConfirmationNumber(String orderConfirmationNumber) {
		this.orderConfirmationNumber = orderConfirmationNumber;
	}
	public String getTwoTapPurchaseId() {
		return twoTapPurchaseId;
	}
	public void setTwoTapPurchaseId(String twoTapPurchaseId) {
		this.twoTapPurchaseId = twoTapPurchaseId;
	}
	public String getTwoTapProductId() {
		return twoTapProductId;
	}
	public void setTwoTapProductId(String twoTapProductId) {
		this.twoTapProductId = twoTapProductId;
	}
	public String getTwoTapRetailerId() {
		return twoTapRetailerId;
	}
	public void setTwoTapRetailerId(String twoTapRetailerId) {
		this.twoTapRetailerId = twoTapRetailerId;
	}
	public String getShippingOption() {
		return shippingOption;
	}
	public void setShippingOption(String shippingOption) {
		this.shippingOption = shippingOption;
	}
	public Boolean getIsAvailable() {
		return isAvailable;
	}
	public void setIsAvailable(Boolean isAvailable) {
		this.isAvailable = isAvailable;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getIsPurchaseConfirmed() {
		return isPurchaseConfirmed;
	}
	public void setIsPurchaseConfirmed(Boolean isPurchaseConfirmed) {
		this.isPurchaseConfirmed = isPurchaseConfirmed;
	}
	public String getStatusReason() {
		return statusReason;
	}
	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}
	public String getFinalMessage() {
		return finalMessage;
	}
	public void setFinalMessage(String finalMessage) {
		this.finalMessage = finalMessage;
	}
	public String getAffiliateUrl() {
		return affiliateUrl;
	}
	public void setAffiliateUrl(String affiliateUrl) {
		this.affiliateUrl = affiliateUrl;
	}
	public Date getIsAvailableDatetime() {
		return isAvailableDatetime;
	}
	public void setIsAvailableDatetime(Date isAvailableDatetime) {
		this.isAvailableDatetime = isAvailableDatetime;
	}
	public String getTicketRewardsAvailableDate() {
		return ticketRewardsAvailableDate;
	}
	public void setTicketRewardsAvailableDate(String ticketRewardsAvailableDate) {
		this.ticketRewardsAvailableDate = ticketRewardsAvailableDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Date getOrderConfirmedDatetime() {
		return orderConfirmedDatetime;
	}
	public void setOrderConfirmedDatetime(Date orderConfirmedDatetime) {
		this.orderConfirmedDatetime = orderConfirmedDatetime;
	}
	public Integer getStock() {
		return stock;
	}
	public void setStock(Integer stock) {
		this.stock = stock;
	}
	public Integer getLockedQty() {
		return lockedQty;
	}
	public void setLockedQty(Integer lockedQty) {
		this.lockedQty = lockedQty;
	}
	public Long getTicketRewardsItemVariantSgid() {
		return ticketRewardsItemVariantSgid;
	}
	public void setTicketRewardsItemVariantSgid(Long ticketRewardsItemVariantSgid) {
		this.ticketRewardsItemVariantSgid = ticketRewardsItemVariantSgid;
	}
	public String getEventAddress() {
		return eventAddress;
	}
	public void setEventAddress(String eventAddress) {
		this.eventAddress = eventAddress;
	}
	public String getTicketDeliveryMethod() {
		return ticketDeliveryMethod;
	}
	public void setTicketDeliveryMethod(String ticketDeliveryMethod) {
		this.ticketDeliveryMethod = ticketDeliveryMethod;
	}
	public String getTicketDeliveryMethodDescription() {
		return ticketDeliveryMethodDescription;
	}
	public void setTicketDeliveryMethodDescription(String ticketDeliveryMethodDescription) {
		this.ticketDeliveryMethodDescription = ticketDeliveryMethodDescription;
	}
	public String getItemSource() {
		return itemSource;
	}
	public void setItemSource(String itemSource) {
		this.itemSource = itemSource;
	}
	public Long getDeliveryDate1() {
		return deliveryDate1;
	}
	public void setDeliveryDate1(Long deliveryDate1) {
		this.deliveryDate1 = deliveryDate1;
	}
	public Long getDeliveryDate2() {
		return deliveryDate2;
	}
	public void setDeliveryDate2(Long deliveryDate2) {
		this.deliveryDate2 = deliveryDate2;
	}
	public Long getDeliveryDate3() {
		return deliveryDate3;
	}
	public void setDeliveryDate3(Long deliveryDate3) {
		this.deliveryDate3 = deliveryDate3;
	}
	public String getDeliveryHour() {
		return deliveryHour;
	}
	public void setDeliveryHour(String deliveryHour) {
		this.deliveryHour = deliveryHour;
	}
	public String getDeliveryHour1() {
		return deliveryHour1;
	}
	public void setDeliveryHour1(String deliveryHour1) {
		this.deliveryHour1 = deliveryHour1;
	}
	public String getDeliveryHour2() {
		return deliveryHour2;
	}
	public void setDeliveryHour2(String deliveryHour2) {
		this.deliveryHour2 = deliveryHour2;
	}
	public Map<String, Object> getMonthlyRent() {
		return monthlyRent;
	}
	public void setMonthlyRent(Map<String, Object> monthlyRent) {
		this.monthlyRent = monthlyRent;
	}
	/**
	 * @return the userEmail
	 */
	public String getUserEmail() {
		return userEmail;
	}
	/**
	 * @param userEmail the userEmail to set
	 */
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	/**
	 * @return the userType
	 */
	public String getUserType() {
		return userType;
	}
	/**
	 * @param userType the userType to set
	 */
	public void setUserType(String userType) {
		this.userType = userType;
	}
	/**
	 * @return the phNo
	 */
	public String getPhNo() {
		return phNo;
	}
	/**
	 * @param phNo the phNo to set
	 */
	public void setPhNo(String phNo) {
		this.phNo = phNo;
	}

	/**
	 * @return the itemQuantity
	 */
	public Integer getItemQuantity() {
		return itemQuantity;
	}

	/**
	 * @param itemQuantity the itemQuantity to set
	 */
	public void setItemQuantity(Integer itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	/**
	 * @return the month
	 */
	public Integer getMonth() {
		return month;
	}

	/**
	 * @param month the month to set
	 */
	public void setMonth(Integer month) {
		this.month = month;
	}
	
}