package com.inhabitr.admin.dasboard.repository;

import java.util.Date;


import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.UserEntity;

@Repository
public interface UserRepository extends PagingAndSortingRepository<UserEntity, Long>{
	
			@Modifying
			@Transactional
			@Query(value= "insert into ca_admin_user "
					+ "(ca_admin_id, first_name, last_name, middle_name, email, password, salt, temp_password, password_plain,  status, disabled) "
					+ "VALUES (:ca_admin_id, :first_name, :last_name, :middle_name, :email, :password, :salt, :temp_password, :password_plain, :status, :disabled)",
			        nativeQuery = true)
			public void createUser( 
					@Param("ca_admin_id") String ca_admin_id, @Param("first_name") String first_name,
					@Param("last_name") String last_name, @Param("middle_name") String middle_name,
					@Param("email") String email, @Param("password") byte[] password,
					@Param("salt") byte[] salt, @Param("temp_password") String temp_password,
					@Param("password_plain") String password_plain, 
					@Param("status") Integer status, @Param("disabled") Integer disabled);
			
			
			 @Query(value= "select * from ca_admin_user admin "
			 +"where admin.email LIKE %:email%", nativeQuery = true)
	    	 public List<UserEntity> getUser(@Param("email") String email);
			 
			@Query(value= "update ca_admin_user admin "
					+ "set admin.salt =:salt, admin.password =:password "
					+ "where admin.email =:email", nativeQuery = true)
			public UserEntity update(@Param("salt") byte[] salt, 
					@Param("password") byte[] password, @Param("email") String email);
			
			@Query(value= "select * from ca_admin_user admin "
					+ "where admin.ca_admin_id LIKE %:ca_admin_id%", nativeQuery = true)
			public List<UserEntity> find(@Param("ca_admin_id") String ca_admin_id);
			
			@Query(value= "select * from ca_admin_user  "
					+ "where sgid=:sgid", nativeQuery = true)
			public UserEntity getUserBySgid(@Param("sgid") Integer sgid);
			
			/*@Query(value= "update ca_admin_user  "
					+ " set temp_password =abc123"
					+ " where sgid =1", nativeQuery = true)
			public void updateUser();*/
			
			@Modifying
			@Transactional
			@Query(value= "update ca_admin_user set temp_password=:temp_password where sgid=:sgid", nativeQuery = true)
			public void updateUserDetails(@Param("sgid") Integer sgid, @Param("temp_password") String temp_password);
			
			@Modifying
			@Transactional
			@Query(value= "update ca_admin_user set temp_password=:temp_password,password=:password,salt=:salt where email=:email", nativeQuery = true)
			public void updateUserInfo(@Param("email") String email, @Param("temp_password") String temp_password, @Param("password") byte[] password,
					@Param("salt") byte[] salt);

			
}
