package com.inhabitr.admin.dasboard.model;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import java.math.BigDecimal;

import java.util.HashSet;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "brewer_reward_transactions")
public class BrewerRewardTransactionEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	
	private @Column(name = "brewer_sgid") Long brewerSgid;
	private @Column(name = "property_user_sgid") Long propertyUserSgid;
	private @Column(name = "brewer_reward_sgid") Long brewerRewardSgid;
	private @Column(name = "property_user_reward_sgid") Long propertyUserRewardSgid;
	private @Column(name = "property_sgid") Long propertySgid;
	private @Column(name = "order_id") String orderId;
	private @Column(name = "reward_id") String rewardId;	
	private @Column(name = "resident_id") String residentId;
	private @Column(name = "reward_transaction_amount") BigDecimal rewardTransactionAmount;
	private @Column(name = "payment_status") String paymentStatus;
	private @Column(name = "payment_type") String paymentType;
	private @Column(name = "payment_token") String paymentToken;
	private @Column(name = "payment_object") String paymentObject;
	private @Column(name = "cart_total_amount") BigDecimal cartTotalAmount;
	private @Column(name = "reward_balance") BigDecimal rewardBalance;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "property_sgid", insertable = false, updatable = false)
	private CASLPropertyEntity property;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "brewer_sgid", insertable = false, updatable = false)
	private BrewerEntity brewer;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "property_user_reward_sgid", insertable = false, updatable = false)
	private PropertyUserRewardEntity userReward;

	public Long getBrewerSgid() {
		return brewerSgid;
	}

	public void setBrewerSgid(Long brewerSgid) {
		this.brewerSgid = brewerSgid;
	}

	public Long getPropertyUserSgid() {
		return propertyUserSgid;
	}

	public void setPropertyUserSgid(Long propertyUserSgid) {
		this.propertyUserSgid = propertyUserSgid;
	}

	public String getResidentId() {
		return residentId;
	}

	public void setResidentId(String residentId) {
		this.residentId = residentId;
	}

	public Long getBrewerRewardSgid() {
		return brewerRewardSgid;
	}

	public void setBrewerRewardSgid(Long brewerRewardSgid) {
		this.brewerRewardSgid = brewerRewardSgid;
	}

	public Long getPropertyUserRewardSgid() {
		return propertyUserRewardSgid;
	}

	public void setPropertyUserRewardSgid(Long propertyUserRewardSgid) {
		this.propertyUserRewardSgid = propertyUserRewardSgid;
	}

	public Long getPropertySgid() {
		return propertySgid;
	}

	public void setPropertySgid(Long propertySgid) {
		this.propertySgid = propertySgid;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getRewardId() {
		return rewardId;
	}

	public void setRewardId(String rewardId) {
		this.rewardId = rewardId;
	}

	public BigDecimal getRewardTransactionAmount() {
		return rewardTransactionAmount;
	}

	public void setRewardTransactionAmount(BigDecimal rewardTransactionAmount) {
		this.rewardTransactionAmount = rewardTransactionAmount;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPaymentToken() {
		return paymentToken;
	}

	public void setPaymentToken(String paymentToken) {
		this.paymentToken = paymentToken;
	}

	public String getPaymentObject() {
		return paymentObject;
	}

	public void setPaymentObject(String paymentObject) {
		this.paymentObject = paymentObject;
	}

	public CASLPropertyEntity getProperty() {
		return property;
	}

	public void setProperty(CASLPropertyEntity property) {
		this.property = property;
	}

	public BrewerEntity getBrewer() {
		return brewer;
	}

	public void setBrewer(BrewerEntity brewer) {
		this.brewer = brewer;
	}

	public PropertyUserRewardEntity getUserReward() {
		return userReward;
	}

	public void setUserReward(PropertyUserRewardEntity userReward) {
		this.userReward = userReward;
	}

	public BigDecimal getCartTotalAmount() {
		return cartTotalAmount;
	}

	public void setCartTotalAmount(BigDecimal cartTotalAmount) {
		this.cartTotalAmount = cartTotalAmount;
	}

	public BigDecimal getRewardBalance() {
		return rewardBalance;
	}

	public void setRewardBalance(BigDecimal rewardBalance) {
		this.rewardBalance = rewardBalance;
	}

	public Long getSgid() {
		return sgid;
	}

	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}	
	
}
