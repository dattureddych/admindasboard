package com.inhabitr.admin.dasboard.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
//import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.inhabitr.admin.dasboard.model.PackageEntity;
import com.inhabitr.admin.dasboard.repository.PackageRepository;

@Service
public class PackageService {
	@Autowired
	private PackageRepository packageRepository;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
	
	public List<PackageEntity> getAllPackages(Long catSgid,Boolean isAvailable,String searchStr,Integer pageNo, Integer pageSize,String sortDir,String sortBy) {
		
		getLogger().info("Inside getAllPackages()");

		Pageable paging = PageRequest.of(pageNo, pageSize,
				sortDir.equals("asc") ? Sort.by(sortBy).ascending() : Sort.by("sgid").descending());
		List<PackageEntity> packageEntityList = null;
		Page<Long>searchList1 = null;
		if ((catSgid != null && !catSgid.equals("")) && (isAvailable != null && !isAvailable.equals(""))
				&& (searchStr != null && !searchStr.equals(""))) {
			searchList1 = packageRepository.findPackageByFilters(catSgid, isAvailable, searchStr.toUpperCase(),paging);
			packageEntityList = packageRepository.findPackageByFilters(catSgid, isAvailable, searchStr.toUpperCase(),
					searchList1.getContent());

		}
		if ((catSgid != null && !catSgid.equals("")) && (isAvailable != null && !isAvailable.equals(""))
				&& (searchStr == null || searchStr.equals(""))) {
			searchList1 = packageRepository.findPackageByFilters(catSgid, isAvailable,paging);
			packageEntityList = packageRepository.findPackageByFilters(catSgid, isAvailable, searchList1.getContent());

		}
		if ((catSgid != null && !catSgid.equals("")) && (isAvailable == null || isAvailable.equals(""))
				&& (searchStr != null && !searchStr.equals(""))) {
			searchList1 = packageRepository.findPackageBySearch(catSgid, searchStr.toUpperCase(),paging);
			packageEntityList = packageRepository.findPackageBySearch(catSgid, searchStr.toUpperCase(), searchList1.getContent());
		}
		if ((catSgid == null || catSgid.equals("")) && (isAvailable != null && !isAvailable.equals(""))
				&& (searchStr != null && !searchStr.equals(""))) {
			searchList1 = packageRepository.searchPackageByStatus(isAvailable, searchStr.toUpperCase(),paging);
			packageEntityList = packageRepository.searchPackageByStatus(isAvailable, searchStr.toUpperCase(),searchList1.getContent());
		}
		if ((catSgid == null || catSgid.equals("")) && (isAvailable == null || isAvailable.equals(""))
				&& (searchStr != null && !searchStr.equals(""))) {
			searchList1 = packageRepository.findWithSearch(searchStr.toUpperCase(),paging);
			packageEntityList = packageRepository.findWithSearch(searchStr.toUpperCase(), searchList1.getContent());
		}
		if ((catSgid == null || catSgid.equals("")) && (isAvailable != null && !isAvailable.equals(""))
				&& (searchStr == null || searchStr.equals(""))) {
			searchList1 = packageRepository.findPackageByAvailability(isAvailable,paging);
			packageEntityList = packageRepository.findPackageByAvailability(isAvailable, searchList1.getContent());
		}
		if ((catSgid != null && !catSgid.equals("")) && (isAvailable == null || isAvailable.equals(""))
				&& (searchStr == null || searchStr.equals(""))) {
			searchList1 = packageRepository.findPackageByCategory(catSgid,paging);
			packageEntityList = packageRepository.findPackageByCategory(catSgid, searchList1.getContent());
		}

		if ((catSgid == null || catSgid.equals("")) && (isAvailable == null || isAvailable.equals(""))
				&& (searchStr == null || searchStr.equals(""))) {
			searchList1 = packageRepository.findAllPackageList(paging);
			packageEntityList = packageRepository.findAllPackages(searchList1.getContent());
		}

		if (packageEntityList!=null && packageEntityList.size()>0) {
			return packageEntityList;
		} else {
			return new ArrayList<PackageEntity>();
		}
	}
	
	public void hideOrShowPackage(Boolean isAvailable,Long sgid)throws Exception {
		logger.info("Inside /apps/api/package/hideOrShowPackage/status" +isAvailable+ "&sgid"+sgid);
		Integer status=null;
		packageRepository.updatestatus(isAvailable,sgid);
		if(isAvailable==true) {
			status=1;
		packageRepository.updatePackageStatus(status,sgid); 
		}else {
			status=0;
			packageRepository.updatePackageStatus(status,sgid); 
		}
		return;
	}

	public List<PackageEntity> searchPackage(String queryParam, Integer pageNo, Integer pageSize, String sortDir,
			String sortBy) {
		getLogger().info("Inside searchPackage()");
		Pageable paging = PageRequest.of(pageNo, pageSize,
				sortDir.equals("asc") ? Sort.by(sortBy).ascending() : Sort.by("sgid").descending());
		Page<PackageEntity> searchList = packageRepository.findByNameContainingIgnoreCaseOrSkuContainingIgnoreCase(queryParam, queryParam,paging);
		
		getLogger().info("searchList size=" + searchList.getContent().size());
		if (searchList.hasContent()) {
			return searchList.getContent();
		} else {
			return new ArrayList<PackageEntity>();
		}
	}

	
	


}
