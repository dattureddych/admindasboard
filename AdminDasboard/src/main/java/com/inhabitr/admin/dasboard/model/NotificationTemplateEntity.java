package com.inhabitr.admin.dasboard.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.inhabitr.admin.dashboard.commons.NotificationType;

@Entity @Table(name = "notification_templates")
public class NotificationTemplateEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
    private @Column(name = "name") @Enumerated(EnumType.STRING) NotificationType name;
    private @Column(name = "message_template") String messageTemplate;
    private @Column(name = "sms_template") String smsTemplate;
    private @Column(name = "email_subject_template") String emailSubjectTemplate;
    private @Column(name = "email_message_template") String emailMessageTemplate;

    public NotificationType getName() {
        return name;
    }

    public void setName(NotificationType name) {
        this.name = name;
    }

    public String getMessageTemplate() {
        return messageTemplate;
    }

    public void setMessageTemplate(String messageTemplate) {
        this.messageTemplate = messageTemplate;
    }

    public String getSmsTemplate() {
        return smsTemplate;
    }

    public void setSmsTemplate(String smsTemplate) {
        this.smsTemplate = smsTemplate;
    }

    public String getEmailSubjectTemplate() {
        return emailSubjectTemplate;
    }

    public void setEmailSubjectTemplate(String emailSubjectTemplate) {
        this.emailSubjectTemplate = emailSubjectTemplate;
    }

    public String getEmailMessageTemplate() {
        return emailMessageTemplate;
    }

    public void setEmailMessageTemplate(String emailMessageTemplate) {
        this.emailMessageTemplate = emailMessageTemplate;
    }

	public Long getSgid() {
		return sgid;
	}

	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}
    
    

}

