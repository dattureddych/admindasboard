package com.inhabitr.admin.dasboard.model;


import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import com.koinplus.common.data.GenericKoinPlusDataEntity;

/**
 * @author Dattu Reddy
 */
@Entity @Table(name = "card_payment_request_response")
public class CardPaymentRequestResponseEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	
	private @Column(name = "transaction_id") Long transactionSgid;
	private @Column(name = "payment_status") String paymentStatus;
	private @Column(name = "card_type") String cardType;
	private @Column(name = "card_number") Long cardNumber;
	private @Column(name = "token") String token;
	private @Column(name = "isactive") Integer isactive;
	private @Column(name = "recordstatus") Integer recordStatus;
	private @Column(name = "amount") BigDecimal amount;
	private @Column(name = "response_object") String responseObject;
    private @Column(name = "created_datetime") @Temporal(TemporalType.TIMESTAMP) Date createdDatetime;
    @OneToOne(fetch = FetchType.LAZY) @JoinColumn(name = "customer_id")
    private BrewerEntity brewer;
    @OneToOne(fetch = FetchType.LAZY) @JoinColumn(name = "order_sgid")
    private OrderEntity order;
    @OneToOne(fetch = FetchType.LAZY) @JoinColumn(name = "cart_sgid")
    private CartEntity cart;
	public Long getTransactionSgid() {
		return transactionSgid;
	}
	public void setTransactionSgid(Long transactionSgid) {
		this.transactionSgid = transactionSgid;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public Long getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(Long cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Integer getIsactive() {
		return isactive;
	}
	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}
	public Integer getRecordStatus() {
		return recordStatus;
	}
	public void setRecordStatus(Integer recordStatus) {
		this.recordStatus = recordStatus;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public Date getCreatedDatetime() {
		return createdDatetime;
	}
	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	public BrewerEntity getBrewer() {
		return brewer;
	}
	public void setBrewer(BrewerEntity brewer) {
		this.brewer = brewer;
	}
	public OrderEntity getOrder() {
		return order;
	}
	public void setOrder(OrderEntity order) {
		this.order = order;
	}
	public CartEntity getCart() {
		return cart;
	}
	public void setCart(CartEntity cart) {
		this.cart = cart;
	}
	public String getResponseObject() {
		return responseObject;
	}
	public void setResponseObject(String responseObject) {
		this.responseObject = responseObject;
	}
	public Long getSgid() {
		return sgid;
	}
	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}
	
    
}
