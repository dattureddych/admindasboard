package com.inhabitr.admin.dasboard.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;


import javax.persistence.Transient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inhabitr.admin.dasboard.model.CategoryEntity;
import com.inhabitr.admin.dasboard.model.CategoryGroup;
import com.inhabitr.admin.dasboard.model.ItemEntity;
import com.inhabitr.admin.dasboard.model.PackageEntity;
import com.inhabitr.admin.dasboard.model.ProductCombinationsEntity;
import com.inhabitr.admin.dasboard.model.ProductImageEntity;
import com.inhabitr.admin.dasboard.response.SearchResponse;
import com.inhabitr.admin.dasboard.services.CategoryService;
import com.inhabitr.admin.dasboard.services.ItemService;

@RestController
@CrossOrigin
@RequestMapping("/apps/api/items")
public class ItemController {
	@Autowired
	private ItemService service;
	
	@Autowired
	private CategoryService catService;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	public static final String imageURL="https://d12kqwzvfrkt5o.cloudfront.net/products/";
	public Logger getLogger() {
		return logger;
	}

	@PostMapping("/getItemBytype/{type}/")
	public List<ItemEntity> getItemBytype(@PathVariable("type") String productType,
			@RequestParam(defaultValue = "") Long category_sgid, @RequestParam(defaultValue = "") Boolean isAvailable,
			@RequestParam(defaultValue = "") String searchStr, @RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(defaultValue = "asc") String sortDir,
			@RequestParam(defaultValue = "display_name") String sortBy) {
		getLogger().info("Inside /apps/api/items/getItemBytype/{" + productType + "}?pageNo=" + pageNo + "&pageSize="
				+ pageSize);
		Integer count = 0;
		Integer unavailableSkuCount = 0;
		String variation_detail="";
		List<ItemEntity> l = service.searchByFilters(category_sgid, isAvailable, searchStr, pageNo, pageSize, sortDir,
				sortBy);
		
	CategoryEntity categoryEntity = null;
		
		for (ItemEntity item : l) {
			count = service.findPackagesCount(item.getSgid());
			item.setPackageCount(count);
			unavailableSkuCount = service.findUnavailableVariantsCount(item.getSgid());
			item.setUnavailbleVariantsCount(unavailableSkuCount);
			
			count = 0;
			unavailableSkuCount = 0;
			
		}
		List<ItemEntity> ll = new ArrayList<ItemEntity>();

		for (int i = 0; i < l.size(); i++) {
			ItemEntity prod = l.get(i);
			categoryEntity = catService.getCategory(prod.getProduct().getCategoryId());
			if(prod.getProduct()!=null && prod.getProduct().getProductCombinations()!=null){
			for (int j = 0; j < prod.getProduct().getProductCombinations().size(); j++) {
				ProductCombinationsEntity combination=prod.getProduct().getProductCombinations().get(j);
				if(prod.getProduct().getProductCombinations().get(j).getAssetValue()!=null) {
					Double assetPrice=	prod.getProduct().getProductCombinations().get(j).getAssetValue();
					if(categoryEntity.getBuyMultiplier()!=null) {
					assetPrice=(((assetPrice)*categoryEntity.getBuyMultiplier().doubleValue()));
					}else {
						assetPrice=(((assetPrice)*1.3));
					}
					assetPrice=assetPrice+(assetPrice*0.10);
				Integer	assetPrice1=assetPrice.intValue();
					prod.getProduct().getProductCombinations().get(j).setAssetValue(assetPrice1.doubleValue());
				}
				variation_detail = service.getVariationAttributeDetail(prod.getProduct().getSgid(),categoryEntity.getSgid(),combination.getSgid());
				//if(combination!=null) {
					
				//}
				prod.getProduct().getProductCombinations().get(j).setVariation_detail(variation_detail);
				
				for (int k = 0; k < combination.getProductImages().size(); k++) {
					List<ProductImageEntity> img=  combination.getProductImages();
					System.out.println(img.get(k).getImage());
					img.get(k).setImage(imageURL+prod.getProduct().getSku()+"/"+combination.getSku()+"/"+img.get(k).getImage());
					System.out.println(imageURL+prod.getProduct().getSku()+"/"+combination.getSku()+"/"+img.get(k).getImage());
					
				}
			}
				
			}
		
			if (!ll.contains(prod)) {

				ll.add(prod);
			}

		}

		return l;

	}

	@PostMapping("/packageCount")
	public ResponseEntity<List<ItemEntity>> search(@RequestParam(defaultValue = "") Long sgid) {
		logger.info("Inside /apps/api/items/packageCount?sgid=" + sgid);
		return new ResponseEntity<List<ItemEntity>>(service.findPackageCount(sgid), HttpStatus.OK);
	}

	@PostMapping("/packagesCount")
	public Integer findPackageCount(@RequestParam(defaultValue = "") Long sgid) {
		logger.info("Inside /apps/api/items/packageCount?sgid=" + sgid);

		Integer count = service.findPackagesCount(sgid);
		return count;
	}

	@PutMapping("/hideorShowProduct/")
	public String hideOrShowProduct(@RequestParam Boolean isAvailable, @RequestParam(defaultValue = "") Long sgid) {
		logger.info("Inside /apps/api/package/hideorShowProduct/isAvailable" + isAvailable + "&sgid" + sgid);
		try {
			service.hideOrShowProduct(isAvailable, sgid);
		} catch (Exception e) {
			e.printStackTrace();
			return "failure";
		}
		return "success";

	}

	@PostMapping("/getItemBySgid")
	public List<ItemEntity> getItemBySgid(@RequestParam("type") String productType,
			@RequestParam(defaultValue = "") Long category_sgid, @RequestParam(defaultValue = "") Boolean isAvailable,
			@RequestParam(defaultValue = "0") Integer pageNo, @RequestParam(defaultValue = "10") Integer pageSize,
			@RequestParam(defaultValue = "asc") String sortDir,
			@RequestParam(defaultValue = "display_name") String sortBy) {
		getLogger().info("Inside /apps/api/items/getItemBytype/{" + productType + "}?pageNo=" + pageNo + "&pageSize="
				+ pageSize);
		return (List<ItemEntity>) service.findByproductTypeAndSgid(productType, category_sgid, pageNo, pageSize,
				sortDir, sortBy);
		// return (service.searchByFilters(category_sgid,isAvailable,pageNo,
		// pageSize,sortDir,sortBy));
	}

	@PostMapping("/search")
	public List<ItemEntity> search(@RequestParam(defaultValue = "") Long sgid, @RequestParam("name") String name,
			@RequestParam(defaultValue = "0") Integer pageNo, @RequestParam(defaultValue = "10000") Integer pageSize,
			@RequestParam(defaultValue = "asc") String sortDir, @RequestParam(defaultValue = "name") String sortBy) {
		getLogger().info("Inside /apps/api/package/search/" + name);
		return (service.searchProduct(name, sgid, pageNo, pageSize, sortDir, sortBy));
	}

	@PostMapping("/searchByCategory")
	public List<ItemEntity> searchByFilters(@RequestParam(defaultValue = "") Long category_sgid,
			@RequestParam(defaultValue = "") Boolean isAvailable, @RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10000") Integer pageSize, @RequestParam(defaultValue = "asc") String sortDir,
			@RequestParam(defaultValue = "name") String sortBy) {
		getLogger().info("Inside /apps/api/item/searchByCategory/" + category_sgid);
		return (List<ItemEntity>) service.findByproductTypeAndSgid("PRODUCT", category_sgid, pageNo, pageSize, sortDir,
				sortBy);
		// (service.searchByFilters(category_sgid,isAvailable,pageNo,
		// pageSize,sortDir,sortBy));
	}

	@PostMapping("/findUnavailableVariantsCount")
	public Integer findUnavailableVariantsCount(@RequestParam(defaultValue = "") Long sgid) {
		logger.info("Inside /apps/api/items/packageCount?sgid=" + sgid);

		Integer count = service.findUnavailableVariantsCount(sgid);
		return count;
	}

}
