package com.inhabitr.admin.dasboard.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inhabitr.admin.dasboard.model.CartEntity;
import com.inhabitr.admin.dasboard.model.CartTokenEntity;
import com.inhabitr.admin.dasboard.repository.CartTokenRepository;


@Service
public class CartTokenService<cartTokenRepository> {
	@Autowired
	private CartTokenRepository cartTokenRepository;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
	public CartTokenEntity getActiveCart(Long cartSgid,String status,Long location) {
		logger.info("Inside getActiveCart() Starting.....");
		CartTokenEntity searchList = cartTokenRepository.getActiveCart(cartSgid,status);
		logger.info("...searchList Count..."+searchList);
	
		logger.info("Exit getActiveCart() !!!");
		return searchList;
	}		
	
	public CartTokenEntity getTokenDetails(Long cartSgid,String token) {
		logger.info("Inside getTokenDetails() Starting.....");
		CartTokenEntity searchList = cartTokenRepository.getTokenDetails(cartSgid,token.toUpperCase());
		logger.info("...searchList Count..."+searchList);
	
		logger.info("Exit getTokenDetails() !!!");
		return searchList;
	}		
}
