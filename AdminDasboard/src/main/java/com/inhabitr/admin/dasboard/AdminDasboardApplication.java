package com.inhabitr.admin.dasboard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


//import com.koinplus.common.exception.KoinPlusCommonExceptionCode;
import com.koinplus.common.util.KoinPlusDateUtil;
import com.koinplus.common.util.KoinPlusStringUtils;
import com.koinplus.common.util.KoinPlusEncryptionUtil;
import com.koinplus.common.webservice.GenericRestClient;

//@EnableJpaRepositories
@SpringBootApplication

@Configuration
@EnableAutoConfiguration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "com.inhabitr.admin.dasboard.repository" })
@ComponentScan("com.inhabitr.admin.dasboard")
@PropertySource("classpath:application.properties")
public class AdminDasboardApplication{	

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
	public static void main(String[] args) {
		System.out.println("Testing");
		SpringApplication.run(AdminDasboardApplication.class, args);
		System.out.println("Testing111");
	}
	
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/apps").allowedOrigins("http://ca-studentdev.inhabitr.com:8086");
			}
		};
	}
	
	 @Bean
	    public GenericRestClient genericRestClient() {
			return new GenericRestClient();

	    }
	 
	 
	 @Bean
	    public KoinPlusDateUtil koinPlusDateUtil() {
			return new KoinPlusDateUtil();

	    }
	 
	 @Bean
	    public KoinPlusStringUtils koinPlusStringUtils() {
			return new KoinPlusStringUtils();

	    }
	 
	 @Bean
	    public KoinPlusEncryptionUtil encryptionUtil() {
			return new KoinPlusEncryptionUtil();

	    }
	

	
	 
	/* @Bean 
	 public InhabitrException inhabitrException() {
		 return new InhabitrException();
	 }*/
}
