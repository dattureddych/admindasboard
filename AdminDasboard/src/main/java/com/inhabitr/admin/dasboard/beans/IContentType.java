package com.inhabitr.admin.dasboard.beans;

public interface IContentType {
	public static final String TEXT = "text/plain";
	public static final String HTML = "text/html; charset=utf-8";
}
