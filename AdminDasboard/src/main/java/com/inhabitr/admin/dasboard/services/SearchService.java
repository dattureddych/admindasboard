package com.inhabitr.admin.dasboard.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inhabitr.admin.dasboard.model.CategoryEntity;
import com.inhabitr.admin.dasboard.model.ItemEntity;
import com.inhabitr.admin.dasboard.model.PackageEntity;
import com.inhabitr.admin.dasboard.repository.SearchRepository;
import com.inhabitr.admin.dasboard.response.SearchResponse;

@Service
public class SearchService {
	
	@Autowired
	private SearchRepository searchRepository;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
	public List<SearchResponse> search(String name) {
		getLogger().info("Inside search()");
		List<Object> searchList = searchRepository.findByDisplayName(name);
		getLogger().info("searchList size="+searchList.size());
		SearchResponse response = null;
		//Set<CategoryEntity> catList = new HashSet();
		List<SearchResponse> itemList = new ArrayList();
		Iterator itr = searchList.iterator();
		
		while(itr.hasNext()) {
			Object[] obj = (Object[]) itr.next();
			response = new SearchResponse();
			
			response.setId(String.valueOf(obj[0]));
			response.setItemName(String.valueOf(obj[1]));
			response.setImageUrl(String.valueOf(obj[2]));			
			response.setCategoryName(String.valueOf(obj[3]));

			itemList.add(response);
		}
		return itemList;
	}
	
	public List<SearchResponse> searchCustomer(String queryParam) {
		getLogger().info("Inside searchCustomer()");
		List<Object> searchList = searchRepository.findByFirstName(queryParam);
		getLogger().info("searchList size="+searchList.size());
		SearchResponse response = null;
		//Set<CategoryEntity> catList = new HashSet();
		List<SearchResponse> itemList = new ArrayList();
		Iterator itr = searchList.iterator();
		
		while(itr.hasNext()) {
			Object[] obj = (Object[]) itr.next();
			response = new SearchResponse();
			
			response.setId((String.valueOf(obj[0]).equals(null)?"":String.valueOf(obj[0])));	
			response.setFirstName((String.valueOf(obj[1]).equals(null)?"":String.valueOf(obj[1])));	
			response.setLastName((String.valueOf(obj[2]).equals(null)?"":String.valueOf(obj[2])));			
			response.setMiddleName((String.valueOf(obj[3]).equals(null)?"":String.valueOf(obj[3])));	
			response.setEmailAddress((String.valueOf(obj[4]).equals(null)?"":String.valueOf(obj[4])));	
			response.setMobileNumber((String.valueOf(obj[5]).equals(null)?"":String.valueOf(obj[5])));	
			response.setHomeNumber((String.valueOf(obj[6]).equals(null)?"":String.valueOf(obj[6])));		
			
			itemList.add(response);
		}
		return itemList;
	}
	
	public List<SearchResponse> searchServiceRequest(String queryParam) {
		getLogger().info("Inside searchServiceRequest()");
		List<Object> searchList = searchRepository.findByMiddleName(queryParam);
		getLogger().info("searchList size="+searchList.size());
		SearchResponse response = null;
		//Set<CategoryEntity> catList = new HashSet();
		List<SearchResponse> itemList = new ArrayList();
		Iterator itr = searchList.iterator();
		
		while(itr.hasNext()) {
			Object[] obj = (Object[]) itr.next();
			response = new SearchResponse();
			
			response.setId((String.valueOf(obj[0]).equals(null)?"":String.valueOf(obj[0])));	
			response.setFirstName((String.valueOf(obj[1]).equals(null)?"":String.valueOf(obj[1])));	
			response.setLastName((String.valueOf(obj[2]).equals(null)?"":String.valueOf(obj[2])));			
			response.setMiddleName((String.valueOf(obj[3]).equals(null)?"":String.valueOf(obj[3])));	
			response.setEmailAddress((String.valueOf(obj[4]).equals(null)?"":String.valueOf(obj[4])));	
			response.setMobileNumber((String.valueOf(obj[5]).equals(null)?"":String.valueOf(obj[5])));	
			response.setHomeNumber((String.valueOf(obj[6]).equals(null)?"":String.valueOf(obj[6])));		
			
			itemList.add(response);
		}
		return itemList;
	}
	
	
	/*public List<PackageEntity> searchPackage(String queryParam) {
		getLogger().info("Inside searchPackage()");
		List<Object> searchList = searchRepository.findByName(queryParam);
		getLogger().info("searchList size="+searchList.size());
		PackageEntity response = null;
		//Set<CategoryEntity> catList = new HashSet();
		List<PackageEntity> itemList = new ArrayList();
		Iterator itr = searchList.iterator();
		
		/*while(itr.hasNext()) {
			Object[] obj = (Object[]) itr.next();
			response = new PackageEntity();
			
			response.setId((String.valueOf(obj[0]).equals(null)?"":String.valueOf(obj[0])));	
			response.setFirstName((String.valueOf(obj[1]).equals(null)?"":String.valueOf(obj[1])));	
			response.setLastName((String.valueOf(obj[2]).equals(null)?"":String.valueOf(obj[2])));			
			response.setMiddleName((String.valueOf(obj[3]).equals(null)?"":String.valueOf(obj[3])));	
			response.setEmailAddress((String.valueOf(obj[4]).equals(null)?"":String.valueOf(obj[4])));	
			response.setMobileNumber((String.valueOf(obj[5]).equals(null)?"":String.valueOf(obj[5])));	
			response.setHomeNumber((String.valueOf(obj[6]).equals(null)?"":String.valueOf(obj[6])));		
			
			itemList.add(response);
		}*/
		//return itemList;
	//}
	
	public List<SearchResponse> searchPricing(String queryParam) {
		getLogger().info("Inside searchPricing()");
		List<Object> searchList = searchRepository.findBySalutation(queryParam);
		getLogger().info("searchList size="+searchList.size());
		SearchResponse response = null;
		List<SearchResponse> itemList = new ArrayList();
		Iterator itr = searchList.iterator();
		
		while(itr.hasNext()) {
			Object[] obj = (Object[]) itr.next();
			response = new SearchResponse();
			
			response.setId((String.valueOf(obj[0]).equals(null)?"":String.valueOf(obj[0])));	
			response.setFirstName((String.valueOf(obj[1]).equals(null)?"":String.valueOf(obj[1])));	
			response.setLastName((String.valueOf(obj[2]).equals(null)?"":String.valueOf(obj[2])));			
			response.setMiddleName((String.valueOf(obj[3]).equals(null)?"":String.valueOf(obj[3])));	
			response.setEmailAddress((String.valueOf(obj[4]).equals(null)?"":String.valueOf(obj[4])));	
			response.setMobileNumber((String.valueOf(obj[5]).equals(null)?"":String.valueOf(obj[5])));	
			response.setHomeNumber((String.valueOf(obj[6]).equals(null)?"":String.valueOf(obj[6])));		
			
			itemList.add(response);
		}
		return itemList;
	}
	
	public List<SearchResponse> searchCartConfiguration(String queryParam) {
		getLogger().info("Inside searchCartConfiguration()");
		List<Object> searchList = searchRepository.findByHomePhoneNumber(queryParam);
		getLogger().info("searchList size="+searchList.size());
		SearchResponse response = null;
		//Set<CategoryEntity> catList = new HashSet();
		List<SearchResponse> itemList = new ArrayList();
		Iterator itr = searchList.iterator();
		
		while(itr.hasNext()) {
			Object[] obj = (Object[]) itr.next();
			response = new SearchResponse();
			
			response.setId((String.valueOf(obj[0]).equals(null)?"":String.valueOf(obj[0])));	
			response.setFirstName((String.valueOf(obj[1]).equals(null)?"":String.valueOf(obj[1])));	
			response.setLastName((String.valueOf(obj[2]).equals(null)?"":String.valueOf(obj[2])));			
			response.setMiddleName((String.valueOf(obj[3]).equals(null)?"":String.valueOf(obj[3])));	
			response.setEmailAddress((String.valueOf(obj[4]).equals(null)?"":String.valueOf(obj[4])));	
			response.setMobileNumber((String.valueOf(obj[5]).equals(null)?"":String.valueOf(obj[5])));	
			response.setHomeNumber((String.valueOf(obj[6]).equals(null)?"":String.valueOf(obj[6])));		
			
			itemList.add(response);
		}
		return itemList;
	}
	
	public List<SearchResponse> searchOrder(String queryParam) {
		getLogger().info("Inside searchOrder()");
		List<Object> searchList = searchRepository.findByLastName(queryParam);
		getLogger().info("searchList size="+searchList.size());
		SearchResponse response = null;
		//Set<CategoryEntity> catList = new HashSet();
		List<SearchResponse> itemList = new ArrayList();
		Iterator itr = searchList.iterator();
		
		while(itr.hasNext()) {
			Object[] obj = (Object[]) itr.next();
			response = new SearchResponse();
			
			response.setId((String.valueOf(obj[0]).equals(null)?"":String.valueOf(obj[0])));	
			response.setFirstName((String.valueOf(obj[1]).equals(null)?"":String.valueOf(obj[1])));	
			response.setLastName((String.valueOf(obj[2]).equals(null)?"":String.valueOf(obj[2])));			
			response.setMiddleName((String.valueOf(obj[3]).equals(null)?"":String.valueOf(obj[3])));	
			response.setEmailAddress((String.valueOf(obj[4]).equals(null)?"":String.valueOf(obj[4])));	
			response.setMobileNumber((String.valueOf(obj[5]).equals(null)?"":String.valueOf(obj[5])));	
			response.setHomeNumber((String.valueOf(obj[6]).equals(null)?"":String.valueOf(obj[6])));		
			
			itemList.add(response);
		}
		return itemList;
	}
}


