package com.inhabitr.admin.dasboard.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



@Entity @Table(name = "cart_item")
public class CartItemEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	
	private @Column(name = "cart_sgid") Long cartSgid;
	private @Column(name = "item_name") String itemName;
	private @Column(name = "sku") String sku;
	private @Column(name = "item_sgid") Long itemSgid;
	private @Column(name = "item_image_url") String itemImageUrl;
    private @Column(name = "item_price") BigDecimal itemPrice;
    private @Column(name = "item_affirm_price") BigDecimal itemAffirmPrice;
    private @Column(name = "item_affirm_total_price") BigDecimal itemAffirmTotalPrice;
    private @Column(name = "total_final_price") BigDecimal totalFinalPrice;    
    private @Column(name = "ops_db_cart_item_sgid") Long opsDbCartItemSgid;
	private @Column(name = "ops_db_cart_status") String opsDbCartStatus;
	private @Column(name = "item_type") @Enumerated(EnumType.STRING) ItemType itemType = ItemType.PRODUCT;
	private @Column(name = "trendbrew_order_number") String trendbrewOrderNumber;
	private @Column(name = "item_quantity") Integer itemQuantity;
	private @Column(name = "checkout_flag") Boolean checkoutFlag;
    private @Column(name = "is_rent") Boolean isRent;
    private @Column(name = "status") String status;
    private @Column(name = "month") Integer month;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cartItem")
    private List<CartPackageProductsEntity> cartPackageProducts;
    private @Column(name = "is_buy_used") Boolean isBuyUsed;
    private @Column(name = "is_buy_new") Boolean isBuyNew;
    
    private @Column(name = "product_package_variation_sgid") Long productPackageVariationSgid;
    
	public Long getSgid() {
		return sgid;
	}
	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}
	public Long getCartSgid() {
		return cartSgid;
	}
	public void setCartSgid(Long cartSgid) {
		this.cartSgid = cartSgid;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public Long getItemSgid() {
		return itemSgid;
	}
	public void setItemSgid(Long itemSgid) {
		this.itemSgid = itemSgid;
	}
	public String getItemImageUrl() {
		return itemImageUrl;
	}
	public void setItemImageUrl(String itemImageUrl) {
		this.itemImageUrl = itemImageUrl;
	}
	public BigDecimal getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(BigDecimal itemPrice) {
		this.itemPrice = itemPrice;
	}
	public BigDecimal getItemAffirmPrice() {
		return itemAffirmPrice;
	}
	public void setItemAffirmPrice(BigDecimal itemAffirmPrice) {
		this.itemAffirmPrice = itemAffirmPrice;
	}
	public BigDecimal getItemAffirmTotalPrice() {
		return itemAffirmTotalPrice;
	}
	public void setItemAffirmTotalPrice(BigDecimal itemAffirmTotalPrice) {
		this.itemAffirmTotalPrice = itemAffirmTotalPrice;
	}
	public BigDecimal getTotalFinalPrice() {
		return totalFinalPrice;
	}
	public void setTotalFinalPrice(BigDecimal totalFinalPrice) {
		this.totalFinalPrice = totalFinalPrice;
	}
	public Long getOpsDbCartItemSgid() {
		return opsDbCartItemSgid;
	}
	public void setOpsDbCartItemSgid(Long opsDbCartItemSgid) {
		this.opsDbCartItemSgid = opsDbCartItemSgid;
	}
	public String getOpsDbCartStatus() {
		return opsDbCartStatus;
	}
	public void setOpsDbCartStatus(String opsDbCartStatus) {
		this.opsDbCartStatus = opsDbCartStatus;
	}
	public ItemType getItemType() {
		return itemType;
	}
	public void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}
	public Integer getItemQuantity() {
		return itemQuantity;
	}
	public void setItemQuantity(Integer itemQuantity) {
		this.itemQuantity = itemQuantity;
	}
	public Boolean getCheckoutFlag() {
		return checkoutFlag;
	}
	public void setCheckoutFlag(Boolean checkoutFlag) {
		this.checkoutFlag = checkoutFlag;
	}
	public Boolean getIsRent() {
		return isRent;
	}
	public void setIsRent(Boolean isRent) {
		this.isRent = isRent;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTrendbrewOrderNumber() {
		return trendbrewOrderNumber;
	}
	public void setTrendbrewOrderNumber(String trendbrewOrderNumber) {
		this.trendbrewOrderNumber = trendbrewOrderNumber;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public List<CartPackageProductsEntity> getCartPackageProducts() {
		return cartPackageProducts;
	}
	public void setCartPackageProducts(List<CartPackageProductsEntity> cartPackageProducts) {
		this.cartPackageProducts = cartPackageProducts;
	}
	public Boolean getIsBuyUsed() {
		return isBuyUsed;
	}
	public void setIsBuyUsed(Boolean isBuyUsed) {
		this.isBuyUsed = isBuyUsed;
	}
	public Boolean getIsBuyNew() {
		return isBuyNew;
	}
	public void setIsBuyNew(Boolean isBuyNew) {
		this.isBuyNew = isBuyNew;
	}
	public Long getProductPackageVariationSgid() {
		return productPackageVariationSgid;
	}
	public void setProductPackageVariationSgid(Long productPackageVariationSgid) {
		this.productPackageVariationSgid = productPackageVariationSgid;
	}
	
}
