package com.inhabitr.admin.dasboard.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Anshu Gupta
 */
@Entity @Table(name = "city")
public class CityEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
	private @Column(name = "name") String name;
	private @Column(name = "state_id") Long stateSgid;
	private @Column(name = "image") String image;
	private @Column(name = "available_online") String availableOnline;
	private @Column(name = "status") Integer status;
	private @Column(name = "city_group") Integer cityGroup;
	private @Column(name = "banner1") String banner1;
	private @Column(name = "banner1_text") String banner1Text;
	private @Column(name = "banner2") String banner2;
	private @Column(name = "banner2_text") String banner2Text;
	private @Column(name = "banner3") String banner3;
	private @Column(name = "banner3_text") String banner3Text;
	private @Column(name = "launch_date") Integer launchDate;
	private @Column(name = "city_i_text") String cityIText;
	private @Column(name = "coupon_code") String couponCode;
	private @Column(name = "coupon_text") String couponText;
	private @Column(name = "meta_title") String metaTitle;
	private @Column(name = "meta_description") String metaDescription;
	private @Column(name = "meta_keywords") String metaKeywords;
	private @Column(name = "canonical_link") String canonicalLink;
	private @Column(name = "clearance_banner") String clearanceBanner;
	private @Column(name = "clearance_banner_mobile") String clearanceBannerMobile;
	private @Column(name = "created_datetime") @Temporal(TemporalType.TIMESTAMP) Date createdDatetime;
	@ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "state_id", insertable=false, updatable=false)
    private StateEntity state;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "city")
    private List<UniversityEntity> university;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getStateSgid() {
		return stateSgid;
	}
	public void setStateSgid(Long stateSgid) {
		this.stateSgid = stateSgid;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getAvailableOnline() {
		return availableOnline;
	}
	public void setAvailableOnline(String availableOnline) {
		this.availableOnline = availableOnline;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getCityGroup() {
		return cityGroup;
	}
	public void setCityGroup(Integer cityGroup) {
		this.cityGroup = cityGroup;
	}
	public String getBanner1() {
		return banner1;
	}
	public void setBanner1(String banner1) {
		this.banner1 = banner1;
	}
	public String getBanner1Text() {
		return banner1Text;
	}
	public void setBanner1Text(String banner1Text) {
		this.banner1Text = banner1Text;
	}
	public String getBanner2() {
		return banner2;
	}
	public void setBanner2(String banner2) {
		this.banner2 = banner2;
	}
	public String getBanner2Text() {
		return banner2Text;
	}
	public void setBanner2Text(String banner2Text) {
		this.banner2Text = banner2Text;
	}
	public String getBanner3() {
		return banner3;
	}
	public void setBanner3(String banner3) {
		this.banner3 = banner3;
	}
	public String getBanner3Text() {
		return banner3Text;
	}
	public void setBanner3Text(String banner3Text) {
		this.banner3Text = banner3Text;
	}
	public Integer getLaunchDate() {
		return launchDate;
	}
	public void setLaunchDate(Integer launchDate) {
		this.launchDate = launchDate;
	}
	public String getCityIText() {
		return cityIText;
	}
	public void setCityIText(String cityIText) {
		this.cityIText = cityIText;
	}
	public String getCouponCode() {
		return couponCode;
	}
	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}
	public String getCouponText() {
		return couponText;
	}
	public void setCouponText(String couponText) {
		this.couponText = couponText;
	}
	public String getMetaTitle() {
		return metaTitle;
	}
	public void setMetaTitle(String metaTitle) {
		this.metaTitle = metaTitle;
	}
	public String getMetaDescription() {
		return metaDescription;
	}
	public void setMetaDescription(String metaDescription) {
		this.metaDescription = metaDescription;
	}
	public String getMetaKeywords() {
		return metaKeywords;
	}
	public void setMetaKeywords(String metaKeywords) {
		this.metaKeywords = metaKeywords;
	}
	public String getCanonicalLink() {
		return canonicalLink;
	}
	public void setCanonicalLink(String canonicalLink) {
		this.canonicalLink = canonicalLink;
	}
	public String getClearanceBanner() {
		return clearanceBanner;
	}
	public void setClearanceBanner(String clearanceBanner) {
		this.clearanceBanner = clearanceBanner;
	}
	public String getClearanceBannerMobile() {
		return clearanceBannerMobile;
	}
	public void setClearanceBannerMobile(String clearanceBannerMobile) {
		this.clearanceBannerMobile = clearanceBannerMobile;
	}
	public Date getCreatedDatetime() {
		return createdDatetime;
	}
	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	public StateEntity getState() {
		return state;
	}
	public void setState(StateEntity state) {
		this.state = state;
	}
	public Long getSgid() {
		return sgid;
	}
	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}
	
}

