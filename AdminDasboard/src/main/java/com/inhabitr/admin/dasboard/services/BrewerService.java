package com.inhabitr.admin.dasboard.services;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inhabitr.admin.dasboard.model.BrewerEntity;
import com.inhabitr.admin.dasboard.repository.BrewerRepository;

@Service
public class BrewerService<brewerRepository> {
	@Autowired
	private BrewerRepository brewerRepository;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
	public BrewerEntity find(Long brewerId) {
		logger.info("Inside getActiveCart() Starting.....");
		BrewerEntity searchList = brewerRepository.findByBrewerId(brewerId);
		logger.info("...searchList Count..."+searchList);
	
		logger.info("Exit getActiveCart() !!!");
		return searchList;
	}	
}
