package com.inhabitr.admin.dasboard.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inhabitr.admin.dasboard.model.TwoTapCartRequestResponseEntity;
import com.inhabitr.admin.dasboard.repository.TwoTapCartRequestResponseRepository;

@Service
public class TwoTapCartRequestResponseService {
	@Autowired
	private TwoTapCartRequestResponseRepository repository;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
	
	public Long saveCart(TwoTapCartRequestResponseEntity entity) {
		TwoTapCartRequestResponseEntity obj=null;
		try {
			
			if(entity!=null) {
			obj= repository.save(entity);
			}
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		return obj.getSgid();
			
	}
}
