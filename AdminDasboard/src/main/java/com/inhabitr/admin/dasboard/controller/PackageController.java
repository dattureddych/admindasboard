package com.inhabitr.admin.dasboard.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inhabitr.admin.dasboard.model.CategoryEntity;
import com.inhabitr.admin.dasboard.model.CustomerServiceRequestsEntity;
import com.inhabitr.admin.dasboard.model.ItemEntity;
import com.inhabitr.admin.dasboard.model.PackageEntity;
import com.inhabitr.admin.dasboard.model.PackageProductRelationEntity;
import com.inhabitr.admin.dasboard.model.ProductCombinationsEntity;
import com.inhabitr.admin.dasboard.response.SearchResponse;
import com.inhabitr.admin.dasboard.services.CategoryService;
import com.inhabitr.admin.dasboard.services.ItemService;
import com.inhabitr.admin.dasboard.services.PackageProductRelationService;
import com.inhabitr.admin.dasboard.services.PackageService;
import com.inhabitr.admin.dasboard.services.ProductCombinationService;
import com.inhabitr.admin.dasboard.services.SearchService;

//import org.apache.commons.collections4.CollectionUtils;

@RestController
@CrossOrigin
@RequestMapping("/apps/api/package")
public class PackageController {
	@Autowired
	private PackageService service;
	
	@Autowired
	private SearchService searchService;
	
	@Autowired
	private ItemService itemService;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private ProductCombinationService productCombinationService;
	
	@Autowired
	private PackageProductRelationService packageProductService;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public Logger getLogger() {
		return logger;
	}
	@PostMapping("/")
	public List<PackageEntity> getAllPackages(@RequestParam(defaultValue = "") Long category_sgid, @RequestParam(defaultValue = "") Boolean isAvailable,
			@RequestParam(defaultValue = "") String searchStr,@RequestParam(defaultValue = "0") Integer pageNo, 
            @RequestParam(defaultValue = "10") Integer pageSize,@RequestParam(defaultValue = "asc") String sortDir,
            @RequestParam(defaultValue = "name") String sortBy) {
		getLogger().info("Inside /apps/api/package/?pageNo=" + pageNo + "&pageSize="+ pageSize);
		Integer availbleItemCount = 0;
		Integer numberofUnAvailableItems = 0;
		Integer numberofAvailableItems = 0;
		CategoryEntity catObj=new CategoryEntity();
		List<PackageProductRelationEntity> includedItemList = new ArrayList<PackageProductRelationEntity>();
		List<PackageEntity> list= service.getAllPackages(category_sgid,isAvailable,searchStr,pageNo, pageSize,sortDir,sortBy);
		if(list!=null && list.size()>0) {
		for (PackageEntity pac : list) {
			includedItemList =packageProductService.getIncludedItemsList(pac.getSgid().intValue());
			catObj =categoryService.getCategory(pac.getCategorySgid());
			System.out.println(includedItemList);
			pac.setPackageproductrelationentity(includedItemList);
			pac.setCategoryName(catObj.getDisplayName());
			pac.setBuyMultiplier(catObj.getBuyMultiplier()!=null?catObj.getBuyMultiplier():1.3);
			pac.setBuyAffirmMultiplier(catObj.getBuyAffirmMultiplier()!=null?catObj.getBuyAffirmMultiplier():1.3);
			if(includedItemList!=null && includedItemList.size()>0) {
			for(PackageProductRelationEntity packageItem:includedItemList) {
				availbleItemCount = itemService.findAvailbleIncludedItemCount(packageItem.getProductId());
				
				if(availbleItemCount>0) {	
				availbleItemCount=0;
				availbleItemCount = itemService.findAvailbleIncludedVariationCount(packageItem.getProductId(),packageItem.getPackageId());
				if(availbleItemCount>0) {	
				numberofAvailableItems= numberofAvailableItems+1;
				}else {					
					numberofUnAvailableItems= numberofUnAvailableItems+1;
				}
				
				}
				else {					
						numberofUnAvailableItems= numberofUnAvailableItems+1;
				}	
				}
			}			
			pac.setAvailbleItemCount(numberofAvailableItems);
			pac.setUnavailbleItemCount(numberofUnAvailableItems);
			numberofAvailableItems=0;
			numberofUnAvailableItems=0;
			}		
			availbleItemCount = 0;				
	}
		
		if (list!=null && list.size()>0) {
			Double tempPrice=0.0;
			Double buyNewPrice=0.0;
			Double buyNewAffirmPrice = 0.0;		
			for (PackageEntity pac : list) {
			for (PackageProductRelationEntity packageProductEntity : pac.getPackageproductrelationentity()) {
				if (packageProductEntity.getOptional()!=null && packageProductEntity.getOptional().equals(0) && packageProductEntity.getStatus()!=null && packageProductEntity.getStatus().equals(1)) {
					ProductCombinationsEntity productCombinationEntity =new ProductCombinationsEntity();// = packageProductEntity.getProductSkuVariation();
					productCombinationEntity= productCombinationService.findVariantDetail(packageProductEntity.getProductId());
					if (productCombinationEntity != null) {
						ProductCombinationsEntity productCombinationResult = this.productCombinationPriceCaliculations(null, productCombinationEntity);
						if(productCombinationResult.getAssetValue()!=null && productCombinationResult.getAssetValue()>0){
							buyNewPrice = buyNewPrice+(productCombinationResult.getAssetValue());							
						}						
						if(productCombinationResult.getBuyNewAffirmPrice()!=null && productCombinationResult.getBuyNewAffirmPrice()>0){
							buyNewAffirmPrice = buyNewAffirmPrice+(productCombinationResult.getBuyNewAffirmPrice());
						}										
					}
				}
			}	
			tempPrice=(buyNewPrice*10.25)/100;
			/*if(pac.getBuyMultiplier()==null) {
			buyNewPrice=(buyNewPrice+tempPrice)*1.3;
			}else {
			
			}*/
			buyNewPrice=(buyNewPrice+tempPrice)*pac.getBuyMultiplier();
			pac.setSale(buyNewPrice.intValue());
			 tempPrice=0.0;
			 buyNewPrice=0.0;
			 buyNewAffirmPrice = 0.0;		
		}
		}

		logger.info("Inside getAllPackages End");
		return list;
	}
	
	
	
	@PostMapping("/search")
	public List<PackageEntity> search(@RequestParam("name") String name,@RequestParam(defaultValue = "0") Integer pageNo, 
            @RequestParam(defaultValue = "10000") Integer pageSize,@RequestParam(defaultValue = "asc") String sortDir,
            @RequestParam(defaultValue = "name") String sortBy) {
		getLogger().info("Inside /apps/api/package/search/" + name);
		return (service.searchPackage(name,pageNo, pageSize,sortDir,sortBy));
	}
	
	@PutMapping("/hideorShowPackage/")
	public String hideOrShowPackage(@RequestParam Boolean isAvailable,@RequestParam(defaultValue = "") Long sgid) {
		logger.info("Inside /apps/api/package/hideOrShowPackage/status" +isAvailable+ "&sgid"+sgid);
		try {
		service.hideOrShowPackage(isAvailable,sgid);
	}catch(Exception e) {
		e.printStackTrace();
		return "failure";
	}
	return "success";
	}
	
	public ProductCombinationsEntity productCombinationPriceCaliculations(Long cityId, ProductCombinationsEntity combinationData){
		if(combinationData!=null){
					if(combinationData.getAssetValue()!=null 
					&& combinationData.getAssetValue()>0){
				if(combinationData.getProduct()!=null && combinationData.getProduct().getCategoryId()!=null){
					Long categorySgid=combinationData.getProduct().getCategoryId();
					CategoryEntity categoryEntity = new CategoryEntity();
							
					if(categoryEntity!=null && categoryEntity.getBuyMultiplier()!=null && categoryEntity.getBuyMultiplier()>0){
						Double buyNewPrice = combinationData.getPricingAssetValue()*(categoryEntity.getBuyMultiplier());
						if(buyNewPrice!=null && buyNewPrice>0){
							combinationData.setBuyNewPrice(buyNewPrice.doubleValue());
						}
					}
					if(categoryEntity!=null && categoryEntity.getBuyAffirmMultiplier()!=null && categoryEntity.getBuyAffirmMultiplier()>0){
						Double buyNewAffirmPrice = (combinationData.getPricingAssetValue())*(categoryEntity.getBuyAffirmMultiplier());
						
						if(buyNewAffirmPrice!=null && buyNewAffirmPrice>0){
							combinationData.setBuyNewAffirmPrice(buyNewAffirmPrice.doubleValue());
						}
					}else{
						combinationData.setBuyNewAffirmPrice(combinationData.getPricingAssetValue().doubleValue());
					}
					
			
				}else{
					combinationData.setBuyNewPrice(combinationData.getPricingAssetValue().doubleValue());
				}
			}
		}
		return combinationData;
	}
	
}
