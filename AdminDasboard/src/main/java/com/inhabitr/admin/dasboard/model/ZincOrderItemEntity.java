package com.inhabitr.admin.dasboard.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "zinc_order_item")
public class ZincOrderItemEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;

	private @Column(name = "item_sgid") Long itemSgid;
	private @Column(name = "order_package_product_sgid") Long packageItemSgid;
	private @Column(name = "order_sgid") Long orderSgids;
	private @Column(name = "zinc_order_sgid") Long zincOrderSgid;
	private @Column(name = "product_link") String productLink;
	private @Column(name = "product_id") String productId;
	private @Column(name = "zinc_status") String zincStatus;
	private @Column(name = "tracking_url") String trackingUrl;
	private @Column(name = "zinc_message") String zincMessage;
	private @Column(name = "zinc_request_id") String zincRequestId;
	private @Column(name = "zinc_response") String zincResponse;
	private @Column(name = "zinc_request") String zincRequest;
	private @Column(name = "quantity") int quantity;
	private @Column(name = "price") Double price;
	private @Column(name = "amazon_price") Double amazonPrice;
	private @Column(name = "shipping_mail_sent") Boolean shippingMailSent;
	private @Column(name = "delivery_mail_sent") Boolean deliveryMailSent;
	private @Column(name = "merchant_order_id") String amazonOrderId;
	private @Column(name = "product_name") String itemName;
	
	@Column(name = "delivery_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deliveryDateTime;

	@Column(name = "updated_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDateTime;

	@Transient
	private String deliveryDate;



	public ZincOrderItemEntity() {
		super();
	}

	public ZincOrderItemEntity(Long sgid, Long itemSgid, Long packageItemSgid, Long orderSgid, Long zincOrderSgid,
			String productLink, String productId, String zincStatus, String trackingUrl, String zincMessage,
			String zincRequestId, String zincResponse, String zincRequest, int quantity, Double price,
			Double amazonPrice, Boolean shippingMailSent, Boolean deliveryMailSent, Date deliveryDateTime,
			Date updatedDateTime, String deliveryDate) {
		super();
		this.sgid = sgid;
		this.itemSgid = itemSgid;
		this.packageItemSgid = packageItemSgid;
		this.orderSgids = orderSgid;
		this.zincOrderSgid = zincOrderSgid;
		this.productLink = productLink;
		this.productId = productId;
		this.zincStatus = zincStatus;
		this.trackingUrl = trackingUrl;
		this.zincMessage = zincMessage;
		this.zincRequestId = zincRequestId;
		this.zincResponse = zincResponse;
		this.zincRequest = zincRequest;
		this.quantity = quantity;
		this.price = price;
		this.amazonPrice = amazonPrice;
		this.shippingMailSent = shippingMailSent;
		this.deliveryMailSent = deliveryMailSent;
		this.deliveryDateTime = deliveryDateTime;
		this.updatedDateTime = updatedDateTime;
		this.deliveryDate = deliveryDate;
	}

	public Long getSgid() {
		return sgid;
	}

	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}

	public Long getItemSgid() {
		return itemSgid;
	}

	public void setItemSgid(Long itemSgid) {
		this.itemSgid = itemSgid;
	}

	public Long getPackageItemSgid() {
		return packageItemSgid;
	}

	public void setPackageItemSgid(Long packageItemSgid) {
		this.packageItemSgid = packageItemSgid;
	}

	

	public Long getOrderSgids() {
		return orderSgids;
	}

	public void setOrderSgids(Long orderSgids) {
		this.orderSgids = orderSgids;
	}

	public Long getZincOrderSgid() {
		return zincOrderSgid;
	}

	public void setZincOrderSgid(Long zincOrderSgid) {
		this.zincOrderSgid = zincOrderSgid;
	}

	public String getProductLink() {
		return productLink;
	}

	public void setProductLink(String productLink) {
		this.productLink = productLink;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getZincStatus() {
		return zincStatus;
	}

	public void setZincStatus(String zincStatus) {
		this.zincStatus = zincStatus;
	}

	public String getTrackingUrl() {
		return trackingUrl;
	}

	public void setTrackingUrl(String trackingUrl) {
		this.trackingUrl = trackingUrl;
	}

	public String getZincMessage() {
		return zincMessage;
	}

	public void setZincMessage(String zincMessage) {
		this.zincMessage = zincMessage;
	}

	public String getZincRequestId() {
		return zincRequestId;
	}

	public void setZincRequestId(String zincRequestId) {
		this.zincRequestId = zincRequestId;
	}

	public String getZincResponse() {
		return zincResponse;
	}

	public void setZincResponse(String zincResponse) {
		this.zincResponse = zincResponse;
	}

	public String getZincRequest() {
		return zincRequest;
	}

	public void setZincRequest(String zincRequest) {
		this.zincRequest = zincRequest;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getAmazonPrice() {
		return amazonPrice;
	}

	public void setAmazonPrice(Double amazonPrice) {
		this.amazonPrice = amazonPrice;
	}

	public Boolean getShippingMailSent() {
		return shippingMailSent;
	}

	public void setShippingMailSent(Boolean shippingMailSent) {
		this.shippingMailSent = shippingMailSent;
	}

	public Boolean getDeliveryMailSent() {
		return deliveryMailSent;
	}

	public void setDeliveryMailSent(Boolean deliveryMailSent) {
		this.deliveryMailSent = deliveryMailSent;
	}

	public Date getDeliveryDateTime() {
		return deliveryDateTime;
	}

	public void setDeliveryDateTime(Date deliveryDateTime) {
		this.deliveryDateTime = deliveryDateTime;
	}

	public Date getUpdatedDateTime() {
		return updatedDateTime;
	}

	public void setUpdatedDateTime(Date updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getAmazonOrderId() {
		return amazonOrderId;
	}

	public void setAmazonOrderId(String amazonOrderId) {
		this.amazonOrderId = amazonOrderId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	@Override
	public String toString() {
		return "ZincItems [sgid=" + sgid + ", itemSgid=" + itemSgid + ", packageItemSgid=" + packageItemSgid
				+ ", orderSgid=" + orderSgids + ", zincOrderSgid=" + zincOrderSgid + ", productLink=" + productLink
				+ ", productId=" + productId + ", zincStatus=" + zincStatus + ", trackingUrl=" + trackingUrl
				+ ", zincMessage=" + zincMessage + ", zincRequestId=" + zincRequestId + ", zincResponse=" + zincResponse
				+ ", zincRequest=" + zincRequest + ", quantity=" + quantity + ", price=" + price + ", amazonPrice="
				+ amazonPrice + ", shippingMailSent=" + shippingMailSent + ", deliveryMailSent=" + deliveryMailSent
				+ ", deliveryDateTime=" + deliveryDateTime + ", updatedDateTime=" + updatedDateTime + ", deliveryDate="
				+ deliveryDate + "]";
	}

}
