package com.inhabitr.admin.dasboard.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.inhabitr.admin.dasboard.model.ItemEntity;
import com.inhabitr.admin.dasboard.model.ZipcodeEntity;

@Repository
public interface ZipcodeRepository extends PagingAndSortingRepository<ZipcodeEntity, Long> {
	
	@Query(value = "select * from zipcode "
			+ "WHERE zipcode =:zipcode ", nativeQuery = true)
public ZipcodeEntity findZipcode(@Param("zipcode") Integer zipcode);
}
