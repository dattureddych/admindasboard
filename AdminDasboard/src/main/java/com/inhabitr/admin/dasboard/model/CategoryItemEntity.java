package com.inhabitr.admin.dasboard.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Abhijit Patil
 */

@Entity @Table(name = "item_category")
public class CategoryItemEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sgid", updatable = false, nullable = false)
	private Long sgid;
    
    @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "item_sgid")
    private ItemEntity item;
    
    @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "category_sgid")
    private CategoryEntity category;
    
    public ItemEntity getItem() {
        return item;
    }
    
    public void setItem(ItemEntity item) {
        this.item = item;
    }

    public CategoryEntity getCategory() {
        return category;
    }

    public void setCategory(CategoryEntity category) {
        this.category = category;
    }

	public Long getSgid() {
		return sgid;
	}

	public void setSgid(Long sgid) {
		this.sgid = sgid;
	}
    
    
    
}