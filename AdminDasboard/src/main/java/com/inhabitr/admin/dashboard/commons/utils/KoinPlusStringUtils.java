package com.inhabitr.admin.dashboard.commons.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.math.NumberUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.inhabitr.admin.dashboard.commons.algorithms.PorterStemmer;

public class KoinPlusStringUtils {
	public static String SPACE = " ";
    public static String[] punctuations = {",", "\"", "'", "-", "_", "#", "\\$", "\\.", "&"};

    public static final String EMAIL_REGEXP = "(\\b[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})\\b)";

    /**
     * Convert a collection of Strings into String separated using passed
     * separator
     *
     * @param stringTokens Collection of Strings
     * @param separator separator for tokens
     * @return Separator separator string representation of passed String
     * collection
     */
    public static String getStringForList(Collection<String> stringTokens, String separator) {
        StringBuilder sb = new StringBuilder();
        for (String token : stringTokens) {
            sb.append(token).append(separator);
        }
        return sb.toString().trim();
    }

    /**
     * Convert a collection of Strings into String separated by space
     *
     * @param stringTokens Collection of String
     * @return SPACE separator string representation of passed String collection
     */
    public static String getStringForList(Collection<String> stringTokens) {
        return getStringForList(stringTokens, SPACE);
    }

    public static String encodeURIComponent(String s) {
        String result = null;
        try {
            result = URLEncoder.encode(s, "UTF-8")
                    .replaceAll("\\+", "%20")
                    .replaceAll("%21", "!")
                    .replaceAll("%27", "'")
                    .replaceAll("%28", "(")
                    .replaceAll("%29", ")")
                    .replaceAll("%7E", "~");
        } // This exception should never occur.
        catch (UnsupportedEncodingException e) {
            result = s;
        }

        return result;
    }

    public static Boolean isNumeric(String word) {
        return NumberUtils.isNumber(word);
    }

    public static String getStringJsonMap(String jsonreq) {
        //JSONPObject jsonObject = new JSONObject(json);
    	//JsonObject jsonObject = new JsonParser().parse(jsonreq).getAsJsonObject();
        try {
        	//JsonObject jsonObject = JsonParser.parseString(jsonreq).getAsJsonObject();
        	JsonObject convertedObject = new Gson().fromJson(jsonreq, JsonObject.class);
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
            String json = mapper.writeValueAsString(convertedObject);
            return json;
        } catch (Exception ex) {
        	ex.printStackTrace();
            //Logger.getLogger(KoinPlusStringUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
		return jsonreq;
        }
        
    
    public static String removePunctuations(String str) {
        for (String punctuation : punctuations) {
            str = str.replaceAll(punctuation, " ");
        }
        // remove any more than one consecutive spaces
        str = str.replaceAll("^ +| +$|( )+", "$1");
        return str;
    }

    public static BigDecimal asBigDecimal(String str) {
        String bigDecimalString = str.replaceAll(",", "");
        return new BigDecimal(bigDecimalString);
    }

    public static String escapeDollarSign(String str) {
        if (str.trim().length() == 0)
            return str;
        return str.replaceAll("\\$", "\\\\\\$");
    }

    public static List<String> findEmails(String text) {
        Pattern pattern = Pattern.compile(EMAIL_REGEXP, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(text);
        List<String> emails = new ArrayList<String>();
        while (matcher.find()) {
            emails.add(matcher.group(1));
        }
        return emails;
    }

    public static List<String> getRegexMatches(String text, String regex) {
        List<String> matches = new ArrayList<String>();
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            int groupsCount = matcher.groupCount();
            for (int i = 1; i <= groupsCount; i++) {
                String group = matcher.group(i);
                matches.add(group);
            }
        }
        return matches;
    }

    public static Boolean contains(String text, String regex) {
        return text != null && Pattern.compile(regex).matcher(text).find();
    }

    /**
     * Get a Default Map representation for this JSON
     *
     * @param json JSON String
     * @return Map of String-Object
     */
    public static Map<String, Object> getJsonMap(String json) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            Map<String, Object> objectMap = mapper.readValue(json, Map.class);
            return objectMap;
        } catch (IOException ex) {
            Logger.getLogger(KoinPlusStringUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static <T> T convertJsonToPOJO(String json, Class<T> target) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(json, target);
        } catch (IOException ex) {
            Logger.getLogger(KoinPlusStringUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Get a Default JSON String representation for this Object
     *
     * @param object Object to be Serialized
     * @return JSON string representation of the Object
     */
    public static String getJsonString(Object object) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
            String json = mapper.writeValueAsString(object);
            return json;
        } catch (JsonProcessingException ex) {
            Logger.getLogger(KoinPlusStringUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static String getStringFromTemplate(String template, Map<String, Object> values) {
        String finalString = template;
        String regex = "(\\{\\w+\\})";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(template);
        while (matcher.find()) {
            String param = template.substring(matcher.start() + 1, matcher.end() - 1);
            Object paramValue = null;

            if (values.containsKey(param)) {
                paramValue = values.get(param);
            }
            String replacingString = "";
            if (paramValue != null && !paramValue.toString().equalsIgnoreCase("null")) {
                replacingString = paramValue.toString();
            }
            replacingString = KoinPlusStringUtils.escapeDollarSign(replacingString);
            finalString = finalString.replaceAll("\\{" + param + "\\}", replacingString);
        }
        return finalString;
    }

    public static String getStringFromTemplate(String template, Map<String, Object> values, Map<String, String> otherReplacements) {
        String finalString = template;
        String regex = "(\\{\\w+\\})";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(template);
        while (matcher.find()) {
            String param = template.substring(matcher.start() + 1, matcher.end() - 1);
            Object paramValue = null;

            if (values.containsKey(param)) {
                paramValue = values.get(param);
            }
            String replacingString = "";
            for (Map.Entry<String, String> replacementEntry : otherReplacements.entrySet()) {
                if (param.toLowerCase().startsWith(replacementEntry.getKey().toLowerCase())) {
                    replacingString = replacementEntry.getValue();                    
                    break;
                }
            }
            if (paramValue != null && !paramValue.toString().equalsIgnoreCase("null")) {
                replacingString = paramValue.toString();
            }
            replacingString = KoinPlusStringUtils.escapeDollarSign(replacingString);
            System.out.println("Replacing " + param + " with " + replacingString);
            finalString = finalString.replaceAll("\\{" + param + "\\}", replacingString);
        }
        return finalString;
    }

    public static boolean hasSameLinguisticRoot(String first, String second) {
        return PorterStemmer.getStemmedWord(first).equalsIgnoreCase(PorterStemmer.getStemmedWord(second));
    }

    public static String getLinguisticRoot(String str) {
        return PorterStemmer.getStemmedWord(str);
    }
}
