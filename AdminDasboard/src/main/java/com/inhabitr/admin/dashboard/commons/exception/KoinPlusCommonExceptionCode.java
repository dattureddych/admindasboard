package com.inhabitr.admin.dashboard.commons.exception;

public enum KoinPlusCommonExceptionCode implements KoinPlusExceptionCode {
	SECURITY_FAILED("SF", "SECURITY_FAILED"),
    THIRD_PARTY_COMMUNICATION_FAILED("TP-CF", "THIRD_PARTY_COMMUNICATION_FAILED"),
    NO_USER_SPECIFIED("NU-S", "No User specified."),
    NO_USER_ACCESS_DENIED("NU-AD", "User must be logged in to access this resource."),
    UNSUCCESSFUL_LOGIN_TRY_AGAIN("L-UTA", "Login Unsuccessful, Try again."),
    UNSUCCESSFUL_LOGIN_INVALID_USER_PASSWORD("L-IUP", "Invalid User Name or Password."),
    UNSUCCESSFUL_LOGIN_UNREGISTERED_USER("L-UU", "Unregistered User."),
    UNSUCCESSFUL_LOGIN_RESET_PASSWORD("L-ULRP", "Reset Password to continue."),
    UNSUCCESSFUL_RESET_PASSWORD_TRY_AGAIN("L-URP", "Reset Password Unsuccessful, Try again."),
    UNSUCCESSFUL_SIGNUP_PREREGISTERED_USER("SU-PRU", "Pre-registered User."),
    UNSUCCESSFUL_UPDATE_PREREGISTERED_USER("UP-PRU", "Pre-registered User."),
    UNAUTHORIZED_REQUEST("UN_AUTH_RQ","You do not have authorization for this request."),
    UNKNOWN_USER("UN_U","We do not recognize this user."),
    BAD_REQUEST("BR","Not enough information is specified"),
    LOGIN_SOCIAL_ACCOUNT_CONNECT_DENIED("S-CD","This Social Account is already linked to another TrendBrew account."),
    LOGIN_SOCIAL_ACCOUNT_DISCONNECT_DENIED("S-DD","Social Account is used for Login and hence can not be disconnected."),
    NO_CONTENT("NC","We could not find the information requested."),
    NO_EXISTING_ITEM("NEI","Item is not available."),    
    EXISTING_CART_ITEM("ECT", "Product already added to cart."),
    EXISTING_ITEM("EI", "Product already Exist."),
    CART_ITEM_OUT_OF_STOCK("CI-OS", "Product currently out of stock."),
	NO_LOYALTY_PROMO_CODE("NLPC","Wrong loyalty Promotion Code."),
	UNSUCCESSFUL_ORDER("UN_O","Could not pleace your order."),
	INVALID_ZIPCODE("IZ","Zipcode is not valid."),
	EXISTING_USER_PWD("EUP", "Password already Exist."),
	STRIPE_AUTH_FAILURE("EUP", "Stripe Authintication Failed."),
	STRIPE_INVALID_REQ("EUP", "Invalid Request."),
	STRIPE_INVALID_CARD("EUP", "Invalid Request.");
   

    private String value;
    private String message;

    private KoinPlusCommonExceptionCode(String name, String message) {
        this.value = name;
        this.message = message;
    }

    public String getValue() {
        return value;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
