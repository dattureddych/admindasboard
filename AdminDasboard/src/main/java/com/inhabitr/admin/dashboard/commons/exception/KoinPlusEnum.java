package com.inhabitr.admin.dashboard.commons.exception;

public interface KoinPlusEnum<K extends Object>  {
	public K getValue();
}
