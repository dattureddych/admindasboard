package com.inhabitr.admin.dashboard.commons.exception;


public class KoinPlusException extends Exception {

    protected KoinPlusExceptionCode code;
    protected String originalExceptionMessage;

    public KoinPlusException() {
    }

    public KoinPlusException(KoinPlusExceptionCode exception) {
        super(exception.getValue());
        this.code = exception;
    }

    public KoinPlusException(KoinPlusExceptionCode exception, String message) {
        super(exception.getValue());
        this.code = exception;
        this.originalExceptionMessage = message;
    }

    public KoinPlusExceptionCode getCode() {
        return this.code;
    }

    public String getOriginalExceptionMessage() {
        return originalExceptionMessage;
    }

    public void setOriginalExceptionMessage(String originalExceptionMessage) {
        this.originalExceptionMessage = originalExceptionMessage;
    }
}

