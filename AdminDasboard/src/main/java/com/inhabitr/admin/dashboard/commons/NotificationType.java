package com.inhabitr.admin.dashboard.commons;

import com.koinplus.common.KoinPlusEnum;
import com.koinplus.common.KoinPlusEnumEnhancer;
import com.koinplus.common.vocab.NotificationVisibility;

public enum NotificationType implements KoinPlusEnum<String> {
      RESET_ADMIN_USER_PASSWORD("RESET_ADMIN_USER_PASSWORD",NotificationVisibility.PRIVATE, NotificationVisibility.PRIVATE);

    private String value;
    private NotificationVisibility visibility;
    private NotificationVisibility emailVisibility;
    private Boolean creatorVisible;

    private static final KoinPlusEnumEnhancer<NotificationType> enhancer = new KoinPlusEnumEnhancer<NotificationType>(values());

    private NotificationType(String name) {
        this.value = name;
        this.visibility = NotificationVisibility.NETWORK;
        this.emailVisibility = NotificationVisibility.FOLLOWERS;
        this.creatorVisible = Boolean.TRUE;
    }

    private NotificationType(String name, NotificationVisibility visibility, NotificationVisibility emailVisibility) {
        this.value = name;
        this.visibility = visibility;
        this.emailVisibility = emailVisibility;
        this.creatorVisible = Boolean.TRUE;
    }

    private NotificationType(String name, Boolean creatorVisibility) {
        this.value = name;
        this.visibility = NotificationVisibility.NETWORK;
        this.emailVisibility = NotificationVisibility.FOLLOWERS;
        this.creatorVisible = creatorVisibility;
    }

    // This too is delegation.
    public static NotificationType lookup(String name) {
        return enhancer.lookup(name);
    }

    public String getValue() {
        return value;
    }

    public Boolean isCreatorVisible() {
        return creatorVisible;
    }

    public NotificationVisibility getVisibility() {
        return visibility;
    }

    public NotificationVisibility getEmailVisibility() {
        return emailVisibility;
    }
}
