package com.inhabitr.admin.dashboard.commons;


import java.util.HashMap;
import java.util.Map;

public class KoinPlusEnumEnhancer<E extends Enum<E> & KoinPlusEnum> {

    private Map<Object, E> lookup;

    public KoinPlusEnumEnhancer(E... values) {
        lookup = new HashMap<Object, E>();
        for (E e : values) {
            lookup.put(e.getValue(), e);
        }
    }

    public E lookup(Object name) {
        if (name == null) return null;
        return lookup.get(name);
    }

    public String toString(E e) {
        return e.getValue().toString();
    }
}

