package com.inhabitr.admin.dashboard.commons.exception;

public class InhabitrException extends KoinPlusException {

    public InhabitrException() {
        super();
        this.code = KoinPlusCommonExceptionCode.NO_LOYALTY_PROMO_CODE;
    }

    public InhabitrException(KoinPlusExceptionCode exception) {
        super(exception);
    }
    
    public InhabitrException(String message) {
        super();
        this.originalExceptionMessage = message;
    }

    public InhabitrException(KoinPlusExceptionCode exception, String message) {
        super(exception);
        this.originalExceptionMessage = message;
    }
    
}
