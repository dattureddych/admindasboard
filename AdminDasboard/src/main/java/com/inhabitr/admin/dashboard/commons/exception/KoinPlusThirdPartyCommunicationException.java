package com.inhabitr.admin.dashboard.commons.exception;

public class KoinPlusThirdPartyCommunicationException extends KoinPlusException {

    public KoinPlusThirdPartyCommunicationException() {
        this.code = KoinPlusCommonExceptionCode.THIRD_PARTY_COMMUNICATION_FAILED;
    }    
}

