package com.inhabitr.admin.dashboard.dto;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Date;
public class CartData {	
	private Long id;
    private Date lastAccessDate;
    private Date updatedDate;   
    private BigDecimal subTotalPrice;
    private BigDecimal totalPrice;
    private BigDecimal rentTotalPrice;
    private BigDecimal buyTotalPrice;
    private BigDecimal salesTaxRate;
    private BigDecimal salesTaxPrice;
    private BigDecimal rewardAmount;
    private BigDecimal rewardBalance;
    private BigDecimal totalCartPrice;
    private BigDecimal totalAffirmPrice;
    private Long rewardSgid;
    private Long numCartItems = 0l;
  
    private String httpConfirmUrl;
    private String httpUpdateUrl;
    private Integer leaseLength;
    private String type;
    private String status;
    private BigDecimal promocodeDiscountAmount;
    private BigDecimal promocodeDiscount;
    private BigDecimal affirmSubTotalPrice = BigDecimal.ZERO;
    private BigDecimal affirmTotalPrice = BigDecimal.ZERO;
    private Long promocodesgid;
    private String promocode;
    private Map<String, Object> cartOptionsMap = new HashMap<String, Object>();
    private Map<String, Object> shippingAndHandlingFees = new HashMap<String, Object>();
    private Map<String, Object> addOnFees = new HashMap<String, Object>();
    private Map<String, Object> pricingMultiplier = new HashMap<String, Object>();
    private String token;
    private String cityId = "0";
    private String stateId = "0";
    private String orderNumber;
    private BigDecimal shippingFee;
    private Integer cartSavings;
    private Long  opsDasdBoardcartId;
    private Boolean isAffirmSuccess;
    private String message;
    
	public Long getOpsDasdBoardcartId() {
		return opsDasdBoardcartId;
	}
	public void setOpsDasdBoardcartId(Long opsDasdBoardcartId) {
		this.opsDasdBoardcartId = opsDasdBoardcartId;
	}
	public Integer getCartSavings() {
		return cartSavings;
	}
	public void setCartSavings(Integer cartSavings) {
		this.cartSavings = cartSavings;
	}
	public BigDecimal getShippingFee() {
		return shippingFee;
	}
	public void setShippingFee(BigDecimal shippingFee) {
		this.shippingFee = shippingFee;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getPromocode() {
		return promocode;
	}
	public void setPromocode(String promocode) {
		this.promocode = promocode;
	}
	public Long getPromocodesgid() {
		return promocodesgid;
	}
	public void setPromocodesgid(Long promocodesgid) {
		this.promocodesgid = promocodesgid;
	}
	public BigDecimal getPromocodeDiscountAmount() {
		return promocodeDiscountAmount;
	}
	public void setPromocodeDiscountAmount(BigDecimal promocodeDiscountAmount) {
		this.promocodeDiscountAmount = promocodeDiscountAmount;
	}
	public BigDecimal getAffirmTotalPrice() {
		return affirmTotalPrice;
	}
	public void setAffirmTotalPrice(BigDecimal affirmTotalPrice) {
		this.affirmTotalPrice = affirmTotalPrice;
	}
	
	public BigDecimal getAffirmSubTotalPrice() {
		return affirmSubTotalPrice;
	}
	public void setAffirmSubTotalPrice(BigDecimal affirmSubTotalPrice) {
		this.affirmSubTotalPrice = affirmSubTotalPrice;
	}
	/**
	 * @return the cityId
	 */
	public String getCityId() {
		return cityId;
	}
	/**
	 * @param cityId the cityId to set
	 */
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	/**
	 * @return the stateId
	 */
	public String getStateId() {
		return stateId;
	}
	/**
	 * @param stateId the stateId to set
	 */
	public void setStateId(String stateId) {
		this.stateId = stateId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Map<String, Object> getShippingAndHandlingFees() {
		return shippingAndHandlingFees;
	}
	public void setShippingAndHandlingFees(Map<String, Object> shippingAndHandlingFees) {
		this.shippingAndHandlingFees = shippingAndHandlingFees;
	}
	public Map<String, Object> getAddOnFees() {
		return addOnFees;
	}
	public BigDecimal getPromocodeDiscount() {
		return promocodeDiscount;
	}
	public Map<String, Object> getPricingMultiplier() {
		return pricingMultiplier;
	}
	public void setPricingMultiplier(Map<String, Object> pricingMultiplier) {
		this.pricingMultiplier = pricingMultiplier;
	}
	public void setPromocodeDiscount(BigDecimal promocodeDiscount) {
		this.promocodeDiscount = promocodeDiscount;
	}
	public BigDecimal getSalesTaxRate() {
		return salesTaxRate;
	}
	public void setSalesTaxRate(BigDecimal salesTaxRate) {
		this.salesTaxRate = salesTaxRate;
	}
	public BigDecimal getSalesTaxPrice() {
		return salesTaxPrice;
	}
	public void setSalesTaxPrice(BigDecimal salesTaxPrice) {
		this.salesTaxPrice = salesTaxPrice;
	}
	public BigDecimal getRewardAmount() {
		return rewardAmount;
	}
	public void setRewardAmount(BigDecimal rewardAmount) {
		this.rewardAmount = rewardAmount;
	}
	public BigDecimal getRewardBalance() {
		return rewardBalance;
	}
	public void setRewardBalance(BigDecimal rewardBalance) {
		this.rewardBalance = rewardBalance;
	}
	public Long getRewardSgid() {
		return rewardSgid;
	}
	public void setRewardSgid(Long rewardSgid) {
		this.rewardSgid = rewardSgid;
	}
	public void setAddOnFees(Map<String, Object> addOnFees) {
		this.addOnFees = addOnFees;
	}
	public Integer getLeaseLength() {
		return leaseLength;
	}
	public void setLeaseLength(Integer leaseLength) {
		this.leaseLength = leaseLength;
	}
	public String getHttpConfirmUrl() {
		return httpConfirmUrl;
	}
	public void setHttpConfirmUrl(String httpConfirmUrl) {
		this.httpConfirmUrl = httpConfirmUrl;
	}
	public String getHttpUpdateUrl() {
		return httpUpdateUrl;
	}
	public void setHttpUpdateUrl(String httpUpdateUrl) {
		this.httpUpdateUrl = httpUpdateUrl;
	}
	public Date getLastAccessDate() {
		return lastAccessDate;
	}
	public void setLastAccessDate(Date lastAccessDate) {
		this.lastAccessDate = lastAccessDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public BigDecimal getSubTotalPrice() {
		return subTotalPrice;
	}
	public void setSubTotalPrice(BigDecimal subTotalPrice) {
		this.subTotalPrice = subTotalPrice;
	}
	public BigDecimal getRentTotalPrice() {
		return rentTotalPrice;
	}
	public void setRentTotalPrice(BigDecimal rentTotalPrice) {
		this.rentTotalPrice = rentTotalPrice;
	}
	public BigDecimal getBuyTotalPrice() {
		return buyTotalPrice;
	}
	public void setBuyTotalPrice(BigDecimal buyTotalPrice) {
		this.buyTotalPrice = buyTotalPrice;
	}
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
	public Long getNumCartItems() {
		return numCartItems;
	}
	public void setNumCartItems(Long numCartItems) {
		this.numCartItems = numCartItems;
	}
	public Map<String, Object> getCartOptionsMap() {
		return cartOptionsMap;
	}
	public void setCartOptionsMap(Map<String, Object> cartOptionsMap) {
		this.cartOptionsMap = cartOptionsMap;
	}
	public void addCartOptionsMap(String key, Object value) {
        if (this.cartOptionsMap == null)
            this.cartOptionsMap = new HashMap<String, Object>();
        this.cartOptionsMap.put(key, value);
    }
	public void addShippingAndHandlingFeeMap(String key, Object value) {
        if (this.shippingAndHandlingFees == null)
            this.shippingAndHandlingFees = new HashMap<String, Object>();
        this.shippingAndHandlingFees.put(key, value);
    }
	
	public void addAddOnFeeMap(String key, Object value) {
        if (this.addOnFees == null)
            this.addOnFees = new HashMap<String, Object>();
        this.addOnFees.put(key, value);
    }
	 public void addPricingMultiplier(String key, Object name) {
	        if (this.pricingMultiplier == null)
	            this.pricingMultiplier = new HashMap<String, Object>();
	        this.pricingMultiplier.put(key, name);
	    }
	public String getToken() {
		return token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	public Boolean getIsAffirmSuccess() {
		return isAffirmSuccess;
	}
	public void setIsAffirmSuccess(Boolean isAffirmSuccess) {
		this.isAffirmSuccess = isAffirmSuccess;
	}
	public BigDecimal getTotalCartPrice() {
		return totalCartPrice;
	}
	public void setTotalCartPrice(BigDecimal totalCartPrice) {
		this.totalCartPrice = totalCartPrice;
	}
	public BigDecimal getTotalAffirmPrice() {
		return totalAffirmPrice;
	}
	public void setTotalAffirmPrice(BigDecimal totalAffirmPrice) {
		this.totalAffirmPrice = totalAffirmPrice;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	

}
